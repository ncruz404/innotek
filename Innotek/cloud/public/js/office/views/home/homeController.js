/*
* Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. See full license at the bottom of this file.
*/

'use strict';

angular.module('app.home', []).controller('homeController', ['$http', function($http) {
		var vm = this;
		$http.get("https://graph.microsoft.com/beta/me/userPhoto")
		.success(function(data, status, headers, config) {
		  console.log('HTTP request to Mail API returned successfully.');
		  vm.user = data;
		  console.log(vm.user);
			// $http.get("https://outlook.office365.com/api/beta/me/userphoto")
			// .success(function(data, status, headers, config) {
			//   console.log('HTTP2 request to Mail API returned successfully.');
			//   vm.image = data;
			//   console.log(vm.image);
			// })
			// .error(function(data, status, headers, config) {
			//   console.log('HTTP request to Mail API failed.');
			// })  
		})
		.error(function(data, status, headers, config) {
		  console.log('HTTP request to Mail API failed.');
		});
}]);
// *********************************************************