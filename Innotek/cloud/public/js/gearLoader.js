function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function initGear(divID){
  //create a variable representing our gear div.
  var $gear = $('<div class="gear"></div>');
  
  $('#'+divID).append($gear);
  
  $gear.velocity({
    width: 40,
    height: 40,
    borderWidth: 24},200,'ease-in', addTeeth);

  //create a jQuery object of the element that
  //will be each gear tooth.
  var $gearTooth = $('<div class="gearTooth"></div>');

  //set initial gear tooth position around
  //gear. one might alter this variable to
  //create a gear with missing teeth.
  var rotationIndex = 0;
  
  function addTeeth() {
    //append gear teeth to div.gear element
    for (var i=0;i<12;i++) {
      var xPos = getRandomInt(-400,400);
      var yPos = getRandomInt(-400,400);    
      $gearTooth
        .clone()
        .appendTo($gear)
      .velocity({tranlateX: xPos,translateY:[0,yPos], rotateZ: rotationIndex},1000,'ease')
      //set each gear tooth to be rotated around center of gear slightly
      rotationIndex += 30;
    }
  }
  setTimeout(spinGear,500);
};

//this function initiates the gear spin.
function spinGear() {
  //note that this uses velocity, not
  //the standard jQuery .animate.
  //velocityjs is awesome! See:
  //https://github.com/julianshapiro/velocity
  $('.gear').velocity({rotateZ: 3000}, 36000, 'ease');
}

$('.startAnimation').on('click', function(){
  if ($('.gear').length === 0 ) {
    initGear();
  }
});

$('.stopAnimation').on('click', function(){
  //This is velocity's manual stop command.
  $('.gear').velocity('stop');
  $('.gear').css({'transform': 'rotate(0)'});
  $('.gearTooth').velocity({
    translateX: [250,0]
  },500,'ease');
  $('.gearTooth').velocity({
    translateY: [1000,0]
  },500,'ease-in', function(){
     $('.gear').fadeOut(function(){
       $('.gear').remove();
     });
  });

});