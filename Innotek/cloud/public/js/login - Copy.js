//-------------------------------------------------------------------------------------------------------------
//---------------    Sección que evita que las formas hagan refresh -------------------------------------------
//-------------------------------------------------------------------------------------------------------------
var new_user = false;
$("#signUpBtn").click(
  function(event){
    event.preventDefault();
    signup();
  });

$("#logInBtn").click(
  function(event){
    event.preventDefault();
    login();
  });

  function checarUserYammer(object){
    var yamArray = (object.email).split("@");
    if(yamArray[1] == "softtek.com")
    {

      var User = Parse.Object.extend("User");
      var queryUser = new Parse.Query(User);
      queryUser.equalTo("username",yamArray[0]);
      queryUser.find({
        success: function(userResult){
          if(userResult.length > 0)
          {
            var result = userResult[0];
            loginYammer(yamArray[0],object.id);
          }else
          {
            console.log("No hay en BD");
            signupYammer(yamArray[0],object);
          }
        },
        error: function(error){
          alert("Error al inicializar con Yammer");
        }
      });
    }
  }
//-------------------------------------------------------------------------------------------------------------
//---------------    Sección que evita que las formas hagan refresh -------------------------------------------
//-------------------------------------------------------------------------------------------------------------

function loginYammer(username, pass){
  
  var un = username.toString();
  var pss = pass.toString();
  Parse.User.logIn(un, pss, {
    success: function(user) {     
      
      getLocation();
      
    },
    error: function(user, error) {
      console.log("Screw you");
      modalAlert("El usuario o contraseña son incorrectos");
      //modalAlert("ss");
    }
  });
}

function signupYammer(username,yamObject){
      var user = new Parse.User();
      var un = username.toString();
      var pss = yamObject.id+"";
      var mail = yamObject.email.toString();
      user.set("username", un);
      user.set("password", pss);
      user.set("email", mail); 
      user.set("imageYammer",yamObject.mugshot_url);
      user.set("pais","Mexico");
      user.set("name", yamObject.first_name);
      console.log("before signup"+un+":"+pss+":"+mail);
      user.signUp(null, {
        success: function(user) {
          console.log(user);
          new_user = true;
          loginYammer(un,pss);
          
        },
        error: function(user, error) {        
          switch(error.code){
            case 203: $("#reauth-email").text("La Dirección de correo ya está registrada");
            break;
            case 125: $("#reauth-email").text("La Dirección de correo es inválida");
            break;
            case 202: $("#reauth-email").text("El Nombre de usuario ya está registrada");
            break;
            case -1: $("#reauth-email").text("Debes agregar una contraseña");
            break;
            default: console.log(error.code); alert(error.message);
          }        
        }
      });
}

//-------------------------------------------------------------------------------------------------------------
//-----------------------------      Sign up      -------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
function signup(){
  if($("#signEmail").val().length > 0)
    if($("#signPassword").val() === $("#signPassword2").val()){
      var user = new Parse.User();
      user.set("username", $("#signUser").val());
      user.set("name", $("#signUser").val()); 
      user.set("password", $("#signPassword").val());
      user.set("email", $("#signEmail").val()); 
    
      user.signUp(null, {
        success: function(user) {
          console.log(user);
          new_user = true;
          login2();
            
        },
        error: function(user, error) {        
          switch(error.code){
            case 203: $("#reauth-email").text("La Dirección de correo ya está registrada");
            break;
            case 125: $("#reauth-email").text("La Dirección de correo es inválida");
            break;
            case 202: $("#reauth-email").text("El Nombre de usuario ya está registrada");
            break;
            case -1: $("#reauth-email").text("Debes agregar una contraseña");
            break;
            default: console.log(error.code); alert(error.message);
          }        
        }
      });
    }else
      $("#reauth-email").text("La contraseña debe coincidir");
    else
      $("#reauth-email").text("Debes incluir un correo electrónico");
}
//-------------------------------------------------------------------------------------------------------------
//-----------------------------      Sign up      -------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------



//-------------------------------------------------------------------------------------------------------------
//-----------------------------      Login con credenciales      ----------------------------------------------
//-------------------------------------------------------------------------------------------------------------
function login(){
  Parse.User.logIn($("#inputUser").val(), $("#inputPassword").val(), {
    success: function(user) {     
      
      getLocation();
    },
    error: function(user, error) {
      console.log("Screw you");
      modalAlert("El usuario o contraseña son incorrectos");
      //modalAlert("ss");
    }
  });
}
function modalAlert(message,title){
  var superalertModal = '<!-- Modal -->'+
    '<div class="modal fade" id="maModal" role="dialog">'+
      '<div class="modal-dialog">'+
        '<!-- Modal content-->'+
        '<div class="modal-content">'+
          '<div class="modal-header">'+
            '<button type="button" class="close" data-dismiss="modal">&times;</button>'+
            '<h4 id="maTitle" class="modal-title">'+((title)? title : "")+'</h4>'+
          '</div>'+
          '<div class="text-center modal-body">'+
              '<h3 id="maMessage">'+message+'</h3>'+
          '</div>'+
          '<div class="modal-footer">'+
            '<button id="mabtnClose" type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
          '</div>'+
        '</div>'+
      '</div>'+
    '</div>';
  $('#maModal').remove();
  $("body").append(superalertModal);
  $('#maModal').modal({ show: false})
  $('#maModal').modal('show');
  $('#maModal').on('shown.bs.modal', function () {
    $('#mabtnClose').focus();
  });
}
//-------------------------------------------------------------------------------------------------------------
//-----------------------------      Login con credenciales      ----------------------------------------------
//-------------------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------------------
//-----------------------------      Login con nuevo usuario     ----------------------------------------------
//-------------------------------------------------------------------------------------------------------------
function login2(){
  Parse.User.logIn($("#signUser").val(), $("#signPassword").val(), {
    success: function(user) {      
       getLocation();
    },
    error: function(user, error) {
      console.log("Screw you");
      alert("The login failed. Check error to see why.\n"+ error.message);
    }
  });
}
//-------------------------------------------------------------------------------------------------------------
//-----------------------------      Login con nuevo usuario     ----------------------------------------------
//-------------------------------------------------------------------------------------------------------------
      function getLocation() {
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(showPosition);
          } else { 
              alert("Geolocation is not supported by this browser.");
          }
      }

      function showPosition(position) {
          var userlocation = new Parse.GeoPoint({latitude: position.coords.latitude, longitude: position.coords.longitude});
          Parse.Cloud.run("registerTime",{geopoint: userlocation,ideaID: "mwYHeL3QNl",userID : Parse.User.current().id, device: "web", newUser: new_user, evento:"LogIn"},{
            success: function(response){
              console.log("Saved registry");
              window.location = "home.html";
            },
              error: function(error){
                alert("Error"+ error.message);
              }
            });
      }