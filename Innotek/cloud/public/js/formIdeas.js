var idea = {
	name: "",
	descr: "",
	tags: [],
	isPrivate: false,
	category: 0,
	fileCount: 0,
	challengeID: null
};
var ideaCHSub = false;
var arr = [];
var engine;
var ideasUList = [];
var challengeId = GetUrlValue("k");

$(document).ready(function() {

	var url = window.location.pathname;
	var filename = url.substring(url.lastIndexOf('/')+1);

	if(filename == "viewIdea.html")
	{
		Parse.Cloud.run('getIdea',{ideaID: GetUrlValue("k")},{
			success: function(ideaResult){
				$("#headerNewIdea").css("background-color","red");
				$("#headerNewIdea").css("color","white");
				$("#headerNewIdea").html("<h4>Link to "+ideaResult.get("titulo")+"</h4>");
			},
			error: function(error){
				console.log(error);
			}
		});
	}

	Parse.Cloud.run('getAllTags', {
			cant: 200,
			skip: 0
		},

		{
			success: function(result) {

				for (var i = 0; i < result.length; i++) {
					arr.push({
						value: result[i]
					});
				}

				engine = new Bloodhound({
					local: arr,
					datumTokenizer: function(d) {
						return Bloodhound.tokenizers.whitespace(d.value);
					},
					queryTokenizer: Bloodhound.tokenizers.whitespace
				});


				engine.initialize();

				$('#small').tokenfield({
					delimiter: false,
					typeahead: [
						{
							hint: true,
							highlight: true,
							minLength: 1
								            },
						{
							source: engine.ttAdapter()
								            }
								        ]
				}).on('tokenfield:createtoken', function(e) {

					/*	arr = removeByAttr(arr,"value",e.attrs.value.toLowerCase());*/

					var existingTokens = $(this).tokenfield('getTokens');
					if (existingTokens.length) {

						$.each(existingTokens, function(index, token) {

							if (token.value.toLowerCase() === e.attrs.value.toLowerCase()) {
								e.preventDefault();
							}

						});
					}

				});
			},

			error: function(error) {
			}

		});


	var setCategory = {
		func0: function() {
			idea.category = 0;
			$(".modal-title-NewIdea").html("Create New Idea");
		},
		func1: function() {
			idea.category = 1;
			$(".modal-title-NewIdea").html("Create New Need");
		},
		func2: function() {
			idea.category = 2;
			$(".modal-title-NewIdea").html("Create New Application");
		},
		func3: function() {
			idea.category = 3;
			$(".modal-title-NewIdea").html("Create New POC");
		},
		func4: function() {
			idea.category = 4;
			$(".modal-title-NewIdea").html("Create New Microservice");
		},
		func5: function() {
			idea.category = 5;
			$(".modal-title-NewIdea").html("Create New Other");
		},
	};

	$('.setCategory').click(function() {
		var data = $.parseJSON($(this).attr('data-button'));

		setCategory[data.func]();
	});





	$('#myModal').on('hidden.bs.modal', function() {
		if (idea.challenge != null) {
			$("#myModal").find('h4:first').remove();
			if (!ideaCHSub)
				idea.challenge = null;
		}
	});





	$("#saveChallenge").click(function() {
		createChallenge();
	});




	$(".setCategory").on("click", function() {

		$(".setCategory").removeClass("active");

		if ($(this).hasClass("active")) {
			$(this).removeClass("active");
		}
		else {
			$(this).addClass("active");
		}

	});
});

$('body').on('click', '.unlinkIdea', function(e) {

	e.preventDefault();
	var ideaID = $(e.target).attr("data-value");
	Parse.Cloud.run('deleteChallengeRelation', {
		ideaID: ideaID
	}, {
		success: function(chID) {

			var x=$(e.target).parents().eq(6).attr("id");
			x = x.replace('modalSICH', '');

			$("#cIH" + ideaID + " p").append('Link this Idea <input type="checkbox" name="' + x + 'SICHRadio[]"  value="' + ideaID + '" />');
			$(e.target).remove();
			$("#cIH" + ideaID + " .cardIdeasHeaderLinked").html("Free");
			$("#cIH" + ideaID + " .cardIdeasHeaderLinked").addClass("cardIdeasHeader");
			$("#cIH" + ideaID + " .cardIdeasHeaderLinked").removeClass("cardIdeasHeaderLinked");

		},
		error: function(error) {
		}
	});
});

$('body').on('click', '#finishBtn', function(e) {
	console.log("A FINISHBTN");
	createIdea();
});

$('body').on('click', '#finishBtnColaboration', function(e) {
	createColaborationIdea();
});

$('body').on('click', '#createContribution', function(e) {
	Parse.Cloud.run('getChallenge', {
		challengeID: challengeId
	}, {
		success: function(result) {
			contributeIdeaGrid(result.object);
		},
		error: function(error) {
		}
	});
});

function contributeIdea(challenge) {
	idea.challenge = challenge;
	$("#headerNewIdea").prepend("<h4>Respond to Challenge</h4>");
	$('#myModal').modal('toggle');
}

function contributeIdeaGrid(challenge) {
	idea.challenge = challenge;
	$("#headerNewIdea").prepend("<h4>Respond to Challenge</h4>");
	jQuery("#finishBtn").attr("id", "finishBtnColaboration");
	$("#myModal").modal();
}

function linkIdea(challenge) {
	if ($("#modalSICH" + challenge.id).length == 0)
		Parse.Cloud.run("ideasUser", {
			userID: Parse.User.current().id
		}, {
			success: function(ideasList) {
				ideasUList = ideasList;
				var selectICH = '<div class="modal fade" id="modalSICH' + challenge.id + '">' +
					'<div class="modal-dialog">' +
					'<div class="modal-content">' +
					'<div class="modal-header">' +
					'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
					'<h4 class="modal-title">Select one of your Ideas for Challenge:</h4>' +
					'<h3>' + challenge.get("titulo") + '</h3>' +
					'</div>' +
					'<div class="modal-body">';
				if (ideasList.length > 0) {
					for (var i = 0; i < ideasList.length; i++) {
						var cidea = ideasList[i].idea;

						selectICH += '<div class="row">' +
							'<div id="cIH' + cidea.id + '" class="cardIdeas">' +
							'<div ' + ((cidea.has("challenge")) ? "class='cardIdeasHeaderLinked'>Linked to " + cidea.get("challenge").get("titulo") + "" : 'class="cardIdeasHeader"> Free') + '</div>' +
							'<div id="img-challenge">' +
							'<img class="img-thumbnail-challenge" src="' + cidea.get("photoUrl") + '">' +
							'</div>' +
							'<h4 id="p' + challenge.id + cidea.id + '">' + cidea.get("titulo").toString().substring(0, 40) + '</h4>' +
							'<p>' + (cidea.has("challenge") ? "<button class='unlinkIdea' data-value='" + cidea.id + "' >Unlink</button>" : 'Link this Idea <input type="checkbox" name="' + challenge.id +
								'SICHRadio[]"  value="' + cidea.id + '" />') + '</p>' +
							'</div>' +
							'</div>';
					};
					selectICH += '</div>' +
						'<div class="modal-footer" id="SICHFooter">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>';
					$("body").append(selectICH);
					var row = $("#modalSICH" + challenge.id + " #SICHFooter");
					var btnLink = document.createElement("button"); // Create a <button> element
					t = document.createTextNode("Select"); // Create a text node
					btnLink.appendChild(t);
					btnLink.setAttribute("class", "btn btn-primary");
					btnLink.setAttribute("type", "button");
					btnLink.setAttribute("id", "SICHBtn");
					btnLink.onclick = function() {
							linkIdeaToCH(challenge)
						} // Append the text to <button>
					row.append(btnLink);
				}
				else
					selectICH += "<h3>You dont have any Ideas to Link</h3>";

				$("#modalSICH" + challenge.id).modal('toggle');
			},
			error: function(error) {
			}
		});
	else
		$("#modalSICH" + challenge.id).modal('toggle');
}

function GetUrlValue(VarSearch) {
	var SearchString = window.location.search.substring(1);
	if (SearchString.length > 0) {
		var VariableArray = SearchString.split('&');
		for (var i = 0; i < VariableArray.length; i++) {
			var KeyValuePair = VariableArray[i].split('=');
			if (KeyValuePair[0] == VarSearch) {
				return KeyValuePair[1];
			}
		}
	}
	else
		return null;
}

function getArrayTags(array) {

	var arrayTags = new Array();
	for (var i = 0; i < array.length; i++) {
		arrayTags.push(array[i].value);
	}

	return arrayTags
}

function linkIdeaToCH(challenge) {
	var checked = []
	$("input[name='" + challenge.id + "SICHRadio[]']:checked").each(function() {
		checked.push($(this).val());
	});
	for (var i = 0; i < checked.length; i++) {
		checked[i];
		var idea = checked[i];
		Parse.Cloud.run("getIdea", {
			ideaID: idea
		}, {
			success: function(ideaResult) {
				ideaResult.set("challenge", challenge);
				ideaResult.save(null, {
					success: function(ideaSaved) {
						var aHTML = '<div id="ideaRelated">' +
							'<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">' +
							'<div id="img-challenge">' +
							'<img class="img-thumbnail-challenge" src="' + ideaSaved.get("photoUrl") + '">' +
							'</div>' +
							'</div>' +
							'<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">' +
							'<p>' + ideaSaved.get("descripcion").toString().substring(0, 40) + '<a class="read-more" href=viewIdea.html?k=' + ideaSaved.id + '>See Full Idea&gt;</a></p>' +
							'</div>' +
							'</div>';
						var challengeID = ideaSaved.get("challenge").id;
						ideaSaved.get("challenge").fetch();
						$("#" + challengeID + " #ideaRelated").html("<p>" + ideaSaved.get("challenge").get("countIdeas") + " ideas submitted</p>");
						$("#cIH" + ideaSaved.id + " .cardIdeasHeader").html("Linked to " + ideaSaved.get("challenge").get("titulo"));
						$("#cIH" + ideaSaved.id + " .cardIdeasHeader").addClass("cardIdeasHeaderLinked");
						$("#cIH" + ideaSaved.id + " .cardIdeasHeader").removeClass("cardIdeasHeader");
						$("#cIH" + ideaSaved.id + " p").html("<button >Unlink</button>");
					},
					error: function(error) {
					}
				});
			},
			error: function(error) {
			}
		});
	};
}

function newSavedCard(ideaObj) {
	Parse.Cloud.run("unaIdeaFull", {
		oID: ideaObj.id,
		userID: Parse.User.current().id
	}, {
		success: function(ideaFull) {
			var object = ideaFull[0].idea;
			var comments = 0;
			var likes = 0;
			var attachments = 0;
			if (ideaFull[0].hasOwnProperty('files')) {
				attachments = ideaFull[0].files.length;
			}


			var abstract = object.get('descripcion').substring(0, 300) + "...";
			var imageUser = object.attributes.user.attributes.image.url();
			var imageUserY = object.attributes.user.attributes.imageYammer;
			if (imageUserY != null) imageUser = imageUserY;
			var datestring = object.createdAt.toString();
			var hola = moment(datestring).fromNow();


			$("#my-ideas-cards").prepend("<div class='card' id='" + object.id +
				"' href='#' onClick='clickCard(this.id)'><div id='category'><div class='noactive ideaCategory'></div><div class='noactive needsCategory'></div><div class='noactive appCategory'></div><div class='noactive pocCategory'></div><div class='noactive microCategory'></div><div class='noactive otherCategory'></div></div><h4 class='title'>" +
				object.get('titulo') + "</h4><span class='user-img' style = 'background-image: url(" + imageUser + ")'></span><div id='section2'><h5 class='author' href='#''>by " + object.attributes.user.attributes
				.username + "</h5><i class='material-icons time-icon'>&#xE425;</i><h5 class='date'>" + hola + "</h5><i class='material-icons location-icon'>&#xE0C8;</i> <h5 class='location'>" + object.get(
					"locationCity") + "</h5></a></div><p class='description'>" + abstract + "<a class='read-more'>See Full Idea > </a></p><div id='interaction'><span class='icon comment'>" + comments +
				"</span><span class='icon heart'>" + likes + "</span><span class='icon attachment'>" + attachments + "</span></div><div id='tags' class='tag" + object.id + "'></div></div>");

			ideaCategory(object, '#my-ideas', object.id);
			ideaHasPhotoAttached(object, '#my-ideas', object.id);
			//   addAnimation("#most-recent", object.id, 1000);
			var array = [];
			array.length = 0;
			var tagsWord = object.get('tags').toString();
			array = tagsWord.split(',');
			for (var i = 0; i < array.length; i++) {
				$(".tag" + object.id).append("<a class='tag' href='#' >" + array[i] + "</a>");
			}
			$(".tag" + object.id).append("<a class='clear' href='#' ></a>");
		},
		error: function(error) {
		}
	});
}

function stopGear() {
	$('.gear').velocity('stop');
	$('.gear').css({
		'transform': 'rotate(0)'
	});
	$('.gearTooth').velocity({
		translateX: [250, 0]
	}, 500, 'ease');
	$('.gearTooth').velocity({
		translateY: [1000, 0]
	}, 500, 'ease-in', function() {
		$('.gear').fadeOut(function() {
			$('.gear').remove();
		});
	});
}

function clearAllCH() {
	idea = {
		name: "",
		descr: "",
		tags: [],
		isPrivate: false,
		category: 0,
		fileCount: 0
	};
	$("#chDescr").val("");
	$("#chTitle").val("");
	$("#saveChallenge").html("Save changes");
	$("#saveChallenge").on("click", function() {
		createChallenge();
	});
}

function createChallenge() {
	$("#saveChallenge").html("Processing...");
	//$("#addButton").html('<a class="text-right btn btn-success">Processing...</a>');
	$('#chModal').modal('toggle');
	$('#saveChallenge').off("click");

	newCardCH();
	initGear("NewCardCH");
	var chTitle = $("#chTitle").val();
	var chDescr = $("#chDescr").val();
	if (chDescr.trim().length > 0 && chTitle.trim().length > 0)
		Parse.Cloud.run("saveChallenge", {
			title: chTitle,
			descr: chDescr,
			userID: Parse.User.current().id
		}, {
			success: function(savedCH) {
				stopGear();
				$("#NewCardCH").velocity("transition.fadeOut");
				window.location="grid.html?k="+savedCH.id;
				newSavedCardCH(savedCH);
				clearAllCH();
			},
			error: function(error) {
			}
		});
}

function newCardCH() {
	$("#recently-added-cards").prepend("<div class='card' id='NewCardCH" +
		"' href='#'><div id='category'><div class='noactive ideaCategory'></div><div class='noactive needsCategory'></div><div class='noactive appCategory'></div><div class='noactive pocCategory'></div><div class='noactive microCategory'></div><div class='noactive otherCategory'></div></div><h4 class='titleNewCard'>" +
		"Adding new Challenge..." + "</h4></div>");
}

function newChallengeIdea(ideaCH) {

	Parse.Cloud.run("unaIdeaFull", {
		oID: ideaCH.id,
		userID: Parse.User.current().id
	}, {
		success: function(iCH) {
			var aHTML = '<div id="ideaRelated">' +
				'<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">' +
				'<div id="img-challenge">' +
				'<img class="img-thumbnail-challenge" src="' + iCH[0].idea.get("photoUrl") + '">' +
				'</div>' +
				'</div>' +
				'<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">' +
				'<p>' + iCH[0].idea.get("descripcion").toString().substring(0, 40) + '<a class="read-more" href=viewIdea.html?k=' + iCH[0].idea.id + '>See Full Idea&gt;</a></p>' +
				'</div>' +
				'</div>';
			var challengeID = iCH[0].idea.get("challenge").id;
			$("#" + challengeID + " #ideaRelated").html(iCH[0].idea.get("challenge").get("countIdeas") + " ideas submitted");
			ideaCHSub = false;
		},
		error: function(error) {
		}
	});
}

function newSavedCardCH(challenge) {
	var datestring = challenge.createdAt.toString();
	var hola = moment(datestring).fromNow();

		var aHTML = '<div  class="card challenge" id="' + challenge.id + '" >' +
           '<h4 class="title">' + challenge.get("titulo") + '</h4>' +
           '<div id="section2">' +
           '<a class="author col-xs-12" href="#" id=' + Parse.User.current().id + '>by ' + Parse.User.current().get("username") + '</a>' +
           '<div class="icon-time col-xs-6">' +
           '<i class="material-icons time-icon">:couple_with_heart:</i>' +
           '<h5 class="date">' + hola + '</h5>' +
           '</div>' +
           '<div class="icon-location col-xs-6">' +
           '<i class="material-icons location-icon"></i>' +
           '<h5 class="location">'+Parse.User.current().get("locationCity")+'</h5>' +
           '</div>' +
           '</div>' +
           '<p id="ChallengeDescription">' + challenge.get("descripcion") + '</p>' +
           '<div id="btnChallenge">' +
           '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6"></div>' +
           '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6"></div>' +
           '</div>' +
           '<hr id="lineChalenge">' +
           '<a href="grid.html?k=' + challenge.id + '" class="bold600">See Challenge</a>' +
           '</div>';
	
$("#recently-added-cards").append(aHTML);
       var btnToCreate = $("#" + object.id + " #btnChallenge div:first-child");
       var btnToLink = $("#" + object.id + " #btnChallenge div:nth-child(2)");
       var btn = document.createElement("A"); // Create a <button> element
       var t = document.createTextNode("Contribute Post"); // Create a text node
       btn.appendChild(t);
       btn.setAttribute("class", "contributeIdea");
       btn.setAttribute("data-info", object.id);
       btn.onclick = function() {
           contributeIdea(object);
       }

       btnToCreate.append(btn);


       var btnLink = document.createElement("A"); // Create a <button> element
       t = document.createTextNode("Link Post"); // Create a text node
       btnLink.appendChild(t);
       btnLink.setAttribute("class", "linkIdea");

       btnLink.onclick = function() {
               linkIdea(object);
           } // Append the text to <button>
       btnToLink.append(btnLink);
}

function postMessage() {
	yam.getLoginStatus(function(response) {
		if (response.authResponse) {
			//call the yamPostRequest function if the user is logged in

			yamPostRequest(this);
		}
		else {
			//redirect the user to log in
			window.location = "home.html";
		}
	});
}

function yamPostRequest(val) {
	var msg_value = idea.name + ":" + idea.descr;
	var groupID = "6068022";
	if (msg_value == "") {
		alert("Message body cannot be empty!");
		return false;
	}
	if (groupID == "") {
		var conf = confirm("Group ID is empty, message will be posted to All Company");
		if (conf == false) {
			return false;
		}
	}
	yam.platform.request({
		url: "https://api.yammer.com/api/v1/messages.json",
		method: "POST",
		data: {
			"body": msg_value,
			"group_id": groupID
		},
		success: function(msg) {
			window.location = "home.html";
		},
		error: function(msg) {
			alert("Post was Unsuccessful");
		}
	})
}

function getLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition);
	}
	else {
		alert("Geolocation is not supported by this browser.");
	}
}

function showPosition(position) {
	var userlocation = new Parse.GeoPoint({
		latitude: position.coords.latitude,
		longitude: position.coords.longitude
	});
	Parse.Cloud.run("registerTime", {
		geopoint: userlocation,
		ideaID: "mwYHeL3QNl",
		userID: Parse.User.current().id,
		device: "web",
		newUser: false,
		evento: "submit IDEA"
	}, {
		success: function(response) {
			postMessage();
		},
		error: function(error) {
		}
	});
}

function metLista(act, max, ideaObject) {
	$("#finishBtn").html("Uploading " + (max - act) + " files....");
	$(".titleNewCard").html("<h4>Uploading " + (max - act) + " files....</h4>");
	if (act < max) {
		var file = $("#photoUpload").get(0).files[act];
		var name = $("#photoUpload").get(0).files[act].name;
		var parseFile = new Parse.File(name, file);
		parseFile.save().then(function() {

		});

		var Archivo = Parse.Object.extend("Archivo");
		var archivo = new Archivo();
		archivo.set("file", parseFile);
		archivo.set("nombre", name);
		archivo.set("idea", ideaObject);
		archivo.save(null, {
			success: function(object) {
				console.log("HERES FING ARCHIVO");
				console.log(object);
				console.log(ideaObject);
				act = act + 1;
				metLista(act, max, ideaObject);
			},
			error: function(model, error) {
			}
		});
	}
	else {
		stopGear();
		$("#NewCard").velocity("transition.fadeOut");
		$("#NewCard2").velocity("transition.fadeOut");
		if (idea.challenge != null)
			newChallengeIdea(ideaObject);
		newSavedCard(ideaObject);
		for (var i = 0; i < idea.tags.length; i++) {
			var tagname = "";
			tagname = idea.tags[i];

			Parse.Cloud.run('setNewTag', {
				tagname: tagname
			}, {
				success: function(newtag) {

				},

				error: function(error) {
				}

			});
		}
		//getLocation();
	}
}

function createColaborationIdea() {
	var extensionAcceptance = [];
	idea.name = $("#inputName").val().trim();
	idea.descr = $("#inputDescr").val().trim();
	idea.isPrivate = $("#cmn-toggle-1").prop("checked");
	idea.tags = getArrayTags($('#small').tokenfield('getTokens'));

	if (idea.name === "") {
		$("#inputName").addClass("error");
		$(".error-title").css("display", "block");
	}
	if (idea.name !== "") {
		$("#inputName").removeClass("error");
		$(".error-title").css("display", "none");
	}

	if (idea.descr === "") {
		$("#inputDescr").addClass("error");
		$(".error-descr").css("display", "block");
	}
	if (idea.descr !== "") {
		$("#inputDescr").removeClass("error");
		$(".error-descr").css("display", "none");
	}

	if (idea.tags.length < 1) {
		$(".error-tag").css("display", "block");
		$("#small").parent().addClass("error");
	}
	if (idea.tags.length > 0) {
		$(".error-tag").css("display", "none");
		$("#small").parent().removeClass("error");
	}

	if (!$(".setCategory").hasClass("active")) {
		$(".categorygroup").addClass("error");
		$(".error-category").css("display", "block");
	}
	if ($(".setCategory").hasClass("active")) {
		$(".error-category").css("display", "none");
		$(".categorygroup").removeClass("error");
	}

	var sizeFiles = 0;
	var extensionsForbidden = [];
	var arrayFiles = $('#photoUpload').prop("files").length;
	if (arrayFiles > 0) {

		for (var i = 0; i < arrayFiles; i++) {


			var file = document.getElementById('photoUpload').files[i];
			sizeFiles = sizeFiles + file.size;

			var fname = file.name;
			var re =
                /(\.jpg|\.apk|\.jpeg|\.bmp|\.gif|\.doc|\.dot|\.docx|\.docm|\.dotm|\.docb|\.xls|\.xlt|\.xlm|\.xlsx|\.xlsm|\.xltx|\.xltm|\.xlsb|\.xla|\.xlam|\.xll|\.xlw|\.ppt|\.ptx|\.pdf|\.png|\.log|\.msg|\.odt|\.pages|\.rtf|\.tex|\.txt|\.wpd|\.wps|\.csv|\.dat|\.key|\.pps|\.xml|\.aif|\.m4a|\.mid|\.mp3|\.mpa|\.wav|\.wma|\.mp4|\.gif|\.psd|\.tif|\.tiff|\.ai|\.eps|\.svg|\.png|\.sql|\.asp|\.aspx|\.css|\.html|\.js|\.php|\.rss|\.xhtml|\.ttf|\.class|\.java)$/i;
			var validation = re.exec(fname);
			if (!re.exec(fname)) {
				extensionAcceptance.push(false);
				var character = /(?:\.([^.]+))?$/;
				var ext = character.exec(fname)[1];
				extensionsForbidden.push(ext);



				$(".error-file-ext").css("display", "block");
				$("#photoUpload").parent().addClass("error");

			}
			else {
				extensionAcceptance.push(true);
			}

		};

		$(".error-file-ext strong").remove();
		extensionsForbidden = eliminateDuplicates(extensionsForbidden);
		for (var i = 0; i < extensionsForbidden.length; i++) {
			$(".error-file-ext").prepend("<strong>" + extensionsForbidden[i] + ", </strong>");
		};

		if (sizeFiles > 25000000) {
			$("#photoUpload").parent().addClass("error");
			$(".error-file").css("display", "block");
		}

		if (sizeFiles < 25000000) {
			$(".error-file").css("display", "none");
			$("#photoUpload").parent().removeClass("error");
		}

	}

	var count = 0;

	for (var i = 0; i < extensionAcceptance.length; i++) {
		if (extensionAcceptance[i] === true) {
			count++;
		}
	};

	if (extensionAcceptance.length === count) {
		$(".error-file-ext").css("display", "none");
		$("#photoUpload").parent().removeClass("error");
	}




	if (idea.name.length > 0 && idea.descr.length > 0 && idea.tags.length > 0 && $(".setCategory").hasClass("active") == true && sizeFiles <= 25000000 && extensionAcceptance.length === count) {

		$("#finishBtn").html("Processing...");
		$('#myModal').modal('hide');
		$('#finishBtn').off("click");
		var files = document.getElementById("photoUpload").files;
		var Idea = Parse.Object.extend("Idea");
		var ideaObject = new Idea();
		ideaObject.set("titulo", idea.name);
		ideaObject.set("descripcion", idea.descr);
		ideaObject.set("esPrivado", idea.isPrivate);
		ideaObject.set("tags", idea.tags);
		ideaObject.set("locationCity", "Monterrey");
		ideaObject.set("category", idea.category);
		ideaObject.set("user", Parse.User.current());
		if (idea.challenge != null) {
			ideaObject.set("challenge", idea.challenge);
			ideaCHSub = true;
		}
		idea.fileCount = files.length;
		if (files.length > 0) {
			ideaObject.save(null,{
				success: function(ideaSaved){
					metLista(0, files.length, ideaSaved);
				},
				error: function(error){
					console.log(error);
				}
			});
		}

		else {
			ideaObject.save(null, {
				success: function(object) {
					clearAll();
					$("#finishBtnColaboration").html("Saved!");
					$("#NewCard").velocity("transition.fadeOut");
					newColaborationSavedCard(object);


					for (var i = 0; i < idea.tags.length; i++) {
						var tagname = "";
						tagname = idea.tags[i];

						Parse.Cloud.run('setNewTag', {
							tagname: tagname
						}, {
							success: function(newtag) {

							},

							error: function(error) {
							}

						});
					}
				},
				error: function(model, error) {
				}
			});
		}
	}
}

function removeByAttr(arr, attr, value) {
	var i = arr.length;
	while (i--) {
		if (arr[i] && arr[i].hasOwnProperty(attr) && (arguments.length > 2 && arr[i][attr] === value)) {

			arr.splice(i, 1);

		}
	}
	return arr;
}

function newColaborationSavedCard(ideaObj) {
	Parse.Cloud.run("unaIdeaFull", {
		oID: ideaObj.id,
		userID: Parse.User.current().id
	}, {
		success: function(ideaFull) {
			var object = ideaFull[0].idea;
			var comments = 0;
			var likes = 0;
			var attachments = 0;
			if (ideaFull[0].hasOwnProperty('files')) {
				attachments = ideaFull[0].files.length;
			}


			var abstract = object.get('descripcion').substring(0, 300) + "...";
			var imageUser = object.attributes.user.attributes.image.url();
			var imageUserY = object.attributes.user.attributes.imageYammer;
			if (imageUserY != null) imageUser = imageUserY;

			var datestring = object.createdAt.toString();
			var array = object.get('tags');
			var stringTags = "";
			var dateString = moment(datestring).fromNow();
			var aHTML = "";
			var photo = object.get("photoUrl");

			if (photo !== undefined) {
				for (var z = 0; z < array.length; z++) {
					stringTags = stringTags.concat("<a class='tag' href='#' >" + array[z] +
						"</a>");
				}

				stringTags = stringTags.concat("<a class='clear' href='#' ></a>");

				aHTML = '<div onClick="clickCard(this.id)" class="card white-panel" id="' + object.id +
					'" href="#"  style="transform: translateY(0px);">' +
					'   <h4 class="title">' + object.get("titulo") + '</h4>' +
					'   <span class="user-img" style="background-image: url(' + (object.get(
							"user").has("imageYammer") ? object.get("user").get("imageYammer") :
						object.get("user").get("image").url()) + ')"></span>' +
					'   <div id="section2">' + '       <div class="icon-time">' +
					'           <i class="material-icons time-icon"></i>' +
					'           <h5 class="date">' + dateString + '</h5>' + '       </div>' +
					'       <div class="icon-location">' +
					'           <i class="material-icons location-icon"></i>' +
					'           <h5 class="location">Monterrey</h5>' + '       </div>' +
					'   </div>' + '   <p class="description">' + object.get("descripcion")
					.substring(1, 100) + '       <img class="img-thumbnail-idea" src="' +
					object.get("photoUrl") +
					'">...<a onClick="clickCard(' + object.id + ')" class="read-more">See Full Idea &gt; </a>' + '   </p>' +
					'   <div id="interaction">' + '       <span class="icon comment">' +
					object.get("commentCount") + '</span>' +
					'       <span class="icon heart">' + object.get("likeCount") +
					'</span>' + '       <span class="icon attachment">' + object.get(
						"fileCount") + '</span>' + '   </div>' + '<div id="tags" class="' +
					object.id + '">' + stringTags + '</div>' + '</div>';
			}
			else {
				for (var j = 0; j < array.length; j++) {
					stringTags = stringTags.concat("<a class='tag' href='#' >" + array[j] +
						"</a>");
				}
				stringTags = stringTags.concat("<a class='clear' href='#' ></a>");
				aHTML = '<div onClick="clickCard(this.id)" class="card white-panel" id="' + object.id +
					'" href="#"  style="transform: translateY(0px);">' +
					'   <h4 class="title">' + object.get("titulo") + '</h4>' +
					'   <span class="user-img" style="background-image: url(' + (object.get(
							"user").has("imageYammer") ? object.get("user").get("imageYammer") :
						object.get("user").get("image").url()) + ')"></span>' +
					'   <div id="section2">' + '       <div class="icon-time">' +
					'           <i class="material-icons time-icon"></i>' +
					'           <h5 class="date">' + dateString + '</h5>' + '       </div>' +
					'       <div class="icon-location">' +
					'           <i class="material-icons location-icon"></i>' +
					'           <h5 class="location">Monterrey</h5>' + '       </div>' +
					'   </div>' + '   <p class="description">' + object.get("descripcion")
					.substring(1, 100) +
					'       ...<a class="read-more">See Full Idea &gt; </a>' + '   </p>' +
					'   <div id="interaction">' + '       <span class="icon comment">' +
					object.get("commentCount") + '</span>' +
					'       <span class="icon heart">' + object.get("likeCount") +
					'</span>' + '       <span class="icon attachment">' + object.get(
						"fileCount") + '</span>' + '   </div>' + '<div id="tags" class="' +
					object.id + '">' + stringTags + '</div>' + '</div>';
			}

			$("#ideaRelatedBox").after(aHTML);
		},
		error: function(error) {
		}
	});
}

function eliminateDuplicates(arr) {
	var z,
		len = arr.length,
		out = [],
		obj = {};

	for (z = 0; z < len; z++) {
		obj[arr[z]] = 0;
	}
	for (z in obj) {
		out.push(z);
	}
	return out;
}

function createIdea() {
	var extensionAcceptance = [];
	idea.name = $("#inputName").val().trim();
	idea.descr = $("#inputDescr").val().trim();
	idea.isPrivate = $("#cmn-toggle-1").prop("checked");
	idea.tags = getArrayTags($('#small').tokenfield('getTokens'));

	if (idea.name === "") {
		$("#inputName").addClass("error");
		$(".error-title").css("display", "block");
	}
	if (idea.name !== "") {
		$("#inputName").removeClass("error");
		$(".error-title").css("display", "none");
	}

	if (idea.descr === "") {
		$("#inputDescr").addClass("error");
		$(".error-descr").css("display", "block");
	}
	if (idea.descr !== "") {
		$("#inputDescr").removeClass("error");
		$(".error-descr").css("display", "none");
	}

	if (idea.tags.length < 1) {
		$(".error-tag").css("display", "block");
		$("#small").parent().addClass("error");
	}
	if (idea.tags.length > 0) {
		$(".error-tag").css("display", "none");
		$("#small").parent().removeClass("error");
	}

	if (!$(".setCategory").hasClass("active")) {
		$(".categorygroup").addClass("error");
		$(".error-category").css("display", "block");
	}
	if ($(".setCategory").hasClass("active")) {
		$(".error-category").css("display", "none");
		$(".categorygroup").removeClass("error");
	}

	var sizeFiles = 0;
	var extensionsForbidden = [];
	var arrayFiles = $('#photoUpload').prop("files").length;
	if (arrayFiles > 0) {

		for (var i = 0; i < arrayFiles; i++) {


			var file = document.getElementById('photoUpload').files[i];
			sizeFiles = sizeFiles + file.size;

			var fname = file.name;
			//copied
			var re =
                /(\.jpg|\.apk|\.jpeg|\.bmp|\.gif|\.doc|\.dot|\.docx|\.docm|\.dotm|\.docb|\.xls|\.xlt|\.xlm|\.xlsx|\.xlsm|\.xltx|\.xltm|\.xlsb|\.xla|\.xlam|\.xll|\.xlw|\.ppt|\.ptx|\.pdf|\.png|\.log|\.msg|\.odt|\.pages|\.rtf|\.tex|\.txt|\.wpd|\.wps|\.csv|\.dat|\.key|\.pps|\.xml|\.aif|\.m4a|\.mid|\.mp3|\.mpa|\.wma|\.asf|\.mp4|\.gif|\.psd|\.tif|\.tiff|\.ai|\.eps|\.svg|\.png|\.sql|\.asp|\.aspx|\.css|\.html|\.js|\.php|\.rss|\.xhtml|\.ttf|\.class|\.java)$/i;
			var validation = re.exec(fname);
			if (!re.exec(fname)) {
				extensionAcceptance.push(false);
				var character = /(?:\.([^.]+))?$/;
				var ext = character.exec(fname)[1];
				extensionsForbidden.push(ext);



				$(".error-file-ext").css("display", "block");
				$("#photoUpload").parent().addClass("error");

			}
			else {
				extensionAcceptance.push(true);
			}

		};

		$(".error-file-ext strong").remove();
		extensionsForbidden = eliminateDuplicates(extensionsForbidden);
		for (var i = 0; i < extensionsForbidden.length; i++) {
			$(".error-file-ext").prepend("<strong>" + extensionsForbidden[i] + ", </strong>");
		};

		if (sizeFiles > 25000000) {
			$("#photoUpload").parent().addClass("error");
			$(".error-file").css("display", "block");
		}

		if (sizeFiles < 25000000) {
			$(".error-file").css("display", "none");
			$("#photoUpload").parent().removeClass("error");
		}

	}

	var count = 0;

	for (var i = 0; i < extensionAcceptance.length; i++) {
		if (extensionAcceptance[i] === true) {
			count++;
		}
	};

	if (extensionAcceptance.length === count) {
		$(".error-file-ext").css("display", "none");
		$("#photoUpload").parent().removeClass("error");
	}




	if (idea.name.length > 0 && idea.descr.length > 0 && idea.tags.length > 0 && $(".setCategory").hasClass("active") == true && sizeFiles <= 25000000 && extensionAcceptance.length === count) {

		$("#finishBtn").html("Processing...");
		
		$('#finishBtn').off("click");

		var url = window.location.pathname.split("/")[2];
		if (url === "home.html" || url === "home.html#") {
			$('#myModal').modal('toggle');
			newCard();
			initGear("NewCard");
			initGear("NewCard2");

		}



		var files = document.getElementById("photoUpload").files;


		var Idea = Parse.Object.extend("Idea");
		var ideaObject = new Idea();
		ideaObject.set("titulo", idea.name);
		ideaObject.set("descripcion", idea.descr);
		ideaObject.set("esPrivado", idea.isPrivate);
		ideaObject.set("tags", idea.tags);
		if(Parse.User.current().get("locationCity") != undefined)
			ideaObject.set("locationCity", Parse.User.current().get("locationCity"));
		else
			ideaObject.set("locationCity", "Not Defined");
		ideaObject.set("category", idea.category);
		ideaObject.set("user", Parse.User.current());

		var filename = window.location.pathname.substring(window.location.pathname.lastIndexOf('/')+1);
		if(filename == "viewIdea.html" || filename == "viewIdea.html#")
		{
			ideaObject.set("ideaParent",{"__type":"Pointer",
                            "className":"Idea",
                            "objectId":GetUrlValue("k")});
		}

		if (idea.challenge != null) {
			ideaObject.set("challenge", idea.challenge);
			ideaCHSub = true;
		}
		idea.fileCount = files.length;
		if (files.length > 0) {
			ideaObject.save(null,{
				success: function(ideaSaved){
					console.log("HERE FING idea");
					console.log(ideaSaved);
					metLista(0, files.length, ideaSaved);
				},
				error: function(error){
					console.log(error);
				}
			});
		}

		else {
			ideaObject.save(null, {
				success: function(object) {
					$("#finishBtn").html("Saved!");

					var url = window.location.pathname.split("/")[2];
					if (url === "home.html" || url === "home.html#") {
						stopGear();
						$("#NewCard").remove();
						$("#NewCard2").remove();
						$("#NewCard").velocity("transition.fadeOut");
						$("#NewCard2").velocity("transition.fadeOut");
					}



					if (idea.challenge != null)
						newChallengeIdea(object);
					newSavedCard(object);

					for (var i = 0; i < idea.tags.length; i++) {
						var tagname = "";
						tagname = idea.tags[i];

						Parse.Cloud.run('setNewTag', {
							tagname: tagname
						}, {
							success: function(newtag) {

								clearAll();
							},

							error: function(error) {
							}

						});
					}

					if (url === "viewIdea.html" || url === "viewIdea.html#") {
						window.location = "viewIdea.html?k="+object.id;
					}
					//getLocation();
				},
				error: function(model, error) {
					alert("Didnt Saved!");
				}
			});
		}
	}
}

function newCard() {
	var imageUser = Parse.User.current().get("image").url();
	var imageUserY = Parse.User.current().get("imageYammer");
	if (imageUserY != null) imageUser = imageUserY;

	$("#my-ideas-cards").prepend("<div class='card' id='NewCard2" +
		"' href='#'><div id='category'><div class='noactive ideaCategory'></div><div class='noactive needsCategory'></div><div class='noactive appCategory'></div><div class='noactive pocCategory'></div><div class='noactive microCategory'></div><div class='noactive otherCategory'></div></div><h4 class='titleNewCard'>" +
		"Adding new Idea..." + "</h4><span class='user-img' style = 'background-image: url(" + imageUser + ")'></span></div>");
}

function clearAll() {
	idea = {
		name: "",
		descr: "",
		tags: [],
		isPrivate: false,
		category: 0,
		fileCount: 0,
		challengeID: null
	};
	$("#inputName").val("");
	$("#inputDescr").val("");
	$("#finishBtn").html("Finish");

	$(".setCategory").removeClass("active");


	while ($('#small').tokenfield('getTokens').length > 0) {
		$('#small').tokenfield('setTokens', []);
	}
	$("#small input").addClass("fixEnter");
	var input = $("#photoUpload");
	input.replaceWith(input.val('').clone(true));
}