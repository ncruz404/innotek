var ideatag;

function GetUrlValue(VarSearch) {
    var SearchString = window.location.search.substring(1);
    if (SearchString.length > 0) {
        var VariableArray = SearchString.split('&');
        for (var i = 0; i < VariableArray.length; i++) {
            var KeyValuePair = VariableArray[i].split('=');
            if (KeyValuePair[0] == VarSearch) {
                return KeyValuePair[1];
            }
        }
    }
    else
        return null;
}

var tagName = GetUrlValue("t").replace('-', ' ');
var ideatag = [];

Parse.Cloud.run('tagIdeas', {
    tagname: tagName,
    cantidad: 200,
    skip: 0
}, {
    success: function(result) {

        $("#title-idea").append("<strong> " + tagName + "</strong>");

        for (var i = 0; i < result.length; i++) {
            ideatag = result[i];
            createGridTags(ideatag.idea, ideatag.comments, ideatag.likes, ideatag.files);
        }

        //createGridTags();
    },
    error: function(error) {
        console.log(error.message);
    }
});

function createGridTags(idea, comments, likes, attachments) {

    var idUser = idea.attributes.user.id;
    var imageUser = idea.attributes.user.attributes.image.url();
    var imageUserY = idea.attributes.user.attributes.imageYammer;
    if (imageUserY != null) imageUser = imageUserY;


    var datestring = idea.createdAt.toString();
    var time = moment(datestring).fromNow();
    var abstract = idea.get('descripcion').substring(0, 300) + "...";

    $("#ideasRelated .pinBoot").append("<div class='card white-panel' id='" + idea.id + "' onClick='clickCard(this.id)' style='transform: translateY(0px);'> <h4 class='title'>" + idea.get("titulo") + "</h4><a class='user-img' id=" + idUser + "   style= 'background-image: url(" + imageUser + ")' href='#'></a> <div id='section2'> <div class='icon-time'> <i class='material-icons time-icon'></i> <h5 class='date'>" + time + "</h5> </div> <div class='icon-location'> <i class='material-icons location-icon'></i> <h5 class='location'>" + idea.get("locationCity") + "</h5> </div> </div> <p class='description'>" + abstract + "<a class= 'read-more'>See Full Idea &gt;</a></p> <div id='interaction'> <span class='icon comment'>" + comments + "</span><span class='icon heart'>" + likes + "</span><span class='icon attachment'>" + attachments + "</span> </div> <div class='" + idea.id + "' id='tags'> </div> </div>");

    if (idea.get("photoUrl") !== undefined) {
        $("#" + idea.id + " p.description").prepend("<img class='img-thumbnail-idea' src='" + idea.get("photoUrl") + "'>");
    }
    var tagsWord = idea.get('tags').toString();
    var array = tagsWord.split(',');
    for (var i = 0; i < array.length; i++) {
        $("#tags" + "." + idea.id).append("<a class='tag' href='#' >" + array[i] + "</a>");
    }
    $("#tags " + "." + idea.id).append("<a class='clear' href='#' ></a>");

    Parse.Cloud.run('getFollowing', {}, {
        success: function(result) {
            var tags = (result.attributes.tagFollowing);
            if (tags.indexOf(tagName) < 0) {
                $("#followBtn.btn-tag").removeClass("following");
                $("#followBtn.btn-tag").html("Follow");
            }
            else {

                $("#followBtn.btn-tag").html("Following");
                $("#followBtn.btn-tag").addClass("following");
            }

        },
        error: function(error) {
            console.log(error);
        }
    });
}


$(document).on('click', 'a.user-img', function() {
    var word = this.id;
       if(word===Parse.User.current().id){
        window.location = "perfilDashboard.html";
    }
    else
    {
        window.location = "user.html?i=" + word;
    }
});









$(document).on('click', '#followBtn.btn-tag', function() {
    var tagName = GetUrlValue("t").replace('-', ' ');
    var tagFollow = [];
    tagFollow.push(tagName.toLowerCase());

    if ($("#followBtn.btn-tag").hasClass("following") === false) {

        $("#followBtn.btn-tag").html("Following");
        $("#followBtn.btn-tag").addClass("following");
        console.log(tagFollow);
        Parse.Cloud.run('setFollowing', {
            tag: tagFollow
        }, {
            success: function(result) {
                console.log(result);

            },
            error: function(error) {
                console.log(error);
            }
        });


    }
    else
    if ($("#followBtn.btn-tag").hasClass("following") === true) {

        Parse.Cloud.run('removeFollowing', {
            tag: tagFollow[0]
        }, {
            success: function(result) {
                console.log(result);

            },
            error: function(error) {
                console.log(error);
            }
        });

        $("#followBtn.btn-tag").removeClass("following");
        $("#followBtn.btn-tag").html("Follow");
    }
});

//click to view idea
function clickCard(clicked_id) {
   
            window.location = "viewIdea.html?k=" + clicked_id;



}


$(document).on('click', '.tag', function() {
    var word = $(this).text();
    word = word.replace(' ', '-');
    window.location = "tags.html?t=" + word;
});



