
//-------------------------------------------------------------------------------------------------------------
//---------------    Necesario para inicializar el componente de parse   --------------------------------------
//-------------------------------------------------------------------------------------------------------------
Parse.initialize("BkC2n3AgwDP4L3Ixs5nndMC1qUV5aKd6TVxMabk4", "F364pcWymED6uIglRk7wWcGt7x9bmXUQKl1qezBp");
//-------------------------------------------------------------------------------------------------------------
//---------------    Necesario para inicializar el componente de parse   --------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//---------------    Sección que evita que se entre sin autorización     --------------------------------------
//-------------------------------------------------------------------------------------------------------------
var currentUser = Parse.User.current();

if (currentUser) {
  Parse.Cloud.run('checkOfficeToken',{token: currentUser.get("token")},{
    success: function(response){
      var url = window.location.pathname;
      var filename = url.substring(url.lastIndexOf('/')+1);
      if(filename == "index.html")
        window.location = "home.html";
    },
    error: function(error){
      Parse.User.logOut();
      window.location = "index.html"
    }
  });
} 
else {
  var url = window.location.pathname;
	var filename = url.substring(url.lastIndexOf('/')+1);
	if(filename != "index.html")
      window.location = "index.html";
}

$("body").append('<script type="text/javascript">'+
        'window.heap = window.heap || [], heap.load = function (e, t) { window.heap.appid = e, window.heap.config = t = t || {}; var r = t.forceSSL || "https:" === document.location.protocol, a = document.createElement("script"); a.type = "text/javascript", a.async = !0, a.src = (r ? "https:" : "http:") + "//cdn.heapanalytics.com/js/heap-" + e + ".js"; var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(a, n); for (var o = function (e) { return function () { heap.push([e].concat(Array.prototype.slice.call(arguments, 0))) } }, p = ["addEventProperties", "addUserProperties", "clearEventProperties", "identify", "removeEventProperty", "setEventProperties", "track", "unsetEventProperty"], c = 0; c < p.length; c++) heap[p[c]] = o(p[c]) };'+
        'heap.load("2843715222");</script>');

$("#logOutBtn").click(function(event){
	event.preventDefault();     
 	logOut();
});

function logOut(){
  event.preventDefault();
  Parse.User.logOut();
  window.location = "https://login.windows.net/common/oauth2/logout?post_logout_redirect_uri=http://produmigration.parseapp.com/";     
}

function requestToken() { 
  // Change clientId and replyUrl to reflect your app's values 
  // found on the Configure tab in the Azure Management Portal. 
  // Also change {your_subdomain} to your subdomain for both endpointUrl and resource. 
  var clientId    = 'a8a0c67e-cc5d-4dcd-bd2b-07fbf1a67353';
  var replyUrl    = 'http://produmigration.parseapp.com/page_home/index.html'; 
  var endpointUrl = 'https://graph.microsoft.com/beta/me/';
  var resource = "https://graph.microsoft.com"; 
  var authServer  = 'https://login.windows.net/common/oauth2/authorize?';  
  var responseType = 'token'; 
  var tenant = 'softtek.com';
  var url = authServer + 
            "response_type=" + encodeURI(responseType) + "&" + 
            "client_id=" + encodeURI(clientId) + "&" + 
            "resource=" + encodeURI(resource) + "&" + 
            "tenant=" + encodeURI(tenant) + "&" +
            "redirect_uri=" + encodeURI(replyUrl); 
  window.location = url; 
}


function requestTokenOutlook() { 
  // Change clientId and replyUrl to reflect your app's values 
  // found on the Configure tab in the Azure Management Portal. 
  // Also change {your_subdomain} to your subdomain for both endpointUrl and resource. 
  var clientId    = 'a8a0c67e-cc5d-4dcd-bd2b-07fbf1a67353';
  var replyUrl    = 'http://produmigration.parseapp.com/page_home/logon.html'; 
  var endpointUrl = 'https://outlook.office.com/api/v1.0/me/';
  var resource = "https://outlook.office.com"; 
  var authServer  = 'https://login.windows.net/common/oauth2/authorize?';  
  var responseType = 'token'; 
  var tenant = 'softtek.com';
  var url = authServer + 
            "response_type=" + encodeURI(responseType) + "&" + 
            "client_id=" + encodeURI(clientId) + "&" + 
            "resource=" + encodeURI(resource) + "&" + 
            "tenant=" + encodeURI(tenant) + "&" +
            "redirect_uri=" + encodeURI(replyUrl); 
  window.location = url; 
}
//-------------------------------------------------------------------------------------------------------------
//---------------    Sección que evita que se entre sin autorización     --------------------------------------
//-------------------------------------------------------------------------------------------------------------

window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
    heap.load("2843715222");

