var object;
var categoriesSelected = [];
var skipSuggested = 0;
var skipPopular = 0;
var skipUserIdeas = 0;
var userIdeas = 0;
var skipRecently = 0;
var skipRecentlyFilter = 0;
var filter;
var filterselected = false;
var openDropdown = false;
var lastRecentlyAdded = false;
var lastRecentlyAddedNoFilter = false;
var popularLoaded = suggestedLoaded = userideasLoaded = recentlyaddedLoaded = false;
var filterUser;
var tagSelected = [];
var follow = false;
var skipRandomTags = 0;
var skipChallenge = 0;
var skipChallengeFilter = 0;
var randomtagscard = false;
var suggestedScroll = 4;
var arraySuggested = [];
var cardrandom = true;
var activeTab = 1;



function checkTour() {
    if ("?t=tour" === window.location.search) {
        walkThrough();
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '';
        window.history.pushState({
            path: newurl
        }, '', newurl);
    }
}

checkFilterLocation();

function checkFilterLocation()
{
    var locationCity = Parse.User.current().get("locationCity");
    if(locationCity != undefined && locationCity.length > 0)
        $("#filterL-100").append(": "+locationCity);
}

loadRecentlyAddedFirst();
//create side bar
Parse.Cloud.run('sideBar', {}, {
    success: function(user) {
        for (var i = 0; i < user.length; i++) {
            var object = user[i];
            createSidebar(object);
        }
    },
    error: function(error) {
        console.log(error.message);
    }
});

//function create column most recent
function createRecentlyAddedCards(object, comments, likes, attachments) {
    if (object.get("category") === 22) {
        var datestring = object.createdAt.toString();
        var hola = moment(datestring).fromNow();
        var aHTML = '<div  class="card challenge text-left" id="' + object.id + '" >' +
            "<div class='card-title-div col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding text-left'>" +
            "<img class='user-img' src='" + imageUser + "' data-toggle='tooltip' data-placement='bottom' title='" +  object.attributes.user.attributes.username + "'>" +
            "<h4 class='title'>" + object.get('titulo') + "</h4>" +
            "</div>" +
            '<div id="section2">' +
            "<div class='icon-time col-xs-6 col-sm-6 col-md-6 col-lg-6'>" +
            '<i class="material-icons time-icon"></i>' +
            '<h5 class="date">' + hola + '</h5>' +
            '</div>' +
            "<div class='icon-location col-xs-6 col-sm-6 col-md-6 col-lg-6'>" +
            '<i class="material-icons location-icon"></i>' +
            '<h5 class="location">'+((object.get("user").get("locationCity")!= undefined && object.get("user").get("locationCity").length > 0)? object.get("user").get("locationCity") : "Monterrey")+'</h5>' +
            '</div>' +
            '</div>' +
            '<p id="ChallengeDescription" class="description">' + object.get("descripcion") + '</p>' +
            '<div id="btnChallenge">' +
            '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6"></div>' +
            '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6"></div>' +
            '</div>' +
            '<hr id="lineChalenge">' +
            '<div class="text-center">' +
            '<a href="grid.html?k=' + object.id + '" class="bold600">See Challenge</a>' +
            '</div>' +
            '</div>';
        $("#recently-added-cards").append(aHTML);
        var btnToCreate = $("#" + object.id + " #btnChallenge div:first-child");
        var btnToLink = $("#" + object.id + " #btnChallenge div:nth-child(2)");
        var btn = document.createElement("A"); // Create a <button> element
        var t = document.createTextNode("Contribute"); // Create a text node
        btn.appendChild(t);
        btn.setAttribute("class", "contributeIdea");
        btn.setAttribute("data-info", object.id);
        btn.onclick = function() {
            contributeIdea(object);
        }
        btnToCreate.append(btn);
        var btnLink = document.createElement("A"); // Create a <button> element
        t = document.createTextNode("Link Post"); // Create a text node
        btnLink.appendChild(t);
        btnLink.setAttribute("class", "linkIdea");
        btnLink.onclick = function() {
                linkIdea(object);
            } // Append the text to <button>
        btnToLink.append(btnLink);
    }
    else {
        createCard(object, comments, likes, attachments, "#recently-added-cards");
        ideaCategory(object, '#most-recent', object.id);
        ideaHasPhotoAttached(object, '#most-recent', object.id);
        //   addAnimation("#most-recent", object.id, 1000);
        var array = [];
        array.length = 0;
        var tagsWord = object.get('tags').toString();
        array = tagsWord.split(',');
        for (var i = 0; i < array.length; i++) {
            $("#most-recent .tag" + object.id).append("<a class='tag' href='#' >" + array[i] + "</a>");
        }
        $("#most-recent .tag" + object.id).append("<a class='clear' href='#' ></a>");
    }
}

function createCard(object, comments, likes, attachments, div) {
    var abstract = object.get('descripcion').substring(0, 300) + "...";
    var imageUser = object.attributes.user.attributes.image.url();
    var imageUserY = object.attributes.user.attributes.imageYammer;
    if (imageUserY != null) imageUser = imageUserY;
    var datestring = object.createdAt.toString();
    var hola = moment(datestring).fromNow();
    $(div).append("<div class='card' id='" + object.id + "' href='#' onClick='clickCard(this.id)'>" +
        "<div id='category'>" +
        "<div class='noactive ideaCategory'></div>" +
        "<div class='noactive needsCategory'></div>" +
        "<div class='noactive appCategory'></div>" +
        "<div class='noactive pocCategory'></div>" +
        "<div class='noactive microCategory'></div>" +
        "<div class='noactive otherCategory'></div>" +
        "</div>" +
        "<div class='card-title-div col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding text-left'>" +
        "<img class='user-img' src='" + imageUser + "' data-toggle='tooltip' data-placement='bottom' title='" +  object.attributes.user.attributes.username + "'>" +
        "<h4 class='title'>" + object.get('titulo') + "</h4>" +
        "</div>" +
        "<div id='section2'>" +
        "<div class='icon-time col-xs-6 col-sm-6 col-md-6 col-lg-6'>" +
        "<i class='material-icons time-icon'>&#xE425;</i>" +
        "<h5 class='date'>" + hola + "</h5>" +
        "</div>" +
        "<div class='icon-location col-xs-6 col-sm-6 col-md-6 col-lg-6'>" +
        "<i class='material-icons location-icon'>&#xE0C8;</i> " +
        "<h5 class='location'>" + ((object.get("user").get("locationCity")!= undefined && object.get("user").get("locationCity").length > 0)? object.get("user").get("locationCity") : "Monterrey") + "</h5>" +
        "</div>" +
        "</a>" +
        "</div>" +
        "<p class='description'>" + abstract + "" +
        "</p>" +
        "<div id='tags' class='col-xs-12 col-sm-12 col-md-12 col-lg-12 tag" + object.id + " no-padding'></div>" +
        "<div id='interaction' class='col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding text-left'>" +
        "<span><i class='fa fa-comment-o'></i> " + comments + "</span>" +
        "<span><i class='fa fa-heart-o'></i> " + likes + "</span>" +
        "<span><i class='fa fa-paperclip'></i> " + attachments + "</span>" +
        "</div>" +
        "<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding-right text-right'>" +
        "<a type='button' class='btn btn-fullidea read-more'>See Full Idea</a>" +
        "</div>" +
        
        "</div>");
}

//function create column most popular
function createPopularCards(object, comments, likes, attachments) {

    createCard(object, comments, likes, attachments, "#most-popular-cards");
    ideaHasPhotoAttached(object, '#most-popular', object.id);
     ideaCategory(object, '#most-popular', object.id);
    //   addAnimation("#most-popular", object.id, 1000);
    var array = [];
    array.length = 0;
    var tagsWord = object.get('tags').toString();
    array = tagsWord.split(',');
    for (var i = 0; i < array.length; i++) {
        $("#most-popular .tag" + object.id).append("<a class='tag' href='#' >" + array[i] + "</a>");
    }
    $("#most-popular .tag" + object.id).append("<a class='clear' href='#' ></a>");
}

//function create column most active
function createCardSuggested(object, comments, likes, attachments) {
    createCard(object, comments, likes, attachments, "#suggested-ideas-cards");
    ideaCategory(object, '#most-active', object.id);
    ideaHasPhotoAttached(object, '#most-active', object.id);
    addAnimation("#most-active", object.id, 1000);
    var array = [];
    array.length = 0;
    var tagsWord = object.get('tags').toString();
    array = tagsWord.split(',');
    for (var i = 0; i < array.length; i++) {
        $("#most-active .tag" + object.id).append("<a class='tag' href='#' >" + array[i] + "</a>");
    }
    $("#most-active .tag" + object.id).append("<a class='clear' href='#' ></a>");
}

//function create column my ideas
function createUserIdeasCards(object, comments, likes, attachments) {
    createCard(object, comments, likes, attachments, "#my-ideas-cards");
    ideaCategory(object, '#my-ideas', object.id);
    ideaHasPhotoAttached(object, '#my-ideas', object.id);
    //       addAnimation("#my-ideas", object.id, 1000);
    var array = [];
    array.length = 0;
    var tagsWord = object.get('tags').toString();
    array = tagsWord.split(',');
    for (var i = 0; i < array.length; i++) {
        $("#my-ideas .tag" + object.id).append("<a class='tag' href='#' >" + array[i] + "</a>");
    }
    $("#my-ideas .tag" + object.id).append("<a class='clear' href='#' ></a>");
}

//function create sidebar
function createSidebar(object) {
    var name = object.get("name").split(' ')[0];
    var imageUser = object.get("image").url();
    var imageUserY = object.get("imageYammer");
    if (imageUserY != null) imageUser = imageUserY;
    $("#sidebar ul").append("<li><a href='user.html?i=" + object.id + "'><img class='user-img' src='" + imageUser + "'><p>" + name +
        "</p></a></li>");
}

//function to know if user has or doesnt have ideas
function columnIsEmpty(category) {
    $(category).append(
        "<div id='column-is-empty-info'><i class='material-icons'>mood_bad</i><h2>No posts yet</h2> <h5 class='message-noideas'>Tell us about your ideas or inventions to improve our company</h5><a class='dropdown-toggle new_idea_a btn first_new_idea' data-toggle='modal' data-target='#myModal' href='#''> Add New Post</a> </div>"
    );
}

//responsive width and height
$(window).resize(function() {
    $("#sidebar").css("height", function(index) {
        return $(document).height();
    });
    $("#headerColumns").css("width", $("body").width() - 60);
    $(".card").css("width", $(".column").width() - 20);
    $(".column").css("height", $(document).height());

    if ($(document).width() < 600) {
        $(".header-column").text("");
        $("#home .bg-icon").css("background-position-x", "center");
    }

    $("#filter-dropdown").width($("#FilterPostChallenge").height());

    if ($(document).width() >= 600) {

        $(".header-column.blue").text("Suggested for you");
        $(".header-column.purple").text("My Posts");
        $(".header-column.red").text("Most Popular");
        $("#home .bg-icon").css("background-position-x", "20px");

        if ($(".header-column.green").find("#filtermenu").length < 1) {
            $(".header-column.green").append('Recently Added <a href="#" id="filtermenu"><i class="material-icons filter">tune</i>');
        }
    }

    if ($(document).width() < 700) {

        $(".header-column.purple").css("display", "none");
        $("#my-ideas").css("display", "none");
        $(".header-column").css("cursor", "pointer");
        $("#headerColumns").css("width", "100%");
        $("#home .column").css("width", "100%");
        $(".header-column").css("width", "33.333%");
        $("#sidebar").css("display", "none");
        $("#columns").css("padding-right", "0");
        $(".card").css("width", $(".column").width() - 20);
    }

    if ($(document).width() < 700 && activeTab === 1) {

        $(".header-column.red").css("background-color", "#cacaca");
        $(".header-column.blue").css("background-color", "#cacaca");
        $(".header-column.green").css("background-color", "#2ecc71");

        $("#most-popular").css("display", "none");
        $("#most-active").css("display", "none");
        $("#most-recent").css("display", "block");

        $("#recently-added-cards").css("width", "100%");
        $("#most-recent").css("width", "100%");
    }



    if ($(document).width() < 700 && activeTab === 2) {
        $(".header-column.red").css("background-color", "#e74c3c");
        $(".header-column.blue").css("background-color", "#cacaca");
        $(".header-column.green").css("background-color", "#cacaca");

        $("#most-popular").css("display", "block");
        $("#most-active").css("display", "none");
        $("#most-recent").css("display", "none");

        $("#most-popular-cards").css("width", "100%");
        $("#most-popular").css("width", "100%");
    }




    if ($(document).width() < 700 && activeTab === 3) {
        $(".header-column.red").css("background-color", "#cacaca");
        $(".header-column.blue").css("background-color", "#3498db");
        $(".header-column.green").css("background-color", "#cacaca");

        $("#most-popular").css("display", "none");
        $("#most-active").css("display", "block");
        $("#most-recent").css("display", "none");

        $("#suggested-ideas-cards").css("width", "100%");
        $("#most-active").css("width", "100%");
    }







    if ($(document).width() >= 700) {
        $(".header-column.red").css("background-color", "#e74c3c");
        $(".header-column.blue").css("background-color", "#3498db");
        $(".header-column.green").css("background-color", "#2ecc71");
        $(".header-column.purple").css("display", "none");
        $("#most-popular").css("display", "block");
        $("#most-recent").css("display", "block");
        $("#most-active").css("display", "block");
        $("#sidebar").css("display", "block");
        $("#columns").css("padding-right", "60px");
    }

    if ($(document).width() > 699 && $(document).width() < 900) {
        $(".header-column.purple").css("display", "none");
        $("#my-ideas").css("display", "none");
        $("#home .column").css("width", "33.333%");
        $(".header-column").css("width", "33.333%");
        $(".card").css("width", $(".column").width() - 20);
        $("#headerColumns").css("width", $("body").width() - 60);
        $("#sidebar").css("display", "block");

    }

    if ($(document).width() > 899) {
        $(".header-column.purple").css("display", "block");
        $("#my-ideas").css("display", "block");
        $("#home .column").css("width", "25%");
        $("#headerColumns").css("width", $("body").width() - 60);
        $(".header-column").css("width", "25%");
        $(".card").css("width", $(".column").width() - 20);
        $("#sidebar").css("display", "block");

    }

});



//click to view idea
function clickCard(clicked_id) {

    window.location = "viewIdea.html?k=" + clicked_id;


}





//post image of idea if it has it
function ideaHasPhotoAttached(object, category, card) {
    if (object.get("photoUrl") !== undefined) {
        $(category + ' div#' + card + ' p.description').prepend('<img class="img-thumbnail-idea" src="' + object.get("photoUrl") + '">');
    }
}

//Animations 
function addAnimation(category, objectId, speed) {
    $(category + " #" + objectId).velocity({
        translateY: [0, 200],
    }, speed);
}

function animateHome() {

    $(".se-pre-con").fadeOut("slow");



    //display navsuperior y sidebar ya que se cargó
    $("#sidebar").css("display", "block");
    //animation sidebar

    //navbar
    $(".navbar-toggle").on("click", function() {
        $(this).toggleClass("active");
    });
    $("#columns").css("display", "block");
    //animation columns
    $("#columns").velocity({
        translateY: [0, 300],
    }, 1500);;
    //change width columns div
    $("#headerColumns").css("width", $("body").width() - 60);
    $("#columns").css("height", function(index) {
        return $(document).height();
    });

    $(".card").css("width", $(".column").width() - 20);
    //height sidebar
    $("#sidebar").css("height", function(index) {
        return $(document).height();
    });
        $(".column").css("height", $(document).height());

    $.when().then(function() {
        $("#recently-added-cards").velocity({
            translateY: [0, 200],
        }, 1000);

    }).then(function() {
        $("#most-popular-cards").velocity({
            translateY: [0, 200],
        }, 1300);

    }).then(function() {
        $("#suggested-ideas-cards").velocity({
            translateY: [0, 200],
        }, 1600);

    }).then(function() {
        $("#my-ideas-cards").velocity({
            translateY: [0, 200],
        }, 1900);

    });

    $("#filter-dropdown").width($("#FilterPostChallenge").height());

    if ($(document).width() < 600) {
        $(".header-column").text("");
        $("#home .bg-icon").css("background-position-x", "center");
    }



    if ($(document).width() < 700) {

        $(".header-column").css("border-right", "1px solid #dddddd");
        $(".header-column.purple").css("display", "none");
        $("#my-ideas").css("display", "none");
        $("#headerColumns").css("width", "100%");
        $("#home .column").css("width", "100%");
        $(".header-column").css("cursor", "pointer");
        $(".header-column").css("width", "33.333%");

        $("#sidebar").css("display", "none");


        $("#columns").css("padding-right", "0");
        $(".card").css("width", $(".column").width() - 20);
    }

    if ($(document).width() < 700 && activeTab === 1) {

        $(".header-column.red").css("background-color", "#cacaca");
        $(".header-column.blue").css("background-color", "#cacaca");
        $(".header-column.green").css("background-color", "#2ecc71");

        $("#most-popular").css("display", "none");
        $("#most-active").css("display", "none");
        $("#most-recent").css("display", "block");

        $("#recently-added-cards").css("width", "100%");
        $("#most-recent").css("width", "100%");
    }



    if ($(document).width() < 700 && activeTab === 2) {
        $(".header-column.red").css("background-color", "#e74c3c");
        $(".header-column.blue").css("background-color", "#cacaca");
        $(".header-column.green").css("background-color", "#cacaca");

        $("#most-popular").css("display", "block");
        $("#most-active").css("display", "none");
        $("#most-recent").css("display", "none");

        $("#most-popular-cards").css("width", "100%");
        $("#most-popular").css("width", "100%");
    }




    if ($(document).width() < 700 && activeTab === 3) {
        $(".header-column.red").css("background-color", "#cacaca");
        $(".header-column.blue").css("background-color", "#3498db");
        $(".header-column.green").css("background-color", "#cacaca");

        $("#most-popular").css("display", "none");
        $("#most-active").css("display", "block");
        $("#most-recent").css("display", "none");

        $("#suggested-ideas-cards").css("width", "100%");
        $("#most-active").css("width", "100%");
    }


    if ($(document).width() >= 700) {
        $(".header-column.red").css("background-color", "#e74c3c");
        $(".header-column.blue").css("background-color", "#3498db");
        $(".header-column.purple").css("display", "none");
        $("#most-popular").css("display", "block");
        $("#most-active").css("display", "block");
        $("#sidebar").css("display", "block");
        $("#sidebar").velocity("transition.bounceRightIn");
        $("#columns").css("padding-right", "60px");
    }

    if ($(document).width() > 699 && $(document).width() < 900) {
        $(".header-column.purple").css("display", "none");
        $("#my-ideas").css("display", "none");
        $("#home .column").css("width", "33.333%");
        $(".header-column").css("width", "33.333%");
        $("#most-recent").css("width", "33.33%");
        $(".card").css("width", $(".column").width() - 20);
        $("#headerColumns").css("width", $("body").width() - 60);

    }

    if ($(document).width() > 899) {
        $(".header-column.purple").css("display", "block");
        $("#my-ideas").css("display", "block");
        $("#home .column").css("width", "25%");
        $("#headerColumns").css("width", $("body").width() - 60);
        $(".header-column").css("width", "25%");
        $(".card").css("width", $(".column").width() - 20);
    }

    checkTour();


}

//shows which ides's category an idea is
function ideaCategory(object, category, objectid) {
    switch (object.get("category")) {
        case 0:
            $(category + " #" + objectid + " #category div.ideaCategory").removeClass("noactive");
            break;
        case 1:
            $(category + " #" + objectid + " #category div.needsCategory").removeClass("noactive");
            break;
        case 2:
            $(category + " #" + objectid + " #category div.appCategory").removeClass("noactive");
            break;
        case 3:
            $(category + " #" + objectid + " #category div.pocCategory").removeClass("noactive");
            break;
        case 4:
            $(category + " #" + objectid + " #category div.microCategory").removeClass("noactive");
            break;
        case 5:
            $(category + " #" + objectid + " #category div.otherCategory").removeClass("noactive");
            break;
        default:
    }
}


$("").click(function() {
    alert("Handler for .click() called.");
});




//open filter recently added 


$('body').on('click', '#filtermenu', function() {

    Parse.Cloud.run('getfilterArray', {

        currentID: Parse.User.current().id
    }, {

        success: function(results) {

            if (results.indexOf(11) > -1) {
                $("#filterI-11").addClass("active");
            }
            if (results.indexOf(22) > -1) {
                $("#filterC-22").addClass("active");
            }
            if (results.indexOf(0) > -1) {
                $("#idea-0").addClass("active");
            }
            if (results.indexOf(1) > -1) {
                $("#need-1").addClass("active");
            }
            if (results.indexOf(2) > -1) {
                $("#app-2").addClass("active");
            }
            if (results.indexOf(3) > -1) {
                $("#poc-3").addClass("active");
            }
            if (results.indexOf(4) > -1) {
                $("#micro-4").addClass("active");
            }
            if (results.indexOf(5) > -1) {
                $("#other-5").addClass("active");
            }
            if (results.indexOf(100) > -1) {
                $("#filterL-100").addClass("active");
            }
        },
        error: function(error) {
            console.log("error getting filters");
        }
    });

    if (openDropdown == false) {
        $("#filter-dropdown").velocity("transition.slideDownIn");
        $("#filter-dropdown").hasClass("open")
        openDropdown = true;
        $("#filtermenu").addClass("open");
        $("#filtermenu i").addClass("open");
    }
    else {
        $("#filter-dropdown").velocity("transition.slideUpBigOut");
        $("#filtermenu").removeClass("open");
        $("#filtermenu i").removeClass("open");
        openDropdown = false;

    }
});






//submit button to save filters 
$('.btn-submitfilter').click(function() {

    //make array with filters
    categoriesSelected = [];

    $('#filter-dropdown .btn-filter.active').each(function() {
        var idFilter = this.id;
        var split = idFilter.split('-', 2)
        var filter = split[1];
        categoriesSelected.push(parseInt(filter));
    });
        console.log(categoriesSelected);

    //delete filter and set filters
    Parse.Cloud.run("deleteFilterArray", {}, {
        success: function(popularideas) {
            Parse.Cloud.run("setFilterArray", {
                filterarray: categoriesSelected
            }, {
                success: function(popularideas) {},
                error: function(object, error) {
                    // The object was not retrieved successfully.
                    // error is a Parse.Error with an error code and message.
                }
            });
        },
        error: function(object, error) {
            // The object was not retrieved successfully.
            // error is a Parse.Error with an error code and message.
        }
    });

    //if there is something selected on filters remove cards and load new cards
    if (categoriesSelected.length > 0) {

        $('#most-recent .card').remove();
        $('#most-recent #column-is-empty-info').remove();
        filterUser = categoriesSelected;
        skipRecentlyFilter = 0;
        skipChallengeFilter = 0;
        loadRecentlyAdded(categoriesSelected);
    }
    else {

        $('#most-recent .card').remove();
        $('#most-recent #column-is-empty-info').remove();
        skipRecently = 0;
        filterUser = new Array();
        loadRecentlyAddedNoFilter();
    }

    //animation
    $("#filter-dropdown").velocity("transition.slideUpOut");
    $("#filtermenu").removeClass("open");
    $("#filtermenu i").removeClass("open");
    openDropdown = false;
    lastRecentlyAdded = false;
    lastRecentlyAddedNoFilter = false;

});


//clear button
$('.btn-clear').click(function() {
    $(".btn-filter").removeClass("active");
});

//click in filter category idea or challenge
$(".postOrChallenge a").click(function() {
    if (this.id == "filterI-11") {
        $("#category-btn a").removeClass("active");
    }
    if ($(this).hasClass("active") == false) {
        $(this).addClass("active");
    }
    else {
        $(this).removeClass("active");
    }
});

//click in filter category microservice, needs, ideas ,etc


    $('body').on('click', ".chooseCategory a", function() {


    if ($("#filterI-11").hasClass("active") == true) {
        $("#filterI-11").removeClass("active");
    }
    if ($(this).hasClass("active") == false) {
        $(this).addClass("active");
    }
    else {
        $(this).removeClass("active");
    }
});

$('body').on('click', ".chooseLocation a", function() {

    if ($(this).hasClass("active") == false) {
        $(this).addClass("active");
    }
    else {
        $(this).removeClass("active");
    }
});

//value is in array
function isInArray(value, array) {
    return array.indexOf(value) > -1;
}

//function div inside div
function ifDivInsideDiv(parent, child) {
    if ($(child).parents(parent).length == 1) {
        return true;
    }
    else {
        return false;
    }
}

//create an array with the filter and call load cards
function saveArrayCategory() {
    categoriesSelected = [];
    Parse.Cloud.run('getfilter', {}, {
        success: function(results) {
            if (results[0] === undefined) {
                Parse.Cloud.run('setFilter', {}, {
                    success: function(results) {
                        if (results.get("idea") === true) {
                            categoriesSelected.push(parseInt("11"));
                        }
                        if (results.get("challenge") === true) {
                            categoriesSelected.push(parseInt("22"));
                        }
                        if (results.get("ideaCat") === true) {
                            categoriesSelected.push(parseInt("0"));
                        }
                        if (results.get("needCat") === true) {
                            categoriesSelected.push(parseInt("1"));
                        }
                        if (results.get("appCat") === true) {
                            categoriesSelected.push(parseInt("2"));
                        }
                        if (results.get("pocCat") === true) {
                            categoriesSelected.push(parseInt("3"));
                        }
                        if (results.get("microCat") === true) {
                            categoriesSelected.push(parseInt("4"));
                        }
                        if (results.get("otherCat") === true) {
                            categoriesSelected.push(parseInt("5"));
                        }
                        loadCards();
                    },
                    error: function(error) {
                        console.log("error setting categories on array");
                    }
                });
            }
            else {
                if (results[0].get("idea") === true) {
                    categoriesSelected.push(parseInt("11"));
                }
                if (results[0].get("challenge") === true) {
                    categoriesSelected.push(parseInt("22"));
                }
                if (results[0].get("ideaCat") === true) {
                    categoriesSelected.push(parseInt("0"));
                }
                if (results[0].get("needCat") === true) {
                    categoriesSelected.push(parseInt("1"));
                }
                if (results[0].get("appCat") === true) {
                    categoriesSelected.push(parseInt("2"));
                }
                if (results[0].get("pocCat") === true) {
                    categoriesSelected.push(parseInt("3"));
                }
                if (results[0].get("microCat") === true) {
                    categoriesSelected.push(parseInt("4"));
                }
                if (results[0].get("otherCat") === true) {
                    categoriesSelected.push(parseInt("5"));
                }
                loadCards();
            }
        },
        error: function(error) {
            console.log("error saving categories on array");
        }
    });
}

function loadCards() {
    Parse.Cloud.run("ideasFull", {
        cantidad: 200,
        skip: 0
    }, {
        success: function(ideafull) {
            var File = Parse.Object.extend("Archivo");
            var query = new Parse.Query(File);
            for (var i = 0; i < ideafull.length; i++) {
                object = ideafull[i];
                if (isInArray(object.idea.get("category"), categoriesSelected) === true) {
                    createRecentlyAddedCards(object.idea, object.comments, object.likes, object.files);
                }
                if (categoriesSelected.length == 0) {
                    createRecentlyAddedCards(object.idea, object.comments, object.likes, object.files);
                }
            }
            if (ifDivInsideDiv("#most-recent", "div.card") == false) {
                $("#most-recent").append(
                    "<div id='column-is-empty-info'><i class='material-icons'>mood_bad</i><h2>No ideas yet</h2> <h5 class='message-noideas'>Tell us about your ideas or inventions to improve our company</h5><a class='dropdown-toggle new_idea_a btn first_new_idea' data-toggle='modal' data-target='#myModal' href='#''> Add New Idea</a> </div>"
                );
            }
        },
        error: function(object, error) {
            // The object was not retrieved successfully.
            // error is a Parse.Error with an error code and message.
        }
    });
}






$('body').on('click', 'a.author', function() {
    var word = this.id;
    if(word===Parse.User.current().id){
        window.location = "perfilDashboard.html";
    }
    else
    {
        window.location = "user.html?i=" + word;
    }
});

function userClick(userid) {
    if(userid===Parse.User.current().id){
        window.location = "perfilDashboard.html";
    }
    else
    {
        window.location = "user.html?i=" + userid;
    }

}


//load recently added first time
function loadRecentlyAddedFirst() {

    Parse.Cloud.run('findFilter', {
        userID: Parse.User.current().id
    }, {
        success: function(results) {

            filterUser = results.get("filter");



            if (results.get("filter").length < 1) {
                loadRecentlyAddedNoFilter();
            }
            else {
                loadRecentlyAdded(filterUser);
            }
            if (!recentlyaddedLoaded) {
                recentlyaddedLoaded = true;
                loadPopular();
            }
        },
        error: function(error) {
            console.log(error);
        }
    });
}

//load recently added with filter
function loadRecentlyAdded(filter) {
    Parse.Cloud.run("getRecentlyFilter", {

        userFilter: filter,
        cantidad: 4,
        skip: skipRecentlyFilter,
        cantCh: 4,
        skipCh: skipChallengeFilter
    }, {
        success: function(recentlyaddedideas) {
            console.log("recentlyadded");
            console.log(recentlyaddedideas);


            if (recentlyaddedideas.length > 0) {
                for (var i = 0; i < recentlyaddedideas.length; i++) {
                    object = recentlyaddedideas[i]
                    createRecentlyAddedCards(object.idea, object.comments, object.likes, object.files);
                }
            }
            if (skipRecentlyFilter == 0 && recentlyaddedideas.length < 1) {
                columnIsEmpty("#recently-added-cards");
            }
            if (skipRecentlyFilter > 0 && recentlyaddedideas.length < 1) {
                lastRecentlyAdded = true;
            }
        },
        error: function(object, error) {
            // The object was not retrieved successfully.
            // error is a Parse.Error with an error code and message.
        }
    });
}

function loadRecentlyAddedNoFilter() {
    Parse.Cloud.run("ideasFullCh", {
        cantidad: 4,
        skip: skipRecently,
        cantCh: 4,
        skipCh: skipChallenge
    }, {
        success: function(ideafull) {



            var File = Parse.Object.extend("Archivo");
            var query = new Parse.Query(File);
            for (var i = 0; i < ideafull.length; i++) {
                object = ideafull[i];
                createRecentlyAddedCards(object.idea, object.comments, object.likes, object.files);
            }
            if (skipRecently == 0 && ideafull.length < 1) {
                columnIsEmpty("#recently-added-cards");
            }
            if (skipRecently > 0 && ideafull.length < 1) {
                lastRecentlyAddedNoFilter = true;
            }
        },
        error: function(object, error) {
            // The object was not retrieved successfully.
            // error is a Parse.Error with an error code and message.
        }
    });
}

//create popular column
function loadPopular() {
    Parse.Cloud.run("popularideas", {
        cantidad: 4,
        skip: skipPopular
    }, {
        success: function(popularideas) {
            for (var i = 0; i < popularideas.length; i++) {
                object = popularideas[i];
                createPopularCards(object.idea, object.comments, object.likes, object.files);
            }
            if (!popularLoaded) {
                popularLoaded = true;
                loadSuggested();
            }

        },
        error: function(object, error) {
            // The object was not retrieved successfully.
            // error is a Parse.Error with an error code and message.
        }
    });
}


function loadSuggested() {

    if (arraySuggested.length <= 0) {

        Parse.Cloud.run("getUser", { //get user
            userID: Parse.User.current().id //user id
        }, {
            success: function(currentUser) { //objeto usuario actual

                Parse.Cloud.run("getSuggestedIdeasTotal", { //agarra todoas las suggested ideas
                    cantidad: 100,
                    skip: skipSuggested

                }, {
                    success: function(suggested) { //4 ideas suggested

                        arraySuggested = suggested;



                        if (arraySuggested.length <= 0) {

                            createRandomTags();


                        }
                        else {
                            var iteracion = (suggested.length >= 4) ? 4 : suggested.length;

                            for (var i = 0; i < iteracion; i++) { //hacer tarjeta

                                object = suggested[i]
                                createCardSuggested(object.idea, object.comments, object.likes, object.files);


                            }
                        }

                        if (!suggestedLoaded) {
                            suggestedLoaded = true;
                            loadUserIdeas();
                        }





                    },
                    error: function(object, error) {
                        // The object was not retrieved successfully.
                        // error is a Parse.Error with an error code and message.
                    }
                });

            },
            error: function(object, error) {

            }
        });
    }
    else {

        var limitarray = 4;

        if (suggestedScroll + 4 > arraySuggested.length) {
            limitarray = arraySuggested.length - suggestedScroll;
        }

        if (suggestedScroll < arraySuggested.length) {


            for (var i = suggestedScroll; i < suggestedScroll + limitarray; i++) { //hacer tarjeta

                object = arraySuggested[i]
                createCardSuggested(object.idea, object.comments, object.likes, object.files);



            }
            suggestedScroll += 4;

        }
        else {

            if (cardrandom) {

                cardrandom = false;
                createRandomTags();
            }
        }
    }

}



//create column user ideas
function loadUserIdeas() {
    Parse.Cloud.run("getUserIdeas", {
        cantidad: 4,
        skip: skipUserIdeas,
        currentID: Parse.User.current().id

    }, {
        success: function(userIdeas) {

            if (userIdeas.length > 0) {
                for (var i = 0; i < userIdeas.length; i++) {
                    object = userIdeas[i]
                        // userIdeas++;
                    createUserIdeasCards(object.idea, object.comments, object.likes, object.files);
                }
            }

            if (skipUserIdeas == 0 && userIdeas.length < 1) {
                $("#my-ideas").append(
                    "<div id='column-is-empty-info'><i class='material-icons'>mood_bad</i><h2>No ideas yet</h2> <h5 class='message-noideas'>Tell us about your ideas or inventions to improve our company</h5><a class='dropdown-toggle new_idea_a btn first_new_idea' data-toggle='modal' data-target='#myModal' href='#''> Add New Idea</a> </div>"
                );
            }
            if (!userideasLoaded) {
                userideasLoaded = true;
                animateHome();
            }

        },
        error: function(object, error) {
            // The object was not retrieved successfully.
            // error is a Parse.Error with an error code and message.
        }
    });
}

//load more ideas when scroll
$(window).scroll(function() {

var heights = $(".columnHome").map(function ()
    {
        return $(this).height();
    }).get(),

    maxHeight = Math.max.apply(null, heights);

    $(".column").css("height", maxHeight+50);

    if (checkVisible($('.most-recent div.card:nth-last-child(2)')) && filterUser.length < 1 && lastRecentlyAddedNoFilter === false) {
        skipRecently++;
        skipChallenge++;
        loadRecentlyAddedNoFilter();
    }

    if (checkVisible($('.most-recent div.card:nth-last-child(2)')) && filterUser.length > 0 && lastRecentlyAdded === false) {
        skipRecentlyFilter++;
        skipChallengeFilter++;
        loadRecentlyAdded(filterUser);
    }

    if (checkVisible($('#most-popular div.card:last-child'))) {
        skipPopular++;
        loadPopular();
    }

    if (checkVisible($('#suggested-ideas-cards div.card:last-child')) === true) {
        // skipSuggested++;
        loadSuggested();

    }

    if (checkVisible($('#my-ideas div.card:last-child'))) {
        skipUserIdeas++;
        loadUserIdeas();
    }

    $(".card").css("width", $(".column").width() - 20);
});

//function to check if element is visible
function checkVisible(elm, eval) {
    eval = eval || "visible";
    var vpH = $(window).height(), // Viewport Height
        st = $(window).scrollTop(), // Scroll Top
        y = ($(elm).offset() !== undefined) ? $(elm).offset().top : 0,
        elementHeight = $(elm).height();
    if (eval == "visible") return ((y < (vpH + st)) && (y > (st - elementHeight)));
    if (eval == "above") return ((y < (vpH + st)));
}






$('body').on('click', '#followBtn.submit', function() {
    tagSelected = [];
    var finish = false;
    follow = false;

    $('.first-follow.active').each(function() {
        var tag = $(this).text();
        var res = tag.toLowerCase()
        tagSelected.push(res);
        finish = true;
    });

    if (finish === true)

    {

        Parse.Cloud.run('setFollowing', {
                tag: tagSelected
            },

            {
                success: function(result) {
                    $("#suggested-ideas-cards").empty();
                    follow = false;
                    skipSuggested = 0;
                    cardrandom = true;
                    arraySuggested = new Array();
                    suggestedScroll = 4;
                    loadSuggested();
                },

                error: function(error) {
                    console.log(error);
                }

            });
    }
});

$('body').on('click', '.first-follow', function() {
    if ($(this).hasClass("active")) {
        $(this).removeClass("active");
    }
    else {
        $(this).addClass("active");
    }

});

function createRandomTags() {
    Parse.Cloud.run("getRandomTags", {
            cantidad: 5,
            skip: skipRandomTags,
            userTags: (currentUser.has("tagFollowing")) ? currentUser.get("tagFollowing") : new Array()
        },

        {
            success: function(randomtags) {



                if (randomtags.length > 1) {

                    $("#suggested-ideas-cards").append('<div class="card suggested-card" id="zJ2SR1udn7" href="#" >' +
                        '<h4 class="title">Tell us your interests to show your more post!</h4> </div>');
                    addTagButtons(randomtags);
                    $(".suggested-card").append('<hr><a class="submit" href="#" id="followBtn" >Submit</a>');

                    $(".suggested-card").velocity({
                        translateY: [0, 200],
                    }, 1000);
                }

                function addTagButtons(randomtags) {
                    for (var i = 0; i < randomtags.length; i++) {
                        $(".suggested-card").append('<a class="first-follow" id="followBtn">' + randomtags[i] + '</a>');
                    }
                }



            }



            ,
            error: function(object, error) {
                // The object was not retrieved successfully.
                // error is a Parse.Error with an error code and message.
            }
        });
}



$('body').on('click', '.header-column.red', function() {

    if ($(document).width() < 700) {

        $(".header-column.red").css("background-color", "#e74c3c");
        $(".header-column.blue").css("background-color", "#cacaca");
        $(".header-column.green").css("background-color", "#cacaca");

        $(".header-column").css("border-right", "1px solid #dddddd");
        $(".header-column.purple").css("display", "none");
        $("#my-ideas").css("display", "none");

        $("#headerColumns").css("width", "100%");
        $("#home .column").css("width", "100%");
        $(".header-column").css("cursor", "pointer");
        $(".header-column").css("width", "33.333%");

        $("#most-popular").css("display", "block");
        $("#most-active").css("display", "none");
        $("#most-recent").css("display", "none");

        $("#sidebar").css("display", "none");

        $("#most-popular-cards").css("width", "100%");
        $("#most-popular").css("width", "100%");
        $("#columns").css("padding-right", "0");
        $(".card").css("width", $(".column").width() - 20);


    }
    activeTab = 2;

});

$('body').on('click', '.header-column.blue', function() {

    if ($(document).width() < 700) {

        $(".header-column.red").css("background-color", "#cacaca");
        $(".header-column.blue").css("background-color", "#3498db");
        $(".header-column.green").css("background-color", "#cacaca");

        $(".header-column").css("border-right", "1px solid #dddddd");
        $(".header-column.purple").css("display", "none");
        $("#my-ideas").css("display", "none");

        $("#headerColumns").css("width", "100%");
        $("#home .column").css("width", "100%");
        $(".header-column").css("cursor", "pointer");
        $(".header-column").css("width", "33.333%");

        $("#most-popular").css("display", "none");
        $("#most-active").css("display", "block");
        $("#most-recent").css("display", "none");

        $("#sidebar").css("display", "none");

        $("#suggested-ideas-cards").css("width", "100%");
        $("#most-active").css("width", "100%");
        $("#columns").css("padding-right", "0");
        $(".card").css("width", $(".column").width() - 20);


    }

    activeTab = 3;

});

$('body').on('click', '.header-column.green', function() {

    if ($(document).width() < 700) {

        $(".header-column.red").css("background-color", "#cacaca");
        $(".header-column.blue").css("background-color", "#cacaca");
        $(".header-column.green").css("background-color", "#2ecc71");

        $(".header-column").css("border-right", "1px solid #dddddd");
        $(".header-column.purple").css("display", "none");
        $("#my-ideas").css("display", "none");

        $("#headerColumns").css("width", "100%");
        $("#home .column").css("width", "100%");
        $(".header-column").css("cursor", "pointer");
        $(".header-column").css("width", "33.333%");

        $("#most-popular").css("display", "none");
        $("#most-active").css("display", "none");
        $("#most-recent").css("display", "block");

        $("#sidebar").css("display", "none");

        $("#recently-added-cards").css("width", "100%");
        $("#most-recent").css("width", "100%");
        $("#columns").css("padding-right", "0");
        $(".card").css("width", $(".column").width() - 20);


    }

    activeTab = 1;

});


$('body').on('click', '.tag', function() {
    var word = $(this).text();
    word = word.replace(' ', '-');
    window.location = "tags.html?t=" + word;
});


