var idea = {name:"", descr: "", tags:[], isPrivate: false};
var key = GetUrlValue("k");
var ideaObj;
			
$(document).ready(function(){
	Tags.bootstrapVersion = "3";
    $("#small").tags({
      suggestions: [],
      tagData: []
    });

	if(key != null)
	{
		var Idea = Parse.Object.extend("Idea");
		var query = new Parse.Query(Idea);
		query.include("user");
		Parse.Cloud.run("unaIdeaFull",{userID : Parse.User.current().id, oID: key}, {
		  success: function(ideaResult) {
		    // The object was retrieved successfully.
		    ideaObj = ideaResult[0].idea;
		    idea.name = ideaObj.get("titulo");
		    idea.descr = ideaObj.get("descripcion");
		    idea.isPrivate = ideaObj.get("esPrivado");
		    idea.tags = ideaObj.get("tags");
		    $("#inputNameSI").html(idea.name);
		    $("#inputDescrSI").html(idea.descr);
		    $("#inputUserSI").html("Por "+ideaObj.get("user").get("username"));
		    for(var i = 0; i < idea.tags.length; i++)
		    	$("#inputTagsSI").append('<li>'+idea.tags[i]+'</li>');
	        var File = Parse.Object.extend("Archivo");
			var query = new Parse.Query(File);
			query.equalTo("idea", ideaObj);
			query.find({
			  success: function(results) {
			    // Do something with the returned Parse.Object values
			    for (var i = 0; i < results.length; i++) { 
			      var object = results[i];
			      loadFiles(object);
			    }
			  },
			  error: function(error) {
			    alert("Error: " + error.code + " " + error.message);
			  }
			});
			isLike();
			isDislike();
			loadComments();
		  },
		  error: function(object, error) {
		    // The object was not retrieved successfully.
		    // error is a Parse.Error with an error code and message.
		    alert("error");
		  }
		});
	}
	

	function loadFiles(object)
	{
		var ext = object.get("nombre").split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) !== -1) {
		    var profilePhoto = object.get("file");
		    console.log(profilePhoto);
			$("#inputImageSI")[0].src = profilePhoto.url();
		}
		$("#fileContainerSI").append("<li class='list-group-item'>"+
			"<a href='"+object.get("file").url()+"'>"+
			object.get("nombre")+"</a></li>");
	}

	function loadComments()
	{
		
		var Comment = Parse.Object.extend("Comentario");
		var query = new Parse.Query(Comment);
		query.equalTo("idea", ideaObj);
		query.include("user");
		query.descending("createdAt");
		query.find({
		  success: function(results) {
		    // Do something with the returned Parse.Object values
		    for (var i = 0; i < results.length; i++) { 
		      var object = results[i];
		      $("#inputCommentsSI").append('<li class="list-group-item">'+
				'<span class="badge">'+object.get("user").get("username")+'</span>'+
				'<p>'+object.get("texto")+'</p>'+
				'</li>');
		    }
		  },
		  error: function(error) {
		    alert("Error: " + error.code + " " + error.message);
		  }
		});
	}

	$("#uploadComment").click(function(){
		var comment = $("#newComment").val();
		var user = Parse.User.current();
		Parse.Cloud.run("aplicarComentario",{userID: user.id, comentario: comment, ideaID: ideaObj.id},{
        		success: function(response){
        			window.location="showIdea.html?k="+ideaObj.id;
        		},
        		error: function(error){
        			alert("Error"+ error.message);
        		}
        	});
	});
	$("#isLike").click(function(){
		var Like = Parse.Object.extend("Like");
		var query = new Parse.Query(Like);
		query.include("idea");
		query.include("user");
		query.equalTo("idea", ideaObj);
		query.equalTo("user", Parse.User.current());
		query.find({
		  success: function(results) {
		    if(results.length == 0)
		    {
		    	var like = new Like();
				like.set("user", Parse.User.current());
				like.set("esPositivo", true);
				like.set("idea", ideaObj);
				like.save(null, {
				  success: function(gameScore) {
				    // Execute any logic that should take place after the object is saved.
				    $("#isLike").html("You Liked this");
				  },
				  error: function(gameScore, error) {
				    // Execute any logic that should take place if the save fails.
				    // error is a Parse.Error with an error code and message.
				    alert('Failed to create new object, with error code: ' + error.message);
				  }
				});
		    }
		    else
		    {
		    	if(!results[0].get("esPositivo"))
		    	{
		    		var like = results[0];
		    		like.set("esPositivo", true);
		    		like.save(null, {
					success: function(gameScore) {
						// Execute any logic that should take place after the object is saved.
						$("#isLike").html("You Liked this");
						$("#isDislike").html("Dislike");
					},
					error: function(gameScore, error) {
						// Execute any logic that should take place if the save fails.
						// error is a Parse.Error with an error code and message.
						alert('Failed to create new object, with error code: ' + error.message);
					}
					});
		    	}
		    	else
		    	{
		    		var like = results[0];
		    		like.destroy({
					  success: function(myObject) {
					    // The object was deleted from the Parse Cloud.
					    $("#isLike").html("Like");
					  },
					  error: function(myObject, error) {
					    // The delete failed.
					    // error is a Parse.Error with an error code and message.
					  }
					});
		    	}
		    }
		  },
		  error: function(error) {
		    alert("Error: " + error.code + " " + error.message);
		  }
		});
		
	});
	$("#isDislike").click(function(){
		var Like = Parse.Object.extend("Like");
		var query = new Parse.Query(Like);
		query.include("idea");
		query.include("user");
		query.equalTo("idea", ideaObj);
		query.equalTo("user", Parse.User.current());
		query.find({
		  success: function(results) {
		    if(results.length == 0)
		    {
		    	var like = new Like();
				like.set("user", Parse.User.current());
				like.set("esPositivo", false);
				like.set("idea", ideaObj);
				like.save(null, {
				  success: function(gameScore) {
				    // Execute any logic that should take place after the object is saved.
				    $("#isDislike").html("You Disliked this");
				  },
				  error: function(gameScore, error) {
				    // Execute any logic that should take place if the save fails.
				    // error is a Parse.Error with an error code and message.
				    alert('Failed to create new object, with error code: ' + error.message);
				  }
				});
		    }
		    else
		    {
		    	if(results[0].get("esPositivo"))
		    	{
		    		var like = results[0];
		    		like.set("esPositivo", false);
		    		like.save(null, {
					success: function(gameScore) {
						// Execute any logic that should take place after the object is saved.
						$("#isLike").html("Like");
						$("#isDislike").html("You Disliked this");
					},
					error: function(gameScore, error) {
						// Execute any logic that should take place if the save fails.
						// error is a Parse.Error with an error code and message.
						alert('Failed to create new object, with error code: ' + error.message);
					}
					});
		    	}
		    	else
		    	{
		    		var like = results[0];
		    		like.destroy({
					  success: function(myObject) {
					    // The object was deleted from the Parse Cloud.
					    $("#isDislike").html("Dislike");
					  },
					  error: function(myObject, error) {
					    // The delete failed.
					    // error is a Parse.Error with an error code and message.
					  }
					});
		    	}
		    }
		  },
		  error: function(error) {
		    alert("Error: " + error.code + " " + error.message);
		  }
		});
		
	});//downvote

	function isLike()
	{
		var Like = Parse.Object.extend("Like");
		var query = new Parse.Query(Like);
		var text = "";
		query.include("idea");
		query.include("user");
		query.equalTo("idea", ideaObj);
		query.equalTo("user", Parse.User.current());
		query.find({
		  success: function(results) {
		    if(results.length > 0)
		    {
		    	if(results[0].get("esPositivo"))
		    		text = "You Liked this";
		    	else
		    		text = "Like";
		    }
		    else
		    {
		    	text = "Like";
		    }
		    $("#isLike").html(text);
		  },
		  error: function(error) {
		    alert("Error: " + error.code + " " + error.message);
		  }
		});
	}//islike

	function isDislike()
	{
		var Like = Parse.Object.extend("Like");
		var query = new Parse.Query(Like);
		query.include("idea");
		query.include("user");
		query.equalTo("idea", ideaObj);
		query.equalTo("user", Parse.User.current());
		query.find({
		  success: function(results) {
		    if(results.length > 0)
		    {
		    	if(!results[0].get("esPositivo"))
		    		text = "You Disliked this";
		    	else
		    		text = "Dislike";
		    }
		    else
		    {
		    	text = "Dislike";
		    }
		    $("#isDislike").html(text);
		  },
		  error: function(error) {
		    alert("Error: " + error.code + " " + error.message);
		  }
		});
	}//isdislike

});