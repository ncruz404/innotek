var searchbar = false;
var quotes = Parse.Object.extend("Quotes");

var query = new Parse.Query(quotes);
query.descending("index");
query.find({
  success: function(quotes) {
    var numberOfQuotes = quotes.length;
    var randomQuoteNumber = Math.floor(getRandomArbitrary(1, numberOfQuotes));
    var quote = Parse.Object.extend("Quotes");
    var query = new Parse.Query(quote);
    query.equalTo("index", randomQuoteNumber);
    query.find({
      success: function(results) {
        var object = results[0];
        $(".se-pre-con").append('<div id="quoteLoading" class="container"><h2>"' + object.get("quotes") + '"</h2><h3>— ' + object.get("author") +
          '</h3></div>');
      },
      error: function(error) {
      }
    });
  },
  error: function(object, error) {}
});

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

var cUser = Parse.User.current();

var imageUser = (cUser.get("imageYammer") == null) ? cUser.get("image").url() : cUser.get("imageYammer");

var nav = '<nav class="navbar navbar-default navbar-fixed-top">' +
  '<div class="container-fluid">' +
  '<a  href="home.html" class="navbar-header">' +
  '</a>' +
  '<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">' +
  '<span class="sr-only">Toggle navigation</span>' +
  '<span class="icon-bar"></span>' +
  '<span class="icon-bar"></span>' +
  '<span class="icon-bar"></span>' +
  '</button>' +
  '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">' +
  '<ul class="nav navbar-nav navbar-right">' +
  '<li class="dropdown search-bar">' +
  '</li> ' +
  '<li><a id="btnSearch" href="#" class="hvr-overline-reveal search-btn" title="Search"><i class="fa fa-search"></i></a></li>' +
  '<li id="comitteSecret"></li>' +
  '<li id="directiveSecret"></li>' +
  '<li id="directiveDash"><a href="perfilDashboard.html" class="hvr-overline-reveal">My Dashboard</a></li>' +
  '<li id="createCHSecret"></li>'+
  '<li><a id="addButton" class="dropdown-toggle new_idea_a add-idea-icon" data-toggle="modal" data-target="#myModal" href="#" title="Create Idea"></a></li>' +
  '<li id="notis" class="dropdown">' +
  '<div onclick="addHideClass();" id="noti_Container" class="dropdown-toggle user-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' +
  '<a id="notification" href="#" title="Notifications"></a>' +
  '<div class="classHide noti_bubble"></div>' +
  '</div>' +
  '<ul id="notisUL" class="dropdown-menu">' +
  '</ul>' +
  '</li>' +
  '<li class="dropdown">' +
  '<a href="#" class="dropdown-toggle user-menu" title="My Profile" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="user-profile-img"' +
  'style="background-image: url(' + (Parse.User.current().has("imageYammer") ? Parse.User.current().get("imageYammer") : Parse.User.current().get("image").url()) + ')"></span><span class="caret"></span></a>' +
  '<ul class="dropdown-menu">' +
  '<li><a href="profile.html">Settings</a></li>' +
  '<li><a href="feedback_button_tutorial.html">Productivity Button API</a></li>' +
  '<li><a class="dropdown-toggle" id="helpBtn" href="#">Help</a></li>' +
  '<li><a onclick="walkThrough()" href="#">Tour</a></li>' +
  '<li><a onclick="visitStore()" href="#">Store</a></li>' +
  '<li role="separator" class="divider"></li>' +
  '<li><a onclick="logOut()" href="#">Logout</a></li>' +
  '</ul>' +
  '</li>' +
  '</ul>' +
  '</div>' +
  '</div>' +
  '</nav>';

var nav2 = '<nav class="navbar navbar-default navbar-fixed-top">' +
  '<div class="container-fluid">' +
  '<a  href="home.html" class="navbar-header">' +
  '</a>' +
  '<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">' +
  '<span class="sr-only">Toggle navigation</span>' +
  '<span class="icon-bar"></span>' +
  '<span class="icon-bar"></span>' +
  '<span class="icon-bar"></span>' +
  '</button>' +
  '<ul class="notificationButton">' +
  '<li id="notis" class="dropdown">' +
  '<div onclick="addHideClass();" id="noti_Container" class="dropdown-toggle user-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' +
  '<a id="notification" href="#" title="Notifications"></a>' +
  '<div class="classHide noti_bubble"></div>' +
  '</div>' +
  '<ul id="notisUL" class="dropdown-menu">' +
  '</ul>' +
  '</li>' +
  '</ul>' +
  '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">' +
  '<ul class="nav navbar-nav navbar-right">' +
  '<li class="dropdown search-bar">' +
  '<a> <input type="text" placeholder="Search Idea" id="productName" class="product-typeahead form-control input-style"></a>' +
  '</li> ' +
  '<li id="directiveDash"><a href="perfilDashboard.html" >My Dashboard</a></li>' +
  '<li><a id="addButton" class="dropdown-toggle new_idea_a" data-toggle="modal" data-target="#myModal" href="#" title="Create Idea">New Post</a></li>' +
  '<li><a href="profile.html">Settings</a></li>' +
  '<li><a href="feedback_button_tutorial.html">Productivity Button API</a></li>' +
  '<li><a class="dropdown-toggle" id="helpBtn" href="#">Help</a></li>' +
  '<li><a onclick="walkThrough()" href="#">Tour</a></li>' +
  '<li><a onclick="visitStore()" href="#">Store</a></li>' +
  '<li><a onclick="logOut()" href="#">Logout</a></li>' +
  '</div>' +
  '</div>' +
  '</nav>';

$("body").append('<!-- Modal -->' + '<div id="allModals">' + '<div class="modal fade" id="myModal" role="dialog">' + '<div class="modal-dialog">' +
  '<!-- Modal content-->' + '<div id="form-new-idea" class="modal-content">' + '<div class="modal-header" id="headerNewIdea">' +
  '<button type="button" class="close" data-dismiss="modal">&times;</button>' + '<h4 class="modal-title">New Idea</h4>' + '</div>' +
  '<div class="modal-body">' + '<div class="panel">' + '<div class="panel-body">' + '<div class="text-right col-xs-12 col-sm-12 col-md-12 col-lg-12 ">' +
  '<div class="switch"><input id="cmn-toggle-1" class="inputPrivate cmn-toggle cmn-toggle-round" type="checkbox"><label for="cmn-toggle-1"></label></div>' +
  '<p>Make private idea</p> ' + '</div>' + '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group ">' + '<label for="">Title</label>' +
  '<span class=\'error-title\'>Write a title for the post</span><input type="text" class="form-control" id="inputName" placeholder="Idea Title">' + '</div>' + '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">' +
  '<span class=\'error-descr\'>Write a description for the post</span><label for="">Description</label>' + '<textarea name="descr" id="inputDescr" class="form-control" rows="3" required="required"></textarea>' + '</div>' +
  '<label for="">Category</label>' + '<div class="btn-group col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group categorygroup" role="group">' +
  '<button type="button" data-button=\'{"func": "func0"}\' class="col-xs-6 col-sm-6 col-md-2 col-lg-2 btn btn-default setCategory ideaBtn">Idea</button>' +
  '<button type="button" data-button=\'{"func": "func1"}\' class="col-xs-6 col-sm-6 col-md-2 col-lg-2 btn btn-default setCategory needBtn">Need</button>' +
  '<button type="button" data-button=\'{"func": "func2"}\' class="col-xs-6 col-sm-6 col-md-2 col-lg-2 noPadding btn btn-default setCategory appBtn">Application</button>' +
  '<button type="button" data-button=\'{"func": "func3"}\' class="col-xs-6 col-sm-6 col-md-2 col-lg-2 btn btn-default setCategory pocBtn">POC</button>' +
  '<button type="button" data-button=\'{"func": "func4"}\' class="col-xs-6 col-sm-6 col-md-2 col-lg-2 noPadding btn btn-default setCategory microBtn">Microservice</button>' +
  '<button type="button" data-button=\'{"func": "func5"}\' class="col-xs-6 col-sm-6 col-md-2 col-lg-2 btn btn-default setCategory otherBtn">Other</button>' + '<span class=\'error-category\'>Choose one category</span></div>' +
  '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">' + '<label for="">Tags</label>' + '<input type="text" class="form-control" id="small" value="" placeholder="Enter tags"/><span class=\'error-tag\'>At least one tag</span>' + '</div>' + '</div>' +
  '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">' + '<input type="file" id="photoUpload" multiple>' + '<span class=\'error-file\'>Maximum size: 25 MB</span><span class=\'error-file-ext\'>File extension not supported</span></div>' + '</div>' + '</div>' +
  '<div class="modal-footer">' + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
  '<a  id="finishBtn" class="text-right btn btn-success">Finish</a>' + '</div>' + '</div>' + '</div>' + '</div>');

$("#small #tagIdeas").addClass("fixEnter");

$("body").append(
  '<div class="modal fade" id="chModal">' +
  '<div class="modal-dialog">' +
  '<div class="modal-content">' +
  '<div class="modal-header">' +
  '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
  '<h4 class="modal-title">Create a Challenge</h4>' +
  '</div>' +
  '<div class="modal-body">' +
  '<p>Title of your challenge</p>' +
  '<input id="chTitle" class="form-control" required="required" type="text">' +
  '<p>What is your challenge about?</p>' +
  '<textarea id="chDescr" class="form-control" rows="3" required="required">' +
  '</textarea>' +
  '</div>' +
  '<div class="modal-footer">' +
  '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
  '<button type="button" id="saveChallenge" class="btn btn-primary">Save changes</button>' +
  '</div>' +
  '</div>' +
  '</div>' +
  '</div>'
);

$("body").append("<script>function addHideClass(){" +
  "if(typeof(Storage) !== 'undefined') {" +
  "if (localStorage.notCount) {" +
  "localStorage.notCount = 0;" +
  "}" +
  "}" +
  "notCount= 0; $('.noti_bubble').addClass('classHide');}</script>");

var notCount = 0;

if (typeof(Storage) !== "undefined") {
  if (!localStorage.notCount) {
    localStorage.notCount = 0;
  }
  else {
    if (Number(localStorage.notCount) > 0) {
      $(".noti_bubble").html(Number(localStorage.notCount));
      $(".noti_bubble").removeClass("classHide");
    }
  }
}

var pusher = new Pusher('7a5e5c7f6307240cf192', {
  encrypted: true
});

var channel = pusher.subscribe("produ");
channel.bind(Parse.User.current().id, function(data) {
  //alert(data.message);
  var num;
  if (typeof(Storage) !== "undefined") {
    if (localStorage.notCount) {
      localStorage.notCount = Number(localStorage.notCount) + 1;
      num = Number(localStorage.notCount);
    }
    else
      num = 1;
  }
  else {
    notCount++;
    num = notCount;
  }
  $(".noti_bubble").html(num);
  $(".noti_bubble").removeClass("classHide");
  if("#notisUL:has(p)")
    $("#notisUL").html("<li>" + data.message + "</li>");
  else
    $("#notisUL").prepend("<li>" + data.message + "</li>");
});

Parse.Cloud.run("isDirective", {
  email: Parse.User.current().get("email")
}, {
  success: function(isIt) {
    if (isIt) {
      $("#directiveSecret").html('<a href="perfilDashboard2.html" class="hvr-overline-reveal">Directive Dashboard</a>');
      $("#createCHSecret").html('<a data-toggle="modal" href="#chModal" class="hvr-overline-reveal">Create Challenge</a>');
    }
  },
  error: function(error) {
    console.log(error);
  }
});

Parse.Cloud.run("isComittee", {
  email: Parse.User.current().get("email")
}, {
  success: function(isIt) {
    if (isIt) {
      $("#comitteSecret").html('<a href="perfilDashboard3.html" class="hvr-overline-reveal">Comittee Dashboard</a>');
      $("#createCHSecret").html('<a data-toggle="modal" href="#chModal" class="hvr-overline-reveal">Create Challenge</a>');
    }
  },
  error: function(error) {
    console.log(error);
  }
});

Parse.Cloud.run('getNotifications', {
  userID: Parse.User.current().id
}, {
  success: function(notis) {

    if(notis.length > 0)
    {
      for (var i = 0; i < notis.length; i++) {
        var image = (notis[i].get("user_action").has("imageYammer")) ?
          notis[i].get("user_action").get("imageYammer") : notis[i].get("user_action").get("image").url();
        $("#notisUL").append("<li>" +
          '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">' +
          '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-2">' +
          '<span class="user-img" style="background-image: url(' + image + ')"></span>' +
          '</div>' +
          '<div class="col-xs-11 col-sm-11 col-md-11 col-lg-10">' +
          "<p><a href='viewIdea.html?k=" + notis[i].get("idRelacionado") + "'>" + notis[i].get("mensaje") + "</a></p>" +
          '<small class="pull-right text-muted">' +
          '<span class="glyphicon glyphicon-time"></span>' + moment(notis[i].createdAt.toString()).fromNow() +
          '</small>' +
          '</div>' +
          '</div>' +
          "</li>");
      };
    }
    else
    {
      $("#notisUL").append("<li>Empty</li>");
    }
  },
  error: function(error) {
  }
});

$("body").append('<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
  '<div class="modal-dialog">' +
  '<div class="modal-content">' +
  '<div class="modal-header">' +
  '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
  '<h4 class="modal-title" id="myModalLabel">How does Productivity work?</h4>' +
  '</div>' +
  '<div class="modal-body">' +
  '<img src="" id="imagepreview" style="width: 400px; height: 264px;" >' +
  '</div>' +
  '<div class="modal-footer">' +
  '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
  '</div>' +
  '</div>' +
  '</div>' +
  '</div>');



$('body').on('click', '#helpBtn', function() {
  $('#imagepreview').attr("src", '../css/img/logo-idea.png'); // here asign the image to the modal when the user click the enlarge link
  $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});

function visitStore() {
  window.location = "store.html";
}

function runSearch() {
  Parse.Cloud.run("getIdeasBuscador", {}, {
    success: function(response) {
      var states = new Bloodhound2({
        datumTokenizer: Bloodhound2.tokenizers.obj.whitespace('title', 'user'),
        queryTokenizer: Bloodhound2.tokenizers.whitespace,
        // `states` is an array of state names defined in "The Basics"
        local: response
      });

      $('#productName').typesearch(null, {
        name: 'best-pictures',
        display: 'title',
        source: states,
        templates: {
          empty: [
                                              '<div class="empty-message">',
                                                'unable to find any Ideas with that description',
                                              '</div>'
                                            ].join('\n'),
          suggestion: function(data) {
            return '<p><strong>' + data.title + '</strong> by ' + data.user + '</p>';
          }
        }
      }).on('typesearch:selected', function(event, datum) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
          dd = '0' + dd
        }

        if (mm < 10) {
          mm = '0' + mm
        }

        today = mm + '/' + dd + '/' + yyyy;

        Parse.Cloud.run("setRecentlySearch", {

          ideaId: datum.id,
          date: today


        }, {
          success: function(response) {

            window.location = "viewIdea.html?k=" + datum.id;
          },
          error: function(error) {
          }
        });


      });

      $("#productName").focus();

      $(".tt-hint").addClass("form-control").addClass("input-style");

      styleSearchBar();
    },
    error: function(error) {
    }
  });
}

if ($(document).width() > 750) {
  $("#navSuperior").html(nav);
}else {
  $("#navSuperior").html(nav2);
  //solamente comentas este metodo y no sale la pantalla negra en version movil
  //runSearch();
}

$('body').on('click', '.search-btn', function() {
    if (searchbar === false) {
      $(".search-btn .fa").removeClass("fa-search").addClass("fa-times");
      $(".search-bar").empty();
      $(".search-bar").append("<a class='dropdown-toggle' href='#' style='visibility: hidden;'> <input type='text' placeholder='Search Idea' id='productName' class='product-typeahead form-control input-style'></a>").velocity("transition.slideRightBigIn");
      runSearch();
      searchbar = true;
    }
    else {
      $(".search-btn .fa").removeClass("fa-times").addClass("fa-search");
      $(".search-bar").velocity("transition.slideRightBigOut");
      searchbar = false;
      console.log("INSIDE REMOVE");
      $("#searchBack").remove();
    }
});




$(window).resize(function()
{

  var width = $(window).width();

  if ($(window).width() > 750) {
    $("#navSuperior").html(nav);
  }else {
    $("#navSuperior").html(nav2);
    console.log("INSIDE NAVSUPERIOR");
    //solamente comentas este metodo y no sale la pantalla negra en version movil
    //runSearch();
  }
});

function styleSearchBar()
{
  $(".search-bar a").css({
    "visibility": "visible"
  });

  $("#navSuperior").append("<div id='searchBack'></div>");

  $("#searchBack").css({
    "width": "100%",
    "background": "black",
    "opacity": ".8",
    "height": "100vh",
    "position": "fixed",
    "top": "0",
    "z-index": "99"
  });

  $("#bs-example-navbar-collapse-1 ul .dropdown.search-bar").css({
    "position": "absolute",
    "left": "5%",
    "width": "90%",
    "top": "70px",
    "z-index": "1000"
  });

  $("#bs-example-navbar-collapse-1 ul .dropdown.search-bar span").css({
    "width": "100%"
  });

  $("#bs-example-navbar-collapse-1 ul .dropdown.search-bar span input").css({
    "height": "40px"
  });

  $("#bs-example-navbar-collapse-1 ul .dropdown.search-bar span .tt-menu").css({
    "width": "100%"
  });

}
