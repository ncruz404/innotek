var idea = {name:"", descr: "", tags:[], isPrivate: false,category: 0, fileCount: 0, challengeID: null};
var ideaCHSub = false;
var arr = [];
var engine;

function contributeIdea(challenge){
	console.log(challenge);
	idea.challenge = challenge;
	$("#headerNewIdea").prepend("<h4>Respond to Challenge</h4>")
	$('#myModal').modal('toggle');
}

var ideasUList = [];
function linkIdea(challenge){
	if(ideasUList.length == 0)
	Parse.Cloud.run("ideasUser",{userID: Parse.User.current().id},{
		success: function(ideasList){
			ideasUList = ideasList;
			var selectICH = '<div class="modal fade" id="modalSICH'+challenge.id+'">'+
				        '<div class="modal-dialog">'+
				            '<div class="modal-content">'+
				                '<div class="modal-header">'+
				                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
				                    '<h4 class="modal-title">Select one of your Ideas</h4>'+
				                '</div>'+
				                '<div class="modal-body">';
			if(ideasList.length > 0)
			{
				for (var i = 0; i < ideasList.length; i++) {
					var cidea = ideasList[i].idea;
					selectICH+= '<div class="row">'+
			                        '<div id="img-challenge">'+
			                            '<img class="img-thumbnail-challenge" src="'+cidea.get("photoUrl")+'">'+
			                        '</div>'+
			                        '<p id="p'+challenge.id+cidea.id+'">'+cidea.get("titulo").toString().substring(0,40)+'</p>'+
			                        ((cidea.has("challenge") && cidea.get("challenge").id == challenge.id)? "Linked":'<input type="checkbox" name="'+challenge.id+'SICHRadio[]"  value="'+cidea.id+'" />')+
			                    '</div>';
				};
				selectICH+=	    	'</div>'+
					                '<div class="modal-footer" id="SICHFooter">'+
					                    '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
					                '</div>'+
					            '</div>'+
					        '</div>'+
					    '</div>';
				$("body").append(selectICH);
				var row = $("#SICHFooter");
				var btnLink = document.createElement("button");        // Create a <button> element
				t = document.createTextNode("Select");       // Create a text node
				btnLink.appendChild(t);
				btnLink.setAttribute("class", "btn btn-primary");
				btnLink.setAttribute("type", "button");
				btnLink.setAttribute("id", "SICHBtn");
				btnLink.onclick = function() { linkIdeaToCH(challenge) }                                // Append the text to <button>
				row.append(btnLink);	
			}
			else
				selectICH+="<h3>You dont have any Ideas to Link</h3>";
			
			$("#modalSICH"+challenge.id).modal('toggle');
		},
		error: function(error){
			console.log(error.message);
		}
	});
	else
		$("#modalSICH"+challenge.id).modal('toggle');
}


function getArrayTags(array){

	var arrayTags = new Array();
	for(var i = 0; i < array.length; i++){
		arrayTags.push(array[i].value);
	}

	return arrayTags
}


function linkIdeaToCH(challenge){
	var checked = []
	$("input[name='"+challenge.id+"SICHRadio[]']:checked").each(function ()
	{
	    checked.push($(this).val());
	});
	console.log(checked);
	for (var i = 0; i < checked.length; i++) {
		checked[i];
		var idea = checked[i];
		Parse.Cloud.run("getIdea",{ideaID: idea},{
			success: function(ideaResult){
				ideaResult.set("challenge",challenge);
				ideaResult.save(null,{
					success: function(ideaSaved){
						var aHTML = '<div id="ideaRelated">'+
										'<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">'+
					                        '<div id="img-challenge">'+
					                            '<img class="img-thumbnail-challenge" src="'+ideaSaved.get("photoUrl")+'">'+
					                        '</div>'+
					                    '</div>'+
				                        '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">'+
				                        	'<p>'+ideaSaved.get("descripcion").toString().substring(0,40)+'<a class="read-more" href=viewIdea.html?k='+ideaSaved.id+'>See Full Idea&gt;</a></p>'+
				                    	'</div>'+
				                    '</div>';
						var challengeID = ideaSaved.get("challenge").id;
						ideaSaved.get("challenge").fetch();
						$("#"+challengeID+" #ideaRelated").html("<p>"+ideaSaved.get("challenge").get("countIdeas")+" ideas submitted</p>");
						$("#p"+ideaSaved.get("challenge").id+ideaSaved.id).append("Linked");
					},
					error: function(error){
						console.log(error.message);
					}
				});
			},
			error: function(error){
				console.log(error.message);
			}
		});
	};
}

$(document).ready(function(){


				Parse.Cloud.run('getAllTags', {
				        cant:200,
				        skip:0
				    }, 

				    {
				        success: function(result) {
				        	
							for (var i = 0; i < result.length; i++) {
							    arr.push({
							        value: result[i]
							    });
							}

							engine = new Bloodhound({	
								  local: arr,
								  datumTokenizer: function(d) {
								    return Bloodhound.tokenizers.whitespace(d.value);
								  },
								  queryTokenizer: Bloodhound.tokenizers.whitespace
								});


								engine.initialize();

								    $('#small').tokenfield({
								        delimiter: false,
								        typeahead: [
								            {
								                 hint: true,
												  highlight: true,
												  minLength: 1
								            }, 
								            {
								                source: engine.ttAdapter()
								            }
								        ]
								    }).on('tokenfield:createtoken', function (e) {
									
								/*	arr = removeByAttr(arr,"value",e.attrs.value.toLowerCase());*/

								        var existingTokens = $(this).tokenfield('getTokens');
								        if (existingTokens.length) {

								            $.each(existingTokens, function(index, token) {

								                if (token.value.toLowerCase() === e.attrs.value.toLowerCase()) {
								                    e.preventDefault();
								                }
								                
								            });
								        }

								    });
								        },

								        error: function(error) {
								           console.log(error);
								         }

				    });


	var setCategory = {
	  func0: function () { idea.category = 0; $(".modal-title-NewIdea").html("Create New Idea");},
	  func1: function () { idea.category = 1; $(".modal-title-NewIdea").html("Create New Need");},
	  func2: function () { idea.category = 2; $(".modal-title-NewIdea").html("Create New Application");},
	  func3: function () { idea.category = 3; $(".modal-title-NewIdea").html("Create New POC");},
	  func4: function () { idea.category = 4; $(".modal-title-NewIdea").html("Create New Microservice");},
	  func5: function () { idea.category = 5; $(".modal-title-NewIdea").html("Create New Other");},
	};

	$('.setCategory').click(function(){
	  var data = $.parseJSON($(this).attr('data-button'));

	  setCategory[data.func]();
	});

	function createIdea(){
		idea.name = $("#inputName").val().trim();
        idea.descr = $("#inputDescr").val().trim();
        idea.isPrivate = $("#cmn-toggle-1").prop("checked");
        idea.tags = getArrayTags($('#small').tokenfield('getTokens'));
        
        if(idea.name.length > 0 && idea.descr.length > 0 )
        {	

        	$("#finishBtn").html("Processing...");
        	//$("#addButton").html('<a class="text-right btn btn-success">Processing...</a>');
        	$('#myModal').modal('toggle');
        	$('#finishBtn').off("click");
        	newCard();
        	initGear("NewCard");
			var files = document.getElementById("photoUpload").files;

			var Idea = Parse.Object.extend("Idea");
			var ideaObject = new Idea();
			    ideaObject.set("titulo",idea.name);
			    ideaObject.set("descripcion",idea.descr);
			    ideaObject.set("esPrivado",idea.isPrivate);
			    ideaObject.set("tags",idea.tags);
			    ideaObject.set("locationCity","Monterrey");
			    ideaObject.set("category",idea.category);
			    ideaObject.set("user",Parse.User.current());







			if(idea.challenge != null)
			{
				ideaObject.set("challenge",idea.challenge);
				ideaCHSub = true;
			}
			idea.fileCount = files.length;
			if(files.length > 0)
			{	
				metLista(0,files.length, ideaObject);	
			}
			else
			{
				ideaObject.save(null, {
			      success: function(object) {
			            $("#finishBtn").html("Saved!");
			        	stopGear();
			        	$("#NewCard").velocity("transition.fadeOut");
			        	if(idea.challenge != null)
			        		newChallengeIdea(object);
						newSavedCard(object);

					for(var i =0; i<idea.tags.length; i++)
					{	
						console.log(idea.tags);
						var tagname = "";
						tagname=idea.tags[i];
						 
						Parse.Cloud.run('setNewTag', {
			                tagname:tagname
			            }, 
			                {
			                    success: function(newtag) {
			                          	console.log(newtag);
			                    },
			
			                    error: function(error) {
			                            console.log(error);
			                    }
			
	                        });
					}
			           	//getLocation();
			      },
			      error: function(model, error) {
			        alert("Didnt Saved!");
			      }
			    });
			}
		}
	}

	function createChallenge(){
		$("#saveChallenge").html("Processing...");
        	//$("#addButton").html('<a class="text-right btn btn-success">Processing...</a>');
        $('#chModal').modal('toggle');
        $('#saveChallenge').off("click");
        	
        newCardCH();
        initGear("NewCardCH");
        var chTitle = $("#chTitle").val();
        var chDescr = $("#chDescr").val();
        if(chDescr.trim().length > 0 && chTitle.trim().length > 0)
        Parse.Cloud.run("saveChallenge",{title: chTitle,descr: chDescr, userID: Parse.User.current().id},{
        	success: function(savedCH){
        		stopGear();
        		$("#NewCardCH").velocity("transition.fadeOut");
        		newSavedCardCH(savedCH);
        		clearAllCH();
        	},
        	error: function(error){
        		console.log(error.message);
        	}
        });
	}

    $("#finishBtn").click(function(){
        createIdea();
    });
	
	function newCardCH() {
        $("#recently-added-cards").prepend("<div class='card' id='NewCardCH"+
                "' href='#'><div id='category'><div class='noactive ideaCategory'></div><div class='noactive needsCategory'></div><div class='noactive appCategory'></div><div class='noactive pocCategory'></div><div class='noactive microCategory'></div><div class='noactive otherCategory'></div></div><h4 class='titleNewCard'>" +
                "Adding new Challenge..." + "</h4></div>");
	}

	function newChallengeIdea(ideaCH){
		
		Parse.Cloud.run("unaIdeaFull",{oID: ideaCH.id, userID: Parse.User.current().id},{
			success: function(iCH){
				console.log(iCH);
				var aHTML = '<div id="ideaRelated">'+
								'<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">'+
				                    '<div id="img-challenge">'+
				                        '<img class="img-thumbnail-challenge" src="'+iCH[0].idea.get("photoUrl")+'">'+
				                    '</div>'+
				                '</div>'+
				                '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">'+
                        			'<p>'+iCH[0].idea.get("descripcion").toString().substring(0,40)+'<a class="read-more" href=viewIdea.html?k='+iCH[0].idea.id+'>See Full Idea&gt;</a></p>'+
                    			'</div>'+
                    		'</div>';
				var challengeID = iCH[0].idea.get("challenge").id;
				$("#"+challengeID+" #ideaRelated").html(iCH[0].idea.get("challenge").get("countIdeas")+" ideas submitted");
				ideaCHSub = false;
			},
			error: function(error){
				console.log(error.message);
			}
		});
	}

	function newSavedCardCH(challenge){
		console.log(challenge);
		var datestring = challenge.createdAt.toString();
        var hola = moment(datestring).fromNow();
		var aHTML = '<div class="card challenge" id="'+challenge.id+'" >'+
					    '<h4 class="title">'+challenge.get("titulo")+'</h4>'+
					    '<div id="section2">'+
					        '<h5 class="author">by '+Parse.User.current().get("username")+'</h5><i class="material-icons time-icon"></i>'+
					        '<h5 class="date">'+hola+'</h5><i class="material-icons location-icon"></i>'+
					        '<h5 class="location">Monterrey</h5>'+
					    '</div>'+
					    '<p id="ChallengeDescription">'+object.get("descripcion")+'</p>'+
					    '<div id="btnChallenge">'+
                        	//'<a href="#" id="contributeIdea" href="#" >Contribute Idea</a>'+
                        	//'<a href="#" id="linkIdea" href="#" >Link Idea</a>'+
                   		'</div>'+
                   		'<hr id="lineChalenge">'+
					    '<a href="grid.html?k='+challenge.id+'" class="bold600">See More</a>'+
					'</div>';
		$("#recently-added-cards").prepend(aHTML);
		var row = $("#"+challenge.id+" #btnChallenge");
		var btn = document.createElement("A");        // Create a <button> element
		var t = document.createTextNode("Contribute Idea");       // Create a text node
		btn.appendChild(t);
		btn.setAttribute("id", "contributeIdea");
		btn.setAttribute("data-info",challenge.id);
		btn.onclick = function() { contributeIdea(challenge); }
		row.append(btn);

		var btnLink = document.createElement("A");        // Create a <button> element
		t = document.createTextNode("Link Idea");       // Create a text node
		btnLink.appendChild(t);
		btnLink.setAttribute("id", "linkIdea");
		btnLink.onclick = function() { linkIdea(challenge); }                                // Append the text to <button>
		row.append(btnLink);	
	}	

	$('#myModal').on('hidden.bs.modal', function () {
  		if(idea.challenge != null)
  		{
  			console.log("CHANGEOO");
  			$("#myModal").find('h4:first').remove();
  			if(!ideaCHSub)
  			idea.challenge = null;
  		}
	});

	function newCard() {
        var imageUser = Parse.User.current().get("image").url();
        var imageUserY = Parse.User.current().get("imageYammer");
        if (imageUserY != null) imageUser = imageUserY;
        $("#recently-added-cards").prepend("<div class='card' id='NewCard"+
                "' href='#'><div id='category'><div class='noactive ideaCategory'></div><div class='noactive needsCategory'></div><div class='noactive appCategory'></div><div class='noactive pocCategory'></div><div class='noactive microCategory'></div><div class='noactive otherCategory'></div></div><h4 class='titleNewCard'>" +
                "Adding new Idea..." + "</h4><span class='user-img' style = 'background-image: url(" + imageUser + ")'></span></div>");
	}

	function newSavedCard(ideaObj){
		console.log(ideaObj);
		Parse.Cloud.run("unaIdeaFull",{oID: ideaObj.id, userID: Parse.User.current().id},{
			success: function(ideaFull){
				var object = ideaFull[0].idea;
				var comments = 0;
				var likes = 0;
				console.log(ideaFull[0]);
				var attachments = 0;
				if(ideaFull[0].hasOwnProperty('files'))
					attachments = ideaFull[0].files.length;

/*				var abstract = object.get('descripcion').substring(0, 300) + "...";
                var imageUser = object.attributes.user.attributes.image.url();
                var imageUserY = object.attributes.user.attributes.imageYammer;
                if (imageUserY != null) imageUser = imageUserY;
                var datestring = object.createdAt.toString();
                var hola = moment(datestring).fromNow();
                $("#recently-added-cards").prepend("<div class='card' id='" + object.id +
                        "' href='#' onClick='clickCard(this.id)'><div id='category'><div class='noactive ideaCategory'></div><div class='noactive needsCategory'></div><div class='noactive appCategory'></div><div class='noactive pocCategory'></div><div class='noactive microCategory'></div><div class='noactive otherCategory'></div></div><h4 class='title'>" +
                        object.get('titulo') + "</h4><span class='user-img' style = 'background-image: url(" + imageUser + ")'></span><div id='section2'><h5 class='author' href='#''>by " +
                        object.attributes.user.attributes.username + "</h5><i class='material-icons time-icon'>&#xE425;</i><h5 class='date'>" + hola +
                        "</h5><i class='material-icons location-icon'>&#xE0C8;</i> <h5 class='location'>" + object.get("locationCity") + "</h5></a></div><p class='description'>" + abstract +
                        "<a class='read-more' href=viewIdea.html?k="+object.id+">See Full Idea > </a></p><div id='interaction'><span class='icon comment'>" + comments + "</span><span class='icon heart'>" + likes +
                        "</span><span class='icon attachment'>" + attachments + "</span></div><div id='tags' class='tag" + object.id + "'></div></div>");
                ideaCategory(object, '.most-recent', object.id);
                ideaHasPhotoAttached(object, '.most-recent', object.id);
                var array = [];
                array.length = 0;
                var tagsWord = object.get('tags').toString();
                array = tagsWord.split(',');
                for (var i = 0; i < array.length; i++) {
                        $(".tag" + object.id).append("<a class='tag' href='' >" + array[i] + "</a>");
                }
                $(".tag" + object.id).append("<a class='clear' href='' ></a>"); 
                clearAll();           
                console.log("finish cread card");  */
                                var abstract = object.get('descripcion').substring(0, 300) + "...";
                var imageUser = object.attributes.user.attributes.image.url();
                var imageUserY = object.attributes.user.attributes.imageYammer;
                if (imageUserY != null) imageUser = imageUserY;
                var datestring = object.createdAt.toString();
                var hola = moment(datestring).fromNow();
                $("#recently-added-cards").prepend("<div class='card' id='" + object.id +
                        "' href='#' onClick='clickCard(this.id)'><div id='category'><div class='noactive ideaCategory'></div><div class='noactive needsCategory'></div><div class='noactive appCategory'></div><div class='noactive pocCategory'></div><div class='noactive microCategory'></div><div class='noactive otherCategory'></div></div><h4 class='title'>" +
                        object.get('titulo') + "</h4><span class='user-img' style = 'background-image: url(" + imageUser + ")'></span><div id='section2'><h5 class='author' href='#''>by " + object.attributes.user.attributes
                        .username + "</h5><i class='material-icons time-icon'>&#xE425;</i><h5 class='date'>" + hola + "</h5><i class='material-icons location-icon'>&#xE0C8;</i> <h5 class='location'>" + object.get(
                                "locationCity") + "</h5></a></div><p class='description'>" + abstract + "<a class='read-more'>See Full Idea > </a></p><div id='interaction'><span class='icon comment'>" + comments +
                        "</span><span class='icon heart'>" + likes + "</span><span class='icon attachment'>" + attachments + "</span></div><div id='tags' class='tag" + object.id + "'></div></div>");
                ideaCategory(object, '#most-recent', object.id);
                ideaHasPhotoAttached(object, '#most-recent', object.id);
             //   addAnimation("#most-recent", object.id, 1000);
                var array = [];
                array.length = 0;
                var tagsWord = object.get('tags').toString();
                array = tagsWord.split(',');
                for (var i = 0; i < array.length; i++) {
                        $(".tag" + object.id).append("<a class='tag' href='#' >" + array[i] + "</a>");
                }
                $(".tag" + object.id).append("<a class='clear' href='#' ></a>"); 
			},
			error: function(error){
				console.log(error.message);
			}
		});
	}

	function stopGear(){
	  	$('.gear').velocity('stop');
	  	$('.gear').css({'transform': 'rotate(0)'});
	  	$('.gearTooth').velocity({
	    	translateX: [250,0]
	  	},500,'ease');
	  	$('.gearTooth').velocity({
	    	translateY: [1000,0]
	  	},500,'ease-in', function(){
		    $('.gear').fadeOut(function(){
		    	$('.gear').remove();
		    });
	  	});
	  	console.log("termino gear");
	}

	function clearAll(){
		idea = {name:"", descr: "", tags:[], isPrivate: false,category: 0, fileCount: 0};
		$("#inputName").val("");
        $("#inputDescr").val("");
        $("#finishBtn").html("Finish");

        $(".setCategory").removeClass("active");
		      

        while($('#small').tokenfield('getTokens').length > 0)
        {
        	$('#small').tokenfield('setTokens', []);
        }
        $("#small input").addClass("fixEnter");
        $("#finishBtn").on("click", function(){
        	createIdea();
		});
		var input = $("#photoUpload");
		input.replaceWith(input.val('').clone(true));
	}

	function clearAllCH(){
		idea = {name:"", descr: "", tags:[], isPrivate: false,category: 0, fileCount: 0};
		$("#chDescr").val("");
        $("#chTitle").val("");
        $("#saveChallenge").html("Save changes");
        $("#saveChallenge").on("click", function(){
        	createChallenge();
		});
	}


	$("#saveChallenge").click(function(){
		createChallenge();
    });

	function postMessage() {  
     yam.getLoginStatus( function(response) {  
       if (response.authResponse) {  
        //call the yamPostRequest function if the user is logged in
       
       yamPostRequest(this);  
       } else {  
        //redirect the user to log in
        window.location = "home.html";  
       }  
     });  
   }

	function yamPostRequest(val) {  
	   var msg_value = idea.name+":"+idea.descr;  
	   var groupID = "6068022";  
	    if(msg_value ==""){  
		     alert ("Message body cannot be empty!");  
		     return false;  
	    }  
	    if(groupID==""){  
	    	var conf = confirm("Group ID is empty, message will be posted to All Company") ;
	    	if(conf==false){return false;}
	    }  
	 	yam.platform.request(  
	   	{   
		    url: "https://api.yammer.com/api/v1/messages.json"  
		    , method: "POST"  
		    , data: {  
		     "body" : msg_value,  
		     "group_id" : groupID  
		    }  
		    , success: function (msg) { window.location = "home.html"; }  
		    , error: function (msg) { alert("Post was Unsuccessful"); }  
		})  
	}


			function getLocation() {
			    if (navigator.geolocation) {
			        navigator.geolocation.getCurrentPosition(showPosition);
			    } else { 
			        alert("Geolocation is not supported by this browser.");
			    }
			}

			function showPosition(position) {
			    var userlocation = new Parse.GeoPoint({latitude: position.coords.latitude, longitude: position.coords.longitude});
			    Parse.Cloud.run("registerTime",{geopoint: userlocation,ideaID: "mwYHeL3QNl",userID : Parse.User.current().id, device: "web", newUser: false, evento:"submit IDEA"},{
						success: function(response){
							console.log("Saved registry");
							postMessage();
						},
					    error: function(error){
					      alert("Error f1:"+ error.message);
					    }
					  });
			}

	function metLista(act,max,ideaObject)
	{
		$("#finishBtn").html("Uploading "+(max-act)+" files....");
		$(".titleNewCard").html("<h4>Uploading "+(max-act)+" files....</h4>");
		if(act < max)
		{
			var file = $("#photoUpload").get(0).files[act];
			var name = $("#photoUpload").get(0).files[act].name;
			var parseFile = new Parse.File(name, file);
			parseFile.save().then(function(){
				  	
			});

			var Archivo = Parse.Object.extend("Archivo");
			var archivo = new Archivo();
			archivo.set("file",parseFile);
			archivo.set("nombre",name);
			archivo.set("idea",ideaObject);
			archivo.save(null, {
		      success: function(object) {
		      	act = act+1;
		        metLista(act,max,ideaObject);
		      },
		      error: function(model, error) {
		        alert("Error f2: "+error.message);
		      }
		    });
		}
		else
		{
			stopGear();
			$("#NewCard").velocity("transition.fadeOut");
			if(idea.challenge != null)
			    newChallengeIdea(ideaObject);
			newSavedCard(ideaObject);
			//getLocation();
		}
	}



$(".setCategory").on("click", function() {

    $(".setCategory").removeClass("active");

        if($(this).hasClass("active")){
                $(this).removeClass("active");
        }
        else{
                $(this).addClass("active");
        }

});






});



										function removeByAttr(arr, attr, value){
										    var i = arr.length;
										    while(i--){
										       if( arr[i] 
										           && arr[i].hasOwnProperty(attr) 
										           && (arguments.length > 2 && arr[i][attr] === value ) ){ 

										           arr.splice(i,1);

										       }
										    }
										    return arr;
										}