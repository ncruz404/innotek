Get-ChildItem -r -include "*.html" | foreach-object { $a = $_.fullname; ( get-content $a ) | foreach-object { $_ -replace "js/securityOldUnsafe.js", "js/security.js" }  | set-content $a }
Write-Host -Object ("Completed 25%") -ForegroundColor Yellow;
Get-ChildItem -r -include "*.html" | foreach-object { $a = $_.fullname; ( get-content $a ) | foreach-object { $_ -replace "js/loginOldUnsafe.js", "js/login.js" }  | set-content $a }
Write-Host -Object ("Completed 50%") -ForegroundColor Yellow;
Get-ChildItem -r -include "*.html" | foreach-object { $a = $_.fullname; ( get-content $a ) | foreach-object { $_ -replace "js/officeLogOldUnsafe.js", "js/officeLog.js" }  | set-content $a }
Write-Host -Object ("Completed 75%") -ForegroundColor Yellow;
Get-ChildItem -r -include "*.js","*.html" | foreach-object { $a = $_.fullname; ( get-content $a ) | foreach-object { $_ -replace "http://localhost:5000/", "https://www.innotek.ai/" }  | set-content $a }
Write-Host -Object ("Completed 100%") -ForegroundColor Green;
