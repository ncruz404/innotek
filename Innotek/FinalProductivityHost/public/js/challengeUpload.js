var key = GetUrlValue("k");    
    
$(document).ready(function() {
    var cUser = Parse.User.current();
    var cIdea = null;
    var cUploads = [];
    var cUploadFiles = [];
    var alreadyUploaded = null;
    var dynFunc = {
      //funcIH: function () { alert("AAAAAAAAA");}
    };
    var extensions = [
        ".doc,.docx",
        ".apk",
        ".ppt,.pptx",
        ".jpg,.png",
        ".xls,.xlsx",
    ];


    function addElement(id,type,text,attr){
        var row = $(id);
        var btn = document.createElement(type);        // Create a <button> element
        var t = document.createTextNode(text);       // Create a text node
        btn.appendChild(t);
        for (var i = 0; i < attr.length; i++) {
            btn.setAttribute(attr[i].src, attr[i].value);
            if(attr[i].src == "data-button")
            {
                btn.onclick = function() { var data = $(this).attr('data-button');
                var value = $(this).attr('data-value');
                console.log(data);
                if(value == undefined || value.length <= 0)
                    dynFunc[data](); 
                else
                {
                    dynFunc[data](value);
                }
                    
                }
            }
        };
        row.append(btn);
    }

    Parse.Cloud.run("getIdea",{ideaID : key},{
    	success: function(rIdea){
    		cIdea = rIdea;
    		if(rIdea.get("user").id == cUser.id)
    			{
    				Parse.Cloud.run("getUploadWeek",{ideaID: key},{
    					success: function(rUploads){
    						console.log(rUploads);
    						cUploads = rUploads;
    						printModule();
    					},
    					error: function(error){
    						console.log(error.message);
    					}
    				});
    			}
    	},
    	error: function(error){
    		console.log(error.message);
    	}
    });

    function printElement(row){
    	switch(row.type){
    		case "text": 
    		return 	'<div class="row">'+
    				'<p>'+row.sign+'</p>'+
    			 	'<input type="text" class="form-control" value="'+row.value+'" required="required" pattern="" title="'+row.sign+'">'+
    			 	'</div>';
    		
    		break;
    		case "file": 
    			return 	'<div class="row">'+
	    				'<p>'+row.sign+'</p>'+
	    			 	'<input type="file" accept="'+extensions[row.value]+'"/>'+
	    			 	'</div>';
    		break;
    	}
    }
    
    var indexWeek = 0;
    		
    function printModule(){
    	if(cIdea.has("challenge"))
    	{
    		var cont = 0;
    		while(alreadyUploaded == null && cont < cUploads.length)
    		{
    			var el = cUploads[cont];
    			if(el.get("week") == indexWeek)
    				alreadyUploaded = el;
    			else
    				if(el.get("week") > indexWeek)
    					cont = cUploads.length;
    				else
    					cont++;
    		}
    		
    		var chalWeek = cIdea.get("challenge").get("semanas")[indexWeek];
    		var start = new Date(chalWeek.start.split("-")[0],Number(chalWeek.start.split("-")[1])-1,chalWeek.start.split("-")[2]);
    		var fin = new Date(chalWeek.fin.split("-")[0],Number(chalWeek.fin.split("-")[1])-1,chalWeek.fin.split("-")[2]);
    		var cDate = new Date();
    		if(start <= cDate && cDate <= fin)
    		{
    			var aHTML ='<div class="modal fade" id="modalChalWeek">'+
					    		'<div class="modal-dialog">'+
					    			'<div class="modal-content">'+
					    				'<div class="modal-header">'+
					    					'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
					    					'<h4 class="modal-title">'+chalWeek.titulo+'</h4>'+
					    				'</div>'+
					    				'<div class="modal-body">'+
					    					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
											for (var i = 0; i < chalWeek.data.length; i++) {
												aHTML+= printElement(chalWeek.data[i]);
											};
				aHTML+=    					'</div>'+
										'</div>'+
					    				'<div class="modal-footer">'+
					    					'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
					    				'</div>'+
					    			'</div>'+
					    		'</div>'+
				    		'</div>';
				$("body").append(aHTML);
				
                if(alreadyUploaded != null)
                    setValues(alreadyUploaded);

    			$("#chalUpLink").html('<a class="btn btn-primary" data-toggle="modal" href="#modalChalWeek">This week challenge</a>');
    			addElement("#modalChalWeek .modal-footer","BUTTON","Save changes",
                    [{src: "id",value:"actionUploadCHW"},{src: "data-button",value:'actionUploadCHW'},
                     {src: "class",value:"btn btn-primary"}]);
                dynFunc['actionUploadCHW'] = function () { uploadChWeek()};
    		}
    	}   	
    }

    function setValues(alreadyUploaded){
        console.log("AAAAAAAAAAAAA");
        console.log(alreadyUploaded);
        var innerDiv = $("#modalChalWeek .modal-body div .row");
        for (var i = 0; i < innerDiv.length; i++) {
            var input = innerDiv[i].childNodes[1];
            switch(input.type){
                case "text": 
                    input.value = alreadyUploaded.get("uploads")[i].value;
                break;
                case "file": 
                    uploadAttachedFile(alreadyUploaded.get("uploads")[i].value,i);
                break;
            }
        };
    }

    function uploadAttachedFile(archivoID,pos){
        var ArchObj = Parse.Object.extend("Archivo");
        var ArchQuery = new Parse.Query(ArchObj);
        ArchQuery.get(archivoID,{
            success: function(rArchivo){
                var innerDiv = $("#modalChalWeek .modal-body div .row");
                var p = innerDiv[pos].childNodes[0];
                p.innerHTML = p.innerHTML + " : <a href='"+rArchivo.get("file").url()+"'>"+ rArchivo.get("nombre")+"</a>";
                cUploadFiles[pos] = rArchivo;
                console.log(cUploadFiles);
            },
            error: function(error){
                console.log(error.message);
            }
        });
    }

    var arrayData;
    var flagReady;
    function createDataElement(row,pos){
    	var input = row.childNodes[1];
    	switch(input.type){
    		case "text": 
    			flagReady--;
    			return {type: "text", value: input.value};
    		break;

    		case "file": 
    	       if(alreadyUploaded != null)
                    if(input.files.length > 0)
                    {
                        var files = input.files;
                        var file = input.files[0];
                        var name = input.files[0].name;
                        var parseFile = new Parse.File(name, file);
                        parseFile.save().then(function(){
                                
                        });
                        var obj = null;
                        var Archivo = Parse.Object.extend("Archivo");
                        var archivo = cUploadFiles[pos];
                        archivo.set("file",parseFile);
                        archivo.set("nombre",name);
                        archivo.save(null, {
                          success: function(object) {
                            obj = {type: "file", value: object.id};
                            return obj;
                          },
                          error: function(model, error) {
                            console.log("Error f2: "+error.message);
                          }
                        }).then(function(object){
                            arrayData[pos] = {type: "file", value: object.id};
                            flagReady--;
                            checkIfReady();
                        });
                    }
                    else
                    {
                        flagReady--;
                        return alreadyUploaded.get("uploads")[pos];
                    }
                        	
                else
                {
                    var files = input.files;
                    var file = input.files[0];
                    var name = input.files[0].name;
                    name = name.replace(/[^\w\s-]/gi, '');
                    console.log(name);
                    var parseFile = new Parse.File(name, file);
                    parseFile.save().then(function(){
                            
                    });
                    var obj = null;
                    var Archivo = Parse.Object.extend("Archivo");
                    var archivo = new Archivo();
                    archivo.set("file",parseFile);
                    archivo.set("nombre",name);
                    archivo.set("idea",cIdea);
                    archivo.save(null, {
                      success: function(object) {
                        obj = {type: "file", value: object.id};
                        return obj;
                      },
                      error: function(model, error) {
                        console.log("Error f2: "+error.message);
                      }
                    }).then(function(object){
                        arrayData[pos] = {type: "file", value: object.id};
                        flagReady--;
                        checkIfReady();
                    });
                }
    		break;
    	}
    }

    function checkIfReady(){
    	if(flagReady <= 0)
    	{
    		var CHUObj = Parse.Object.extend("ChallengeUploads");
    		var newCHUObj;
            if(alreadyUploaded == null)
            {
                newCHUObj = new CHUObj();
                newCHUObj.set("idea",cIdea);
                newCHUObj.set("challenge",cIdea.get("challenge"));
                newCHUObj.set("week",indexWeek);    
            }
            else
            {
                newCHUObj = alreadyUploaded;
            }            
    		newCHUObj.set("uploads",arrayData);
            newCHUObj.save(null,{
    			success: function(rCHUObj){
    				$("#actionUploadCHW").html("Save Changes");
    				window.location = "viewIdea.html?k="+cIdea.id;
                    console.log(rCHUObj);
    			},
    			error: function(error){
    				console.log(error.message);
    			}
    		});
    	}
    }

    function uploadChWeek(){
    	$("#actionUploadCHW").html("Saving...");
    	var innerDiv = $("#modalChalWeek .modal-body div .row");
    	flagReady = innerDiv.length;
        if(alreadyUploaded == null)
            arrayData = new Array();
        else
            arrayData =  alreadyUploaded.get("uploads");
        for (var i = 0; i < innerDiv.length; i++) {
            var newObj = createDataElement(innerDiv[i],i);
            if(newObj == undefined)
                newObj = {};
            arrayData[i] = newObj;
        };
        checkIfReady();
    }

});
