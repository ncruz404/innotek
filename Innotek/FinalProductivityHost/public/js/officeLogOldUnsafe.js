var urlParameterExtraction = new (function () { 
  function splitQueryString(queryStringFormattedString) {     
    var split = queryStringFormattedString.split('&'); 

    // If there are no parameters in URL, do nothing.
    if (split == "") {
      return {};
    } 
    var results = {}; 

    // If there are parameters in URL, extract key/value pairs. 
    for (var i = 0; i < split.length; ++i) { 
      var p = split[i].split('=', 2); 
      if (p.length == 1) 
        results[p[0]] = ""; 
      else 
        results[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " ")); 
    } 
    return results; 
  } 
  // Split the query string (after removing preceding '#'). 
  this.queryStringParameters = splitQueryString(window.location.hash.substr(1)); 
})(); 

// Extract token from urlParameterExtraction object.
var tokenC = urlParameterExtraction.queryStringParameters['access_token'];

if(tokenC != undefined)
	getUserFromO365();
else
	requestToken();

function getUserFromO365() {  
	$("#loadingText").append("Checking your Office account");
	$("#loading").css("background", "url('../css/img/LogoLoader.gif') no-repeat");
  	Parse.Cloud.run("checkOfficeToken",{token: tokenC},{
  		success: function(response){        
  			var formattedResponse = JSON.parse(response.text);
  			checarUserOffice(formattedResponse,tokenC);
  		},
  		error: function(error){
  			Parse.Cloud.run("saveError",{text:tokenC},{
				success: function(result){
					$("#loadingText").html("Your Office account is invalid");
					window.location = "https://login.windows.net/common/oauth2/logout?post_logout_redirect_uri=https://www.innotek.ai/";
				},
				error: function(error){
					console.log(error);
				}
  			});
  		}
  	});     
}
