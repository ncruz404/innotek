 alert("buscador");
	$(document).ready(function () {

 			Parse.Cloud.run("getIdeasBuscador",{},{
	        	success: function(response){
	        		var states = new Bloodhound({
					  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('title','user'),
					  queryTokenizer: Bloodhound.tokenizers.whitespace,
					  // `states` is an array of state names defined in "The Basics"
					  local: response
					});


					$('#productName').typeahead(null, {
					  name: 'best-pictures',
					  display: 'title',
					  source: states,
					  templates: {
					    empty: [
					      '<div class="empty-message">',
					        'There is no Ideas with that description',
					      '</div>'
					    ].join('\n'),
					    suggestion: function(data) {
						    return '<p><strong>' + data.title + '</strong> by ' + data.user + '</p>';
						} 
					  }
					}).on('typeahead:selected', function(event, datum) {
				         
				         //if(current_user.id == datum.uid)
				         
				         //window.location = "viewIdea.html?k="+datum.id;
				      });

				$("#productName").focus();
			    
			    $(".tt-hint").addClass("form-control").addClass("input-style");
	        	},
	        	error: function(error){
	        		console.log(error.message);
	        	}
	        });		  
		});

