function GetUrlValue(VarSearch){
        var SearchString = window.location.search.substring(1);
        if(SearchString.length > 0)
        {       
            var VariableArray = SearchString.split('&');
            for(var i = 0; i < VariableArray.length; i++){
                var KeyValuePair = VariableArray[i].split('=');
                if(KeyValuePair[0] == VarSearch){
                    return KeyValuePair[1];
                }
            }   
        }
        else
                return null;
}


var departmentName=GetUrlValue("d").replace('-', ' ');

$("#titleDepartment").html(departmentName);

console.log(departmentName);


                        
                            Parse.Cloud.run("getItemsSubcategory", {
                                    subcategoryName: departmentName //le mande el primer id
                            }, {
                                    success: function(items) { // return subcategory name array

                                        for (var i = 0; i < items.length; i++) {
                                            createItem(items[i]);
                                        };
                                                        
                                    },
                                    error: function(error) {
                                            console.log(error.message);
                                    }
                            });

                       
                
function createItem(item){

    var image = item.attributes.img.url();

    $("#departmentList").append(

        '<div class="col-sm-4 col-lg-4 col-md-4">'+
        '<div class="thumbnail">'+
            '<div id="thumbnail-img">'+
                '<img src="'+image+'" alt="">'+
            '</div>'+
            '<div class="caption">'+
                '<h4 class="pull-right">'+item.attributes.price+''+
                    '<i class="fa fa-puzzle-piece"></i>'+
                '</h4>'+
                '<h4><a href="#" id="'+item.id+'">'+item.attributes.name+'</a>'+
                '</h4>'+
                '<p>'+item.attributes.description+'</p>'+
            '</div>'+
        '</div>'+
    '</div>');  

}
