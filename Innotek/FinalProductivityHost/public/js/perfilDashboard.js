var ideatag;
var arrayResult = new Array();
var userID = Parse.User.current().id;
var userIdeas = [];

$(document).ready(function() {


        Parse.Cloud.run('getUser', {
                userID: Parse.User.current().id
        }, {
                success: function(result) {
                        $("#title-idea").append(result.get('name'));
                        $("#viewProfileDashboard .container #title-idea").after("<span class='profile-img' style= 'background-image: url(" + result.get("image").url() + ")'></span>");
                        var puzzlesNum = (result.get('puzzles') ===undefined ? 0 : result.get('puzzles'));
                        $(".puzzlesNumber").append(puzzlesNum+'<i class="fa fa-puzzle-piece"></i>');
                },
                error: function(error) {
                }
        });

        Parse.Cloud.run('getUser', {
                userID: Parse.User.current().id
        }, {
                success: function(result) {

                        if (result.get("userFollowing") === undefined) {
                                $("#followBtn").removeClass("following");
                                $("#followBtn").html("Follow");
                        }
                        if (result.get("userFollowing") !== undefined && result.get("userFollowing").indexOf(userID) <= -1) {
                                $("#followBtn").removeClass("following");
                                $("#followBtn").html("Follow");
                        }
                        if (result.get("userFollowing") !== undefined && result.get("userFollowing").indexOf(userID) > -1) {
                                $("#followBtn").html("Following");
                                $("#followBtn").addClass("following");
                        }
                },
                error: function(error) {
                }
        });



        Parse.Cloud.run('getUserIdeasSelected', {

                cantidad: 200,
                skip: 0,
                userID: userID

        }, {
                success: function(result) {



                        arrayResult = result;

                        for (var i = 0; i < result.length; i++) {
                                userIdeas = result[i];
                                // createGridUserIdeas(userIdeas.idea, userIdeas.comments, userIdeas.likes, userIdeas.files, "#allPosts .pinBoot");
                        }

                        if (result.length === 0) {
                                $("#viewProfileDashboard #ideasRelated ").append('<h3 class="text-center no-post">No publications yet</h3>');
                        }



                        for (var i = 0; i < arrayResult.length; i++) {
                                userIdeas = arrayResult[i];
                                createGridUserIdeas(userIdeas.idea, userIdeas.comments, userIdeas.likes, userIdeas.files, ".pinBoot");
                        }

                        if (arrayResult.length === 0) {
                                $("#viewProfileDashboard #ideasRelated ").append('<h3 class="text-center no-post">No publications yet</h3>');
                        }

                }

                ,
                error: function(error) {
                }
        });



});


$('#proposals-tab').on('click', function(event) {

        $(".pinBoot").empty();
        $(".no-post").remove();

        for (var i = 0; i < search(2, arrayResult).length; i++) {
                userIdeas = search(2, arrayResult)[i];
                createGridUserIdeas(userIdeas.idea, userIdeas.comments, userIdeas.likes, userIdeas.files, ".pinBoot");
        }

        if (search(2, arrayResult).length === 0) {
                $("#viewProfileDashboard #ideasRelated ").append('<h3 class="text-center no-post">No proposals yet</h3>');
        }


});

$('#allPost-tab').on('click', function(event) {

        $(".pinBoot").empty();
        $(".no-post").remove();

        for (var i = 0; i < arrayResult.length; i++) {
                userIdeas = arrayResult[i];
                createGridUserIdeas(userIdeas.idea, userIdeas.comments, userIdeas.likes, userIdeas.files, ".pinBoot");
        }

        if (arrayResult.length === 0) {
                $("#viewProfileDashboard #ideasRelated  ").append('<h3 class="text-center no-post">No publications yet</h3>');
        }

});

$('#accepted-tab').on('click', function(event) {

        $(".pinBoot").empty();
        $(".no-post").remove();

        for (var i = 0; i < search(3, arrayResult).length; i++) {
                userIdeas = search(3, arrayResult)[i];
                createGridUserIdeas(userIdeas.idea, userIdeas.comments, userIdeas.likes, userIdeas.files, ".pinBoot");
        }

        if (search(3, arrayResult).length === 0) {
                $("#viewProfileDashboard #ideasRelated ").append('<h3 class="text-center no-post">No accepted proposals yet</h3>');
        }

});




function search(value, array) {

                var newArray = new Array();
                for (var i = 0; i < array.length; i++) {
                        if (array[i].idea.attributes.Fase === value) {
                                newArray.push(array[i]);
                        }
                }

                return newArray;
        }


function createGridUserIdeas(idea, comments, likes, attachments, divId) {




        var imageUser = idea.attributes.user.attributes.image.url();


        var datestring = idea.createdAt.toString();
        var time = moment(datestring).fromNow();
        var abstract = idea.get('descripcion').substring(0, 300) + "...";



        $(divId).append("<div class='card white-panel' id='" + idea.id + "' onClick='clickCard(this.id)' style='transform: translateY(0px);'><a data-value='"+idea.id+"' class='delChal right'>[X]</a> <h4 class='title'>" + idea.get("titulo") + "</h4> <div id='section2'> <div class='icon-time'> <i class='material-icons time-icon'></i> <h5 class='date'>" + time + "</h5> </div> <div class='icon-location'> <i class='material-icons location-icon'></i> <h5 class='location'>" + idea.get("locationCity") + "</h5> </div> </div> <p class='description'>" + abstract + "<a class= 'read-more'>See Full Idea &gt;</a></p> <div id='interaction'> <span class='icon comment'>" + comments + "</span><span class='icon heart'>" + likes + "</span><span class='icon attachment'>" + attachments + "</span> </div> <div class='" + idea.id + "' id='tags'> </div> </div>");

        if (idea.get("photoUrl") !== undefined) {
                $("#" + idea.id + " p.description").prepend("<img class='img-thumbnail-idea' src='" + idea.get("photoUrl") + "'>");
        }
        var tagsWord = idea.get('tags').toString();
        var array = tagsWord.split(',');
        for (var i = 0; i < array.length; i++) {
                $(divId + " #tags" + "." + idea.id).append("<a class='tag' href='#' >" + array[i] + "</a>");
        }
        $(divId + " #tags " + "." + idea.id).append("<a class='clear' href='#' ></a>");



}




$("html").on("click", ".tag", function() {
        var word = $(this).text();
        word = word.replace(' ', '-');

        window.location = "tags.html?t=" + word;
});




$(document).on('click', '#followBtn.user', function() {


        if ($("#followBtn.user").hasClass("following") === false) {

                $("#followBtn.user").html("Following");
                $("#followBtn.user").addClass("following");

                Parse.Cloud.run('setUserFollowing', {
                        user: userID
                }, {
                        success: function(result) {

                        },
                        error: function(error) {
                        }
                });


        }
        else
        if ($("#followBtn.user").hasClass("following") === true) {

                Parse.Cloud.run('removeUserFollowing', {
                        user: userID
                }, {
                        success: function(result) {

                        },
                        error: function(error) {
                        }
                });




                $("#followBtn.user").removeClass("following");
                $("#followBtn.user").html("Follow");
        }
});


$(document).on("click", "a.autor", function() {
        var word = $(this).id;
        window.location = "user.html?i=" + word;
});

$("a.autor").on("click", function() {
        var word = $(this).id;
        window.location = "user.html?i=" + word;
});

function clickCard(clicked_id) {

    window.location = "viewIdea.html?k=" + clicked_id;


}

$('body').on('click', '.delChal', function(e) {
        window.stop();
        e.preventDefault();
        var ideaID = $(e.target).attr("data-value");
        var r = confirm("You sure want to delete this publication?");
        if (r == true) {
        Parse.Cloud.run('eliminarIdea',{ideaID: ideaID},{
            success: function(result){
                $("#"+ideaID).remove();
            },
            error: function(error){
            }
        });
    }
});
