Parse.Cloud.run("getCartHistory", {
}, {
        success: function(history) { // return subcategory name array

            console.log(history);

 

            for (var i = 0; i < history.length; i++) {
                var items = "";
                var totalOrder = 0;
                var orderId =history[i].order;
                var date = new Date(history[i].date);

                var day = date.getDay();
                var year = date.getFullYear();


                var month = new Array();
                month[0] = "January";
                month[1] = "February";
                month[2] = "March";
                month[3] = "April";
                month[4] = "May";
                month[5] = "June";
                month[6] = "July";
                month[7] = "August";
                month[8] = "September";
                month[9] = "October";
                month[10] = "November";
                month[11] = "December";
                var monthLetter = month[date.getMonth()];

                for (var j = 0; j < history[i].items.length; j++) {
                    var actualItem = history[i].items[j];
                    var image = actualItem.attributes.item.attributes.img.url();
                    var quantity = actualItem.attributes.quantity;
                    var description = actualItem.attributes.item.attributes.description;
                    var desc = description.substring(0, 100).concat("...");

                    totalOrder = totalOrder + (actualItem.attributes.item.attributes.price * quantity);
                    items = items.concat(
                        '<div class="row" id="'+actualItem.attributes.item.id+'">'+
                            '<div class="col-xs-3 col-sm-4"><img  class="img-responsive hidden-xs img-order-history" src="'+image+'">'+
                                '<button type="button" id="viewProductHistory"class="btn btn-primary col-xs-12">View</button>'+
                            '</div>'+
                            '<div class="col-xs-9 col-sm-6">'+
                                '<h4 class="product-name"><a href="#"><strong>'+actualItem.attributes.item.attributes.name+'</strong></a></h4>'+
                                '<p>'+desc+'</p>'+
                            '</div>'+
                            '<div class="hidden-xs col-sm-2">'+
                                '<div class="hidden-xs col-sm-2 text-right">'+
                                    '<h6><strong>'+quantity+'</strong></h6>'+
                                '</div>'+
                                '<div class="hidden-xs col-sm-2 text-right">'+
                                    '<h6><strong>'+actualItem.attributes.item.attributes.price+'</strong></h6>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<hr>'
                    );
                };

                $("#orders").append(

                '<div class="panel panel-info">'+
                    '<div class="panel-heading">'+
                        '<div class="panel-title">'+
                            '<div class="row">'+
                                '<div class="col-xs-6">'+
                                    '<h5><span class="glyphicon glyphicon-shopping-cart"></span> Order: <strong>'+orderId+'</strong></h5>'+
                                '</div>'+

                                '<div class="col-xs-6">'+
                                    '<h5>Date of Order: <strong>'+day+"-"+monthLetter+"-"+year+'</strong></h5>'+
                                '</div>'+

                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="panel-body" id="orderHistoryDetail">'+items+'</div>'+
                    '<div class="panel-footer">'+
                        '<div class="row text-center">'+
                            '<div class="col-xs-12">'+
                                '<h4 class="text-right">Total <strong>'+totalOrder+'</strong></h4>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            );

            };



                            
        },
        error: function(error) {
                console.log(error.message);
        }
});

$('body').on('click', '.product-name a', function() {
        var word = $(this).parent().parent().parent().attr('id');
        window.location = "item.html?d=" + word;

});

$('body').on('click', '#viewProductHistory', function() {
        var word = $(this).parent().parent().attr('id');
        window.location = "item.html?d=" + word;

});

$('body').on('click', '.img-order-history', function() {
        var word = $(this).parent().parent().attr('id');
        window.location = "item.html?d=" + word;

});
