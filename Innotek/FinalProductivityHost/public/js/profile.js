$( document ).ready(function() {
    $("#changeSkills").click(function() {
				  var arraySkills = getArrayTags($('#skills').tokenfield('getTokens'));
					if(arraySkills.length<1){
					  $(".error-skillsSettings").css("display","block");
					  $("#small").parent().addClass("error");
					}
					if(arraySkills.length>0){
					  $(".error-skillsSettings").css("display","none");
					  $("#small").parent().removeClass("error");			  
						  Parse.Cloud.run('setSkills', {
							  arraySkills: arraySkills
						  }, {
							  success: function(result) {
								  console.log("Saved");
							  },
							  error: function(model, error) {
//								  alert(error);
							  }
						  });
				  }
			  });
				  var cUser = Parse.User.current();
				  
				  var profilePhoto = cUser.get("image");
				  $("#edit-img").css("background-image",'url("'+profilePhoto.url()+'")');
				  $("#userName").val(cUser.get("name"));

				  $("#changeImageProfile").click(function(){
					  var files = document.getElementById("profileUpload").files;
					  var sizeFile = document.getElementById('profileUpload').files[0].size;					  
					  if(files.length > 0)
					  {
						  var validate = validateExtension(document.getElementById('profileUpload').files[0].name);

						  if(sizeFile <= 10485760 && validate===true){
							  $(".error-profilePicture-size").css("display","none");
							  $("#changeImageProfile").html("Saving ...");
							  var file = $("#profileUpload").get(0).files[0];
							  var name = $("#profileUpload").get(0).files[0].name;
							  var parseFile = new Parse.File(name, file);
							  parseFile.save().then(function(){
									  Parse.Cloud.run('cambiarImagenPerfil',{userID: Parse.User.current().id,File: parseFile},{
										  success: function(object) {
											  $("#changeImageProfile").html("Change Photo");
											  profilePhoto = object.get("image");
									$("#edit-img").css("background-image",'url("'+profilePhoto.url()+'")');
											},
											error: function(model, error) {
											  alert("Didnt Saved!");
											}
									  });
							  });
						  }
						  
						  if(sizeFile>10485760){
							  $(".error-profilePicture-size").css("display","block");
						  }

					  }
				  });
				  $("#changeData").click(function(){
					  if($("#userName").val().trim().length > 0 && $("#userName").val().indexOf('"') == -1)
					  {
						  $("#changeData").html("Saving ...");

						  Parse.Cloud.run("cambiarDatosPerfil",{userID: cUser.id, name: $("#userName").val()},{
							  success: function(response){
								  $("#changeData").html("Update");
								  Parse.User.current().fetch()
							  },
							  error: function(error){
								  alert("Error"+ error.Message);
							  }
						  });
					  }
					  else
						  alert("Invalid Field");
				  });
				  $("#changePass").click(function(){
					  if($("#userPass").val() == $("#userPass2").val() && $("#userPass").val().trim().length > 0 && $("#userPassOld").val().trim().length)
					  {
						  $("#changePass").html("Saving ...");
						  alert($("#userPassOld").val());
						  Parse.Cloud.run("cambiarDatosPerfil",{userID: cUser.id, newPassword: $("#userPass").val(), oldPassword: $("#userPassOld").val()},{
							  success: function(response){
								  $("#changePass").html("Change");
								  Parse.User.current().fetch()
				  
							  },
							  error: function(error){
								  alert("Error"+ error.message);
							  }
						  });
					  }else
					  {
						  alert("Verify your fields");
					  }
				  });
		function validateExtension(fileName){
		  var re = /(\.jpg|\.jpeg|\.png|\.gif|\.JPG|\.JPEG|\.PNG|\.GIF)$/i;
		  var validation = re.exec(fileName);
		  if(!re.exec(fileName))
		  {
			  $(".error-profilePicture-ext").css("display","block");
			  return false;
		  }
		  else{
			  $(".error-profilePicture-ext").css("display","none");
			  return true;
		  }
		}
		$(document).ready(function(){
					  Parse.Cloud.run('getAllTags', {
							  cantidad:200,
							  skip:0
						  }, 
						  {
							  success: function(result) {								  
								  for (var i = 0; i < result.length; i++) {
									  arr.push({
										  value: result[i]
									  });
								  }
								  engine = new Bloodhound({   
										local: arr,
										datumTokenizer: function(d) {
										  return Bloodhound.tokenizers.whitespace(d.value);
										},
										queryTokenizer: Bloodhound.tokenizers.whitespace
									  });
									  engine.initialize();
										  $('#skills').tokenfield({
											  delimiter: false,
											  typeahead: [
												  {
													   hint: true,
														highlight: true,
														minLength: 1,
												  }, 
												  {
													  source: engine.ttAdapter()
												  }
											  ]
										  }).on('tokenfield:createtoken', function (e) {										  
									  /*  arr = removeByAttr(arr,"value",e.attrs.value.toLowerCase());*/
											  var existingTokens = $(this).tokenfield('getTokens');
											  if (existingTokens.length) {

												  $.each(existingTokens, function(index, token) {

													  if (token.value.toLowerCase() === e.attrs.value.toLowerCase()) {
														  e.preventDefault();
													  }
												  });
											  }
										  });
													  Parse.Cloud.run('getSkills', {
													  }, {
														  success: function(result) {
															  $('#skills').tokenfield('setTokens', result);
														  },
														  error: function(model, error) {
//															  alert(error);
														  }
													  });
											  },
											  error: function(error) {
												 console.log(error);
											   }
						  });
		});


    
});
