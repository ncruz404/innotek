var userIdea = "";
var idea = {
	id: "",
	name: "",
	descr: "",
	tags: [],
	isPrivate: false,
	likeCount: 0,
	dislikeCount: 0,
	commentsCount: 0,
	attachmentsCount: 0
};
var key = window.location.search
key = key.replace("?k=","");
var ideaObj;
var roster;
var paisLike = [];
var filesList = {
	image: [],
	apk: []
};



$(document).ready(function() {

	//--------------------Window Size Management-------------------------
	function setSizes() {
		if ($(window).width() < 400) {
			$("#viewIdea .tabDiv li").removeClass("col-xs-6");
			$("#viewIdea .tabDiv li").removeClass("col-xs-3");
			$("#viewIdea .tabDiv li").removeClass("col-xs-2");
			$("#viewIdea .tabDiv li").addClass("col-xs-6");
		}
		if ($(window).width() >= 400 && $(window).width() < 520) {
			$("#viewIdea .tabDiv li").removeClass("col-xs-6");
			$("#viewIdea .tabDiv li").removeClass("col-xs-3");
			$("#viewIdea .tabDiv li").removeClass("col-xs-2");
			$("#viewIdea .tabDiv li").addClass("col-xs-3");
		}
		if ($(window).width() >= 520 && $(window).width() < 768) {
			$("#viewIdea .tabDiv li").removeClass("col-xs-6");
			$("#viewIdea .tabDiv li").removeClass("col-xs-3");
			$("#viewIdea .tabDiv li").removeClass("col-xs-2");
			$("#viewIdea .tabDiv li").addClass("col-xs-2");
		}
		// if ($(window).width() < 768) $("#info-idea").css("margin-top", 0);
		// if ($(window).width() > 768) {
		// 	$("#info-idea").css("margin-top", $(".header-page").height() + 80);
		// 	$("#viewIdea .tabDiv li").removeClass("col-xs-6");
		// 	$("#viewIdea .tabDiv li").removeClass("col-xs-3");
		// 	$("#viewIdea .tabDiv li").removeClass("col-xs-2");
		// }
	}

	$(window).resize(function() {
		if ($(window).width() < 400) {
			$("#viewIdea .tabDiv li").removeClass("col-xs-6");
			$("#viewIdea .tabDiv li").removeClass("col-xs-3");
			$("#viewIdea .tabDiv li").removeClass("col-xs-2");
			$("#viewIdea .tabDiv li").addClass("col-xs-6");
		}
		if ($(window).width() >= 400 && $(window).width() < 520) {
			$("#viewIdea .tabDiv li").removeClass("col-xs-6");
			$("#viewIdea .tabDiv li").removeClass("col-xs-3");
			$("#viewIdea .tabDiv li").removeClass("col-xs-2");
			$("#viewIdea .tabDiv li").addClass("col-xs-3");
		}
		if ($(window).width() >= 520 && $(window).width() < 768) {
			$("#viewIdea .tabDiv li").removeClass("col-xs-6");
			$("#viewIdea .tabDiv li").removeClass("col-xs-3");
			$("#viewIdea .tabDiv li").removeClass("col-xs-2");
			$("#viewIdea .tabDiv li").addClass("col-xs-2");
		}
		if ($(window).width() < 768) $("#info-idea").css("margin-top", 0);
		if ($(window).width() >= 768) {
			$("#viewIdea .tabDiv li").removeClass("col-xs-6");
			$("#viewIdea .tabDiv li").removeClass("col-xs-3");
			$("#info-idea").css("margin-top", $(".header-page").height() + 80);
			$("#viewIdea .tabDiv li").removeClass("col-xs-2");
		}
	});

	//-------------------------------------------------------------------

	//--------------------Function that updates Idea Info-------------------------

	function checkPhase() {
		if (ideaObj.get("Fase") >= 2) {
			$("#menuIdea").append(
				'<li role="presentation"><a id="estBtn" href="#est" data-toggle="tab">Estimations</a></li>'
				);
			$("#my-tab-content").append('<div class="tab-pane" id="est">' + '</div>');
		}
	}

	function getPer(lcount, lpos) {
		var CalcNeg = lpos - lcount;
		var CalcPos = lpos;
		var color = "#8BC163";
		var result = {
			Likes: CalcPos,
			Dislikes: CalcNeg
		};

		var likesTotal = CalcPos + Math.abs(CalcNeg);
		var average = CalcPos + CalcNeg;

		$(".number-average").html("Total: "+average);

		if (likesTotal > 0) {
			color = "#8BC163";
			result.Likes = (CalcPos / likesTotal * 100);
			result.Dislikes = (CalcNeg / likesTotal * 100);
		}

		if (Math.abs(result.Likes) >= Math.abs(result.Dislikes)) {
			$(".demo-5").data("percent", result.Likes);
			$(".avr-perc").css("color", color);
		} else {
			$(".demo-5").data("percent", Math.abs(result.Dislikes));
			color = "#F07467";
			$(".avr-perc").css("color", color);
		}
		$('.demo-5').html("");
		$('.demo-5').percentcircle({
			animate: true,
			diameter: 100,
			guage: 3,
			coverBg: '#fff',
			bgColor: '#efefef',
			fillColor: color,
			percentSize: '18px',
			percentWeight: '20px'
		});
	}

	function setLikeDislike(lcount, lpos) {
		// var CalcNeg = (likesTotal - likes) / 2;
		//       var CalcPos = likesTotal - CalcNeg;
		var lneg = lpos - lcount;
		$("#likesSign").html(lpos + " Likes | " + lneg + " Dislikes");
		idea.likeCount = lpos;
		idea.dislikeCount = lneg;
	}

	function statusLike(statusLike) {
		if (statusLike.Like == "You Like this") {
			$("#isLikeSign").css("color", "white");//#A9D294
			$("#isLikeSign").css("text-shadow", "none");
		}
		if (statusLike.Dislike == "You Dislike this") {
			$("#isDislikeSign").css("color", "white");//#F8AEA8
			$("#isDislikeSign").css("text-shadow", "none");
		}
	}

	function loadAPK(APKid) {
		Parse.Cloud.run('getInstaller', {
			fileID: APKid
		}, {
			success: function(installerAPK) {
				var appLink = installerAPK.get("file");
				$("#DLABtn").attr("href",appLink.url());
				$("#SAEBtnList").attr("data-value",appLink.url());
				$("#shareBtn").attr("data-value",appLink.url());


			},
			error: function(error) {
				console.log(error.message);
			}
		});
	}

	function loadFiles(object) {
		var link = "";

		if (Parse.User.current().id == ideaObj.get("user").id) {
			link = '<a href="" id="' + object.id + '" class="myclass">[X]</a>';
		}
		var ext = object.get("nombre").split('.').pop().toLowerCase();
		if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) !== -1) {
			var profilePhoto = object.get("file");
			//$(".img-cover-idea").css("background-image","url("+profilePhoto.url()+")");
			filesList.image.push(object);
		}
		if ($.inArray(ext, ['apk']) !== -1) {
			var appLink = object.get("file");
			filesList.apk.push(object);
		}
		if (isImage(object.get("file").url()) == true) {
			$("#filelist").append("<li id='li" + object.id +
				"' class='list-group-item'>" + "<img src='" + object.get("file").url() +
				"' height='60' width='auto'><br>" +
				"<a href='" + object.get("file").url() +
				"' download='" + object.get("nombre") + "'>" + object.get("nombre") +
				"</a>" + link + "</li>");
		}else {
			var ext = object.get("nombre").match(/[^\]*(?=\.[^.]+($|\?)/)[0];
				var content ="";

				jQuery.ajax({
					url: object.get("file").url(),
					success: function (result) {
						content = result;
					},
					async: false
				});
				if($.inArray(ext, ['java', 'cs', 'ps1', 'txt', 'py']) == true){
				//alert("in");
				$("#filelist").append("<li id='li" + object.id +
					"' class='list-group-item'><pre><code class='cs'>"+content.toString()+"</code></pre><br>" +
					"<a href='" + object.get("file").url() +
					"' download='" + object.get("nombre") + "'>" + object.get("nombre") +
					"</a>" + link + "</li>");
				hljs.highlightBlock($('code').get(0));
				hljs.lineNumbersBlock($('code').get(0));
			}

			

		} /* reemplazado superior 
		else {
			var ext = object.get("nombre").match(/[^\]*(?=\.[^.]+($|\?)/)[0];
			$("#filelist").append("<li id='li" + object.id +
				"' class='list-group-item'><img src='https://placeholdit.imgix.net/~text?txtsize=150&txt="+ ext +"&w=400&h=300' height='60' width='auto'><br>" +
				"<a href='" + object.get("file").url() +
				"' download='" + object.get("nombre") + "'>" + object.get("nombre") +
				"</a>" + link + "</li>");

		} 
		reemplazado fin*/
	}
	//----------------------------------------------------------------------------

	//--------------------Graph related-------------------------

	function getLikes() {
		Parse.Cloud.run('getCountryLikes', {
			pLike: paisLike,
			ideaID: ideaObj.id
		}, {
			success: function(pLike) {
				paisLike = pLike;
				graph();
			},
			error: function(error) {
				console.log(error.message);
			}
		});
	}

	$('#graphFix2').click(function(e) {
		e.preventDefault();
		$(this).tab('show');
		graph();
	});

	function graph() {
		$("#morris-bar-chart").html("");
		var catPais = [];
		var LikeData = [];
		var DislikeData = [];
		for (var i = 0; i < paisLike.length; i++) {
			catPais.push(paisLike[i].name);
			LikeData.push(paisLike[i].like);
			DislikeData.push(paisLike[i].dislike);
		}
		$('#morris-bar-chart').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: "Likes on the Idea"
			},
			subtitle: {
				text: 'Likes vs Dislikes'
			},
			xAxis: {
				categories: catPais,
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: "Total"
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:f} </b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					stacking: 'normal',
					dataLabels: {
						enabled: true,
						color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) ||
						'white',
						style: {
							textShadow: '0 0 3px black'
						}
					}
				}
			},
			series: [{
				name: "Like",
				data: LikeData,
				color: "green"
			}, {
				name: "Dislike",
				data: DislikeData,
				color: "red"
			}]
		});
		// scrollToElement($("#morris-bar-chart"));
	}

	var arcArray = [];
	var dataP = {};
	$('#graphCov').click(function(e) {
		e.preventDefault();
		$(this).tab('show');
		if (arcArray.length == 0) dataCoverage();
		else coverage();
	});

	function dataCoverage() {
		Parse.Cloud.run("getAnalyticsCoverage", {
			ideaID: idea.id,
			device: "web"
		}, {
			success: function(locResp) {
				console.log("COVERAGE HERE----");
				console.log(locResp);
				var pais = [];
				var countPais = [];
				var bubblesArray = [];
				var originGpoint = locResp.origin;
				var response = locResp.destinations;
				var countries = Datamap.prototype.worldTopo.objects.world.geometries;
				for (var i = 0; i < response.length; i++) {
					for (var t = 0; t < 1; t++) {
						var gp = response[i].get("gpoints")[t];
						var currentC = response[i].get("countries")[t];
						if (gp != null && gp.toString().indexOf("ParseGeoPoint") > -1) gp =
							castGeoPoint(gp);
						if (gp != null) {
							var cid = "undefined";
							for (var x = 0, j = countries.length; x < j; x++) {
								//console.log(countries[i].id, countries[i].properties.name)
								if (countries[x].properties.name == currentC) cid = countries[x].id;
							}
							if (currentC == "United States") cid = "USA";
							if (pais.length == 0 || pais.indexOf(cid) == -1) {
								pais.push(cid);
								countPais.push(1);
								bubblesArray.push({
									name: currentC,
									latitude: gp.latitude,
									longitude: gp.longitude,
									radius: 1,
									fillKey: 'lt50'
								});
								if (ideaObj.get("locationCountry") != currentC) {
									arcArray.push({
										origin: {
											latitude: originGpoint.latitude,
											longitude: originGpoint.longitude
										},
										destination: {
											latitude: gp.latitude,
											longitude: gp.longitude
										}
									});
								}
							} else {
								countPais[pais.indexOf(cid)]++;
								if (countPais[pais.indexOf(cid)] < 15) {
									bubblesArray[pais.indexOf(cid)].radius = 5;
									bubblesArray[pais.indexOf(cid)].fillKey = 'lt50';
								} else {
									bubblesArray[pais.indexOf(cid)].radius = 18;
									bubblesArray[pais.indexOf(cid)].fillKey = 'gt50';
								}
							}
							//console.log(data.result
						}
					}
				}
				for (var i = 0; i < pais.length; i++) {
					var c = 0;
					var values = ['LOW', 'MEDIUM', 'HIGH'];
					if (countPais[i] > 3 && countPais[i] < 8) c = 1;
					if (countPais[i] >= 8) c = 2;
					dataP[pais[i]] = {
						fillKey: values[c],
						numberOfThings: countPais[i]
					};
				}
				coverage();
			},
			error: function(error) {
				console.log("Error" + error.message);
			}
		});
}

function coverage() {
	$("#containermap").html("");
	var map = new Datamap({
		scope: 'world',
		element: document.getElementById('containermap'),
		projection: 'mercator',
		height: 450,
		fills: {
			HIGH: 'rgb(46, 117, 182)',
			LOW: 'rgb(222, 235, 247)',
			MEDIUM: 'rgb(157, 195, 230)',
			UNKNOWN: 'rgb(0,0,0)',
			defaultFill: 'rgb(226, 217, 217)'
		},
		data: dataP,
		geographyConfig: {
			popupTemplate: function(geo, data) {
				return ['<div class="hoverinfo"><strong>', 'Number of things in ' +
				geo.properties.name, ': ' + ((data != null) ? data.numberOfThings :
					0), '</strong></div>'
				].join('');
			}
		}
	});
	map.arc(arcArray, {
		strokeWidth: 3,
		strokeColor: 'rgb(255, 0, 0)'
	});
		// var omap = new Datamap({
		//        scope: 'world',
		//        element: document.getElementById('containerOthermap'),
		//        projection: 'mercator',
		//        height: 450,
		//        fills: {
		//          defaultFill: '#f0af0a',
		//          lt50: 'rgba(0,244,244,0.9)',
		//          gt50: 'red'
		//        },
		//        geographyConfig: {
		//            popupTemplate: function(geo, data) {
		//                return ['<div class="hoverinfo"><strong>',
		//                        'Number of things in ' + geo.properties.name,
		//                        ': ' + ((data != null)? data.numberOfThings : 0),
		//                        '</strong></div>'].join('');
		//            }
		//        }
		//      })
		//      //sample of the arc plugin
		//      omap.arc(arcArray, {strokeWidth: 3,
		//         strokeColor: 'rgb(255, 0, 0)'});
		//       //bubbles, custom popup on hover template
		//     omap.bubbles(bubblesArray, {
		//       popupTemplate: function(geo, data) {
		//         return "<div class='hoverinfo'>It is " + data.name + "</div>";
		//       }
		//     });
		// scrollToElement($("#coverage"));
		$("#divCove").css("display", "none");
	}

	var xArray = [];
	var dataArray = [];
	var resultArray = [];

	$('#graphRet').click(function(e) {
		e.preventDefault();
		$(this).tab('show');
		if (dataArray.length == 0) dataRetention();
		else retention();
	});

	function dataRetention() {
		Parse.Cloud.run("getAnalyticsRetention", {
			ideaID: idea.id
		}, {
			success: function(response) {
				if (response.length > 0) {
					var datoNow = new Date(response[response.length - 1].rt.get(
						"creation"));
					var datoFin = new Date();
					datoNow = new Date(datoNow.getUTCFullYear(), datoNow.getUTCMonth(),
						datoNow.getUTCDate(), datoNow.getUTCHours(), datoNow.getUTCMinutes(),
						datoNow.getUTCSeconds());
					datoFin = new Date(datoFin.getUTCFullYear(), datoFin.getUTCMonth(),
						datoFin.getUTCDate(), datoFin.getUTCHours(), datoFin.getUTCMinutes(),
						datoFin.getUTCSeconds());
					for (var i = 0; i < response.length; i++) {
						var now = response[i].rt.get("creation");
						var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(), now
							.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds()
							);
						resultArray.push(now_utc.toLocaleDateString());
					}
					var now = response[response.length - 1].rt.get("creation");
					var begin = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),
						now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
					//begin.setDate(begin.getDate()+1);
					while (begin < datoFin) {
						xArray.push(begin.toLocaleDateString());
						begin.setDate(begin.getDate() + 1);
					}
					var lengtho = xArray.length;
					var maxGraph = [];
					var lastMax = 0;
					for (var i = 0; i < response.length; i++) {
						var ob = response[i];
						var min = 0;
						var max = 0;
						if (ob.rd.length == 0) {
							var day = ob.rt.get("creation");
							var dayUTC = new Date(day.getUTCFullYear(), day.getUTCMonth(), day
								.getUTCDate(), day.getUTCHours(), day.getUTCMinutes(), day.getUTCSeconds()
								);
							dayUTC.setDate(dayUTC.getDate() + 1);
							var index = xArray.lastIndexOf(dayUTC.toLocaleDateString());
							dataArray.push([index, i, (0).toFixed(2)]);
							max = index;
						} else
						for (var j = 0; j < ob.rd.length; j++) {
							var day = ob.rd[j].get("day");
							var dayUTC = new Date(day.getUTCFullYear(), day.getUTCMonth(),
								day.getUTCDate(), day.getUTCHours(), day.getUTCMinutes(), day.getUTCSeconds()
								);
							var p = ob.rt.get("newUsers").length;
							var d = ob.rd[j].get("activeUsers");
							var r = ((d / p) * 100).toFixed(2);
								//var index = (lengtho - ob.rd.length) + j;
								var index = xArray.lastIndexOf(dayUTC.toLocaleDateString());
								dataArray.push([index, i, r]);
								while (min + 1 != index && j > 0) {
									min++;
									dataArray.push([min, i, (0).toFixed(2)]);
								}
								min = index;
								max = index;
							}
							if (max > lastMax) lastMax = max;
							maxGraph.push(max);
						}
						for (var i = 0; i < maxGraph.length; i++) {
							var start = maxGraph[i] + 1;
							for (var j = start; j < lastMax + 1; j++) {
								dataArray.push([j, i, (0).toFixed(2)]);
							};
						};
						retention();
					}
				},
				error: function(error) {
					console.log(error.message);
				}
			});
}

function retention() {
	$("#containerRet").html("");
	$('#containerRet').highcharts({
		chart: {
			type: 'heatmap',
			marginTop: 40,
			marginBottom: 80
		},
		title: {
			text: 'Retention Graph'
		},
		xAxis: {
			categories: xArray
		},
		yAxis: {
			categories: resultArray,
			title: null
		},
		colorAxis: {
			min: 0,
			minColor: '#FFFFFF',
			maxColor: Highcharts.getOptions().colors[0]
		},
		legend: {
			align: 'right',
			layout: 'vertical',
			margin: 0,
			verticalAlign: 'top',
			y: 25,
			symbolHeight: 280
		},
		tooltip: {
			formatter: function() {
				return 'Of the users who first sign up in <b>' + this.series.yAxis.categories[
				this.point.y] + '</b> <br><b>' + this.point.value +
				'</b>% were active in <br><b>' + this.series.xAxis.categories[this.point
				.x] + '</b>';
			}
		},
		series: [{
			name: 'Sales per employee',
			borderWidth: 1,
			data: dataArray,
			dataLabels: {
				enabled: true,
				color: '#000000'
			}
		}]
	});
		// scrollToElement($("#containerRet"));
	}

	// $('#graphAdo').click(function(e) {
	// 	e.preventDefault();
	// 	$(this).tab('show');
	// 	adoption();
	// });

var datosComment = [];

function adoption() {
	Parse.Cloud.run("getAnalyticsAdoption", {
		ideaID: idea.id
	}, {
		success: function(response) {
			datosComment = response;
			movePlot(0);
			var opHTML = "<option value='0'>Daily Active Users</option>";
			opHTML += "<option value='1'>Daily New Users</option>";
			opHTML += "<option value='2'>Minutes Spent</option>";
			opHTML += "<option value='3'>New Users / Active Users</option>";
			$("#selectPlot").html(opHTML);
		},
		error: function(error) {
			console.log(error.message);
		}
	});
}

$("#selectPlot").change(function() {
	movePlot($("#selectPlot").val());
});

var automationData = [];

$.ajax({ 
	type: 'GET',
	url: 'https://automationplatform.azurewebsites.net/api/innoteklink?id='+key+'',
	dataType: 'json',
	contentType: "application/json; charset=utf-8",
	success: function (data) {
		automationData = data;
		console.log("data: ");
		console.log(automationData.activeUsers.xAxis);
	}
});

function movePlot(id) {
	var layout = ["active users", "new users", "minutes spent"];
	var categories = [];
	var dataAdoption = [];
	var dataLabel = datosComment[id].label;
	var dataGraph = datosComment[id].graph;
	var tableComment = datosComment[id].data;
	for (var i = 0; i < tableComment.length; i++) {
		categories.push(new Date(tableComment[i][0]).toLocaleDateString());
		dataAdoption.push(tableComment[i][1]);
	}
	//console.log(dataAdoption);
	console.log(automationData.activeUsers.xAxis);
	$('#flot-line-chart').highcharts({
		chart: {
			type: dataGraph
		},
		title: {
			text: dataLabel
		},
		subtitle: {
			text: 'Last Month'
		},
		xAxis: {
			categories: automationData.activeUsers.xAxis,
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: dataLabel
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			'<td style="padding:0"><b>{point.y:f} </b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0,
				borderWidth: 0
			}
		},
		series: [{
			name: dataLabel,
			data: automationData.activeUsers.data
		}]
	});
		// scrollToElement($("#flot-line-chart"));
	}

	//----------------------------------------------------------

	$('#childrenNodes').click(function(e) {
		e.preventDefault();
		$(this).tab('show');
		childNodes(key);
	});

	function childNodes(iID)
	{
		console.log("iID");
		console.log(iID);
		Parse.Cloud.run('getChildrenNodes',{ideaID: iID}, {
			success: function(results){
				if(results.children.length <= 0 && results.parent == undefined)
				{
					$("#myholder").html("<h1>No publications found</h1>");
				}
				else
				{
					var graph = new joint.dia.Graph;
					var paper = new joint.dia.Paper({
						el: $('#myholder'),
						width: 1024,
						height: 900,
						model: graph,
						gridSize: 1
					});

					paper.on('element:pointerdown', function(cellView, evt, x, y) {
				    	// var allLinks = graph.getLinks();
				    	// var allElements = graph.getElements();
				    	// var allOfElementLinks = Enumerable.From(allLinks).
				    	// Where(function(x){
				    	// 	return x.attributes.source.id == cellView.model.id ||
				    	// 	x.attributes.target.id == cellView.model.id;
				    	// }).ToArray();
				    	// var allOfElements = new Array();
				    	// for (var i = 0; i < allOfElementLinks.length; i++) {
				    	// 	var elemento = allOfElementLinks[i].attributes;
				    	// 	if(allOfElements.length == 0 || allOfElements.indexOf(elemento.source.id) <= -1)
				    	// 		allOfElements.push(elemento.source.id);

				    	// 	if(allOfElements.length == 0 || allOfElements.indexOf(elemento.target.id) <= -1)
				    	// 		allOfElements.push(elemento.target.id);
				    	// };
				    	// for (var i = 0; i < allLinks.length; i++) {
				    	// 	if(allOfElementLinks.indexOf(allLinks[i]) <= -1)
				    	// 		allLinks[i].remove();
				    	// };
				    	// for (var i = 0; i < allElements.length; i++) {
				    	// 	if(allOfElements.indexOf(allElements[i].id) <= -1)
				    	// 		allElements[i].remove();
				    	// };
				    	// console.log(cellView.model.attributes.attrs.ideaID);
				    	// // $("#myholder").html("");
				    	// childNodes(cellView.model.attributes.attrs.ideaID);

				    });

joint.shapes.custom = {};
					// The following custom shape creates a link out of the whole element.
					joint.shapes.custom.ElementLink = joint.shapes.basic.Rect.extend({
					    // Note the `<a>` SVG element surrounding the rest of the markup.
					    markup: '<a><g class="rotatable"><g class="scalable"><rect/></g><text/></g></a>',
					    defaults: joint.util.deepSupplement({
					    	type: 'custom.ElementLink'
					    }, joint.shapes.basic.Rect.prototype.defaults)
					});
					// The following custom shape creates a link only out of the label inside the element.
					joint.shapes.custom.ElementLabelLink = joint.shapes.basic.Rect.extend({
					    // Note the `<a>` SVG element surrounding the rest of the markup.
					    markup: '<g class="rotatable"><g class="scalable"><rect/></g><a><text/></a><image/></g>',
					    defaults: joint.util.deepSupplement({
					    	type: 'custom.ElementLabelLink',
					    	size: { width: 150, height: 60 },
					    	attrs: {
					    		'rect': { fill: '#FFFFFF', stroke: 'black', width: 100, height: 60 }
					            // 'image': { 'ref-x': 2, 'ref-y': 2, ref: 'rect', width: 16, height: 16 }
					        }
					    }, joint.shapes.basic.Rect.prototype.defaults)
					});

					var rect = new joint.shapes.basic.Rect({
						position: { x: window.outerWidth/3, y: window.outerHeight/4 },
						size: { width: 150, height: 30 },
						attrs: { rect: { fill: '#F04E4E',stroke: "#BB3A3A" }, text: { text: results.idea.get("titulo"), fill: 'white' } }
					});
					rect.attr({"ideaID": results.idea.id});
					if(results.parent != undefined)
					{
						var rect2 = new joint.shapes.custom.ElementLabelLink({
							position: { x: window.outerWidth/3, y: window.outerHeight/4 },
							size: { width: 250, height: 30 },
							attrs: { rect: { fill: '#985CB8', stroke: "#693F80" },
							a: { 'xlink:class': 'moveRelated', 'xlink:show': 'new', cursor: 'pointer' },
							image: { 'xlink:href': 'https://placehold.it/16x16' },
							text: { text: results.parent.get("titulo")+" - (Parent)", fill: 'white' } }
						});
						rect2.attr({"ideaID": results.parent.id});
						rect2.translate(-300,-100);
						var link = new joint.dia.Link({
							source: { id: rect.id },
							target: { id: rect2.id },
							attrs: {}
						});
						link.attr({"isParent": true});
						graph.addCells([rect, rect2, link]);
					}

					for (var i = 0; i < results.children.length; i++) {
						var node = results.children[i];
						var rect2 = new joint.shapes.custom.ElementLabelLink({
							position: { x: window.outerWidth/3, y: window.outerHeight/4 },
							size: { width: 150, height: 30 },
							attrs: { rect: { fill: '#4E6BF0', stroke: "#3A4BBB"},
							a: { 'xlink:href': 'viewIdea.html?k='+node.id, 'xlink:show': 'new', cursor: 'pointer' },
							image: { 'xlink:href': 'https://placehold.it/16x16' },
							text: { text: node.get("titulo"), fill: 'white' } }
						});
						rect2.attr({"ideaID": node.id});
						rect2.translate(300,100*i);
						var link = new joint.dia.Link({
							source: { id: rect.id },
							target: { id: rect2.id },
							attrs:{}
						});
						link.attr({"isParent": false});
						graph.addCells([rect, rect2, link]);
					};
				}
			},
			error: function(error){
				console.log(error);
			}
		});
}

function doCarousel(iID)
{
	Parse.Cloud.run('getCarousel',{ideaID: iID},{
		success: function(listCarousel){
			console.log("carousel");
			if(iID != "")
			{
				$("#description-text").hide();
				$("#ideaCarousel").show();
				for (var i = 0; i < listCarousel.length; i++) {
					var archivoCarousel = listCarousel[i];
					$(".carousel-indicators").append('<li data-target="#myCarousel" data-slide-to="'+i+'" '+((i==0)? 'class="active"': "")+'></li>');
					var divCarousel = "";
					switch(archivoCarousel.type)
					{
						case "image":
						divCarousel = 	'<div class="item '+((i==0)? 'active': "")+'">'+
						'<img src="'+archivoCarousel.url+'"">'+
						'</div>';
						break;
						case "video":
						divCarousel = 	'<div class="item '+((i==0)? 'active': "")+'">'+
						'<video controls autoplay">'+
						'<source src="'+archivoCarousel.url+'" type="video/mp4">'+
						'Your browser does not support the video tag.'+
						'</video>'+
						'</div>';
						break;
					}
					$(".carousel-inner").append(divCarousel);
				};
			}
			console.log(listCarousel);

			if(! $(".carousel-inner").children().length > 0)
			{
				$(".left.carousel-control").css("display", "none");
				$(".right.carousel-control").css("display", "none");
			}
		},
		error: function(error){
			console.log(error);
		}
	});
}

$('#tabs').tab();
if (key != null) {
	Parse.Cloud.run("unaIdeaFull", {
		userID: Parse.User.current().id,
		oID: key
	}, {
		success: function(ideaResult) {
			console.log(ideaResult);
			ideaObj = ideaResult[0].idea;
			idea.id = ideaObj.id;
			idea.name = ideaObj.get("titulo");
			idea.descr = ideaObj.get("descripcion");
			idea.isPrivate = ideaObj.get("esPrivado");
			idea.tags = ideaObj.get("tags");
			if (!ideaObj.has("hasAPK")) {
				$("#graphCov").parent().remove();
				$("#graphAdo").parent().remove();
				$("#graphRet").parent().remove();
				$("#shareBtn").parent().remove();
				$("#DLABtn").remove();
				$("#SAEBtnList").remove();
			}

			$("#title-idea").prepend(idea.name);
			$("#commentsSign").append(ideaObj.attributes.commentCount +
				" Comments");

			userIdea = ideaObj.get("user").id;
				// $("#user-idea").html(ideaObj.get("user").get("name"));

				Parse.Cloud.run('getUser', {
					userID: Parse.User.current().id
				}, {

					success: function(result) {
						console.log(result.get("userFollowing"));
						if (result.get("userFollowing") === undefined) {
							$("#followBtnIdea").removeClass("following");
							$("#followBtnIdea").html("Follow");
							console.log("uno");
						}
						if (result.get("userFollowing") !== undefined && result.get("userFollowing").indexOf(userIdea) <= -1) {
							$("#followBtnIdea").removeClass("following");
							$("#followBtnIdea").html("Follow");
							console.log("dos");
						}
						if (result.get("userFollowing") !== undefined && result.get("userFollowing").indexOf(userIdea) > -1) {
							$("#followBtnIdea").html("Following");
							$("#followBtnIdea").addClass("following");
							console.log("tres");
						}
					},
					error: function(error) {
						console.log(error.message);
					}
				});


				$(".location-label").append((ideaObj.get("user").get("locationCity")!= undefined && ideaObj.get("user").get("locationCity").length > 0)? ideaObj.get("user").get("locationCity") : "Monterrey");
				$("#viewIdea .user-info").append(
					"<span class='profile-img' style= 'background-image: url(" + ideaObj
						.attributes.user.attributes.image.url() + ")'></span>");
				$('.user-idea').attr('id', ideaObj.get("user").id);
				$(".user-idea").html(ideaObj.get("user").get("name"));
				$("#dateSign").html("Created on " + printDate(ideaObj.createdAt.toString()) + " by <a class='user' href='#' id='user-idea'>" + ideaObj.get("user").get("name") + "</a>");
				// $("#dateSign").html("Created on " + printDate(ideaObj.createdAt.toString()));
				doCarousel(ideaObj.id);
				// if (ideaObj.has("photoUrl")) {
				// 	$("#description-text").append('<div class="img-cover-idea"> </div>' +
				// 		idea.descr);
				// 	$(".img-cover-idea").css("background-image", "url(" + ideaObj.get(
				// 		"photoUrl") + ")");
				// 	if(idea.id == "m4SfH1Eunv")
				// 	{
				// 		$("#descr-text").show();
				// 		$("#descr-text").html(idea.descr);
				// 	}
				// } else {
				// 	$("#description-text").append(idea.descr);
				// }
				$("#descr-text").show();
				$("#descr-text").html(idea.descr);
				if (ideaObj.attributes.fileCount < 1) {
					$("#attachTab").parent().remove();
				}
				if (ideaResult[0].roster.length < 1 && ideaObj.attributes.user.id !==
					Parse.User.current().id) {
					$("#rosterBtn").parent().remove();
			}
			for (var i = 0; i < idea.tags.length; i++) {
				$(".tags-label div").append("<a href='#' class='tag'>" + idea.tags[i] +
					'</a>');
			}
			switch (ideaObj.attributes.category) {
				case 0:
				$("#categoryIdea").html("Idea").addClass("categoryIdea");
				break;
				case 1:
				$("#categoryIdea").html("Need").addClass("categoryNeed");
				break;
				case 2:
				$("#categoryIdea").html("Application").addClass("categoryApp");
				break;
				case 3:
				$("#categoryIdea").html("POC").addClass("categoryPOC");
				break;
				case 4:
				$("#categoryIdea").html("Microservice").addClass("categoryMicro");
				break;
				case 5:
				$("#categoryIdea").html("Other").addClass("categoryOther");
				break;
			}

			var File = Parse.Object.extend("Archivo");
			var query = new Parse.Query(File);
			query.equalTo("idea", ideaObj);
			setLikeDislike(ideaObj.get("likeCount"), ideaObj.get("likePos"));
			getPer(ideaObj.get("likeCount"), ideaObj.get("likePos"));
			getLikes();
			query.find({
				success: function(results) {
					setSizes();
						// Do something with the returned Parse.Object values
						$("#filesSign").html(results.length);
						for (var i = 0; i < results.length; i++) {
							var object = results[i];
							loadFiles(object);
						}
						if (ideaObj.has("hasAPK")) loadAPK(ideaObj.get("hasAPK"));
						//IF user is creator or in group
						checkPhase();
						if (Parse.User.current().id == ideaObj.get("user").id ||
							ideaResult[0].roster.lastIndexOf(Parse.User.current().id) > -1) {
							challengeRespond();
						var imgSection = "";
						if (filesList.image.length > 0) {
							imgSection +=
							'<p>Multiple image files have been found, please select the one that will appear as cover</p>';
							for (var i = 0; i < filesList.image.length; i++) {
								imgSection += '<div id="radio' + filesList.image[i].id +
								'" class="radio">' +
								'<label><input type="radio" name="optradioImg" value="' +
								filesList.image[i].get("file").url() + '" >' + filesList.image[
								i].get("nombre") + '</label>' + '<a data-button="' +
								filesList.image[i].id +
								'" class="deleteCoverImage">Delete</a>' + '</div>';
							};
						} else {
							imgSection += 'You dont have any image files';
						}
						var apkSection = "";
						if (filesList.apk.length > 0) {
							apkSection +=
							'<p>Multiple .apk files have been found, please select the one that will appear as your android installer</p>';
							for (var i = 0; i < filesList.apk.length; i++) {
								apkSection += '<div class="radio">' +
								'<label><input type="radio" name="optradioAPK" value="' +
								filesList.apk[i].id + '" >' + filesList.apk[i].get("nombre") +
								'</label>' + '</div>';
							};
						} else {
							apkSection += 'You dont have any apk files';
						}
						//MODAL - LINK TO AUTOMATION-------------------------------------------------------------------
						// Get the modal
						$('#autoLink').show();
						var modal2 = document.getElementById('auLinkModal');
						// Get the button that opens the modal
						var btn2 = document.getElementById("AutomationLink");
						// Get the <span> element that closes the modal
						var span2 = document.getElementsByClassName("close")[0];
						// When the user clicks on the button, open the modal 
						btn2.onclick = function() {
							modal2.style.display = "block";
						}
						// When the user clicks on <span> (x), close the modal
						span2.onclick = function() {
							modal2.style.display = "none";
						}
						// When the user clicks anywhere outside of the modal, close it
						window.onclick = function(event) {
							if (event.target == modal2) {
								modal2.style.display = "none";
							}
						}

						var linking = document.getElementById('auLink');
						// var recordedAction = document.getElementById('recordedAction')[0];
						// var auKey = document.getElementById('auKey')[0];

						linking.onclick = function(){
							var recordedAction = $('#recordedAction').val();
							var auKey = $('#auKey').val();
							//alert(recordedAction + " " + auKey);
							var inlink = {InnotekKey: key, Key: auKey, Action: recordedAction};
							$.ajax({ 
								type: 'POST',
								url: 'https://automationplatform.azurewebsites.net/api/innoteklink',
								dataType: 'json',
								data: JSON.stringify(inlink),
								contentType: "application/json; charset=utf-8",
								success: function (data) {
									alert(data);
								}
							});
						}



						$("#showEdit").append(
							'<a href="" id="changeDescr" data-toggle="modal" data-target="#modalDescr" class="">[Edit Idea]</a>'
							);
						var descrModal = '<!-- Modal -->' +
						'<div class="modal fade" id="modalDescr" role="dialog">' +
						'<div class="modal-dialog">' + '<!-- Modal content-->' +
						'<div class="modal-content">' + '<ul class="nav nav-tabs editIdeaModal">' +
						'<li role="presentation" class="active"><a href="#editideatab" data-toggle="tab">Edit Idea</a></li>' +
						'<li role="presentation"><a href="#chimgtab" data-toggle="tab">Change Image </a></li>' +
						'<li role="presentation"><a href="#chapktab" data-toggle="tab">Change Android File </a></li>' +
						'</ul>' + '<div class=" tab-content">' +
						'<div  class="tab-pane active" id="editideatab">' +
						'<div class="modal-header">' +
						'<button type="button" class="close" data-dismiss="modal">&times;</button>' +
						'<h4 class="modal-title">Edit idea</h4>' + '</div>' +
						'<div class="modal-body">' +
						'<div class="panel panel-default">' +
						'<div class="panel-body">' +
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">' +
						'<label for="">Description</label>' +
						'	<span class=\'error-descr\'>Write a description for the post</span>' +
						'<textarea id="descrText" class="form-control" rows="3" required="required">' +
						idea.descr + '</textarea>' + '</div>' +
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">' +
						'<label for="">Tags</label>' +
						'<span class=\'error-tag\'>At least one tag</span>' +
						'<input type="text" class="form-control clear" id="smallEdit" value="" placeholder="Enter tags"/>' +
						'</div>' +
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">' +
						'<input type="file" id="photoEdit" multiple>' + '</div>' +
						'</div>' + '</div>' + '</div>' + '<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
						'<a  id="finishBtnEdit" class="text-right btn btn-success">Save</a>' +
						'</div>' + '</div>' +
						'<div  class="tab-pane" id="chimgtab">' +
						'<div class="modal-header">' +
						'<button type="button" class="close" data-dismiss="modal">&times;</button>' +
						'<h4 class="modal-title">Select Image file</h4>' + '</div>' +
						'<div class="modal-body">' +
						'<div class="panel panel-default">' +
						'<div class="panel-body">' + imgSection + '</div>' + '</div>' +
						'</div>' + '<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
						'<a  id="saveCoverImage" class="text-right btn btn-success">Save</a>' +
						'</div>' + '</div>' +
						'<div  class="tab-pane" id="chapktab">' +
						'<div class="modal-header">' +
						'<button type="button" class="close" data-dismiss="modal">&times;</button>' +
						'<h4 class="modal-title">Select APK file</h4>' + '</div>' +
						'<div class="modal-body">' +
						'<div class="panel panel-default">' +
						'<div class="panel-body">' + apkSection + '</div>' + '</div>' +
						'</div>' + '<div class="modal-footer">' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
						'<a  id="saveAPKFile" class="text-right btn btn-success">Save</a>' +
						'</div>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>';
						$("#allModals").append(descrModal);
						Parse.Cloud.run('getAllTags', {
							cantidad: 200,
							skip: 0
						}, {
							success: function(result) {
								for (var i = 0; i < result.length; i++) {
									arr.push({
										value: result[i]
									});
								}
								engine = new Bloodhound2({
									local: arr,
									datumTokenizer: function(d) {
										return Bloodhound.tokenizers.whitespace(d.value);
									},
									queryTokenizer: Bloodhound.tokenizers.whitespace
								});
								engine.initialize();
								$('#smallEdit').tokenfield({
									delimiter: false,
									typeahead: [{
										hint: true,
										highlight: true,
										minLength: 1
									}, {
										source: engine.ttAdapter()
									}]
								}).on('tokenfield:createtoken', function(e) {
									/*	arr = removeByAttr(arr,"value",e.attrs.value.toLowerCase());*/
									var existingTokens = $(this).tokenfield('getTokens');
									if (existingTokens.length) {
										$.each(existingTokens, function(index, token) {
											if (token.value.toLowerCase() === e.attrs.value.toLowerCase()) {
												e.preventDefault();
											}
										});
									}
								});
								$('#smallEdit').tokenfield('setTokens', idea.tags);
							},
							error: function(error) {
								console.log(error);
							}
						});
$("#saveCoverImage").click(function() {
	$("#saveCoverImage").html("Saving Cover Image...");
	var url = $('input[name=optradioImg]:checked').val();
	ideaObj.set("photoUrl", url);
	ideaObj.save(null, {
		success: function(savedIdea) {
			console.log(savedIdea);
			$(".img-cover-idea").css("background-image", "url(" +
				url + ")");
			$("#saveCoverImage").html("Save");
		},
		error: function(error) {
			console.log(error.message);
		}
	});
});
$(".deleteCoverImage").click(function() {
	var archID = $(event.target).attr("data-button");
	Parse.Cloud.run("deleteCoverImage", {
		archivoID: archID
	}, {
		success: function(photoR) {
			$(".img-cover-idea").css("background-image", "url(" +
				photoR + ")");
			$("#radio" + archID).remove();
			console.log(photoR);
		},
		error: function(error) {
			console.log(error.message);
		}
	});
});
$("#saveAPKFile").click(function() {
	$("#saveAPKFile").html("Saving Cover Image...");
	var id = $('input[name=optradioAPK]:checked').val();
	ideaObj.set("hasAPK", id);
	ideaObj.save(null, {
		success: function(savedIdea) {
			$("#saveAPKFile").html("Save");
		},
		error: function(error) {
			console.log(error.message);
		}
	});
});
$("#finishBtnEdit").click(function() {
	idea.descr = $("#descrText").val().trim();
	idea.tags = getArrayTags($('#smallEdit').tokenfield(
		'getTokens'));
	if (idea.descr.length > 0 && idea.tags.length > 0) {
		$("#finishBtnEdit").html("Procesing...");
		$('#finishBtnEdit').off("click");
		var files = document.getElementById("photoEdit").files;
		Parse.Cloud.run("cambiarDatosIdea", {
			ideaID: idea.id,
			descripcion: idea.descr,
			tagsIdea: idea.tags
		}, {
			success: function(object) {
				if (files.length > 0) {
					metLista(0, files.length, ideaObj);

				} else {
					$("#finishBtnEdit").html("Saved!");
					window.location = "viewIdea.html?k=" + idea.id;
				}
			},
			error: function(error) {
				console.log(error.Message);
			}
		});
	}
	if (idea.descr.length < 1) {
		$(".error-descr").css("display", "block");
	}
	if (idea.descr.length > 0) {
		$(".error-descr").css("display", "none");
	}
	if (idea.tags.length < 1) {
		$(".error-tag").css("display", "block");
	}
	if (idea.tags.length > 0) {
		$(".error-tag").css("display", "none");
	}
});
}
},
error: function(error) {
	console.log("Error: " + error.code + " " + error.message);
}
});
loadComments();
adoption();
statusLike(ideaResult[0].likeStatus);
dataCoverage();
},
error: function(error) {
				// The object was not retrieved successfully.
				// error is a Parse.Error with an error code and message.
				console.log(error.message);
			}
		});
	}// fin if(key!=null)

	var userEstimations = undefined;
	$('body').on('click', '#estBtn', function(e) {
		e.preventDefault();
		if (userEstimations == undefined) Parse.Cloud.run('getUserEstimations', {
			ideaID: ideaObj.id
		}, {
			success: function(estRes) {
				if (estRes.length > 0) userEstimations = estRes[0];
				else {
					var estObj = Parse.Object.extend("Estimacion");
					userEstimations = new estObj();
					userEstimations.set("user", Parse.User.current());
					userEstimations.set("idea", ideaObj);
				}
				userEstimationsPrint();
			},
			error: function(error) {
				console.log(error);
			}
		});
			else userEstimationsPrint();
		});

	function userEstimationsPrint() {
		if (userEstimations != undefined) {
			$("#est").html('<div class="form-group">' +
				'<legend>Estimations</legend>' + '</div>' +
				'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">' +
				'<label for="input-tiempo" class="col-sm-12">How many months would you give this project to develop?</label>' +
				'<input type="number" min="0" name="" id="input-tiempo" class="form-control" value="" required="required" pattern="" title="">' +
				'</div>' + '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">' +
				'<label for="input-dinero" class="col-sm-12">How much money would you give this project as budget?</label>' +
				'<input type="number" min="0" name="" id="input-dinero" class="form-control" value="" required="required" pattern="" title="">' +
				'</div>' + '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">' +
				'<label for="input-caphum" class="col-sm-12">How many people would you need to make this project?</label>' +
				'<input type="number" min="0" name="" id="input-caphum" class="form-control" value="" required="required" pattern="" title="">' +
				'</div>' + '<div class="form-group">' + '<div class="col-sm-12">' +
				'<button id="submit-Est" type="submit" class="btn btn-primary">Submit</button>' +
				'</div>' + '</div>');
if (userEstimations.has("estimaciones")) {
	$("#est .form-group legend").after(
		"<a id='allEstStats'>Look all stats</a>");
	estValues = userEstimations.get("estimaciones");
	$("#input-tiempo").val(estValues.time);
	$("#input-dinero").val(estValues.money);
	$("#input-caphum").val(estValues.people);
}
}
}

$('body').on('click', '#allEstStats', function(e) {
	Parse.Cloud.run('AllEstimations', {
		ideaID: ideaObj.id
	}, {
		success: function(allData) {
			console.log(allData);
			$("#est").html('<div class="row">' + '<legend>Owner</legend>' +
				'<p>Time:' + allData.owner.time + '</p>' + '<p>Money:' + allData.owner
				.money + '</p>' + '<p>People:' + allData.owner.people + '</p>	' +
				'</div>	');
			$("#est").append('<div class="row">' + '<legend>Roster(' + allData.roster
				.count + ')</legend>' + '<p>Time:' + allData.roster.time + '</p>' +
			'<p>Money:' + allData.roster.money + '</p>' + '<p>People:' +
			allData.roster.people + '</p>	' + '</div>	');
			$("#est").append('<div class="row">' + '<legend>Community(' +
				allData.community.count + ')</legend>' + '<p>Time:' + allData.community
			.time + '</p>' + '<p>Money:' + allData.community.money + '</p>' +
			'<p>People:' + allData.community.people + '</p>	' + '</div>	');
		},
		error: function(error) {
			console.log(error);
		}
	});
});

$('body').on('click', '#submit-Est', function(e) {
	var time = $("#input-tiempo").val();
	var money = $("#input-dinero").val();
	var people = $("#input-caphum").val();
	if (time.length > 0 && money.length > 0 && people.length > 0) {
		userEstimations.set("estimaciones", {
			time: time,
			money: money,
			people: people
		});
		userEstimations.save(null, {
			success: function(savedEst) {
				console.log("saved estimations");
			},
			error: function(error) {
				console.log(error.message);
			}
		});
	}
});

function challengeRespond() {
	if (ideaObj.has("challenge")) {
		var challenge_idea = ideaObj.get("challenge");
		if (challenge_idea.get("confPublicada")) {
			var today = new Date();
			var start = challenge_idea.get("fechaInicio");
			var rest = today.getDate() - start.getDate();
			var index = Math.floor(rest / challenge_idea.get("semanas").length);
			console.log(index);
		}
	} else console.log(ideaObj);
}

function metLista(act, max, ideaObject) {
	$("#finishBtnEdit").html("Uploading " + (max - act) + " files....");
	if (act < max) {
		var file = $("#photoEdit").get(0).files[act];
		var name = $("#photoEdit").get(0).files[act].name;
		var parseFile = new Parse.File(name, file);
		parseFile.save().then(function() {});
		var Archivo = Parse.Object.extend("Archivo");
		var archivo = new Archivo();
		archivo.set("file", parseFile);
		archivo.set("nombre", name);
		archivo.set("idea", ideaObject);
		archivo.save(null, {
			success: function(object) {
				act = act + 1;
				metLista(act, max, ideaObject);
			},
			error: function(model, error) {
				alert("Didnt Saved!");
				console.log(error.message);
			}
		});
	} else {
		window.location = "viewIdea.html?k=" + idea.id;
	}
}

function printDate(date) {
	var dateArray = date.split(" ");
	return dateArray[1] + " " + dateArray[2] + " " + dateArray[3];
}

$('body').on('click', 'a.myclass', function(e) {
	e.preventDefault();
		//alert(e.target.id);
		Parse.Cloud.run("quitarArchivo", {
			archivoID: e.target.id
		}, {
			success: function(response) {
				if (response.Status) {
					$("#li" + e.target.id).remove();
					location.reload();
				}
			},
			error: function(error) {
				console.log("Error" + error.message);
			}
		});
	});

$('body').on('click', '#SAEBtnList', function(e) {

	e.preventDefault();
	var url = $(e.target).attr("data-value");
	SendMyself(url);
});

	// $('body').on('click', '#shareBtn', function(e) {
	// 	var url = $(e.target).attr("data-value");
	// 	$("#sendUsersSH").attr("placeholder", "Loading contacts");
	// 	ShareApp(url);
	// });

$(document).ready(function(e) {
	var url = $(e.target).attr("data-value");
	ShareApp(url);
});

function SendMyself(ExUrl){
	Parse.Cloud.run("getUserToken",{userID: Parse.User.current().id},{
		success: function(token){
			try
			{
				console.log(token);
				$("#SAEBtnList").html('Sending mail please wait...');
				var message = '{"Message": {"Subject": "Executable of '+ideaObj.get("titulo")+'", "Body": {"ContentType": "HTML", "Content": "<table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' height=\'100%\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed;margin:0;padding:0;background-color:#f2f2f2;height:100%!important;width:100%!important\'> <tbody><tr> <td align=\'center\' valign=\'top\' style=\'margin:0;padding:0;border-top:5px solid #ffffff;height:100%!important;width:100%!important\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody> <tr> <td align=\'center\' valign=\'top\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed;background-color:#f2f2f2;border-top:0;border-bottom:0\'> <tbody><tr> <td align=\'center\' valign=\'top\' style=\'padding-top:20px;padding-bottom:20px\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'600\' style=\'border-collapse:collapse;table-layout:fixed\'><tbody> <tr> <td align=\'center\' colspan=\'3\' valign=\'top\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed;background-color: #3498db;border-bottom: 2px solid #2e8ece;\'> <tbody><tr> <td align=\'center\' valign=\'top\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody><tr> <td valign=\'top\' style=\'padding-top:20px;padding-bottom:20px\'><table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody> <tr> <td valign=\'top\'> <table align=\'left\' border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'600\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody><tr> <td valign=\'top\' style=\'padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#ffffff;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left\'> <h1 style=\'display:block;margin:0;padding:0;font-family:Helvetica;font-size:26px;font-style:normal;font-weight:bold;line-height:125%;letter-spacing:normal;text-align:left;color:#ffffff!important\'>Hello:</h1> </td> </tr> </tbody></table> </td> </tr> </tbody></table><table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody> <tr> <td valign=\'top\'> <table align=\'left\' border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'600\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody><tr> <td valign=\'top\' style=\'padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#ffffff;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left\'> You can try now our new application, any suggestion or comments please do not hesitate to write us on Productivity.<br><br>Thanks </td> </tr> </tbody></table> </td> </tr> </tbody></table><table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody> <tr> <td style=\'padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px\' valign=\'top\' align=\'center\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' style=\'border-collapse:separate!important;border:2px solid #f2f2f2;border-radius:4px;background-color:#ffffff;table-layout:fixed\'> <tbody> <tr> <td align=\'center\' valign=\'middle\' style=\'font-family:Arial;font-size:16px; padding: 20px;\'> <a title=\'Download Application\' href=\''+ExUrl+'\'=\' style=\'font-weight:bold;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color: #2c3e50;word-wrap:break-word;display:block\' target=\'_blank\'>Download Application</a> </td> </tr> </tbody> </table> </td> </tr> </tbody></table></td> </tr> </tbody></table> </td> </tr> </tbody></table> </td> </tr> </tbody></table></td></tr></tbody></table></td></tr> <tr> <td align=\'center\' valign=\'top\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed;background-color:#f2f2f2;border-top:0;border-bottom:0\'> <tbody><tr> <td align=\'center\' valign=\'top\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'600\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody><tr> <td valign=\'top\' style=\'padding-top:10px;padding-bottom:10px\'></td> </tr> </tbody></table> </td> </tr> </tbody></table> </td> </tr> </tbody></table> </td> </tr> </tbody></table>"}, "ToRecipients": [{"EmailAddress": {"Address": "'+Parse.User.current().get("email")+'"} }], "Attachments": [] }, "SaveToSentItems": "false"}';
				var params = message;
				var endpointUrl = 'https://graph.microsoft.com/beta/me/microsoft.graph.sendMail';

				var xhr = new XMLHttpRequest();
				xhr.open("POST", endpointUrl,true);
						// The APIs require an OAuth access token in the Authorization header, formatted like this: 'Authorization: Bearer <token>'.
						xhr.setRequestHeader("Authorization", "Bearer " + token.graph);
						xhr.setRequestHeader("Content-type", "application/json");
						xhr.setRequestHeader("Content-length", params.length);
						xhr.setRequestHeader("Connection", "close");

						// Process the response from the API.
						xhr.onload = function () {
							if (xhr.status == 202) {
						    //var formattedResponse = JSON.stringify(JSON.parse(xhr.response), undefined, 2);
						    // var formattedResponse = JSON.parse(xhr.response);
						    // console.log(formattedResponse);
						    alert("Message Sent to your Mailbox");
						    $("#SAEBtnList").html('Send App to My Email');
						} else {
							console.log("HTTP " + xhr.status + "<br>" + xhr.response);
						}
					}
						// Make request.
						xhr.send(params);
					}
					catch (err)
					{
						console.log("Exception: " + err.message);
					}
				},
				error: function(error){
					console.log(error.message);
				}
			});
}
var listSendEmails = [];

function ShareApp(ExUrl) {
	$("#appInstallerHolder").val(ExUrl);
	Parse.Cloud.run("getUserToken", {
		userID: Parse.User.current().id
	}, {
		success: function(token) {
			try {
				listSendEmails = [];
					//var endpointUrl = 'https://graph.microsoft.com/beta/me/contacts';
					var endpointUrl = 'https://graph.microsoft.com/beta/users';
					var xhr = new XMLHttpRequest();
					xhr.open("GET", endpointUrl);
					// The APIs require an OAuth access token in the Authorization header, formatted like this: 'Authorization: Bearer <token>'.
					xhr.setRequestHeader("Authorization", "Bearer " + token.graph);
					// Process the response from the API.
					xhr.onload = function() {
						if (xhr.status == 200) {
								//var formattedResponse = JSON.stringify(JSON.parse(xhr.response), undefined, 2);
								// var formattedResponse = JSON.parse(xhr.response);
								// console.log(formattedResponse);
								var formattedResponse = JSON.parse(xhr.response);
								console.log(formattedResponse);
								var allusers = [];
								for (var i = 0; i < formattedResponse.value.length; i++) {
									//console.log(formattedResponse.value[i]);
									var object = formattedResponse.value[i];
									allusers.push({
										//name: object.emailAddresses[0].name,
										//id: object.emailAddresses[0].address
										name: object.displayName,
										id: object.mail
									});
								};
								if (allusers.length > 0) loadUsersSendSH(allusers);
								else alert("You dont have any contacts in your directory");
							} else {
								console.log("HTTP " + xhr.status + "<br>" + xhr.response);
							}
						}
						// Make request.
						xhr.send();
					} catch (err) {
						console.log("Exception: " + err.message);
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
}

function loadUsersSendSH(allusers) {
	var states = new Bloodhound2({
		datumTokenizer: Bloodhound2.tokenizers.obj.whitespace('name'),
		queryTokenizer: Bloodhound2.tokenizers.whitespace,
			// `states` is an array of state names defined in "The Basics"
			local: allusers
		});
	$('#sendUsersSH').typesearch(null, {
		name: 'best-pictures',
		display: 'name',
		source: states,
		templates: {
			empty: ['<div class="empty-message">',
			'unable to find any Users with that name', '</div>'
			].join('\n'),
			suggestion: function(data) {
				return '<p><strong>' + data.name + '</strong></p>';
			}
		}
	}).on('typesearch:selected', function(event, datum) {
		$("#sendUsersSH").blur();
			//if(current_user.id == datum.uid)
			if (listSendEmails.lastIndexOf(datum.id) == -1) {
				var user = datum.id.split("@")[0];
				var objec = $("#li" + user);
				var $link = $('<a></a>', { // Create a new jQuery object
					html: "[X]"
				}).click(function() {
					// Your code here...
					event.preventDefault();
					var elem = document.getElementById("li" + user);
					elem.parentNode.removeChild(elem);
					listSendEmails.splice(listSendEmails.lastIndexOf(datum.id), 1);
				});
				var $list = $('<li></li>', {
					id: "li" + user,
					html: datum.id
				});
				$list.append($link);
				$("#sendListSH").append($list);
				listSendEmails.push(datum.id);
				$("#sendUsersSH").val("");
			}
		});
	$("#sendUsersSH").focus();
	$("#sendUsersSH").attr("placeholder", "Search person");
	$('.twitter-typesearch').css("display", "block");
}

$("#SHABtnAction").click(function() {
	event.preventDefault();
	Parse.Cloud.run("getUserToken", {
		userID: Parse.User.current().id
	}, {
		success: function(token) {
			var stringEmails = "";
				// $('#sendListSH li').each(function(i)
				// {
				//   console.log($(this).attr('id')); // This is your rel value
				// });
for (var i = 0; i < listSendEmails.length; i++) {
	stringEmails += '{"EmailAddress": {"Address": "' + listSendEmails[i] +
	'"}}';
	if (i < listSendEmails.length - 1) stringEmails += ",";
};
try {
	$("#SHABtnAction").html('Sending mail please wait...');
	var message = '{"Message": {"Subject": "' + Parse.User.current().get(
		"name") + ' wants you to try out ' + ideaObj.get("titulo") +
	'", "Body": {"ContentType": "HTML", "Content": "<table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' height=\'100%\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed;margin:0;padding:0;background-color:#f2f2f2;height:100%!important;width:100%!important\'> <tbody><tr> <td align=\'center\' valign=\'top\' style=\'margin:0;padding:0;border-top:5px solid #ffffff;height:100%!important;width:100%!important\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody> <tr> <td align=\'center\' valign=\'top\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed;background-color:#f2f2f2;border-top:0;border-bottom:0\'> <tbody><tr> <td align=\'center\' valign=\'top\' style=\'padding-top:20px;padding-bottom:20px\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'600\' style=\'border-collapse:collapse;table-layout:fixed\'><tbody> <tr> <td align=\'center\' colspan=\'3\' valign=\'top\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed;background-color: #3498db;border-bottom: 2px solid #2e8ece;\'> <tbody><tr> <td align=\'center\' valign=\'top\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody><tr> <td valign=\'top\' style=\'padding-top:20px;padding-bottom:20px\'><table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody> <tr> <td valign=\'top\'> <table align=\'left\' border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'600\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody><tr> <td valign=\'top\' style=\'padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#ffffff;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left\'> <h1 style=\'display:block;margin:0;padding:0;font-family:Helvetica;font-size:26px;font-style:normal;font-weight:bold;line-height:125%;letter-spacing:normal;text-align:left;color:#ffffff!important\'>Hello:</h1> </td> </tr> </tbody></table> </td> </tr> </tbody></table><table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody> <tr> <td valign=\'top\'> <table align=\'left\' border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'600\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody><tr> <td valign=\'top\' style=\'padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#ffffff;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left\'> You can try now our new application, any suggestion or comments please do not hesitate to write us on Productivity.<br><br>Thanks </td> </tr> </tbody></table> </td> </tr> </tbody></table><table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody> <tr> <td style=\'padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px\' valign=\'top\' align=\'center\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' style=\'border-collapse:separate!important;border:2px solid #f2f2f2;border-radius:4px;background-color:#ffffff;table-layout:fixed\'> <tbody> <tr> <td align=\'center\' valign=\'middle\' style=\'font-family:Arial;font-size:16px; padding: 20px;\'> <a title=\'Download Application\' href=\'' +
	$('#appInstallerHolder').val() +
	'\'=\' style=\'font-weight:bold;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color: #2c3e50;word-wrap:break-word;display:block\' target=\'_blank\'>Download Application</a> </td> </tr> </tbody> </table> </td> </tr> </tbody></table></td> </tr> </tbody></table> </td> </tr> </tbody></table> </td> </tr> </tbody></table></td></tr></tbody></table></td></tr> <tr> <td align=\'center\' valign=\'top\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'100%\' style=\'border-collapse:collapse;table-layout:fixed;background-color:#f2f2f2;border-top:0;border-bottom:0\'> <tbody><tr> <td align=\'center\' valign=\'top\'> <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' width=\'600\' style=\'border-collapse:collapse;table-layout:fixed\'> <tbody><tr> <td valign=\'top\' style=\'padding-top:10px;padding-bottom:10px\'></td> </tr> </tbody></table> </td> </tr> </tbody></table> </td> </tr> </tbody></table> </td> </tr> </tbody></table>"}, "ToRecipients": [' +
	stringEmails +
	'], "Attachments": [] }, "SaveToSentItems": "false"}';
	var params = message;
	var endpointUrl = 'https://graph.microsoft.com/beta/me/microsoft.graph.sendMail';
	var xhr = new XMLHttpRequest();
	xhr.open("POST", endpointUrl, true);
					// The APIs require an OAuth access token in the Authorization header, formatted like this: 'Authorization: Bearer <token>'.
					xhr.setRequestHeader("Authorization", "Bearer " + token.graph);
					xhr.setRequestHeader("Content-type", "application/json");
					xhr.setRequestHeader("Content-length", params.length);
					xhr.setRequestHeader("Connection", "close");
					// Process the response from the API.
					xhr.onload = function() {
						if (xhr.status == 202) {
								//var formattedResponse = JSON.stringify(JSON.parse(xhr.response), undefined, 2);
								// var formattedResponse = JSON.parse(xhr.response);
								// console.log(formattedResponse);
								alert("Message Sent");
								$("#SHABtnAction").html('Send');
								$('#modalShare').modal('toggle');
							} else {
								console.log("HTTP " + xhr.status + "<br>" + xhr.response);
							}
						}
						// Make request.
						xhr.send(params);
					} catch (err) {
						console.log("Exception: " + err.message);
					}
				},
				error: function(error) {
					console.log(error.message);
				}
			});
});

	// $('#commentDiv').click(function(e) {
	// 	e.preventDefault();
	// 	$(this).tab('show');
	// 	loadComments();
	// });

$('body').ready(function() {

});

var arrayRoster = [];
$('#rosterBtn').click(function(e) {
	e.preventDefault();
	$(this).tab('show');
	if (arrayRoster.length == 0) dataRoster();
	else roster();
});

function dataRoster() {
	Parse.Cloud.run("getRoster", {
		ideaID: idea.id
	}, {
		success: function(rosterData) {
			arrayRoster = rosterData;
			roster();
		},
		error: function(error) {
			console.log(error.message);
		}
	});
}
var rosterSelect = "";

function roster() {
	$("#bodyRoster").html("");
	var op = false;
	for (var i = 0; i < arrayRoster.length; i++) {
		var object = arrayRoster[i];
		var image = object.get("user").get("image").url();
		var tHTML = "<tr id='" + object.id + "'>" +
		"<td> <img width='40' class='left' src='" + image + "' />" + object.get(
			"user").get("name") + "</td>" + "<td id='trS" + object.id + "'>" +
		object.get("rol").get("titulo") + "</td>" + "<td id='trD" + object.id +
		"'>" + object.get("descripcion") + "</td>" + "</tr>";
		$("#bodyRoster").append(tHTML);
		if (ideaObj.get("user").id == Parse.User.current().id) {
			var tHTML = "<tr id='" + object.id + "'>" +
			"<td> <img width='40' class='left' src='" + image + "' />" + object.get(
				"user").get("name") + "</td>" + "<td id='trS" + object.id + "'>" +
			object.get("rol").get("titulo") + "</td>" + "<td id='trD" + object.id +
			"'>" + object.get("descripcion") + "</td>" + "<td id='editAction" +
			object.id + "'></td>" + "</tr>";
			$("#bodyRoster").html(tHTML);
			var row = $("#editAction" + object.id);
				var btn = document.createElement("A"); // Create a <button> element
				var t = document.createTextNode("Edit"); // Create a text node
				btn.appendChild(t);
				btn.setAttribute("id", "(a)" + object.id);
				btn.onclick = function() {
					editField();
					} // Append the text to <button>
					row.append(btn);
				}
			};
			if (ideaObj.get("user").id == Parse.User.current().id) {
				Parse.Cloud.run("getRosterAsSelect", {}, {
					success: function(selectRoster) {
						rosterSelect = selectRoster;
						var aHTML = "<tr>" +
						'<td class="text-left"><input id="hiddenValueUser" type="hidden" value="" ><input type="text" placeholder="Search User" id="rosterSU" class="product-typeahead form-control input-style"></td>' +
						"<td>" + selectRoster + "</td>" + "<td><input id='descrRol'></td>" +
						'<td id="actionRow"></td>' + "</tr>";
						$("#bodyRoster").append(aHTML);
						var row = $("#actionRow");
					var btn = document.createElement("BUTTON"); // Create a <button> element
					var t = document.createTextNode("Save"); // Create a text node
					btn.appendChild(t);
					btn.onclick = function() {
						saveRosterRole();
						} // Append the text to <button> 
						row.append(btn);
						startInputUser();
					},
					error: function(error) {
						console.log(error.message);
					}
				});
			}
		}


		function saveRosterRole() {
			var userID = $("#hiddenValueUser").val();
			var roleID = $("#selectRosterRoles").val();
			var descr = $("#descrRol").val();
			if (userID.trim().length > 0 && descr.trim().length > 0) {
				Parse.Cloud.run("saveRosterUser", {
					ideaID: ideaObj.id,
					userID: userID,
					rosterRol: roleID,
					descr: descr
				}, {
					success: function(savedRoster) {
						arrayRoster.push(savedRoster);
						var object = savedRoster;
						var image = object.get("user").get("image").url();
						var tHTML = "<tr id='" + object.id + "'>" +
						"<td> <img width='40' class='left' src='" + image + "' />" + object
						.get("user").get("name") + "</td>" + "<td id='trS" + object.id +
						"'>" + object.get("rol").get("titulo") + "</td>" + "<td id='trD" +
						object.id + "'>" + object.get("descripcion") + "</td>" +
						"<td id='editAction" + object.id + "'></td>" + "</tr>";
						$('#bodyRoster tr:last').before(tHTML);
						$("#hiddenValueUser").val("");
						$("#descrRol").val("");
						$("#rosterSU").val("");
						$("#rosterSU").focus();
						var row = $("#editAction" + object.id);
					var btn = document.createElement("A"); // Create a <button> element
					var t = document.createTextNode("Edit"); // Create a text node
					btn.appendChild(t);
					btn.setAttribute("id", "(a)" + object.id);
					btn.onclick = function() {
						editField();
						} // Append the text to <button>
						row.append(btn);
					},
					error: function(error) {
						console.log(error.message);
					}
				});
}
}

function startInputUser() {
	Parse.Cloud.run("getAllUsers", {}, {
		success: function(usersList) {
			var states = new Bloodhound2({
				datumTokenizer: Bloodhound2.tokenizers.obj.whitespace('name'),
				queryTokenizer: Bloodhound2.tokenizers.whitespace,
					// `states` is an array of state names defined in "The Basics"
					local: usersList
				});
			$('#rosterSU').typesearch(null, {
				name: 'best-pictures',
				display: 'name',
				source: states,
				templates: {
					empty: ['<div class="empty-message">',
					'unable to find any Users with that name', '</div>'
					].join('\n'),
					suggestion: function(data) {
						return '<p><img width="40" src="' + data.photo + '" /><strong>' +
						data.name + '</strong></p>';
					}
				}
			}).on('typesearch:selected', function(event, datum) {
					//if(current_user.id == datum.uid)
					$("#hiddenValueUser").val(datum.id);
					$("#selectRosterRoles").focus();
				});
			$("#rosterSU").focus();
		},
		error: function(error) {
			console.log(error.message);
		}
	});
}

function editField() {
	var idR = event.target.id.replace("(a)", "");
	var rosterObj = Parse.Object.extend("Roster");
	var rosterQuery = new Parse.Query(rosterObj);
	rosterQuery.include("rol");
	rosterQuery.get(idR, {
		success: function(rosterResult) {
			$("#trD" + idR).html("<input id='descrRol" + idR + "' value='" +
				rosterResult.get("descripcion") + "'>");
			$("#trS" + idR).html(rosterSelect);
			$("#trS" + idR + " select").val(rosterResult.get("rol").id);
			$("#editAction" + idR).html("");
			var row = $("#editAction" + idR);
				var btn = document.createElement("BUTTON"); // Create a <button> element
				var t = document.createTextNode("Save"); // Create a text node
				btn.appendChild(t);
				btn.setAttribute("id", "(save)" + idR);
				btn.onclick = function() {
					saveField();
				}
				var btnDel = document.createElement("BUTTON"); // Create a <button> element
				var tDel = document.createTextNode("Delete"); // Create a text node
				btnDel.appendChild(tDel);
				btnDel.setAttribute("id", "(delete)" + idR);
				btnDel.onclick = function() {
					deleteField();
					} // Append the text to <button>
					row.append(btn);
					row.append(btnDel);
				},
				error: function(error) {
					console.log(error.message);
				}
			});
}

function resetField(savedRoster) {
	var idR = savedRoster.id;
	$("#trD" + idR).html(savedRoster.get("descripcion"));
	$("#trS" + idR).html(savedRoster.get("rol").get("titulo"));
	$("#editAction" + idR).html("");
	var row = $("#editAction" + idR);
		var btn = document.createElement("A"); // Create a <button> element
		var t = document.createTextNode("Edit"); // Create a text node
		btn.appendChild(t);
		btn.setAttribute("id", "(a)" + idR);
		btn.onclick = function() {
			editField();
			} // Append the text to <button>
			row.append(btn);
		}

		function saveField() {
			var idR = event.target.id.replace("(save)", "");
			var rosterObj = Parse.Object.extend("Roster");
			var rosterQuery = new Parse.Query(rosterObj);
			rosterQuery.include("rol");
			rosterQuery.get(idR, {
				success: function(rosterResult) {
					var rolID = $("#trS" + idR + " select").val();
					rosterResult.set("descripcion", $("#descrRol" + idR).val());
					if (rolID == rosterResult.get("rol").id) {
						rosterResult.save(null, {
							success: function(savedRoster) {
								resetField(savedRoster);
							},
							error: function(error) {
								console.log(error.message);
							}
						});
					} else {
						var RRObj = Parse.Object.extend("RosterRol");
						var RRQuery = new Parse.Query(RRObj);
						RRQuery.get(rolID, {
							success: function(rRRes) {
								rosterResult.set("rol", rRRes);
								rosterResult.save(null, {
									success: function(savedRoster) {
										resetField(savedRoster);
									},
									error: function(error) {
										console.log(error.message);
									}
								});
							},
							error: function(error) {
								console.log(error.message);
							}
						});
					}
				},
				error: function(error) {
					console.log(error.message);
				}
			});
}

function deleteField() {
	var r = confirm("You sure want to delete this user!");
	if (r == true) {
		var idR = event.target.id.replace("(delete)", "");
		var rosterObj = Parse.Object.extend("Roster");
		var rosterQuery = new Parse.Query(rosterObj);
		rosterQuery.get(idR, {
			success: function(rosterResult) {
				rosterResult.destroy({
					success: function(rosterD) {
						$('#' + idR).remove();
					},
					error: function(error) {
						console.log(error.message);
					}
				});
			},
			error: function(error) {
				console.log(error.message);
			}
		});
	} else {}
}

function castGeoPoint(gpOrigin) {
	var gpString = gpOrigin.toString();
	var b = gpString.indexOf('[') + 1;
	var f = gpString.indexOf(']');
	var arrayGP = gpString.substring(b, f).split(',');
	if (arrayGP.length > 2 && arrayGP.length == 4) {
		arrayGP[0] = arrayGP[0] + "." + arrayGP[1];
		arrayGP[1] = arrayGP[2] + "." + arrayGP[3];
	}
	var parseGP = new Parse.GeoPoint(Number(arrayGP[0]), Number(arrayGP[1]));
	return parseGP;
}

function loadComments() {
	$("#listComments").html("");
	Parse.Cloud.run('loadComments', {
		ideaID: ideaObj.id
	}, {
		success: function(results) {
				// Do something with the returned Parse.Object values
				console.log("COMMENTS YAY!");
				console.log(results);
				for (var i = 0; i < results.length; i++) {
					var object = results[i];
					var image = object.get("user").get("image").url();
					var txt = object.get("texto");
					var hHTML = "";
					hHTML = '<li class="clearfix">' +
					'<span class="chat-img pull-left">' + '<img src="' + image +
					'" width="30" height="30" alt="User Avatar" class="img-circle" />' +
					'</span>' + '<div class="chat-body clearfix">' +
					'<div class="header">' + '<strong class="left primary-font">' +
					object.get("user").get("username") + '</strong>' +
					'<small class="text-muted">' +
					'<span class="glyphicon glyphicon-time"></span>' + moment(object.createdAt
						.toString()).fromNow() + ((object.get("user").id == Parse.User.current()
							.id) ? "<a id='delComment' data-value='" + object.id +
						"'>[X]</a>" : "") + '</small>' + '</div>' + '<p>' + txt + '</p>' +
						'</div>' + '</li>';
						$("#listComments").append(hHTML);
					}
				},
				error: function(error) {
					console.log("Error: " + error.code + " " + error.message);
				}
			});
}
$('body').on('click', '#delComment', function(e) {
	e.preventDefault();
	var r = confirm("You sure want to delete this comment?");
	if (r == true) {
		Parse.Cloud.run("eliminarComentario", {
			commentID: $(e.target).attr("data-value")
		}, {
			success: function(message) {
				var liDel = $(e.target).closest("li");
				$(liDel).remove();
			},
			error: function(error) {
				console.log(error);
			}
		});
	}
});
$("#uploadComment").click(function() {
	var comment = $("#newComment").val();
	var user = Parse.User.current();
	Parse.Cloud.run("aplicarComentario", {
		userID: user.id,
		comentario: comment,
		ideaID: ideaObj.id
	}, {
		success: function(response) {
			Parse.Cloud.run('setPoints', {
				action: "comment"
			}, {
				success: function(result) {
					window.location = "viewIdea.html?k=" + ideaObj.id;
				},
				error: function(error) {
					window.location = "viewIdea.html?k=" + ideaObj.id;
				}
			});
		},
		error: function(error) {
			console.log("Error" + error.message);
		}
	});
});
$("#isLike").click(function(event) {
	event.preventDefault();
	Parse.Cloud.run("aplicarLike", {
		userID: Parse.User.current().id,
		ideaID: ideaObj.id,
		esPositivo: true
	}, {
		success: function(rLikes) {
			setLikeDislike(rLikes.Count, rLikes.Pos);
			getPer(rLikes.Count, rLikes.Pos);
			if (rLikes.Like == "You Like this") {
					$("#isLikeSign").css("color", "white");//#A9D294
					$("#isLikeSign").css("text-shadow", "none");
					$("#isDislikeSign").css("color", "#F8AEA8");//white
					$("#isDislikeSign").css("text-shadow",
						"1px 1px rgba(58, 24, 24, 1)");
				} else {
					$("#isLikeSign").css("color", "#A9D294");//white
					$("#isLikeSign").css("text-shadow", "1px 1px rgba(58, 24, 24, 1)");
				}
			},
			error: function(error) {
				console.log(error.message);
			}
		});
});
$("#isDislike").click(function(event) {
	event.preventDefault();
	Parse.Cloud.run("aplicarLike", {
		userID: Parse.User.current().id,
		ideaID: ideaObj.id,
		esPositivo: false
	}, {
		success: function(rLikes) {
			setLikeDislike(rLikes.Count, rLikes.Pos);
			getPer(rLikes.Count, rLikes.Pos);
			if (rLikes.Dislike == "You Dislike this") {
					$("#isDislikeSign").css("color", "white");//#F8AEA8
					$("#isDislikeSign").css("text-shadow", "none");
					$("#isLikeSign").css("color", "#A9D294");//white
					$("#isLikeSign").css("text-shadow", "1px 1px rgba(58, 24, 24, 1)");
				} else {
					$("#isDislikeSign").css("color", "#F8AEA8");//white
					$("#isDislikeSign").css("text-shadow",
						"1px 1px rgba(58, 24, 24, 1)");
				}
			},
			error: function(error) {
				console.log(error.message);
			}
		});
});

function isImage(filename) {
	var ext = getExtension(filename);
	switch (ext.toLowerCase()) {
		case 'jpg':
		case 'gif':
		case 'jpge':
		case 'png':
		case 'bmp':
		return true;
	}
	return false;
}

function getExtension(filename) {
	var parts = filename.split('.');
	return parts[parts.length - 1];
}
});

$('body').on('click', '#user-idea', function() {
	var word = userIdea;
	if(word===Parse.User.current().id){
		window.location = "perfilDashboard.html";
	}
	else
	{
		window.location = "user.html?i=" + word;
	}
});

 // Join Idea's roster
 $(document).on('click', '#joinBtn',function(){

 	console.log("entro join de nuevo");
 	Parse.Cloud.run("saveRosterUser", {
 		ideaID: ideaObj.id,
 		userID: Parse.User.current().id,
 		rosterRol: "y3i0BqPfwT",
 		descr: "other"
 	}, {
 		success: function(savedRoster) {
 			console.log("join idea entered save roster")
 		},
 		error: function(error) {
 			console.log(error.message);
 		}
 	});

 });


// follow idea owner
$(document).on('click', '#followBtnIdea', function() {

	console.log(userIdea);

	if ($("#followBtnIdea").hasClass("following") === false) {

		$("#followBtnIdea").html("Following");
		$("#followBtnIdea").addClass("following");

		Parse.Cloud.run('setUserFollowing', {
			user: userIdea
		}, {
			success: function(result) {
				console.log(result);

			},
			error: function(error) {
				console.log(error);
			}
		});


	}
	else
		if ($("#followBtnIdea").hasClass("following") === true) {

			Parse.Cloud.run('removeUserFollowing', {
				user: userIdea
			}, {
				success: function(result) {
					console.log(result);

				},
				error: function(error) {
					console.log(error);
				}
			});




			$("#followBtnIdea").removeClass("following");
			$("#followBtnIdea").html("Follow");
		}
	});

// follow idea owner end
$('body').on('click', '.tag', function() {
	var word = $(this).text();
	word = word.replace(' ', '-');
	window.location = "tags.html?t=" + word;
});

$('body').on('click', '.translate', function(e) {
	e.preventDefault();
	Parse.Cloud.run("getLanguage", {
		descr: $(e.target).attr("data-value")
	}, {
		success: function(message) {
			console.log("WORKED");
			console.log(message);
		},
		error: function(error) {
			console.log(error);
		}
	});
});

function showDiv(n)
{

	$("#divDescr").css("display", "none");
	$("#divAttach").css("display", "none");
	$("#divStats").css("display", "none");
	$("#divCove").css("display", "none");
	$("#roster").css("display", "none");
	$("#related").css("display", "none");

	if(n==1)
		$("#divDescr").css("display", "block");
	else if(n==2)
		$("#divAttach").css("display", "block");
	else if(n==3)
		$("#divStats").css("display", "block");
	else if(n==4)
		$("#divCove").css("display", "block");
	else if(n==5)
		$("#roster").css("display", "block");
	else if(n==6)
		$("#related").css("display", "block");

}



