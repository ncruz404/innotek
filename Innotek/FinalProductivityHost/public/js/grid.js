var key = GetUrlValue("k");
var challengeObj;

var userId = "";
var cUser = Parse.User.current();
var nWeek = {
    tipo: -1
};
var acc = 0;
$(document).ready(function() {
    var dynFunc = {
        //funcIH: function () { alert("AAAAAAAAA");}
    };

    function addElement(id, type, text, attr) {
        var row = $(id);
        var btn = document.createElement(type); // Create a <button> element
        var t = document.createTextNode(text); // Create a text node
        btn.appendChild(t);
        for (var i = 0; i < attr.length; i++) {
            btn.setAttribute(attr[i].src, attr[i].value);
            if (attr[i].src == "data-button") {
                btn.onclick = function() {
                    var data = $(this).attr('data-button');
                    var value = $(this).attr('data-value');
                    console.log(data);
                    if (value == undefined || value.length <= 0) dynFunc[data]();
                    else {
                        dynFunc[data](value);
                    }
                }
            }
        };
        row.append(btn);
    }
    var domWeeks;

    function modalWeekLoad() {
        if (!challengeObj.object.get("confPublicada")) {
            if (challengeObj.object.get("semanas").length > 0) {
                $("#chWeekModal .modal-body").html("");
                $("#actionChWBtn").attr("data-button", "submitWeekCalendar");
                $("#actionChWBtn").html("Publish week calendar");
                var all_weeks = challengeObj.object.get("semanas");
                $("#chWeekModal .modal-body").append(
                    '<div id="weekLabels" class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>'
                );
                $("#chWeekModal .modal-body").append(
                    "<div id='weekSort' class='col-xs-9 col-sm-9 col-md-9 col-lg-9'><div id='wsortable'></div></div>"
                );
                for (var i = 0; i < all_weeks.length; i++) {
                    $("#wsortable").append('<div id="week' + (i + 1) + '" data-value="' + i +
                        '" class="week"><div class="col-xs-9 col-sm-9 col-md-9 col-lg-9"><p></p></div><div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" id="week' +
                        (i + 1) + '-del"></div></div>');
                    $("#weekLabels").append("<div><p>Week " + (i + 1) + "</p></div>")
                    addElement("#week" + (i + 1) + " p", "A", all_weeks[i].titulo, [{
                        src: "data-value",
                        value: i
                    }, {
                        src: "data-button",
                        value: "openWeek" + i
                    }]);
                    dynFunc["openWeek" + i] = function(el) {
                        openWeek(el);
                    };
                    addElement("#week" + (i + 1) + "-del", "A", "", [{
                        src: "id",
                        value: "deleteWeek"
                    }, {
                        src: "data-value",
                        value: i
                    }, {
                        src: "data-button",
                        value: "delWeek" + i
                    }, {
                        src: "class",
                        value: "hvr-icon-buzz-out"
                    }]);
                    dynFunc["delWeek" + i] = function(el) {
                        delWeek(el);
                    };
                };
                dragula([document.getElementById("wsortable")]).on('drop', function(el,
                    source) {
                    console.log(source);
                    domWeeks = source;
                });
                domWeeks = document.getElementById("wsortable");
            } else {
                $("#chWeekModal .modal-body").html(
                    "<p>You dont have any weeks for this challenge</p>");
                $("#actionChWBtn").attr("data-button", "none");
            }
            addElement("#chWeekModal .modal-body", "BUTTON", "Add a Week", [{
                src: "id",
                value: "addWBtn"
            }, {
                src: "data-button",
                value: 'funcAddWk'
            }]);
            dynFunc['funcAddWk'] = function() {
                dataAddForm(); /*dataAddWeek()*/
            };
            dynFunc['submitWeekCalendar'] = function() {
                submitWeekCalendar()
            };
        } else {
            $("#chWeekModal .modal-body").html(
                "You have published your week calendar");
            $("#chWeekModal .modal-footer").html("");
            $("#addWBackBtn").css("display", "none");
        }
    }

    function delWeek(val) {
        var sem = challengeObj.object.get("semanas");
        sem.splice(val, 1);
        challengeObj.object.set("semanas", sem);
        challengeObj.object.save(null, {
            success: function(challengeSaved) {
                challengeObj.object = challengeSaved;
                modalWeekLoad();
            },
            error: function(error) {
                console.log(error.message);
            }
        });
    }

    function openWeek(val) {
        var row = challengeObj.object.get("semanas")[val];
        $("#chWeekModal .modal-body").html("");
        $("#chWeekModal .modal-body").css("overflow-y", "auto");
        addElement("#chWeekModal .modal-header", "BUTTON", "<", [{
            src: "id",
            value: "addWBackBtn"
        }, {
            src: "data-button",
            value: 'addWBackBtn'
        }, {
            src: "class",
            value: 'close left'
        }]);
        dynFunc['addWBackBtn'] = function() {
            modalWeekLoad()
        };
        var mD =
            '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">' +
            '<div id="menuDates" class="row">' +
            '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-group">' +
            '<label>Start</label>' + '<input type="date" id="dstart" />' + '</div>' +
            '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-group">' +
            '<label>Finish</label>' + '<input type="date" id="dfin" />' + '</div>' +
            '<span id="helpDate" class="help-block">End Date cannot be before Start Date</span>' +
            '</div>' + '</div>';
        var mT =
            '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">' +
            '<label>Week Title</label>' +
            '<input class="form-control" name ="name" type="text" id="menuTitle" placeholder="e.g. Presentation"  maxlength="60" aria-describedby="helpTitle"/>' +
            '<span id="helpTitle" class="help-block">Title required minimum 5 characters</span>' +
            '</div>';
        var mI =
            '<div id="menuItems" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">' +
            '<span id="helpForm" class="help-block">Minimum 1 form required </span>' +
            '</div>';
        var mR =
            '<div id="menuRender" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>';
        $("#chWeekModal .modal-body").append(mT + mD + mI + mR);
        $("#menuTitle").val(row.titulo);
        $("#dstart").val(row.start);
        $("#dfin").val(row.fin);
        for (var i = 0; i < row.data.length; i++) {
            var crow = row.data[i];
            addViewElementData(crow);
        };
        addElement("#menuItems", "BUTTON", "Add Form", [{
            src: "data-button",
            value: "funcAddST"
        }, {
            src: "data-value",
            value: "text"
        }, {
            src: "class",
            value: "btn  hvr-icon-grow hvr-fade addItemChallenge"
        }]);
        dynFunc['funcAddST'] = function() {
            addViewElement("text")
        };
        addElement("#menuItems", "BUTTON", "Add File", [{
            src: "data-button",
            value: "funcAddF"
        }, {
            src: "data-value",
            value: "file"
        }, {
            src: "class",
            value: "btn  hvr-icon-grow hvr-fade addItemChallenge"
        }]);
        dynFunc['funcAddF'] = function() {
            addViewElement("file")
        };
        $("#actionChWBtn").attr("data-button", "updateWeekForm");
        $("#actionChWBtn").html("Update");
        dynFunc['updateWeekForm'] = function() {
            saveNewWeekForm(val)
        };
    }

    function updateWeekForm(lrow, val) {
        validateInputs();
        var startDate = new Date($("#dstart").val());
        var endDate = new Date($("#dfin").val());
        if (startDate < endDate && $("#menuTitle").val().length >= 5 && $("#dfin")
            .val() !== "" && $("#dstart").val() !== "" && document.getElementById(
                "menuRender").children.length > 0) {
            $("#chWeekModal .modal-body").html("<h3>Saving...</h3>");
            var arrayWeeks = challengeObj.object.get("semanas");
            arrayWeeks[val] = lrow;
            challengeObj.object.set("semanas", arrayWeeks);
            challengeObj.object.save(null, {
                success: function(challengeSaved) {
                    challengeObj.object = challengeSaved;
                    modalWeekLoad();
                },
                error: function(error) {
                    console.log(error.message);
                }
            });
        }
    }

    function dataAddForm() {
        $("#chWeekModal .modal-body").html("");
        $("#chWeekModal .modal-body").css("overflow-y", "auto");
        addElement("#chWeekModal .modal-header", "BUTTON", "<", [{
            src: "id",
            value: "addWBackBtn"
        }, {
            src: "data-button",
            value: 'addWBackBtn'
        }, {
            src: "class",
            value: 'close left'
        }]);
        dynFunc['addWBackBtn'] = function() {
            modalWeekLoad()
        };
        var mD =
            '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">' +
            '<div id="menuDates" class="row">' +
            '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-group">' +
            '<label>Start</label>' + '<input type="date" id="dstart" />' + '</div>' +
            '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 form-group">' +
            '<label>Finish</label>' + '<input type="date" id="dfin" />' + '</div>' +
            '<span id="helpDate" class="help-block">End Date cannot be before Start Date</span>' +
            '</div>' + '</div>';
        var mT =
            '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">' +
            '<label>Week Title</label>' +
            '<input class="form-control" name ="name" type="text" id="menuTitle" placeholder="e.g. Presentation"  maxlength="60" aria-describedby="helpTitle"/>' +
            '<span id="helpTitle" class="help-block">Title required minimum 5 characters</span>' +
            '</div>';
        var mI =
            '<div id="menuItems" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">' +
            '<span id="helpForm" class="help-block">Minimum 1 form required </span>' +
            '</div>';
        var mR =
            '<div id="menuRender" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>';
        $("#chWeekModal .modal-body").append(mT + mD + mI + mR);
        addElement("#menuItems", "BUTTON", "Add Form", [{
            src: "data-button",
            value: "funcAddST"
        }, {
            src: "data-value",
            value: "text"
        }, {
            src: "class",
            value: "btn  hvr-icon-grow hvr-fade addItemChallenge"
        }]);
        dynFunc['funcAddST'] = function() {
            addViewElement("text")
        };
        addElement("#menuItems", "BUTTON", "Add File", [{
            src: "data-button",
            value: "funcAddF"
        }, {
            src: "data-value",
            value: "file"
        }, {
            src: "class",
            value: "btn  hvr-icon-grow hvr-fade addItemChallenge"
        }]);
        dynFunc['funcAddF'] = function() {
            addViewElement("file")
        };
        $("#actionChWBtn").attr("data-button", "saveNewWeekForm");
        $("#actionChWBtn").html("Save");
        dynFunc['saveNewWeekForm'] = function() {
            saveNewWeekForm()
        };
    }

    function saveNewWeekForm(val) {
        validateInputs();
        var startDate = new Date($("#dstart").val());
        var endDate = new Date($("#dfin").val());
        if (startDate < endDate && $("#menuTitle").val().length >= 5 && $("#dfin")
            .val() !== "" && $("#dstart").val() !== "" && document.getElementById(
                "menuRender").children.length > 0) {
            var lrow = {
                titulo: $("#menuTitle").val(),
                start: $("#dstart").val(),
                fin: $("#dfin").val(),
                data: new Array()
            };
            $.each($('#menuRender'), function(i, wso) {
                $('div form', wso).each(function(divid, dive) {
                    switch ($(dive).attr("id")) {
                        case "newInputText":
                            console.log($(dive));
                            console.log($(dive.childNodes[0]));
                            var row = {
                                type: "text",
                                sign: $(dive.childNodes[0].lastChild.firstChild).context.value,
                                value: $(dive.childNodes[1].lastChild.firstChild).context.value
                            };
                            lrow.data.push(row);
                            break;
                        case "newInputFile":
                            var row = {
                                type: "file",
                                sign: $(dive.childNodes[0].lastChild.firstChild).context.value,
                                value: $(dive.childNodes[1].lastChild.firstChild).context.value
                            };
                            lrow.data.push(row);
                            break;
                    }
                });
            });
            if (val == undefined) nextAddWeek2(lrow);
            else updateWeekForm(lrow, val);
        }
    }

    function addViewElement(ele) {
        switch (ele) {
            case "text":
                $("#menuRender").append("<div id='ac" + acc +
                    "'><BR><form id='newInputText' class='form-horizontal'><div class='form-group'><span id='helpFormTitle' class='help-block'>Title required</span><label class='col-sm-2 control-label'>Title</label><div class='col-sm-10'><input type='text' class='form-control'/></div></div><div class='form-group'><label class='col-sm-2 control-label'>Predefined Value</label><div class='col-sm-10'><input class='textFull form-control' type='text' value='' placeholder=''/></div></div></form></div>"
                );
                $("#menuRender .modText").focus();
                addElement("#ac" + acc + " #newInputText", "A", "Delete", [{
                    src: "data-button",
                    value: "funcDel"
                }, {
                    src: "data-value",
                    value: (acc)
                }, {
                    src: "class",
                    value: "btn  hvr-icon-buzz-out hvr-fade deleteForm"
                }]);
                dynFunc['funcDel'] = function(num) {
                    deleteElement(num);
                };
                acc++;
                return;
            case "file":
                dataAddWeek();
                return;
        };
    }

    function addViewElementData(eleObj) {
        console.log(eleObj.type);
        switch (eleObj.type) {
            case "text":
                $("#menuRender").append("<div id='ac" + acc + "'>" + "<BR>" +
                    "<form id='newInputText' class='form-horizontal'>" +
                    "<div class='form-group'>" +
                    "<span id='helpFormTitle' class='help-block'>Title required</span>" +
                    "<label class='col-sm-2 control-label'>Title</label>" +
                    "<div class='col-sm-10'>" +
                    "<input type='text' class='form-control' value='" + eleObj.sign +
                    "'/>" + "</div>" + "</div>" + "<div class='form-group'>" +
                    "<label class='col-sm-2 control-label'>Predefined Value</label>" +
                    "<div class='col-sm-10'>" +
                    "<input class='textFull form-control' type='text' value='" + eleObj.value +
                    "' placeholder=''/>" + "</div>" + "</div>" + "</form>");
                $("#menuRender .modText").focus();
                addElement("#ac" + acc + " #newInputText", "A", "Delete", [{
                    src: "data-button",
                    value: "funcDel"
                }, {
                    src: "data-value",
                    value: (acc)
                }, {
                    src: "class",
                    value: "btn  hvr-icon-buzz-out hvr-fade deleteForm"
                }]);
                dynFunc['funcDel'] = function(num) {
                    deleteElement(num);
                };
                acc++;
                return;
            case "file":
                dataAddWeek(eleObj);
                return;
        };
    }

    function deleteElement(num) {
        console.log(num);
        var container = $("#menuRender")[0];
        var child = $("#ac" + num)[0];
        console.log(child);
        container.removeChild(child);
    }

    function submitWeekCalendar() {
        var arrayWeekFinal = new Array();
        $.each($('#wsortable'), function(i, wso) {
            $('div.week', wso).each(function(divid, dive) {
                var index = Number($(dive).attr("data-value"));
                var d = challengeObj.object.get("semanas")[index];
                arrayWeekFinal.push(d);
            });
        });
        console.log(arrayWeekFinal);
        $("#chWeekModal .modal-body").html("");
        addElement("#chWeekModal .modal-header", "BUTTON", "<", [{
            src: "id",
            value: "addWBackBtn"
        }, {
            src: "data-button",
            value: 'addWBackBtn'
        }, {
            src: "class",
            value: 'close left'
        }]);
        dynFunc['addWBackBtn'] = function() {
            modalWeekLoad()
        };
        $("#chWeekModal .modal-body").append(
            "<BR><h3>Do you wish to publish this calendar?</h3>");
        for (var i = 0; i < arrayWeekFinal.length; i++) {
            $("#chWeekModal .modal-body").append("<p>Week " + (i + 1) + " : " +
                arrayWeekFinal[i].titulo + "</p>");
        };
        $("#actionChWBtn").attr("data-button", "confirmWeekCalendar");
        $("#actionChWBtn").html("Confirm");
        dynFunc['confirmWeekCalendar'] = function() {
            confirmWeekCalendar(arrayWeekFinal)
        };
    }

    function confirmWeekCalendar(arrayFinal) {
        $("#chWeekModal .modal-body").html("<h3>Saving...</h3>");
        challengeObj.object.set("semanas", arrayFinal);
        challengeObj.object.set("confPublicada", true);
        challengeObj.object.save(null, {
            success: function(savedCH) {
                challengeObj.object = savedCH;
                $("#chWeekModal .modal-body").html(
                    "You have published your week calendar");
                $("#chWeekModal .modal-footer").html("");
                $("#chWeekModal").modal('toggle');
            },
            error: function(error) {
                console.log(error.message);
            }
        });
    }
    var AWOptions = "";

    function dataAddWeek(eleObj) {
        if (AWOptions.length <= 0) {
            Parse.Cloud.run("getSemanaTipoAsOptions", null, {
                success: function(options) {
                    AWOptions = options;
                    viewAddWeek2(eleObj);
                },
                error: function(error) {
                    console.log(error.message);
                }
            });
        } else viewAddWeek2(eleObj);
    }

    function viewAddWeek2(eleObj) {
        $("#menuRender").append("<div id='ac" + acc + "'></div>")
        $("#ac" + acc).append("<BR>");
        var aHTML = '<form id="newInputFile" class="form-horizontal">' +
            '<div class="form-group">' +
            '<span id="helpFormTitle" class="help-block">Title required</span>' +
            '<label class="col-sm-2 control-label">Title</label>' +
            '<div class="col-sm-10">' + "<input type='text' class='form-control' />" +
            '</div>' + '</div>' + '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Type of File</label>' +
            '<div class="col-sm-10">' +
            '<select name="" id="addWSelect" class="form-control">' + AWOptions +
            '</select>' + '</div>' + '</div>' + '</form>';
        $("#ac" + acc).append(aHTML);
        $("#menuRender .modText").focus();
        var ne = $("#menuRender")[0].childNodes.length;
        addElement("#ac" + acc + " #newInputFile", "A", "Delete", [{
            src: "data-button",
            value: "funcDel"
        }, {
            src: "data-value",
            value: (acc)
        }, {
            src: "class",
            value: "btn  hvr-icon-buzz-out hvr-fade deleteForm"
        }]);
        dynFunc['funcDel'] = function(num) {
            deleteElement(num);
        };
        acc++;
        if (eleObj != null && eleObj != undefined) {
            var fileElement = $("#newInputFile").last();
            fileElement[0].childNodes[0].lastChild.firstChild.value = eleObj.sign;
            fileElement[0].childNodes[1].lastChild.firstChild = eleObj.value;
        }
    }

    function viewAddWeek() {
        $("#chWeekModal .modal-body").html("");
        addElement("#chWeekModal .modal-header", "BUTTON", "<", [{
            src: "id",
            value: "addWBackBtn"
        }, {
            src: "data-button",
            value: 'addWBackBtn'
        }, {
            src: "class",
            value: 'close left'
        }]);
        dynFunc['addWBackBtn'] = function() {
            modalWeekLoad()
        };
        $("#chWeekModal .modal-body").append("<BR>");
        var aHTML = 'What is this week objective?' +
            '<input type="text" id="addWTitle" class="form-control" value="" required="required" pattern="" title="">' +
            'Select a type of file to upload' +
            '<select name="" id="addWSelect" class="form-control">' + AWOptions +
            '</select>';
        $("#chWeekModal .modal-body").append(aHTML);
        $("#actionChWBtn").attr("data-button", "addWNextBtn");
        $("#actionChWBtn").html("Next");
        dynFunc['addWNextBtn'] = function() {
            nextAddWeek()
        };
    }

    function nextAddWeek2(lrow) {
        $("#chWeekModal .modal-body").html("<h3>Saving...</h3>");
        var arrayWeeks = challengeObj.object.get("semanas");
        arrayWeeks.push(lrow);
        challengeObj.object.set("semanas", arrayWeeks);
        challengeObj.object.save(null, {
            success: function(challengeSaved) {
                challengeObj.object = challengeSaved;
                modalWeekLoad();
            },
            error: function(error) {
                console.log(error.message);
            }
        });
    }

    function nextAddWeek() {
        var tipo = $("#addWSelect").val();
        var title = $("#addWTitle").val();
        console.log(tipo);
        $("#chWeekModal .modal-body").html("<h3>Saving...</h3>");
        var arrayWeeks = challengeObj.object.get("semanas");
        arrayWeeks.push({
            tipo: tipo,
            titulo: title
        });
        challengeObj.object.set("semanas", arrayWeeks);
        challengeObj.object.save(null, {
            success: function(challengeSaved) {
                challengeObj.object = challengeSaved;
                modalWeekLoad();
            },
            error: function(error) {
                console.log(error.message);
            }
        });
    }
    Parse.Cloud.run("getChallenge", {
        challengeID: key
    }, {
        success: function(challengeR) {
            challengeObj = challengeR;
            console.log(challengeObj.object.get("user"));
            if (cUser.id == challengeObj.object.get("user").id) {
                var chHTML = '<div class="modal fade" id="chWeekModal">' +
                    '<div class="modal-dialog">' + '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal" aria-+hidden="true">&times;</button>' +
                    '<h4 class="modal-title">Weeks of this challenge</h4>' + '</div>' +
                    '<div class="modal-body">  ' + '</div>' +
                    '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-default" data-dismiss="modal" id="closeModelChallenge">Close</button>' +
                    '</div>' + '</div>' + '</div>' + '</div>';
                $("body").append(chHTML);
                addElement("#chWeekModal .modal-footer", "BUTTON",
                    "Publish week calendar", [{
                        src: "id",
                        value: "actionChWBtn"
                    }, {
                        src: "data-button",
                        value: 'none'
                    }, {
                        src: "class",
                        value: "btn btn-primary"
                    }]);
                dynFunc['none'] = function() {
                    alert("You dont have any weeks");
                };
                modalWeekLoad();
            }
            $("#chDescrTxt").html(challengeObj.object.get("descripcion"));
            $("#chTitleTxt").html(challengeObj.object.get("titulo"));
            $("#user-idea").html(challengeObj.object.get("user").get("name"));

            if(challengeObj.object.get("user").id === Parse.User.current().id)
            {
                $("#viewChallenge .header-page .container .infoChallenge").after(
                    '<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">'+
                        '<a class="btn btn-primary" data-toggle="modal" href="#chWeekModal" id="weeksBtn">Add or Edit Weeks</a>'+
                    '</div>');
            }


            userid = challengeObj.object.get("user").id;
            $("#viewChallenge .user-info").append(
                "<span class='profile-img' style= 'background-image: url(" +
                challengeObj.object.get("user").get("image").url() + ")'></span>");
            console.log(challengeObj);
            for (var i = 0; i < challengeObj.ideas.length; i++) {
                var cIdea = challengeObj.ideas[i];
                var photo = cIdea.get("photoUrl");
                var array = cIdea.get('tags');
                var stringTags = "";
                var dateString = moment(cIdea.createdAt.toString()).fromNow();
                var uploadsIdea = Enumerable.From(challengeObj.uploads).Where(function(x) {
                                            return cIdea.id == x.attributes.idea.id;
                                        }).ToArray();
                var uploadString = (uploadsIdea.length > 0)? "<a class='modalWeeksCommited' data-value='"+cIdea.id+"'>This idea has commited weeks</a>" : "";    
                if (photo !== undefined) {
                    for (var z = 0; z < array.length; z++) {
                        stringTags = stringTags.concat("<a class='tag' href='#' >" + array[z] +
                            "</a>");
                    }
                    stringTags = stringTags.concat("<a class='clear' href='#' ></a>");
                    var aHTML = '<div onClick="clickCard(this.id)" class="card white-panel" id="' + cIdea.id +
                        '" href="#"  style="transform: translateY(0px);">' +
                        uploadString+
                        '   <h4 class="title">' + cIdea.get("titulo") + '</h4>' +
                        '   <span class="user-img" id="'+cIdea.get("user").id+'" style="background-image: url(' +
                            cIdea.get("user").get("image").url() + ')"></span>' +
                        '   <div id="section2">' + '       <div class="icon-time">' +
                        '           <i class="material-icons time-icon"></i>' +
                        '           <h5 class="date">'+dateString+'</h5>' + '       </div>' +
                        '       <div class="icon-location">' +
                        '           <i class="material-icons location-icon"></i>' +
                        '           <h5 class="location">Monterrey</h5>' + '       </div>' +
                        '   </div>' + '   <p class="description">' + cIdea.get("descripcion")
                        .substring(1, 100) + '       <img class="img-thumbnail-idea" src="' +
                        cIdea.get("photoUrl") +
                        '">...<a onClick="clickCard('+cIdea.id+')" class="read-more">See Full Idea &gt; </a>' + '   </p>' +
                        '   <div id="interaction">' + '       <span class="icon comment">' +
                        cIdea.get("commentCount") + '</span>' +
                        '       <span class="icon heart">' + cIdea.get("likeCount") +
                        '</span>' + '       <span class="icon attachment">' + cIdea.get(
                            "fileCount") + '</span>' + '   </div>' + '<div id="tags" class="' +
                        cIdea.id + '">' + stringTags + '</div>' + '</div>';
                } else {
                    for (var j = 0; j < array.length; j++) {
                        stringTags = stringTags.concat("<a class='tag' href='#' >" + array[j] +
                            "</a>");
                    }
                    stringTags = stringTags.concat("<a class='clear' href='#' ></a>");
                    var aHTML = '<div onClick="clickCard(this.id)" class="card white-panel" id="' + cIdea.id +
                        '" href="#"  style="transform: translateY(0px);">' +
                        uploadString+
                        '   <h4 class="title">' + cIdea.get("titulo") + '</h4>' +
                        '   <span class="user-img" id="'+cIdea.get("user").id+'" style="background-image: url(' +
                         cIdea.get("user").get("image").url() + ')"></span>' +
                        '   <div id="section2">' + '       <div class="icon-time">' +
                        '           <i class="material-icons time-icon"></i>' +
                        '           <h5 class="date">'+dateString+'</h5>' + '       </div>' +
                        '       <div class="icon-location">' +
                        '           <i class="material-icons location-icon"></i>' +
                        '           <h5 class="location">'+cIdea.get("locationCity")+'</h5>' + '       </div>' +
                        '   </div>' + '   <p class="description">' + cIdea.get("descripcion")
                        .substring(1, 100) +
                        '       ...<a class="read-more">See Full Idea &gt; </a>' + '   </p>' +
                        '   <div id="interaction">' + '       <span class="icon comment">' +
                        cIdea.get("commentCount") + '</span>' +
                        '       <span class="icon heart">' + cIdea.get("likeCount") +
                        '</span>' + '       <span class="icon attachment">' + cIdea.get(
                            "fileCount") + '</span>' + '   </div>' + '<div id="tags" class="' +
                        cIdea.id + '">' + stringTags + '</div>' + '</div>';
                }
                $(".pinBoot").append(aHTML);
                    if($(window).width()>768){
                        $("#viewChallenge #ideasRelated").css("padding-top",$("#viewChallenge .header-page").height()+20);
                    }
                        
            };
        },
        error: function(error) {
            console.log(error);
        }
    });

function printUploadsWeeks(uploadsArray){
    var txt = "";
    $.each(uploadsArray, function( index, value ) {
        txt+= '<li><a class="openSingleWeekCommited" data-modal-value="'+value.get("idea").id+'" data-value="'+value.id+'">Week '+value.get("week")+'</a></li>';
    })
    return txt;
}

$('body').on('click', '.openSingleWeekCommited', function(e) {
        e.preventDefault();
        var uploadID = $(e.target).attr("data-value");
        var ideaID = $(e.target).attr("data-modal-value");
        $("#modalWeeksCommited"+ideaID+" .modal-body").html("");
        addElement("#modalWeeksCommited"+ideaID+" .modal-body","BUTTON","<",
            [{src: "id",value:"addWBackBtn"},{src: "data-button",value:'returnWeeksCommited'},
            {src: "class",value:'close left'},{src: "data-value",value:ideaID}]);
        dynFunc['returnWeeksCommited'] = function (id) { returnWeeksCommited(id)};
        $("#modalWeeksCommited"+ideaID+" .modal-body").append("<p></p>");
        var currentUpload = Enumerable.From(challengeObj.uploads).Where(function(x) {
            return uploadID == x.id;
        }).ToArray()[0];
        for (var i = 0; i < currentUpload.get("uploads").length; i++) {
            var currentElement = currentUpload.get("uploads")[i];
            switch(currentElement.type){
                case "text" : 
                    $("#modalWeeksCommited"+ideaID+" .modal-body").append(
                        '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
                            '<strong>'+challengeObj.object.get("semanas")[currentUpload.get("week")].data[i].sign+' : </strong>'+
                            currentElement.value+
                        '</div>');
                break;
                case "file" :
                    $("#modalWeeksCommited"+ideaID+" .modal-body").append(
                        '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
                            '<strong>'+challengeObj.object.get("semanas")[currentUpload.get("week")].data[i].sign+' : </strong>'+
                            '<p id="cWF'+currentElement.value+'">Loading File...</p>'+
                        '</div>');
                    loadCommitedFile(currentElement.value);
                break;
            }
        };
        $('#modalWeeksCommited'+ideaID+' .modal-body').css({
              width:'auto', //probably not needed
              height: (currentUpload.get("uploads").length*50)+'px'
        });
});

function loadCommitedFile(fileID){
    Parse.Cloud.run('getInstaller',{fileID: fileID},{
            success: function(fileResult){
                $("#cWF"+fileID).html("<a href='"+fileResult.get("file").url()+"'>"+fileResult.get("nombre")+"</a>");
            },
            error: function(error){
                console.log(error);
            }
    });
}

function returnWeeksCommited(ideaID){
    var uploadsIdea = Enumerable.From(challengeObj.uploads).Where(function(x) {
        return ideaID == x.attributes.idea.id;
    }).ToArray();
    $("#modalWeeksCommited"+ideaID+" .modal-body").html('<ul>'+
        printUploadsWeeks(uploadsIdea)+
    '</ul>');
    $('#modalWeeksCommited'+ideaID+' .modal-body').css({
              width:'auto', //probably not needed
              height: 'auto'
    });
}

$('body').on('click', '.modalWeeksCommited', function(e) {
        window.stop();
        e.preventDefault();
        var ideaID = $(e.target).attr("data-value");
        var uploadsIdea = Enumerable.From(challengeObj.uploads).Where(function(x) {
            return ideaID == x.attributes.idea.id;
        }).ToArray();
        var currentIdea = Enumerable.From(challengeObj.ideas).Where(function(x) {
            return ideaID == x.id;
        }).ToArray()[0];
        console.log(uploadsIdea);
        if($("modalWeeksCommited"+ideaID).length <= 0)
            $("body").append('<div class="modal-commited modal fade" id="modalWeeksCommited'+ideaID+'">'+
                '<div class="modal-dialog">'+
                    '<div class="modal-content">'+
                        '<div class="modal-header">'+
                            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
                            '<h4 class="modal-title">Weeks commited by '+currentIdea.get("titulo")+'</h4>'+
                        '</div>'+
                        '<div class="modal-body">'+
                            '<ul>'+
                                printUploadsWeeks(uploadsIdea)+
                            '</ul>'+
                        '</div>'+
                        '<div class="modal-footer">'+
                            '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>');
        $("#modalWeeksCommited"+ideaID).modal('toggle');
});

$('body').on('click', '.tag', function() {
    var word = $(this).text();
    word = word.replace(' ', '-');
    window.location = "tags.html?t=" + word;
});

});
(function($, window, document, undefined) {
    var pluginName = 'pinterest_grid',
        defaults = {
            padding_x: 10,
            padding_y: 10,
            no_columns: 3,
            margin_bottom: 50,
            single_column_breakpoint: 700
        },
        columns,
        $article,
        article_width;

    function Plugin(element, options) {
        this.element = element;
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }
    Plugin.prototype.init = function() {
        var self = this,
            resize_finish;
        $(window).resize(function() {
            clearTimeout(resize_finish);
            resize_finish = setTimeout(function() {
                self.make_layout_change(self);
            }, 11);
        });
        self.make_layout_change(self);
        setTimeout(function() {
            $(window).resize();
        }, 500);
    };
    Plugin.prototype.calculate = function(single_column_mode) {
        var self = this,
            tallest = 0,
            row = 0,
            $container = $(this.element),
            container_width = $container.width();
        $article = $(this.element).children();
        if (single_column_mode === true) {
            article_width = $container.width() - self.options.padding_x;
        } else {
            article_width = ($container.width() - self.options.padding_x * self.options
                .no_columns) / self.options.no_columns;
        }
        $article.each(function() {
            $(this).css('width', article_width);
        });
        columns = self.options.no_columns;
        $article.each(function(index) {
            var current_column,
                left_out = 0,
                top = 0,
                $this = $(this),
                prevAll = $this.prevAll(),
                tallest = 0;
            if (single_column_mode === false) {
                current_column = (index % columns);
            } else {
                current_column = 0;
            }
            for (var t = 0; t < columns; t++) {
                $this.removeClass('c' + t);
            }
            if (index % columns === 0) {
                row++;
            }
            $this.addClass('c' + current_column);
            $this.addClass('r' + row);
            prevAll.each(function(index) {
                if ($(this).hasClass('c' + current_column)) {
                    top += $(this).outerHeight() + self.options.padding_y;
                }
            });
            if (single_column_mode === true) {
                left_out = 0;
            } else {
                left_out = (index % columns) * (article_width + self.options.padding_x);
            }
            $this.css({
                'left': left_out,
                'top': top
            });
        });
        this.tallest($container);
        $(window).resize();
    };
    Plugin.prototype.tallest = function(_container) {
        var column_heights = [],
            largest = 0;
        for (var z = 0; z < columns; z++) {
            var temp_height = 0;
            _container.find('.c' + z).each(function() {
                temp_height += $(this).outerHeight();
            });
            column_heights[z] = temp_height;
        }
        largest = Math.max.apply(Math, column_heights);
        _container.css('height', largest + (this.options.padding_y + this.options.margin_bottom));
    };
    Plugin.prototype.make_layout_change = function(_self) {
        if ($(window).width() < _self.options.single_column_breakpoint) {
            _self.calculate(true);
        } else {
            _self.calculate(false);
        }
    };
    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    }
})(jQuery, window, document);

function validateInputs() {
    var startDate = new Date($("#dstart").val());
    var endDate = new Date($("#dfin").val());
    console.log(startDate);
    console.log(endDate);
    if ($("#dstart").val() === "") {
        $("#dstart").addClass("has-error-date");
    }
    if ($("#dstart").val() !== "") {
        $("#dstart").removeClass("has-error-date");
    }
    if ($("#dfin").val() === "") {
        $("#dfin").addClass("has-error-date");
    }
    if ($("#dfin").val() !== "") {
        $("#dfin").removeClass("has-error-date");
    }
    if (startDate > endDate) {
        $("#dstart").addClass("has-error-date");
        $("#menuDates").addClass('has-error');
        $("#dfin").addClass("has-error-date");
    }
    if (startDate < endDate) {
        $("#menuDates .alert-danger").remove();
        $("#dstart").removeClass("has-error-date");
        $("#menuDates").removeClass('has-error');
        $("#dfin").removeClass("has-error-date");
    }
    if ($("#menuTitle").val() === "" || $("#menuTitle").val().length < 5) {
        $("#menuTitle").parent('div').addClass('has-error');
    }
    if ($("#menuTitle").val() !== "" && $("#menuTitle").val().length >= 5) {
        $("#menuTitle").parent('div').removeClass('has-error');
    }
    if (document.getElementById("menuRender").children.length <= 0) {
        $("#menuItems").addClass("has-error");
    }
    if (document.getElementById("menuRender").children.length > 0) {
        $("#menuItems").removeClass("has-error");
    }
    $.each($('#menuRender'), function(i, wso) {
        $('div form', wso).each(function(divid, dive) {
            switch ($(dive).attr("id")) {
                case "newInputText":
                    if ($(dive.childNodes[0].lastChild.firstChild).context.value !== "") {
                        $(dive.childNodes[0]).removeClass("has-error");
                    }
                    if ($(dive.childNodes[0].lastChild.firstChild).context.value === "") {
                        $(dive.childNodes[0]).addClass("has-error");
                    }
                    break;
                case "newInputFile":
                    if ($(dive.childNodes[0].lastChild.firstChild).context.value !== "") {
                        $(dive.childNodes[0]).removeClass("has-error");
                    }
                    if ($(dive.childNodes[0].lastChild.firstChild).context.value === "") {
                        $(dive.childNodes[0]).addClass("has-error");
                    }
                    break;
            }
        });
    });
}

function clickCard(clicked_id) {

    window.location = "viewIdea.html?k=" + clicked_id;


}




$('body').on('click', '#user-idea', function() {

        if(userid===Parse.User.current().id){
        window.location = "perfilDashboard.html";
    }
    else
    {
            window.location = "user.html?i=" + userid;
    }

});

$(document).on("click", "span.user-img", function() {
        var word = $(this).attr('id');
                if(word===Parse.User.current().id){
        window.location = "perfilDashboard.html";
    }
    else
    {
                   window.location = "user.html?i=" + word;
    }

});






$(window).resize(function() {

    if($(window).width()>767){
        $("#viewChallenge #ideasRelated").css("padding-top",$("#viewChallenge .header-page").height()+20);
    }
    else{
        $("#viewChallenge #ideasRelated").css("padding-top",20);
    }

});
