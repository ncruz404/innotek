


  Parse.Cloud.run('getUser', {
    userID: Parse.User.current().id
  }, {
    success: function(user) {

      if (user.get("first_time") === undefined || user.get("first_time") == false) {

        //$('#modalAddSkills').modal('toggle');
        Parse.Cloud.run('setSkills', {
            arraySkills: ["productivity"]
        }, {
            success: function(result) {
              console.log("Saved");
              $('#modalAddSkills').modal('hide');
                walkThrough();
                Parse.User.current().set("first_time", true);
                Parse.User.current().save();
            },
            error: function(model, error) {
                //alert(error);
            }
        });

      }

    },
    error: function(error) {
      console.log(error.message);
    }
  });



function walkThrough(){

  var url = window.location.pathname.split("/")[2];



  if(url=== "home.html" || url === "home.html#"){


    var user= Parse.User.current().get("name");
 
    var tour = new Tour({
        storage : false
    });



   tour.addSteps([

     {
       element: "body",
       placement: "top",
        backdrop: true,
        animation:true,
       title: "Welcome to Productivity, "+user+"!",
       content: "To get you started, here are our tips on using this website"
     },
     {
       element: "#addButton",
       placement: "bottom",
       title: "Add new Ideas",
       content: "Here is the section where you can share with the community ideas or inventions to improve our/your company"
     },
     {
       element: "#most-recent",
       placement: "top",
       title: "Recently Added",
       content: "This column contains the lastest ideas uploaded"
     },
      {
       element: "#most-popular",
       placement: "top",
       title: "Most Popular",
       content: "This column contains the most popular which are the most liked"
     },
      {
       element: "#most-active",
       placement: "top",
       title: "Suggested for you",
       content: "This ideas recommended to you according to your interests"
     },
      {
       element: "#my-ideas",
       placement: "top",
       title: "My Ideas",
       content: "This column contains all the ideas made by you"
     },
     {
       element: "#sidebar",
       placement: "left",
       title: "And finally The People",
       content: " If you are on this section is because you are doing a good job improving our Company. We hope to see you here!"
     },
     {
       element: "body",
       placement: "top",
       title: "Thank you",
       content: "We hope you enjoy Productivity and help to make our/your Company a better place."
     },

   ]);
 


    // Initialize the tour
    tour.init();
    
    // Start the tour
    tour.start();

 
    
  }
  else{

  window.location = "home.html?t=tour" ;

}

 
}



    $("#changeSkillsModal").click(function() {

      var arraySkills = getArrayTags($('#skills').tokenfield('getTokens'));

      if(arraySkills.length<1){
        $("#error-skillsModal").css("display","block");
        $("#small").parent().addClass("error");
      }
      if(arraySkills.length>0){
        $("#error-skillsModal").css("display","none");
        $("#small").parent().removeClass("error");
      



        Parse.Cloud.run('setSkills', {
            arraySkills: ["Softtek"]
        }, {
            success: function(result) {
              console.log("Saved");
              $('#modalAddSkills').modal('hide');
                walkThrough();
                Parse.User.current().set("first_time", true);
                Parse.User.current().save();
            },
            error: function(model, error) {
               // alert(error);
            }
        });


        }
    });





    $(document).ready(function(){





        Parse.Cloud.run('getAllTags', {
                cantidad:200,
                skip:0
            }, 

            {
                success: function(result) {
                  

                  var arrayFirstTime = [];
              for (var i = 0; i < result.length; i++) {
                  arrayFirstTime.push({
                      value: result[i]
                  });
              }
              engine2 = new Bloodhound({ 
                  local: arrayFirstTime,
                  datumTokenizer: function(d) {
                    return Bloodhound.tokenizers.whitespace(d.value);
                  },
                  queryTokenizer: Bloodhound.tokenizers.whitespace
                });
              

                engine2.initialize();

                    $('#skills').tokenfield({
                        delimiter: false,
                        typeahead: [
                            {
                                 hint: true,
                          highlight: true
                            }, 
                            {
                                source: engine2.ttAdapter()
                            }
                        ]
                    }).on('tokenfield:createtoken', function (e) {
                  
                /*  arr = removeByAttr(arr,"value",e.attrs.value.toLowerCase());*/

                        var existingTokens = $(this).tokenfield('getTokens');
                        if (existingTokens.length) {

                            $.each(existingTokens, function(index, token) {

                                if (token.value.toLowerCase() === e.attrs.value.toLowerCase()) {
                                    e.preventDefault();
                                }
                                
                            });
                        }

                    });

                            Parse.Cloud.run('getSkills', {
                          }, {
                              success: function(result) {
                                $('#skills').tokenfield('setTokens', result);
                              },
                              error: function(model, error) {
//                                  alert(error);
                              }
                          });
                      

                    
                        },

                        error: function(error) {
                           console.log(error);
                         }

            });

});


