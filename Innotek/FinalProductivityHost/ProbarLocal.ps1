Get-ChildItem -r -include "*.html" | foreach-object { $a = $_.fullname; ( get-content $a ) | foreach-object { $_ -replace "js/security.js","js/securityOldUnsafe.js" }  | set-content $a }
Write-Host -Object ("Completed 25%") -ForegroundColor Yellow;
Get-ChildItem -r -include "*.html" | foreach-object { $a = $_.fullname; ( get-content $a ) | foreach-object { $_ -replace "js/login.js","js/loginOldUnsafe.js" }  | set-content $a }
Write-Host -Object ("Completed 50%") -ForegroundColor Yellow;
Get-ChildItem -r -include "*.html" | foreach-object { $a = $_.fullname; ( get-content $a ) | foreach-object { $_ -replace "js/officeLog.js","js/officeLogOldUnsafe.js" }  | set-content $a }
Write-Host -Object ("Completed 75%") -ForegroundColor Yellow;
Get-ChildItem -r -include "*.js","*.html" | foreach-object { $a = $_.fullname; ( get-content $a ) | foreach-object { $_ -replace "https://www.innotek.ai/","http://localhost:5000/" }  | set-content $a }
Write-Host -Object ("Completed 100%") -ForegroundColor Green;
