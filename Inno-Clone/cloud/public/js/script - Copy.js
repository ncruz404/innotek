var object;
var categoriesSelected = [];
//create card for recently added
Parse.Cloud.run("ideasFull", {
        cantidad: 200,
        skip: 0
}, {
        success: function(ideafull) {
                if (ideafull.length == 0) {}
                /*for (var i = 0; i < ideafull.length; i++) {
                        object = ideafull[i];
                        createCard(object.idea, object.comments, object.likes, object.files);
                }*/
                saveArrayCategory();

               

                //Create popular ideas 
                Parse.Cloud.run("popularideas", {
                        cantidad: 200,
                        skip: 0
                }, {
                        success: function(popularideas) {
                                for (var i = 0; i < popularideas.length; i++) {
                                        object = popularideas[i];
                                        createCardPopular(object.idea, object.comments, object.likes, object.files);
                                }
                                //create active ideas
                                Parse.Cloud.run("activeideas", {
                                        cantidad: 200,
                                        skip: 0
                                }, {
                                        success: function(activeideas) {
                                                for (var i = 0; i < activeideas.length; i++) {
                                                        object = activeideas[i];
                                                        createCardActive(object.idea, object.comments, object.likes, object.files);
                                                }
                                                //create card for my ideas
                                                Parse.Cloud.run("ideasFull", {
                                                        cantidad: 200,
                                                        skip: 0
                                                }, {
                                                        success: function(ideafull) {
                                                                var File = Parse.Object.extend(
                                                                        "Archivo");
                                                                var query = new Parse.Query(File);
                                                                var acum = 0;
                                                                for (var i = 0; i < ideafull.length; i++) {
                                                                        object = ideafull[i];
                                                                        if (object.idea.get("user").id ==
                                                                                Parse.User.current().id &&
                                                                                object.idea.has("titulo")
                                                                        ) {
                                                                                acum++;
                                                                                createCardCategory4(
                                                                                        object.idea,
                                                                                        object.comments,
                                                                                        object.likes,
                                                                                        object.files
                                                                                );
                                                                        }
                                                                }
                                                                if (acum === 0) {
                                                                        columnIsEmpty("#my-ideas");
                                                                }
                                                                animateHome();
                                                        },
                                                        error: function(object, error) {
                                                                // The object was not retrieved successfully.
                                                                // error is a Parse.Error with an error code and message.
                                                        }
                                                });
                                        },
                                        error: function(object, error) {
                                                // The object was not retrieved successfully.
                                                // error is a Parse.Error with an error code and message.
                                        }
                                });
                        },
                        error: function(object, error) {
                                // The object was not retrieved successfully.
                                // error is a Parse.Error with an error code and message.
                        }
                });
        },
        error: function(object, error) {
                // The object was not retrieved successfully.
                // error is a Parse.Error with an error code and message.
        }
});
//create side bar
Parse.Cloud.run('sideBar', {}, {
        success: function(user) {
                for (var i = 0; i < user.length; i++) {
                        var object = user[i];
                        if (object.id != "7eTMkQ1lmD") createSidebar(object);
                }
        },
        error: function(error) {
                console.log(error.message);
        }
});
//function create column most recent
function createCard(object, comments, likes, attachments) {
        var datestring = object.createdAt.toString();
        var hola = moment(datestring).fromNow();
        if(object.has("esPrivado"))
        {
                var abstract = object.get('descripcion').substring(0, 300) + "...";
                var imageUser = object.attributes.user.attributes.image.url();
                var imageUserY = object.attributes.user.attributes.imageYammer;
                if (imageUserY != null) imageUser = imageUserY;
                $(".most-recent").append("<div class='card' id='" + object.id +
                        "' href='#' onClick='clickCard(this.id)'><div id='category'><div class='noactive ideaCategory'></div><div class='noactive needsCategory'></div><div class='noactive appCategory'></div><div class='noactive pocCategory'></div><div class='noactive microCategory'></div><div class='noactive otherCategory'></div></div><h4 class='title'>" +
                        object.get('titulo') + "</h4><span class='user-img' style = 'background-image: url(" + imageUser + ")'></span><div id='section2'><h5 class='author' href='#''>by " +
                        object.attributes.user.attributes.username + "</h5><i class='material-icons time-icon'>&#xE425;</i><h5 class='date'>" + hola +
                        "</h5><i class='material-icons location-icon'>&#xE0C8;</i> <h5 class='location'>" + object.get("locationCity") + "</h5></a></div><p class='description'>" + abstract +
                        "<a class='read-more'>See Full Idea > </a></p><div id='interaction'><span class='icon comment'>" + comments + "</span><span class='icon heart'>" + likes +
                        "</span><span class='icon attachment'>" + attachments + "</span></div><div id='tags' class='tag" + object.id + "'></div></div>");
                ideaCategory(object, '.most-recent', object.id);
                ideaHasPhotoAttached(object, '.most-recent', object.id);
                addAnimation(".most-recent", object.id, 1500);
                var array = [];
                array.length = 0;
                var tagsWord = object.get('tags').toString();
                array = tagsWord.split(',');
                for (var i = 0; i < array.length; i++) {
                        $(".tag" + object.id).append("<a class='tag' href='' >" + array[i] + "</a>");
                }
                $(".tag" + object.id).append("<a class='clear' href='' ></a>");               
        }
        else
        {
                var aHTML =     '<div class="card challenge" id="'+object.id+'" >'+
                                '    <h4 class="title">'+object.get("descripcion")+'</h4>'+
                                '    <div id="section2">'+
                                '            <h5 class="author">by '+object.get("user").get("username")+'</h5><i class="material-icons time-icon"></i>'+
                                '            <h5 class="date">'+hola+'</h5><i class="material-icons location-icon"></i>'+
                                '            <h5 class="location">Monterrey</h5>'+
                                '    </div>'+
                                '    <div id="btnChallenge">'+
                                '   </div>'+
                                '   <div id="ideaRelated">'+
                                '       <p>'+((object.get("countIdeas") > 0 )? object.get("countIdeas")+" ideas submitted" : "")+'</p>'+
                                '   </div><BR>'+
                                '   <a href="grid.html?k='+object.id+'" class="bold600">See More</a>'+
                                '</div>';
                $(".most-recent").append(aHTML);
                var row = $("#"+object.id+" #btnChallenge");
                var btn = document.createElement("A");        // Create a <button> element
                var t = document.createTextNode("Contribute Idea");       // Create a text node
                btn.appendChild(t);
                btn.setAttribute("id", "contributeIdea");
                btn.setAttribute("data-info",object.id);
                btn.onclick = function() { contributeIdea(object); }
                row.append(btn);

                var btnLink = document.createElement("A");        // Create a <button> element
                t = document.createTextNode("Link Idea");       // Create a text node
                btnLink.appendChild(t);
                btnLink.setAttribute("id", "linkIdea");
                btnLink.onclick = function() { linkIdea(object); }                                // Append the text to <button>
                row.append(btnLink);
                addAnimation(".most-recent", object.id, 1500);
        }
                
}

//function create column most popular
function createCardPopular(object, comments, likes, attachments) {
                var abstract = object.get('descripcion').substring(0, 300) + "...";
                var imageUser = object.attributes.user.attributes.image.url();
                var imageUserY = object.attributes.user.attributes.imageYammer;
                if (imageUserY != null) imageUser = imageUserY;
                var datestring = object.createdAt.toString();
                var hola = moment(datestring).fromNow();
                $("#most-popular").append("<div class='card' id='" + object.id +
                        "' href='#' onClick='clickCard(this.id)'><div id='category'><div class='noactive ideaCategory'></div><div class='noactive needsCategory'></div><div class='noactive appCategory'></div><div class='noactive pocCategory'></div><div class='noactive microCategory'></div><div class='noactive otherCategory'></div></div><h4 class='title'>" +
                        object.get('titulo') + "</h4><span class='user-img' style = 'background-image: url(" + imageUser + ")'></span><div id='section2'><h5 class='author' href='#''>by " +
                        object.attributes.user.attributes.username + "</h5><i class='material-icons time-icon'>&#xE425;</i><h5 class='date'>" + hola +
                        "</h5><i class='material-icons location-icon'>&#xE0C8;</i> <h5 class='location'>" + object.get("locationCity") + "</h5></a></div><p class='description'>" + abstract +
                        "<a class='read-more'>See Full Idea > </a></p><div id='interaction'><span class='icon comment'>" + comments + "</span><span class='icon heart'>" + likes +
                        "</span><span class='icon attachment'>" + attachments + "</span></div><div id='tags' class='tag" + object.id + "'></div></div>");
                ideaCategory(object, '#most-popular', object.id);
                ideaHasPhotoAttached(object, '#most-popular', object.id);
                addAnimation("#most-popular", object.id, 1600);
                var array = [];
                array.length = 0;
                var tagsWord = object.get('tags').toString();
                array = tagsWord.split(',');
                for (var i = 0; i < array.length; i++) {
                        $(".tag2" + object.id).append("<a class='tag' href='' >" + array[i] + "</a>");
                }
                $(".tag2" + object.id).append("<a class='clear' href='' ></a>");
        }
        //function create column most active

function createCardActive(object, comments, likes, attachments) {
                var abstract = object.get('descripcion').substring(0, 300) + "...";
                var imageUser = object.attributes.user.attributes.image.url();
                var imageUserY = object.attributes.user.attributes.imageYammer;
                if (imageUserY != null) imageUser = imageUserY;
                var datestring = object.createdAt.toString();
                var hola = moment(datestring).fromNow();
                $("#most-active").append("<div class='card' id='" + object.id +
                        "' href='#' onClick='clickCard(this.id)'><div id='category'><div class='noactive ideaCategory'></div><div class='noactive needsCategory'></div><div class='noactive appCategory'></div><div class='noactive pocCategory'></div><div class='noactive microCategory'></div><div class='noactive otherCategory'></div></div><h4 class='title'>" +
                        object.get('titulo') + "</h4><span class='user-img' style = 'background-image: url(" + imageUser + ")'></span><h5 class='author' href='#''>by " + object.attributes.user
                        .attributes.username + "</h5><i class='material-icons time-icon'>&#xE425;</i><h5 class='date'>" + hola +
                        "</h5><i class='material-icons location-icon'>&#xE0C8;</i> <h5 class='location'>" + object.get("locationCity") + "</h5></a><p class='description'>" + abstract +
                        "<a class='read-more'>See Full Idea > </a></p><div id='interaction'><span class='icon comment'>" + comments + "</span><span class='icon heart'>" + likes +
                        "</span><span class='icon attachment'>" + attachments + "</span></div><div id='tags' class='tag" + object.id + "'></div></div>");
                ideaCategory(object, '#most-active', object.id);
                ideaHasPhotoAttached(object, '#most-active', object.id);
                addAnimation("#most-active", object.id, 1700);
                var array = [];
                array.length = 0;
                var tagsWord = object.get('tags').toString();
                array = tagsWord.split(',');
                for (var i = 0; i < array.length; i++) {
                        $(".tag3" + object.id).append("<a class='tag' href='' >" + array[i] + "</a>");
                }
                $(".tag3" + object.id).append("<a class='clear' href='' ></a>");
        }
        //function create column my ideas

function createCardCategory4(object, comments, likes, attachments) {
        if(object.has("esPrivado")){
                var abstract = object.get('descripcion').substring(0, 300) + "...";
                var imageUser = object.attributes.user.attributes.image.url();
                var imageUserY = object.attributes.user.attributes.imageYammer;
                if (imageUserY != null) imageUser = imageUserY;
                var datestring = object.createdAt.toString();
                var hola = moment(datestring).fromNow();
                $("#my-ideas").append("<div class='card' id='" + object.id +
                        "' href='#' onClick='clickCard(this.id)'><div id='category'><div class='noactive ideaCategory'></div><div class='noactive needsCategory'></div><div class='noactive appCategory'></div><div class='noactive pocCategory'></div><div class='noactive microCategory'></div><div class='noactive otherCategory'></div></div><h4 class='title'>" +
                        object.get('titulo') + "</h4><span class='user-img' style = 'background-image: url(" + imageUser + ")'></span><h5 class='author' href='#''>by " + object.attributes.user
                        .attributes.username + "</h5><i class='material-icons time-icon'>&#xE425;</i><h5 class='date'>" + hola +
                        "</h5><i class='material-icons location-icon'>&#xE0C8;</i> <h5 class='location'>" + object.get("locationCity") + "</h5></a><p class='description'>" + abstract +
                        "<a class='read-more'>See Full Idea > </a></p><div id='interaction'><span class='icon comment'>" + comments + "</span><span class='icon heart'>" + likes +
                        "</span><span class='icon attachment'>" + attachments + "</span></div><div id='tags' class='tag" + object.id + "'></div></div>");
                ideaCategory(object, '#my-ideas', object.id);
                ideaHasPhotoAttached(object, '#my-ideas', object.id);
                addAnimation("#my-ideas", object.id, 1800);
                var array = [];
                array.length = 0;
                var tagsWord = object.get('tags').toString();
                array = tagsWord.split(',');
                for (var i = 0; i < array.length; i++) {
                        $(".tag4" + object.id).append("<a class='tag' href='' >" + array[i] + "</a>");
                }
                $(".tag4" + object.id).append("<a class='clear' href='' ></a>");
        }
}
        //function create sidebar

function createSidebar(object) {
                var imageUser = object.get("image").url();
                var imageUserY = object.get("imageYammer");
                if (imageUserY != null) imageUser = imageUserY;
                $("#sidebar ul").append("<li><a href='perfilDashboard.html?k=" + object.id + "'><span class='user-img' style = 'background-image: url(" + imageUser + ")'></span><p>" + object.get(
                        "name") + "</p></a></li>");
        }
        //function to know if user has or doesnt have ideas

function columnIsEmpty(category) {
                $(category).append(
                        "<div id='column-is-empty-info'><i class='material-icons'>mood_bad</i><h2>No ideas yet</h2> <h5 class='message-noideas'>Tell us about your ideas or inventions to improve our company</h5><a class='dropdown-toggle new_idea_a btn first_new_idea' data-toggle='modal' data-target='#myModal' href='#''> Add New Idea</a> </div>"
                );
        }
        //responsive width and height
$(window).resize(function() {
        $("#columns").css("width", function(index) {
                return $(window).width() - 60;
        });
        $("#sidebar").css("height", function(index) {
                return $(document).height();
        });
        $("#header-column").css("width", $(".column").width());
});
//click to view idea
function clickCard(clicked_id) {
                window.location = "viewIdea.html?k=" + clicked_id;
        }
        //post image of idea if it has it

function ideaHasPhotoAttached(object, category, card) {
                if (object.get("photoUrl") !== undefined) {
                        $(category + ' div#' + card + ' p.description').prepend('<img class="img-thumbnail-idea" src="' + object.get("photoUrl") + '">');
                }
        }
        //Animations 

function addAnimation(category, objectId, speed) {
        $(category + " #" + objectId).velocity({
                translateY: [0, 200],
        }, speed);
}

function animateHome() {
                $(".se-pre-con").fadeOut("slow");
                $("#columns").css("display", "block");
                //animation columns
                $("#columns").velocity({
                        translateY: [0, 300],
                }, 2000);
                //display navsuperior y sidebar ya que se cargó
                $("#sidebar").css("display", "block");
                //$("#navSuperior").css("display", "block");
                //animation sidebar
                /*    $("#sidebar").velocity({
        translateX: [0, 600],
    }, {
        queue: false
    }, 2000);*/
                //animation sidebar
                $("#sidebar").velocity("transition.bounceRightIn");
                //navbar
                $(".navbar-toggle").on("click", function() {
                        $(this).toggleClass("active");
                });
                //change width columns div
                $("#columns").css("width", function(index) {
                        return $(window).width() - 60;
                });
                //change width columns div
                $("#columns").css("height", function(index) {
                        return $(document).height();
                });
                //height sidebar
                $("#sidebar").css("height", function(index) {
                        return $(document).height();
                });
                //width header columns
                $(".header-column").css("width", $(".column").width());
        }
        //shows which ides's category an idea is

function ideaCategory(object, category, objectid) {
                switch (object.get("category")) {
                        case 0:
                                $(category + " #" + objectid + " #category div.ideaCategory").removeClass("noactive");
                                break;
                        case 1:
                                $(category + " #" + objectid + " #category div.needsCategory").removeClass("noactive");
                                break;
                        case 2:
                                $(category + " #" + objectid + " #category div.appCategory").removeClass("noactive");
                                break;
                        case 3:
                                $(category + " #" + objectid + " #category div.pocCategory").removeClass("noactive");
                                break;
                        case 4:
                                $(category + " #" + objectid + " #category div.microCategory").removeClass("noactive");
                                break;
                        case 5:
                                $(category + " #" + objectid + " #category div.otherCategory").removeClass("noactive");
                                break;
                        default:
                }
        }
        //open filter recently added 
var openDropdown = false;
$('#filtermenu').click(function() {



Parse.Cloud.run('getfilter', {}, {
        success: function(results) {
                if(results[0].get("idea")==true){
                        $("#filter-idea").addClass("active");
                }
                if(results[0].get("challenge")==true){
                        $("#filter-challenge").addClass("active");
                }
                if(results[0].get("ideaCat")==true){
                        $("#filter-0").addClass("active");
                }
                if(results[0].get("needCat")==true){
                        $("#filter-1").addClass("active");
                }
                if(results[0].get("appCat")==true){
                        $("#filter-2").addClass("active");
                }
                if(results[0].get("pocCat")==true){
                        $("#filter-3").addClass("active");
                }
                if(results[0].get("microCat")==true){
                        $("#filter-4").addClass("active");
                }
                if(results[0].get("otherCat")==true){
                        $("#filter-5").addClass("active");
                }
        },
        error: function(error) {
                console.log("error getting filters");
        }
});












        if (openDropdown == false) {
                $("#filter-dropdown").velocity("transition.slideDownIn");
                $("#filter-dropdown").hasClass("open")
                openDropdown = true;
                $("#filtermenu").addClass("open");
                $("#filtermenu i").addClass("open");
        } else {
                $("#filter-dropdown").velocity("transition.slideUpBigOut");
                openDropdown = false;
                $("#filtermenu").removeClass("open");
                $("#filtermenu i").removeClass("open");
        }
});
//submit button
/*var categoriesSelected = [];
$('.btn-submitfilter').click(function() {
        categoriesSelected = [];
        $('#category-btn a.active').each(function() {
                var myString = this.id;
                var stringLength = myString.length; // this will be 16
                var lastChar = myString.charAt(stringLength - 1);
                categoriesSelected.push(parseInt(lastChar));
        });
        $('.most-recent .card').remove();
        $('.most-recent #column-is-empty-info').remove();
        //create card for my ideas
        Parse.Cloud.run("ideasFull", {
                cantidad: 200,
                skip: 0
        }, {
                success: function(ideafull) {
                        var File = Parse.Object.extend("Archivo");
                        var query = new Parse.Query(File);
                        for (var i = 0; i < ideafull.length; i++) {
                                object = ideafull[i];
                                if (isInArray(object.idea.get("category"), categoriesSelected) === true) {
                                        createCard(object.idea, object.comments, object.likes, object.files);
                                }
                        }
                        if (ifDivInsideDiv(".most-recent", "div.card") == false) {
                                $(".most-recent").append(
                                        "<div id='column-is-empty-info'><i class='material-icons'>mood_bad</i><h2>No ideas yet</h2> <h5 class='message-noideas'>Tell us about your ideas or inventions to improve our company</h5><a class='dropdown-toggle new_idea_a btn first_new_idea' data-toggle='modal' data-target='#myModal' href='#''> Add New Idea</a> </div>"
                                );
                        }
                },
                error: function(object, error) {
                        // The object was not retrieved successfully.
                        // error is a Parse.Error with an error code and message.
                }
        });
        $("#filter-dropdown").velocity("transition.slideUpOut");
        $("#filtermenu").removeClass("open");
        $("#filtermenu i").removeClass("open");
});*/




$('.btn-submitfilter').click(function() {

        categoriesSelected = [];
        $('#category-btn a.active').each(function() {
                var myString = this.id;
                var stringLength = myString.length; // this will be 16
                var lastChar = myString.charAt(stringLength - 1);
                categoriesSelected.push(parseInt(lastChar));

        });


        // Create the object.
var Filter = Parse.Object.extend("Filter");
//var filter = new Filter();

var query = new Parse.Query(Filter);
query.equalTo("userId", Parse.User.current().id);
query.find({
        success: function(results){
                var filter = results[0];

                if(categoriesSelected.length==0){
                        filter.set("idea", false);
                        filter.set("challenge", false);
                        filter.set("ideaCat", false);
                        filter.set("needCat", false);
                        filter.set("appCat", false);
                        filter.set("pocCat", false);
                        filter.set("otherCat", false);
                        filter.set("microCat", false);

                        $('.most-recent .card').remove();
        $('.most-recent #column-is-empty-info').remove();

        loadCardsNoFilter();
                }
                else{

                if (isInArray(11, categoriesSelected) === true){
                    filter.set("idea", true);    
                }
                else{
                    filter.set("idea", false);     
                }

                if (isInArray(22, categoriesSelected) === true){
                    filter.set("challenge", true);    
                }
                else{
                    filter.set("challenge", false);     
                }

                if (isInArray(0, categoriesSelected) === true){
                    filter.set("ideaCat", true);    
                }
                else{
                    filter.set("ideaCat", false);     
                }

                if (isInArray(1, categoriesSelected) === true){
                    filter.set("needCat", true);    
                }
                else{
                    filter.set("needCat", false);     
                }

                if (isInArray(2, categoriesSelected) === true){
                    filter.set("appCat", true);    
                }
                else{
                    filter.set("appCat", false);     
                }

                if (isInArray(3, categoriesSelected) === true){
                    filter.set("pocCat", true);    
                }
                else{
                    filter.set("pocCat", false);     
                }

                if (isInArray(4, categoriesSelected) === true){
                    filter.set("microCat", true);    
                }
                else{
                    filter.set("microCat", false);     
                }

                if (isInArray(5, categoriesSelected) === true){
                    filter.set("otherCat", true);    
                }
                else{
                    filter.set("otherCat", false);     
                }
        }


                filter.save(null, {
                  success: function(filter) {
                        console.log(filter);
                    // Now let's update it with some new data. In this case, only cheatMode and score
                    // will get sent to the cloud. playerName hasn't changed.
                //filter.set("idea", false);
                //filter.set("challenge", false);
                    //filter.save();
                  }
                });
        },
        error: function(error){

        }
});




//create card for my ideas
        
//load cards

if(categoriesSelected.length>0){

         $('.most-recent .card').remove();
        $('.most-recent #column-is-empty-info').remove();

        loadCards();}


        $("#filter-dropdown").velocity("transition.slideUpOut");
        $("#filtermenu").removeClass("open");
        $("#filtermenu i").removeClass("open");
});



//clear button
$('.btn-clear').click(function() {
        $(".btn-filter").removeClass("active");

});
//click in filter category idea or challenge
$(".btn-filter").click(function() {
        if ($(this).hasClass("active") == false) {
                $(this).addClass("active");
        } else {
                $(this).removeClass("active");
        }




});
//click in filter category microservice, needs, ideas ,etc
$("#cateogyr-btn a").click(function() {
        if ($(this).hasClass("active") == false) {





                $(this).addClass("active");

        } else {
                $(this).removeClass("active");
        }





});
//value is in array
function isInArray(value, array) {
                return array.indexOf(value) > -1;
        }
        //function div inside div

function ifDivInsideDiv(parent, child) {
        if ($(child).parents(parent).length == 1) {
                return true;
        } else {
                return false;
        }
}

//parse cloud get filters
/*Parse.Cloud.run("getFilter", {
        cantidad: 200,
        skip: 0,
        userID: Parse.User.current().id
}, {
        success: function(result) {
                console.log("success");
        },
        error: function(object, error) {

                console.log("didnt work");
        }
});*/



//create an array with the filter and call load cards
function saveArrayCategory(){

        categoriesSelected = [];
       

        Parse.Cloud.run('getfilter', {}, {

       

                success: function(results) {

                if(results[0]===undefined){

                Parse.Cloud.run('setFilter', {}, {

                        success: function(results) 
                        {

                            

                            if(results.get("idea")===true){
                            categoriesSelected.push(parseInt("11"));
                            }
                            if(results.get("challenge")===true){
                                categoriesSelected.push(parseInt("22"));
                            }
                            if(results.get("ideaCat")===true){
                                categoriesSelected.push(parseInt("0"));
                            }
                            if(results.get("needCat")===true){
                                categoriesSelected.push(parseInt("1"));
                            }
                            if(results.get("appCat")===true){
                                categoriesSelected.push(parseInt("2"));
                            }
                            if(results.get("pocCat")===true){
                                categoriesSelected.push(parseInt("3"));
                            }
                            if(results.get("microCat")===true){
                                categoriesSelected.push(parseInt("4"));
                            }
                            if(results.get("otherCat")===true){
                                categoriesSelected.push(parseInt("5"));
                            }

                         loadCards();

                        },

                        error: function(error) {
                        console.log("error setting categories on array");
                        }
                        });

                }
                else
                {
                 if(results[0].get("idea")===true){
                        categoriesSelected.push(parseInt("11"));
                    }
                    if(results[0].get("challenge")===true){
                        categoriesSelected.push(parseInt("22"));
                    }
                    if(results[0].get("ideaCat")===true){
                        categoriesSelected.push(parseInt("0"));
                    }
                    if(results[0].get("needCat")===true){
                        categoriesSelected.push(parseInt("1"));
                    }
                    if(results[0].get("appCat")===true){
                        categoriesSelected.push(parseInt("2"));
                    }
                    if(results[0].get("pocCat")===true){
                        categoriesSelected.push(parseInt("3"));
                    }
                    if(results[0].get("microCat")===true){
                        categoriesSelected.push(parseInt("4"));
                    }
                    if(results[0].get("otherCat")===true){
                        categoriesSelected.push(parseInt("5"));
                    }
                     loadCards();
                 }

        

  },
  error: function(error) {
        console.log("error saving categories on array");
  }
});


}


function loadCards (){

        Parse.Cloud.run("ideasFull", {
                cantidad: 200,
                skip: 0
        }, {
                success: function(ideafull) {
                        
                        var File = Parse.Object.extend("Archivo");
                        var query = new Parse.Query(File);
                        for (var i = 0; i < ideafull.length; i++) {
                                object = ideafull[i];
                                if (/*isInArray(object.idea.get("category"), categoriesSelected) === true*/true) {

                                        createCard(object.idea, object.comments, object.likes, object.files);
                                }
                                if(categoriesSelected.length==0){
                                     createCard(object.idea, object.comments, object.likes, object.files);   
                                }
                        }
                        if (ifDivInsideDiv(".most-recent", "div.card") == false) {
                                $(".most-recent").append(
                                        "<div id='column-is-empty-info'><i class='material-icons'>mood_bad</i><h2>No ideas yet</h2> <h5 class='message-noideas'>Tell us about your ideas or inventions to improve our company</h5><a class='dropdown-toggle new_idea_a btn first_new_idea' data-toggle='modal' data-target='#myModal' href='#''> Add New Idea</a> </div>"
                                );
                        }
                },
                error: function(object, error) {
                        // The object was not retrieved successfully.
                        // error is a Parse.Error with an error code and message.
                }
        });
}


function loadCardsNoFilter (){

        Parse.Cloud.run("ideasFull", {
                cantidad: 200,
                skip: 0
        }, {
                success: function(ideafull) {
                        
                        var File = Parse.Object.extend("Archivo");
                        var query = new Parse.Query(File);
                        for (var i = 0; i < ideafull.length; i++) {
                                object = ideafull[i];
                                        createCard(object.idea, object.comments, object.likes, object.files);
                                  
                        }
                        if (ifDivInsideDiv(".most-recent", "div.card") == false) {
                                $(".most-recent").append(
                                        "<div id='column-is-empty-info'><i class='material-icons'>mood_bad</i><h2>No ideas yet</h2> <h5 class='message-noideas'>Tell us about your ideas or inventions to improve our company</h5><a class='dropdown-toggle new_idea_a btn first_new_idea' data-toggle='modal' data-target='#myModal' href='#''> Add New Idea</a> </div>"
                                );
                        }
                },
                error: function(object, error) {
                        // The object was not retrieved successfully.
                        // error is a Parse.Error with an error code and message.
                }
        });
}

/*Parse.Cloud.run('setFilter', {}, {

       

  success: function(results) {




        

  },
  error: function(error) {
        console.log("error setting categories on array");
  }
});*/