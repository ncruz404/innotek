var draw_qrcode = function(text, typeNumber, errorCorrectLevel) {
  document.write(create_qrcode(text, typeNumber, errorCorrectLevel) );
};

var create_qrcode = function(text, typeNumber, errorCorrectLevel, table) {

  var qr = qrcode(typeNumber || 4, errorCorrectLevel || 'M');
  qr.addData(text);
  qr.make();

//  return qr.createTableTag();
  return qr.createImgTag();
};

var update_qrcode = function() {
  var form = document.forms['qrForm'];
  var text = form.elements['msg'].value.
    replace(/^[\s\u3000]+|[\s\u3000]+$/g, '');
  var t = form.elements['t'].value;
  var e = form.elements['e'].value;
  document.getElementById('qr').innerHTML = create_qrcode(text, t, e);
};


var QRid;


function creaQR() { 

  $.ajax({
    type: "POST",
    url: "https://api.parse.com/1/classes/QR",   
    data: '{"logged": false}',       
    headers: {"X-Parse-Application-Id": "j44FZf3JN2oz0iHBkLgcAlSSKS9JKRkKds9iMssX",
              "X-Parse-REST-API-Key": "tpyxLwknfrWjNvlXUqxG82l8TAYUnQFEi80PCKCj"},
    success: function(QR) {
      document.getElementById('qr').innerHTML = create_qrcode(QR.objectId, 8, 'H');
      QRid = QR.objectId;      
      window.setInterval(checkQR, 300);   
    },
    error: function(error) {
      console.log(error);
    }    
  }); 
}

function checkQR() {
  
  $.ajax({
    type: "GET",
    url: "https://api.parse.com/1/classes/QR/"+QRid,          
    headers: {"X-Parse-Application-Id": "j44FZf3JN2oz0iHBkLgcAlSSKS9JKRkKds9iMssX",
              "X-Parse-REST-API-Key": "tpyxLwknfrWjNvlXUqxG82l8TAYUnQFEi80PCKCj"},
    success: function(QR) {     
      if(QR.logged)
        creaSession();          
    },
    error: function(error) {
      console.log(error);
    }
  }); 

}

function creaSession(){
  Parse.User.logIn("ricardo.garza", "123qwe", {
    success: function(user) {     
      window.location = "home.html" 
    },
    error: function(user, error) {
      console.log("Screw you");
      alert("The login failed. Check error to see why.\n"+ error.message);
    }
  });
}