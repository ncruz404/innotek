﻿var LINQ = require('./linq.min.js');
var Pusher = require('./pusher.js');

//query para traer la página "page" de 10 ideas públicas más recientes.
//parámetros: 
//  -page: número de página (zero-based)
Parse.Cloud.define("firstIdeas", function(request, response) {
    Parse.Cloud.useMasterKey();
    var page = request.params.page; 
    var query = new Parse.Query("Idea");
    query.equalTo("esPrivado", false);
    query.limit(10);
    query.skip(10*page);
    query.include("user");
    query.descending("updatedAt");
    query.find({useMasterKey: true})
    .then(function(results) {
            response.success(results);
          },
          function(error) {
            response.error("Error: " + error.code + " " + error.message);
          });
});

//Type: Get
//Function that gives you a challenge class 
//Last modified: pending
//Last modified by: Samuel Martínez
//Created: pending
//Created by: Samuel Martínez
//(Parameters)
//  -challengeID: challenge’s objectId
//return
//  -hasmap object
//      --object (challenge object)
//      --ideas (the ideas that have this challenge)

Parse.Cloud.define('getChallenge', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();
    var challengeID = request.params.challengeID;
    var cQuery = new Parse.Query("Challenge");
    cQuery.include("user");
    cQuery.get(challengeID,{
        success: function(cCH){

            var queryIdeas = new Parse.Query("Idea");
            queryIdeas.equalTo("challenge",cCH);
            queryIdeas.include("user");
            queryIdeas.find({useMasterKey: true})
            .then(
                function(ideasR){
                    var queryUploads = new Parse.Query("ChallengeUploads");
                    queryUploads.include("idea");
                    queryUploads.equalTo("challenge",cCH);
                    queryUploads.find({useMasterKey: true})
                    .then(
                        function(chUResults){
                            response.success({object: cCH, ideas: ideasR, uploads: chUResults});
                        },
                        function(error){
                            response.error(error.message);
                        }
                    );
                },
                function(error){
                    console.error(error.message);
                    response.error(error.message);
                }
            );
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

//*CHECK LATER
Parse.Cloud.define('getWeeksofChallenge', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();
    var challengeID = request.params.challengeID;
    var cQuery = new Parse.Query("Challenge");
    cQuery.include("user");
    cQuery.get(challengeID,{
        success: function(cCH){
            var weekQuery = new Parse.Query("ChallengeWeek");
            weekQuery.equalTo("challenge",cCH);
            weekQuery.find({useMasterKey: true})
            .then(
                function(cWeeks){
                    response.success(cWeeks);
                },
                function(error){
                    response.error(error.message);
                }
            );
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

//Type: Get
//Function that indicates gives you the Idea class 
//Last modified: pending
//Last modified by: Samuel Martínez
//Created: pending
//Created by: Samuel Martínez
//(Parameters)
//  -ideaID: publication’s objectId
//return
//  -idea object with challenge included

Parse.Cloud.define("getIdea", function(request, response) {
    Parse.Cloud.useMasterKey();
    var ideaID = request.params.ideaID; 
    var query = new Parse.Query("Idea");
    query.include("user");
    query.include("ideaParent");
    query.include("challenge");
    query.get(ideaID,{
      success: function(result) {
        response.success(result);
      },
      error: function(error) {
        response.error("Error: " + error.code + " " + error.message);
      }
    });
});

Parse.Cloud.define("getCountryLikes", function(request, response){
        Parse.Cloud.useMasterKey();
        var ideaID = request.params.ideaID;
        var queryIdea = new Parse.Query("Idea");
        queryIdea.get(ideaID,{
            success: function(ideaObj){
                //var paisLike = request.params.pLike; 
                var paisLike = new Array();
                var paisLikeIndex = new Array();
                var positivo = 0;
                var Like = Parse.Object.extend("Like");
                var query = new Parse.Query(Like);
                query.equalTo("idea", ideaObj);
                query.include("user");
                for(var i = 0; i < paisLike.length; i++)
                {
                    paisLike[i].like = 0;
                    paisLike[i].dislike = 0;
                }
                query.find({useMasterKey: true})
                .then(
                  function(results) {
                    // Do something with the returned Parse.Object values
                    if(results.length <= 0)
                    {
                        paisLike.push({name:"Not valued",like:0,dislike:0});
                    }
                    else
                    {
                        for (var i = 0; i < results.length; i++) {
                            var object = results[i];
                            var isPos = object.get("esPositivo");
                            var loc = (object.get("user").has("locationCity"))? object.get("user").get("locationCity"): "GDC China";
                            if(paisLikeIndex.length == 0 || paisLikeIndex.indexOf(loc) <= -1)
                            {
                                if(isPos)
                                    paisLike.push({name: loc,like:1,dislike:0});
                                else
                                    paisLike.push({name: loc,like:0,dislike:1});

                                paisLikeIndex.push(loc);
                            }
                            else
                            {
                                var act = paisLike[paisLikeIndex.indexOf(loc)];
                                if(isPos)
                                    act.like++;
                                else
                                    act.dislike++;
                            }
                        };
                    }
                    /*for (var i = 0; i < results.length; i++) { 
                        var object = results[i];
                        for (var j = paisLike.length - 1; j >= 0; j--) {
                            if(object.get("user").get("pais") == paisLike[j].name)
                            {
                                switch (object.get("esPositivo"))
                                {
                                    case true: paisLike[j].like++; break;
                                    case false: paisLike[j].dislike++; break;
                                }
                            }
                        };
                        if(object.get("esPositivo"))
                            positivo++;
                    } */

                    response.success(paisLike);
                    //graph();
                  },
                  function(error) {
                    response.error("Error: " + error.code + " " + error.message);
                  }
                );
            },
            error: function(error){
                response.error(error.message);
            }
        });
        
});

Parse.Cloud.define("sideBar", function(request, response) {
    Parse.Cloud.useMasterKey();
    var user = Parse.Object.extend("User");
    var query = new Parse.Query(user);
    //query.notEqualTo("objectId","7eTMkQ1lmD");
    query.descending("points");
    query.limit(10);
    query.find({useMasterKey: true})
    .then(
        function(user) {
            response.success(user);
        },
        function(error) {
        //function(object, error) { //old
            // The object was not retrieved successfully.
            // error is a Parse.Error with an error code and message.
            response.error(error.message);
        }
    );
});

//*Check later
Parse.Cloud.define('checarOffice', function(request, response){
    Parse.Cloud.useMasterKey();
    var mail = request.params.mail;
    var queryUser = new Parse.Query("User");
    queryUser.equalTo("email",mail);
    queryUser.find({useMasterKey: true})
    .then(
        function(userResult){
         response.success(userResult);
        },
        function(error){
          response.error("Error al inicializar con Office CC");
        }
    );
});

//Type: Get
//Function that gives you a list of comments ordered by priority
//Last modified: pending
//Last modified by: Samuel Martínez
//Created: pending
//Created by: Samuel Martínez
//(Parameters)
//  -ideaID: publication’s objectId
//return
//  -array of Comment objects

Parse.Cloud.define("loadComments", function(request, response) {
    Parse.Cloud.useMasterKey();
    var ideaID = request.params.ideaID;
    var Comment = Parse.Object.extend("Comentario");
    var query = new Parse.Query(Comment);
    var queryIdea = new Parse.Query("Idea");
    queryIdea.get(ideaID,{
        success: function(ideaResult){
            query.equalTo("idea", ideaResult);
            query.include("user");
            query.descending("createdAt");
            query.find({useMasterKey: true})
            .then(
              function(results) {
                var queryDirective = new Parse.Query("Directivo");
                queryDirective.find({useMasterKey: true})
                .then(
                    function(directiveResults){
                        if(directiveResults.length > 0)
                        {
                            var directiveArray = [];
                            for (var i = directiveResults.length - 1; i >= 0; i--) {
                                directiveArray.push(directiveResults[i].get("email"));
                            };
                            console.log("DIRECTIVES YAY!");
                            console.log(directiveArray);
                            var queryComDir = LINQ.Enumerable.From(results)
                                .Where(function (x) { 
                                    return directiveArray.lastIndexOf(x.attributes.user.attributes.email) > -1
                                }).ToArray();
                            var queryComNotDir = LINQ.Enumerable.From(results)
                                .Where(function (x) { 
                                    return directiveArray.lastIndexOf(x.attributes.user.attributes.email) <= -1
                                }).ToArray();
                            var finalCommentArray = queryComDir.concat(queryComNotDir);
                            response.success(finalCommentArray);
                        }
                        else
                            response.success(results);
                    },
                    function(error){
                        response.error(error.message);
                    }
                );
              },
              function(error) {
                response.error("Error: " + error.code + " " + error.message);
              }
            );
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

//*Check later
Parse.Cloud.define("likeUser", function(request,response){
    var key = request.params.userID;
    var User = Parse.Object.extend("User");
    var query = new Parse.Query(User);
        
    query.get(key,{
        success: function(userResult){
            var Idea = Parse.Object.extend("Idea");
            var queryIdeas = new Parse.Query(Idea);
            queryIdeas.include("user");
            queryIdeas.equalTo("user",userResult);
            queryIdeas.find({useMasterKey: true})
            .then(
                function(ideasResult){
                    var result = {
                        user: userResult.id,
                        ideas: ideasResult,
                        count: ideasResult.length
                    };
                    response.success(result);
                },
                function(error) {
                    response.error("Error: " + error.code + " " + error.message);
                }
            );
        },
        error: function(error) {
            response.error("Error: " + error.code + " " + error.message);
        }
    });
});
    
        
Parse.Cloud.define("likePais", function(request, response) { 
    Parse.Cloud.useMasterKey(); //Made after the migration
    var key = request.params.ideaID;
    var ideaObj;
    var paisLike = [
    {name: "Mexico", like: 0 , dislike: 0},
    {name: "USA", like: 0 , dislike: 0},
    {name: "China", like: 0 , dislike: 0}];
        
    if(key != undefined)
    {
        var Idea = Parse.Object.extend("Idea");
        var query = new Parse.Query(Idea);
        query.include("user");
        query.get(key, {
          success: function(ideaResult) {
            // The object was retrieved successfully.
            ideaObj = ideaResult;
            var Like = Parse.Object.extend("Like");
            var query = new Parse.Query(Like);
            query.equalTo("idea", ideaObj);
            query.include("user");
            query.find({useMasterKey: true})
            .then(
              function(results) {
                // Do something with the returned Parse.Object values
                for (var i = 0; i < results.length; i++) { 
                    var object = results[i];
                    for (var j = paisLike.length - 1; j >= 0; j--) {
                        if(object.get("user").get("pais") == paisLike[j].name)
                        {
                            switch (object.get("esPositivo"))
                            {
                                case true: paisLike[j].like++; break;
                                case false: paisLike[j].dislike--; break;
                            }
                        }
                    };
                }
                response.success(paisLike);
              },
              function(error) {
                response.error("Error: " + error.code + " " + error.message);
              }
            );
          },
          error: function(object, error) {
            // The object was not retrieved successfully.
            // error is a Parse.Error with an error code and message.
            response.error("Error: " + error.code + " " + error.message);
          }
        });
        
    }
});
        
//query para traer la página "skip" de "cantidad" ideas públicas más recientes.
//parámetros: 
//  -skip: número de página (zero-based)
//  -cantidad: número de ideas por página
//return
//  -hashmap de Parseobjects que tienen:
//      --idea
//      --suma de likes positivos+negativos
//      --num de comentarios
//      --num de archivos
Parse.Cloud.define("ideasFull", function (request, response) {  
    //var Idea = Parse.Object.extend("Idea");
    Parse.Cloud.useMasterKey();
    var query = new Parse.Query("Idea");
    var cant = request.params.cantidad; 
    var skip = request.params.skip;
    ideas =  [];
    ideasIndex = [];
    files = [];
    comments = [];
    likes = [];
    query.equalTo("esPrivado", false);
    query.include("user");
    query.descending("createdAt");
    query.limit(cant);
    query.skip(cant*skip);
    query.find({useMasterKey: true})
    .then(
        function (idea) {
            var queryCH = new Parse.Query("Challenge");
            queryCH.ascending("createdAt");
            queryCH.limit(20);
            queryCH.include("user");
            queryCH.find({useMasterKey: true})
            .then(
                function(challenges)
                {

                    var views = []; 
                 
                    for (var i = idea.length - 1; i >= 0; i--) {
                     
                        var likeSum = idea[i].get("likeCount");
                        var commentSum = idea[i].get("commentCount");
                        var fileSum = idea[i].get("fileCount");
                        
                        // var val ={like:0, dislike: 0};

                        // val.like = ideas[i].get("likePos");
                        // val.dislike = val.like - likeSum;
                        views.push({idea: idea[i], comments: commentSum, likes: likeSum, files: fileSum});
                    };

                    for (var i = 0; i < challenges.length; i++) {
                        //views.push({idea: challenges[i],comments: 0, likes: 0,files: 0});
                    };
                    var linqQuery = LINQ.Enumerable.From(views).OrderByDescending("$.idea.createdAt").ToArray();
                    response.success(linqQuery);
                },
                function(errorCH){
                    response.error(errorCH);
                }
            );
            //console.log(Enumerable.From(ideas).ToArray());            
        },
        function (error) {
            // body...
            response.error(error);
        }
    );
});
     
Parse.Cloud.define("ideasUser", function (request, response) {  
    //var Idea = Parse.Object.extend("Idea");
    Parse.Cloud.useMasterKey();
    var currentUserID = request.params.userID;
    var currentUser;
    var User = Parse.Object.extend("User");
    var queryUser = new Parse.Query(User);
    queryUser.get(currentUserID,{
      success: function(userResult){
        currentUser = userResult;
      },
      error: function(error){
        response.error("Error al buscar el usuario");
      }  
    }).then(function(){
 
        var query = new Parse.Query("Idea");
        var cant = request.params.cantidad; 
        var skip = request.params.skip;    
        ideas =  [];
        ideasIndex = [];
        files = [];
        comments = [];
        likes = [];
 
        if(currentUserID != request.params.reqID)
        query.equalTo("esPrivado", false);
 
        query.include("user");
        query.include("challenge");
        query.descending("createdAt");
        query.limit(cant);
        query.equalTo("user",currentUser);
        query.skip(cant*skip);
        query.find({useMasterKey: true})
        .then(
            function (idea) {
                for (var i = idea.length - 1; i >= 0; i--) {
                    ideas.push(idea[i]);
                    ideasIndex.push(idea[i].id);
                };          
                //console.log(Enumerable.From(ideas).ToArray());            
            },
            function (error) {
                // body...
            }
        ).then(function() {
            var promises = [];
            for (var i = ideas.length - 1; i >= 0; i--) {
                //var Like = Parse.Object.extend("Like");
                var queryLike = new Parse.Query("Like");
                queryLike.containedIn("idea", [ideas[i]]);
                promises.push(queryLike.find({ //this one works with promises
                    success: function (like) { //so, it's weird
                        if(like.length > 0)                  
                            likes[ideasIndex.lastIndexOf(like[0].get("idea").id)] = like;
                    },
                    error: function (error) {
                        response.error("Error: " + error.code + " " + error.message);
                    }   
                }));
            };
            Parse.Promise.when(promises).then(function  (argument) {
                //console.log(Enumerable.From(likes).ToArray());
                makeObjects();
            });
        }); 
              
        function makeObjects() {
            var views = []; 
              
            for (var i = ideas.length - 1; i >= 0; i--) {
              
                var likeSum = 0;
                var commentSum = ideas[i].get("commentCount");
                var fileSum = ideas[i].get("fileCount");
                var likeTotal = 0;
              
                if(likes[i] != undefined)           
                    for (var j = likes[i].length - 1; j >= 0; j--) {
                        if(likes[i][j].get("esPositivo"))
                            likeSum++;
                        else
                            likeSum--;
                        likeTotal++;
                    };

                views.push({idea: ideas[i], comments: commentSum,likesTotal: likeTotal, likes: likeSum, likesTotal: likeTotal, files: fileSum});
            };
            response.success(views);
        }
          
 
    });
});
 
//query para traer una idea con sus likes, comentarios y archivos completos
//parámetros: 
//  -oI: objectId de la idea
//return
//  -hashmap de Parseobjects que tienen:
//      --idea
//      --suma de likes positivos+negativos
//      --arreglo de comentarios
//      --arreglo de archivos
Parse.Cloud.define("unaIdeaFull", function (request, response) { 
    Parse.Cloud.useMasterKey();
    //var Idea = Parse.Object.extend("Idea");
    var query = new Parse.Query("Idea");
    var oID = request.params.oID; ; 
     
    //GET CURRENT USER
    var currentUserID = request.params.userID;
    var currentUser;
    var User = Parse.Object.extend("User");
    var queryUser = new Parse.Query(User);
    queryUser.get(currentUserID,{
      success: function(userResult){
        currentUser = userResult;
      },
      error: function(error){
        response.error("Error al buscar el usuario");
      }  
    });
 
    ideas =  [];
    ideasIndex = [];
    files = [];
    comments = [];
    likes = [];
    roster = [];
    likeArray = {Like:"Like",Dislike:"Dislike"};
    query.equalTo("objectId", oID);
    query.include("user");
    query.include("challenge");
    query.descending("createdAt");
    query.find({useMasterKey: true})
    .then(
        function (idea) {
            for (var i = idea.length - 1; i >= 0; i--) {
                ideas.push(idea[i]);
                ideasIndex.push(idea[i].id);
            };          
            //console.log(Enumerable.From(ideas).ToArray());            
        },
        function (error) {
            // body...
        }
    ).then(function() {
        var promises = [];
        for (var i = ideas.length - 1; i >= 0; i--) {
            //var Archivo = Parse.Object.extend("Archivo");
            var queryArchivo = new Parse.Query("Archivo");
            queryArchivo.containedIn("idea", [ideas[i]]);
            promises.push(queryArchivo.find({ //this one also works with promises
                success: function (file) { //it's also weird, have to test
                    if(file.length > 0)                  
                        files[ideasIndex.lastIndexOf(file[0].get("idea").id)] = file;
                },
                error: function (error) {
                    response.error("Error: " + error.code + " " + error.message);
                }   
            }));
        };
        Parse.Promise.when(promises).then(function  (argument) {
            //console.log(Enumerable.From(files).ToArray());
        });
    }).then(function() {
        Parse.Cloud.useMasterKey();
        var promises = [];
        for (var i = ideas.length - 1; i >= 0; i--) {
            //var Comentario = Parse.Object.extend("Comentario");
            var queryComment = new Parse.Query("Comentario");
            queryComment.containedIn("idea", [ideas[i]]);
            queryComment.descending("createdAt");
            queryComment.include("user");
            promises.push(queryComment.find({ //more promises to check
                success: function (comment) {
                    if(comment.length > 0)                   
                        comments[ideasIndex.lastIndexOf(comment[0].get("idea").id)] = comment;
                },
                error: function (error) {
                    response.error("Error: " + error.code + " " + error.message);
                }   
            }));
        };
        Parse.Promise.when(promises).then(function  (argument) {
            //console.log(Enumerable.From(comments).ToArray());
        });
    }).then(function(){
        var promises = [];
        for (var i = ideas.length - 1; i >= 0; i--) {
            //var Comentario = Parse.Object.extend("Comentario");
            var queryLike = new Parse.Query("Like");
            queryLike.include("idea");
            queryLike.include("user");
            queryLike.equalTo("idea",ideas[i]);
            queryLike.equalTo("user",currentUser);
            promises.push(queryLike.find({ //yet another weird with promises
                success: function (likeResult) {
                    if(likeResult.length > 0)
                    {
                        var objIdea = likeResult[0];
                        if(objIdea.get("esPositivo"))
                            likeArray.Like = "You Like this";
                        else
                            likeArray.Dislike = "You Dislike this";
                    }
                },
                error: function (error) {
                    response.error("Error: " + error.code + " " + error.message);
                }   
            }));
        };
        Parse.Promise.when(promises).then(function  (argument) {
            //console.log(Enumerable.From(comments).ToArray());
        });
    }).then(function() {
        var promises = [];
        for (var i = ideas.length - 1; i >= 0; i--) {
            //var Like = Parse.Object.extend("Like");
            var queryLike = new Parse.Query("Like");
            queryLike.containedIn("idea", [ideas[i]]);
            promises.push(queryLike.find({ //one more promise weirdo
                success: function (like) {
                    if(like.length > 0)                  
                        likes[ideasIndex.lastIndexOf(like[0].get("idea").id)] = like;
                },
                error: function (error) {
                    response.error("Error: " + error.code + " " + error.message);
                }   
            }));
        };
        Parse.Promise.when(promises).then(function  (argument) {
            //console.log(Enumerable.From(likes).ToArray());
        });
    }).then(function() {
        var promises = [];
        for (var i = ideas.length - 1; i >= 0; i--) {
            //var Like = Parse.Object.extend("Like");
            var queryRos = new Parse.Query("Roster");
            queryRos.include("user");
            queryRos.containedIn("idea", [ideas[i]]);

            promises.push(queryRos.find({ //another promises stuff
                success: function (ros) {
                    if(ros.length > 0)                  
                        roster[ideasIndex.lastIndexOf(ros[0].get("idea").id)] = ros;

                },
                error: function (error) {
                    response.error("Error: " + error.code + " " + error.message);
                }   
            }));
        };
        Parse.Promise.when(promises).then(function  (argument) {
            //console.log(Enumerable.From(likes).ToArray());
            makeObjects();
        });
    }); 
         
    function makeObjects() {
        var views = []; 
         
        for (var i = ideas.length - 1; i >= 0; i--) {
         
            var likeSum = 0;
            var commentSum = 0;
            var fileSum = 0;
            var totalLikes = 0;
            var val = {like: 0, dislike: 0};
            var allRostersId = new Array();
            if(roster[i] != undefined)
                for (var j = 0; j < roster[i].length; j++) {
                   allRostersId.push(roster[i][j].get("user").id);
                };
            if(likes[i] != undefined)           
                for (var j = likes[i].length - 1; j >= 0; j--) {
                    if(likes[i][j].get("esPositivo"))
                        {
                            likeSum++;
                            val.like++;
                        }
                    else
                    {
                        likeSum--;
                        val.dislike++;
                    }
                    totalLikes++;
                };

            views.push({roster: allRostersId,idea: ideas[i], comments: comments[i],valoracion: val,likesTotal: totalLikes, likes: likeSum, files: files[i], likeStatus: likeArray});
        };
        response.success(views);
    }
});
   
   
   
   
//query para traer la página "skip" de "cantidad" ideas públicas más recientes.
//parámetros: 
//  -skip: número de página (zero-based)
//  -cantidad: número de ideas por página
//return
//  -hashmap de Parseobjects que tienen:
//      --idea
//      --suma de likes positivos+negativos
//      --num de comentarios
//      --num de archivos
Parse.Cloud.define("popularideas", function (request, response) {  
    //var Idea = Parse.Object.extend("Idea");
    Parse.Cloud.useMasterKey();
    var query = new Parse.Query("Idea");
    var cant = request.params.cantidad; 
    var skip = request.params.skip; 
    ideas =  [];
    ideasIndex = [];
    files = [];
    comments = [];
    likes = [];
    query.equalTo("esPrivado", false);
    query.include("user");
    query.descending("createdAt");
    query.descending("likeCount");
    query.limit(cant);
    query.skip(cant*skip);
    query.find({useMasterKey: true})
    .then(
        function (idea) {
            for (var i = idea.length - 1; i >= 0; i--) {
                ideas.push(idea[i]);
                ideasIndex.push(idea[i].id);
            };          

            var views = []; 
         
            for (var i = ideas.length - 1; i >= 0; i--) {
             
                var likeSum = ideas[i].get("likeCount");
                var commentSum = ideas[i].get("commentCount");
                var fileSum = ideas[i].get("fileCount");
             
                views.push({idea: ideas[i], comments: commentSum, likes: likeSum, files: fileSum});
            };

            var sort_by = function(field, reverse, primer){
   
               var key = primer ? 
                   function(x) {return primer(x[field])} : 
                   function(x) {return x[field]};
               
               reverse = !reverse ? 1 : -1;
               
               return function (a, b) {
                   return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
                 } 
            }

            views.sort(sort_by('likes', true, parseInt));
   
            response.success(views);

            //console.log(Enumerable.From(ideas).ToArray());            
        },
        function (error) {
            // body...
        }
    );
});
   
   
   
//query para traer la página "skip" de "cantidad" ideas públicas más recientes.
//parámetros: 
//  -skip: número de página (zero-based)
//  -cantidad: número de ideas por página
//return
//  -hashmap de Parseobjects que tienen:
//      --idea
//      --suma de likes positivos+negativos
//      --num de comentarios
//      --num de archivos
Parse.Cloud.define("activeideas", function (request, response) {  
    //var Idea = Parse.Object.extend("Idea");
    Parse.Cloud.useMasterKey();
    var query = new Parse.Query("Idea");
    var cant = request.params.cantidad; 
    var skip = request.params.skip; 
    ideas =  [];
    ideasIndex = [];
    files = [];
    comments = [];
    likes = [];
    query.equalTo("esPrivado", false);
    query.include("user");
    query.descending("createdAt");
    query.descending("commentCount");
    query.limit(cant);
    query.skip(cant*skip);
    query.find({useMasterKey: true})
    .then(
        function (idea) {
            for (var i = idea.length - 1; i >= 0; i--) {
                ideas.push(idea[i]);
                ideasIndex.push(idea[i].id);
            };

            var views = []; 
         
            for (var i = ideas.length - 1; i >= 0; i--) {
             
                var likeSum = ideas[i].get("likeCount");
                var commentSum = ideas[i].get("commentCount");
                var fileSum = ideas[i].get("fileCount");

                views.push({idea: ideas[i], comments: commentSum, likes: likeSum, files: fileSum});
            };
       
            var sort_by = function(field, reverse, primer){
       
               var key = primer ? 
                   function(x) {return primer(x[field])} : 
                   function(x) {return x[field]};
               
               reverse = !reverse ? 1 : -1;
               
               return function (a, b) {
                   return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
                 } 
            }
       
            views.sort(sort_by('comments', true, parseInt));
       
            response.success(views);          
            //console.log(Enumerable.From(ideas).ToArray());            
        },
        function (error) {
            // body...
        }
    );
});
  
  
//Query para enviar un like
//parámetros:
//  -idea
//  -usuario
//  -esPositivo
Parse.Cloud.define("aplicarLike", function(request,response){
    Parse.Cloud.useMasterKey();
    //parametros
    var usuario = request.params.userID;
    var idea = request.params.ideaID;
    var positivo = request.params.esPositivo;
    //Objetos
    var User = Parse.Object.extend("User");
    var IdeaObj = Parse.Object.extend("Idea");
    var Like = Parse.Object.extend("Like");
 
    //Reult like
    var rLikes = {Like:"Like", Dislike: "Dislike", Count: 0, Pos: 0};
 
    var queryUser = new Parse.Query(User);
    queryUser.get(usuario,{
        success: function(userResult){
            var queryIdea = new Parse.Query(IdeaObj);
            queryIdea.get(idea,{
                success: function(ideaResult){
                    var queryLike = new Parse.Query(Like);
                    queryLike.include("idea");
                    queryLike.include("user");
                    queryLike.equalTo("idea",ideaResult);
                    queryLike.equalTo("user",userResult);
                    rLikes.Count = ideaResult.get("likeCount");
                    rLikes.Pos = ideaResult.get("likePos");
                    queryLike.find({useMasterKey: true})
                    .then(
                        function(likeResult){
                            if(likeResult.length == 0)
                            {
                                var newLike = new Like();
                                newLike.set("user", userResult);
                                newLike.set("esPositivo", positivo);
                                newLike.set("idea", ideaResult);
                                newLike.save(null, {
                                  success: function(gameScore) {
                                    // Execute any logic that should take place after the object is saved.
                                    var count = 0;
                                    if(positivo)
                                    {
                                        rLikes.Like = "You Like this";
                                        count = 1;
                                        ideaResult.increment("likeCount");
                                        ideaResult.increment("likePos");
                                        console.log("********************----HERE IN LIKE POS"+ideaResult.get("likeCount"));
                                    }
                                    else
                                    {
                                        rLikes.Dislike = "You Dislike this";
                                        count = -1;
                                        console.log(ideaResult.get("likeCount"));
                                        ideaResult.increment("likeCount",-1);
                                        console.log("********************----HERE IN LIKE NEG"+ideaResult.get("likeCount"));
                                    }
                                    // var currentLikeCount = Number((ideaResult.get("likeCount") == undefined)? 0 : ideaResult.get("likeCount"));

                                    // ideaResult.set("likeCount",currentLikeCount+count);

                                    // var currentLikePos = Number((ideaResult.get("likePos") == undefined)? 0 : ideaResult.get("likePos"));
                                    // if(count == 1) ideaResult.set("likePos",currentLikePos+count);
                                    
                                    ideaResult.save(null,{
                                        success: function(obj){
                                            rLikes.Count = obj.get("likeCount");
                                            rLikes.Pos = obj.get("likePos");
                                            console.log(rLikes);
                                            response.success(rLikes);
                                        },
                                        error: function(errorSI){
                                            response.error(errorSI.message);
                                        }
                                    });
                                    
                                  },
                                  error: function(gameScore, error) {
                                    // Execute any logic that should take place if the save fails.
                                    // error is a Parse.Error with an error code and message.
                                    console.error('Failed to create new object, with error code: ' + error.message);
                                  }
                                });
                            }
                            else
                            {
                                var likeObject = likeResult[0];
                                if(likeObject.get("esPositivo") != positivo)
                                {
                                    likeObject.set("esPositivo",positivo);
                                    likeObject.save(null, {
                                      success: function(gameScore) {
                                        // Execute any logic that should take place after the object is saved.
                                        var count = 0;

                                        if(positivo)
                                        {
                                            rLikes.Like = "You Like this";
                                            count = 2;
                                            ideaResult.increment("likeCount",2);
                                            ideaResult.increment("likePos");
                                            console.log("********************----HERE CH LIKE POS"+ideaResult.get("likeCount"));
                                        }
                                        else
                                        {
                                            rLikes.Dislike = "You Dislike this";
                                            count = -2;
                                            ideaResult.increment("likeCount",-2);
                                            ideaResult.increment("likePos",-1);
                                            console.log("********************----HERE CH LIKE NEG"+ideaResult.get("likeCount"));
                                        }
 
                                        // var currentLikeCount = Number((ideaResult.get("likeCount") == undefined)? 0 : ideaResult.get("likeCount"));
                                        // ideaResult.set("likeCount",currentLikeCount+count);

                                        // var currentLikePos = Number((ideaResult.get("likePos") == undefined)? 0 : ideaResult.get("likePos"));
                                        // if(count == 2) ideaResult.set("likePos",currentLikePos+1);
                                        // else ideaResult.set("likePos",currentLikePos-1);

                                        ideaResult.save(null,{
                                            success: function(obj){
                                                rLikes.Count = obj.get("likeCount");
                                                rLikes.Pos = obj.get("likePos");
                                                console.log(rLikes);
                                                response.success(rLikes);
                                            },
                                            error: function(errorSI){
                                                response.error(errorSI.message);
                                            }
                                        });
                                      },
                                      error: function(gameScore, error) {
                                        // Execute any logic that should take place if the save fails.
                                        // error is a Parse.Error with an error code and message.
                                        response.error('Failed to create new object, with error code: ' + error.message);
                                      }
                                    });
                                }
                                else
                                {
                                    likeObject.destroy({
                                      success: function(myObject) {
                                        // The object was deleted from the Parse Cloud.
                                        var count = 0;
                                        if(positivo)
                                        {
                                            count = -1;
                                            ideaResult.increment("likeCount",-1);
                                            ideaResult.increment("likePos",-1);
                                            console.log("********************----HERE DEL LIKE POS"+ideaResult.get("likeCount"));

                                        }
                                        else
                                        {
                                            count = 1;
                                            ideaResult.increment("likeCount");
                                            console.log("********************----HERE DEL LIKE NEG"+ideaResult.get("likeCount"));
                                        }
                                        // var currentLikeCount = Number((ideaResult.get("likeCount") == undefined)? 0 : ideaResult.get("likeCount"));
                                        // ideaResult.set("likeCount",currentLikeCount+count);

                                        // var currentLikePos = Number((ideaResult.get("likePos") == undefined)? 0 : ideaResult.get("likePos"));
                                        // if(count == -1) ideaResult.set("likePos",currentLikePos+count);

                                        ideaResult.save(null,{
                                            success: function(obj){
                                                rLikes.Count = obj.get("likeCount");
                                                rLikes.Pos = obj.get("likePos");
                                                console.log(rLikes);
                                                response.success(rLikes);
                                            },
                                            error: function(errorSI){
                                                response.error(errorSI.message);
                                            }
                                        });
                                      },
                                      error: function(myObject, error) {
                                        // The delete failed.
                                        // error is a Parse.Error with an error code and message.
                                      }
                                    });
                                }
                            }
                        },
                        function(error){
                            response.error("Error :"+error.message);
                        }
                    );
                },
                error: function(error){
                    response.error("Error :"+error.message);
                }
            });
        },
        error: function(error){
            response.error("Error : "+ error.message);
        }
    });
});
 
//Type: Insert
//Function that lets you save a comment
//Last modified: pending
//Last modified by: Samuel Martínez
//Created: pending
//Created by: Samuel Martínez
//(Parameters)
//  -userID: user’s objectId
//  -comentario: comment text
//  -ideaID: publication’s objectId
//return
//  -boolean

Parse.Cloud.define("aplicarComentario", function(request,response){

    Parse.Cloud.useMasterKey(); //Made after the migration
    //parametros
    var usuario = request.params.userID;
    var idea = request.params.ideaID;
    var comment = request.params.comentario;
    //Objetos
    var User = Parse.Object.extend("User");
    var IdeaObj = Parse.Object.extend("Idea");
    var Comentario = Parse.Object.extend("Comentario");
    var result = {Status:false};
 
    var queryUser = new Parse.Query(User);
    queryUser.get(usuario,{
        success: function(userResult){
            var queryIdea = new Parse.Query(IdeaObj);
            queryIdea.get(idea,{
                success: function(ideaResult){
                    var comObj = new Comentario();
                    comObj.set("user",userResult);
                    comObj.set("idea",ideaResult);
                    comObj.set("texto",comment);
                    comObj.save(null,{
                        success: function(newComentario)
                        {
                            result.Status = true;
                            response.success(result);
                        },
                        error: function(errorR)
                        {
                            response.error(result);
                        }
                    });
                },
                error: function(error){
                    response.error(result);
                }
            });
        },
        error: function(error){
            response.error(result);
        }
    });
});
 

Parse.Cloud.define("subirIdea", function(request,response){
    Parse.Cloud.useMasterKey(); //Made after the migration
    //parametros
    var usuario = request.params.userID;
    var titulo = request.params.titulo;
    var descripcion = request.params.descripcion;
    var esPrivado = request.params.esPrivado;
    var tags = request.params.tags;
    var category = request.params.category;

    //Objetos
    var User = Parse.Object.extend("User");
    var IdeaObj = Parse.Object.extend("Idea");
    var result = {Status:false, ideaID: "undefined"};
 
    var queryUser = new Parse.Query(User);
    queryUser.get(usuario,{
        success: function(userResult){
            var newIdea = new IdeaObj();
            newIdea.set("user",userResult);
            newIdea.set("titulo",titulo);
            newIdea.set("descripcion",descripcion);
            newIdea.set("esPrivado",esPrivado);
            newIdea.set("category",category);
            newIdea.set("tags",tags);
            newIdea.save(null,{
                success: function(createdIdea)
                {
                    result.Status = true;
                    result.ideaID = createdIdea.id;
                    response.success(result);
                },
                error: function(errorR)
                {
                    response.error(result);
                }
            });
                 
        },
        error: function(error){
            response.error(result);
        }
    });
});
 
//Necesita
//-file (Tipo Archivo)
//-name (Nombre del archivo con extension)
//-ideaID
//Regresa un result con el atributo Status tipo booleano
Parse.Cloud.define("subirArchivo", function(request,response){
    Parse.Cloud.useMasterKey(); //Made after the migration
    //parametros
    var idea = request.params.ideaID;
    var parseFile = request.params.file;
    var name = request.params.name;
 
    //Objetos
    var Archivo = Parse.Object.extend("Archivo");
    var IdeaObj = Parse.Object.extend("Idea");
    var result = {Status:false, Message:""};
 
    var queryIdea = new Parse.Query(IdeaObj);
    queryIdea.get(idea,{
        success: function(ideaResult){
             
            var newFile = new Archivo();
            newFile.set("file",parseFile);
            newFile.set("nombre",name);
            newFile.set("idea",ideaResult);
            newFile.save(null,{
                success: function(newComentario)
                {
                    result.Status = true;
                    result.Message = "Guardado";
                    response.success(result);
                },
                error: function(errorR)
                {
                    result.Message = errorR.message;
                    response.error(result);
                }
            });
                 
        },
        error: function(error){
            result.Message = error.message;
            response.error(result);
        }
    });
});
 
Parse.Cloud.define("quitarArchivo", function(request,response){
    Parse.Cloud.useMasterKey(); //Made after the migration
    //parametros
    var arch = request.params.archivoID;
    //Objetos
    var Archivo = Parse.Object.extend("Archivo");
    var result = {Status:false, Message:""};
 
    var queryArchivo = new Parse.Query(Archivo);
    queryArchivo.get(arch,{
        success: function(archivoResult){
             
            archivoResult.destroy({
                success: function(newComentario)
                {
                    result.Status = true;
                    result.Message = "Guardado";
                    response.success(result);
                },
                error: function(errorR)
                {
                    result.Message = errorR.message;
                    response.error(result);
                }
            });
                 
        },
        error: function(error){
            result.Message = error.message;
            response.error(result);
        }
    });
});

Parse.Cloud.define("cambiarImagenPerfil",function(request,response){
    Parse.Cloud.useMasterKey();
    var userID = request.params.userID;
    var file = request.params.File;
    var uQuery = new Parse.Query("User");
    uQuery.get(userID,{
        success: function(cUser){
            cUser.set("image",file);
            cUser.save(null, {
              success: function(object) {
                response.success(object);
              },
              error: function(model, error) {
                response.error("Didnt Saved!");
              }
            });
        },
        error: function(error){
            response.error(error.message);
        }
    });
});
 
//Necesita
//-userID 
//-file (parseFile)
//-name
//-newPassword
//-oldPassword
//Regresa un result con el atributo Status tipo booleano
Parse.Cloud.define("cambiarDatosPerfil", function(request,response){
    //parametros
    Parse.Cloud.useMasterKey();
    var userID = request.params.userID;
    var parseFile = request.params.file;
    var name = request.params.name;
    var newPassword = request.params.newPassword;
    var oldPassword = request.params.oldPassword;
 
    //Objetos
    var User = Parse.Object.extend("User");
    var result = {Status:false, Message:""};
 
    var queryUser = new Parse.Query(User);
    queryUser.get(userID,{
        success: function(userResult){
             
            if(parseFile != null)
            {
                userResult.set("image",parseFile);
                result.Message+="Cambio Imagen, ";
            }
 
            if(name != null)
            {
                userResult.set("name",name);
                result.Message+="Cambio Nombre, ";
            }
 
            if(newPassword != null && oldPassword == userResult.get("password"))
            {
                userResult.set("password",newPassword);
                result.Message+="Cambio Password, ";
            }
 
            var queryPass = new Parse.Query(User);
            queryPass.equalTo("password", oldPassword);
            queryPass.equalTo("username",userResult.get("username"));
            Parse.User.logIn(userResult.get("username"), oldPassword,{
                success: function(userPassResult)
                {
 
                    userResult.set("password", newPassword);
                    result.Message+= "Cambio Password, ";
                    userResult.save(null,{
                        success: function(newComentario)
                        {
                            result.Status = true;
                            result.Message += "Guardado";
                            response.success(result);
                        },
                        error: function(errorR)
                        {
                            result.Message = errorR.message;
                            response.error(result);
                        }
                    });
                },
                error: function(errorQ)
                {
                    result.Message+= "No cambio la Contraseña, ";
                    userResult.save(null,{
                        success: function(newComentario)
                        {
                            result.Status = true;
                            result.Message += "Guardado";
                            response.success(result);
                        },
                        error: function(errorR)
                        {
                            result.Message = errorR.message;
                            response.error(result);
                        }
                    });
                }
            });
                 
        },
        error: function(error){
            result.Message = error.message;
            response.error(result);
        }
    });
});
 
Parse.Cloud.define("cambiarDatosIdea", function(request,response){
    Parse.Cloud.useMasterKey();
    //parametros
    var idea = request.params.ideaID;
    var descripcion = request.params.descripcion;
    var tags = request.params.tagsIdea;
 
    //Objetos
    var IdeaClass = Parse.Object.extend("Idea");
 
    var queryIdea = new Parse.Query(IdeaClass);
    queryIdea.get(idea,{
        success: function(ideaResult){
            ideaResult.set("descripcion",descripcion);
            ideaResult.set("tags",tags);
            ideaResult.save(null,{
                success: function(createdIdea)
                {
                    response.success("Guardado");
                },
                error: function(errorR)
                {
                    response.error(errorR.message);
                }
            });
                 
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.define("saveChallenge",function(request,response){
    Parse.Cloud.useMasterKey();
    var title = request.params.title;
    var descr = request.params.descr;
    var userID = request.params.userID;
    var userQ = new Parse.Query("User");
    userQ.get(userID,{
        success: function(user){
            var CH = Parse.Object.extend("Challenge");
            var CHObject = new CH();
            CHObject.set("descripcion",descr);
            CHObject.set("titulo",title);
            CHObject.set("user",user);
            CHObject.save(null,{
                success: function(savedCH){
                    response.success(savedCH);
                },
                error: function(error){
                    response.error(error.message);
                }
            });
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.define("getSemanaTipoAsOptions",function(request,response){
    Parse.Cloud.useMasterKey();
    var stQ = new Parse.Query("SemanaTipo");
    stQ.find({useMasterKey: true})
    .then(
       function(stR){
           var result = "";
           for (var i = 0; i < stR.length; i++) {
               result+= "<option value='"+stR[i].get("tipo")+"'>"+stR[i].get("descripcion")+"</option>";
           };
           response.success(result);
       },
       function(error){
           response.error(error.message);
       }
   );
});

Parse.Cloud.beforeSave("RegisterTime", function (request, response) {
    //check if user being saved has profile picture.
    if (!request.object.has("countries") || request.object.get("gpoints").length > request.object.get("countries").length) {
        console.log("yes");
        var gpList = request.object.get("gpoints");
        var countries = (request.object.has("countries"))? request.object.get("countries") : [];
        var gp = gpList[gpList.length -1];
        if(gp!= null && gp.toString().indexOf("ParseGeoPoint") > -1)
        {
            var gpString = gp.toString();
            var b = gpString.indexOf('[')+1;
            var f = gpString.indexOf(']');
            var arrayGP = gpString.substring(b,f).split(',');
            if(arrayGP.length > 2 && arrayGP.length == 4)
            {
                arrayGP[0] = arrayGP[0]+"."+arrayGP[1];
                arrayGP[1] = arrayGP[2]+"."+arrayGP[3];
            }
            var parseGP = new Parse.GeoPoint(Number(arrayGP[0]),Number(arrayGP[1]));
            gp = parseGP;
        }
        console.log(gp.latitude+" : "+gp.longitude);
        Parse.Cloud.httpRequest({
            method: 'GET',
            url: "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDkqRzKqUWgxkmI9rMI3cvo9EvYvM6oVXA&latlng="+gp.latitude+","+gp.longitude+"&sensor=false&language=en",
            success: function(httpResponse) {
                var data = httpResponse.data;
                var address = data.results[data.results.length -1];
                var listAddress = address.address_components;
                var listPossibleC = LINQ.Enumerable.From(listAddress).Where(function (x) { 
                    return x.types.lastIndexOf("country") > -1
                }).ToArray();
                var currentC;
                if(listPossibleC.length > 0)
                    currentC = listPossibleC[0].long_name;
                else
                    currentC = "WC";
                countries.push(currentC);
                request.object.set("countries",countries);
                console.log(countries);          
                response.success();
            },
            error: function(httpResponse) {
              response.error('Request failed with response code ' + httpResponse.status);
            }
        });
    }
    else
    {
        console.log("no");
        response.success();
    }   
});

Parse.Cloud.define("setUserLocation",function(request,response){
    Parse.Cloud.useMasterKey();
    var user = request.user;
    if(user.has("token"))
    {
        var token = 'Bearer '+user.get("token");
          Parse.Cloud.httpRequest({
            method: 'GET',
                url: "https://graph.microsoft.com/beta/me",
                body: {
                        "tenant": "softtek.com"
                    },
                headers: {
                    'Authorization': token
                },
            success: function(httpResponse) {
                var country = (httpResponse.data.country!=null)?httpResponse.data.country: "Not Defined";
                var city = (httpResponse.data.officeLocation!= null)?httpResponse.data.officeLocation : "Not Defined";
                user.set("locationCountry",country);
                user.set("locationCity",city);
                user.save(null,{
                    success: function(saved){
                        response.success(saved);
                    },
                    error: function(error){
                        response.error(error.message)
                    }
                });
            },
            error: function(httpResponse) {
              response.error('Request failed with response code ' + httpResponse.status);
            }
          });
    }
    else
        response.success(user);
}); 
 
Parse.Cloud.beforeSave("Challenge", function (request, response) {
    //check if user being saved has profile picture.
    if (!request.object.has("category"))
        request.object.set("category",22);
    if (!request.object.has("countIdeas"))
        request.object.set("countIdeas",0);
    if (!request.object.has("semanas"))
        request.object.set("semanas",new Array());
    if (!request.object.has("confPublicada"))
        request.object.set("confPublicada",false);

    if(request.object.has("confPublicada") && request.object.dirty("confPublicada") && request.object.get("confPublicada"))
        request.object.set("fechaInicio",new Date());
    
    response.success();   
});

Parse.Cloud.afterSave(Parse.User, function (request, response) {
    //check if user being saved has profile picture.
    Parse.Cloud.useMasterKey();
    var user = request.object;
    var queryRole = new Parse.Query(Parse.Role);
    queryRole.equalTo('name', "ValidUser");
    queryRole.first({
        success: function(result) { // Role Object
            var role = result;
            role.getUsers().add(user);
            role.save({
                success: function(result) {    // User Object
                    console.log('SIGN UP And Added to Role');
                },
                error: function(role, error) {
                    console.log(error.code);
                    console.log(error.message);
                    console.error('User failed to assoicate to Role ');
                }
            });
        },
        error: function(error) {
            console.error('Failed to find Role with name : '  + error);
        }
    });
});

//Sólo descripcion y tags
Parse.Cloud.beforeSave(Parse.User, function (request, response) {
    //check if user being saved has profile picture.

    if(request.object.has("token") && request.object.get("email"))
    {
        var token = 'Bearer '+request.object.get("token");
      Parse.Cloud.httpRequest({
        method: 'GET',
            url: "https://graph.microsoft.com/beta/me",
            body: {
                    "tenant": "softtek.com"
                },
            headers: {
                'Authorization': token
            },
        success: function(httpResponse) {
            var postACL = new Parse.ACL();
            postACL.setPublicReadAccess(false);
            request.object.setACL(postACL);

            console.log(httpResponse);

            if(request.object.get("email") != httpResponse.data.mail)
                response.error("not matching email");

            if(request.object.get("username") != httpResponse.data.mailNickname)
                response.error("not matching email");

            if(!request.object.has("tagFollowing"))
                request.object.set("tagFollowing",new Array());

            if(!request.object.has("locationCountry"))
                request.object.set("locationCountry",httpResponse.data.country);
            
            if(!request.object.has("locationCity") || request.object.get("locationCity") == "mobile-User")
                request.object.set("locationCity",httpResponse.data.officeLocation);

            if(!request.object.has("puzzles"))
                request.object.set("puzzles",0);

            if(!request.object.has("points"))
                request.object.set("points",0);

            if (!request.object.has("image")) {
                Parse.Config.get().then(function(config) {
                    //request.object.set("image", config.get("defaultProfilePicture"));
                    //request.object.set("thumbnail", config.get("defaultTB"));
                    request.object.set("image", {
                    "__type": "File",
                    "name": "default-user.png",
                    "url": "https://parsefiles.back4app.com/BkC2n3AgwDP4L3Ixs5nndMC1qUV5aKd6TVxMabk4/792e10715c335ba2bcb44a6831c697f9_4a6-default-user.png"
                });
                    request.object.set("thumbnail", {
                    "__type": "File",
                    "name": "default-user.png",
                    "url": "https://parsefiles.back4app.com/BkC2n3AgwDP4L3Ixs5nndMC1qUV5aKd6TVxMabk4/792e10715c335ba2bcb44a6831c697f9_4a6-default-user.png"
                });
                    response.success();
                });
            } 
            else {
                response.success();
            }
        },
        error: function(httpResponse) {
          Parse.Cloud.httpRequest({
            method: 'GET',
                url: "https://api.office.com/discovery/v1.0/me",
                body: {
                        "tenant": "softtek.com"
                    },
                headers: {
                    'Authorization': token
                },
            success: function(httpResponse) {
                var postACL = new Parse.ACL();
                postACL.setPublicReadAccess(false);
                request.object.setACL(postACL);
                console.log("AQUI ESTA EL OBJETO");
                console.log(httpResponse);
                /*if(request.object.get("email") != httpResponse.data.mail)
                response.error("not matching email");
                
                if(request.object.get("username") != httpResponse.data.mailNickname)
                response.error("not matching email");*/

                if(!request.object.has("tagFollowing"))
                request.object.set("tagFollowing",new Array());
            
                if(!request.object.has("locationCity"))
                request.object.set("locationCity","mobile-User");

                if (!request.object.has("image")) {
                    Parse.Config.get().then(function(config) {
                        //request.object.set("image", config.get("defaultProfilePicture"));
                        //request.object.set("thumbnail", config.get("defaultTB"));
                        request.object.set("image", {
                    "__type": "File",
                    "name": "default-user.png",
                    "url": "https://parsefiles.back4app.com/BkC2n3AgwDP4L3Ixs5nndMC1qUV5aKd6TVxMabk4/792e10715c335ba2bcb44a6831c697f9_4a6-default-user.png"
                });
                    request.object.set("thumbnail", {
                    "__type": "File",
                    "name": "default-user.png",
                    "url": "https://parsefiles.back4app.com/BkC2n3AgwDP4L3Ixs5nndMC1qUV5aKd6TVxMabk4/792e10715c335ba2bcb44a6831c697f9_4a6-default-user.png"
                });
                        response.success();
                    });
                } 
                else {
                    response.success();
                }
            },
            error: function(httpResponse) {
              response.error("ERROR AUTH OFFICE CC");
            }
          });
        }
      });
    }
    else
    {
        response.error("missing stuff");
    }
});



Parse.Cloud.job("finishSession", function(request, status) {
  // Set up to modify user data
    var d = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
    var dt = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
    d.setDate(d.getDate()+1);
  Parse.Cloud.useMasterKey();
    var RTObject = Parse.Object.extend("RegisterTime");
    var queryRT = new Parse.Query(RTObject);
    queryRT.include("idea");
    queryRT.greaterThanOrEqualTo("createdAt",dt);
    queryRT.lessThan("createdAt",d);
    queryRT.find({useMasterKey: true})
    .then(
        function(RTResults){
            var ma = [];
            saveSH(RTResults,dt,ma,0);
        },
        function(error){
            status.error("Error "+error.message);
        }
    );

    function saveSH(arr,today,media,id){

        if(id < arr.length)
        {
            var object = arr[id];
            var date1 = object.get("start");
            var date2 = object.get("finish");
            var min = Math.abs(date1 - date2) / 6e4;
            var acc = object.get("acc");
            var m = (min+acc);
            
            if(media.length == 0)
            {
                media.push({idea: object.get("idea"), mean: m, total: 1});
            }else
            {
                var op = true;
                for(var i= 0; i< media.length; i++)
                {
                    if(media[i].idea.id == object.get("idea").id)
                    {
                        media[i].mean+= m;
                        media[i].total++;
                        op = false;
                    }
                }
                if(op)
                    media.push({idea: object.get("idea"), mean: m, total: 1});
            }

            id++;
            saveSH(arr,today,media,id);
                
        }
        else
        {
            
            saveSHMedia(media,0,today);
        }
    }

    function saveSHMedia(media,id,today)
    {
        if(id < media.length)
        {
            media[id].mean = media[id].mean/ media[id].total;
            console.log(media[id].mean);
            var SHObject = Parse.Object.extend("SessionHistory");
            var querySH = new Parse.Query(SHObject);
            querySH.equalTo("idea",media[id].idea);
            querySH.equalTo("day",today.getDate());
            querySH.equalTo("month",today.getMonth());
            querySH.equalTo("year",today.getFullYear());
            querySH.find({useMasterKey: true})
            .then(
                function(SHResults){
                    if(SHResults.length > 0)
                    {
                        var actual = SHResults[0];
                        actual.set("media",media[id].mean);
                        actual.save(null,{
                            success: function(createdIdea)
                            {
                                id++;
                                saveSHMedia(media,id,today);
                            },
                            error: function(errorR)
                            {
                                status.error(errorR.message);
                            }
                        });
                    }
                    else
                        status.success("Vacio");
                },
                function(errorSH){
                    status.error(errorSH.message);
                }
            );
        }
        else
        {
            status.success("Day Saved");
        }
    }

});


Parse.Cloud.define('registerTime', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();
    var userID = request.params.userID;
    var device = request.params.device;
    var newUser = false;
    var evento = request.params.evento;
    var ideaID = request.params.ideaID;
    var geopoint = request.params.geopoint;

    var Idea = Parse.Object.extend("Idea");
    var User = Parse.Object.extend("User");
    var RTObject = Parse.Object.extend("RegisterTime");
    var SHObject = Parse.Object.extend("SessionHistory");
    var queryUser = new Parse.Query(User);
    var queryIdea = new Parse.Query(Idea);
    


    queryIdea.get(ideaID,{
        success: function(ideaResult){
            
                    var userResult = userID;
                    var d = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
                    var dt = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
                    d.setDate(d.getDate()+1);

                    var queryRegister = new Parse.Query(RTObject);
                    queryRegister.include("user");
                    queryRegister.include("idea");
                    queryRegister.equalTo("userIDString",userResult);
                    queryRegister.equalTo("device",device);
                    queryRegister.equalTo("idea",ideaResult);
                    queryRegister.greaterThanOrEqualTo("createdAt",dt);
                    queryRegister.lessThan("createdAt",d);
                    queryRegister.find({useMasterKey: true})
                    .then(
                        function(registerResults){
                            if(registerResults.length > 0)
                            {
                                var object = registerResults[0];
                                var dateR = new Date();
                                var date1 = object.get("start");
                                var date2 = object.get("finish");
                                
                                var gap = Math.abs(date2 - dateR) / 6e4;
                                if(gap > 120)
                                {
                                    var min = Math.abs(date1 - date2) / 6e4;
                                    var acc = object.get("acc");
                                    acc+= min;
                                    object.set("acc",acc);
                                    var it = object.get("it");
                                    it++;
                                    object.set("it",it);
                                    object.set("start", new Date());
                                    object.set("finish", new Date());
                                }
                                else
                                {
                                    object.set("finish", new Date());
                                }

                                if(device != "web2" && geopoint !=null)
                                {
                                    var gpoints = object.get("gpoints");
                                    if(gpoints.length == 0)
                                        gpoints.push(geopoint);
                                    else
                                    {   
                                        var op = true;
                                        for(var i=0; i < gpoints.length; i++)
                                        {
                                            if(gpoints[i] != null && geopoint != null)
                                            if(gpoints[i].latitude == geopoint.latitude && gpoints[i].longitude == geopoint.longitude)
                                                op = false;
                                        }
                                        if(op)
                                            gpoints.push(geopoint);
                                    }

                                    object.set("gpoints",gpoints);
                                }

                                var log = object.get("log");
                                log.push(evento+" "+( new Date().toString()));
                                object.set("log",log);

                                object.save(null,{
                                    success: function(createdIdea)
                                    {
                                        response.success("Editado");
                                    },
                                    error: function(errorR)
                                    {
                                        response.error(errorR.message);
                                    }
                                });

                            }else
                            {
                                var object = new RTObject();
                                object.set("userIDString",userResult);
                                object.set("idea",ideaResult);
                                object.set("device",device);
                                object.set("start", new Date());
                                object.set("finish", new Date());
                                object.set("acc", 1);
                                object.set("it", 1);
                                
                                var gpoints = [];
                                    gpoints.push(geopoint);

                                if(device != "web2" && geopoint != null)
                                object.set("gpoints", gpoints);
                                var log = [];
                                log.push(evento+" "+( new Date().toString()));
                                object.set("log",log);

                                object.save(null,{
                                    success: function(createdIdea)
                                    {
                                        var querySH = new Parse.Query(SHObject);
                                        var act_day = new Date();

                                        querySH.include("idea");
                                        querySH.equalTo("idea",ideaResult);
                                        querySH.equalTo("day", act_day.getDate());
                                        querySH.equalTo("month", act_day.getMonth());
                                        querySH.equalTo("year", act_day.getFullYear());
                                        querySH.find({useMasterKey: true})
                                        .then(
                                            function(SHResults){
                                                var objectSH;
                                                var res = "";
                                                if(SHResults.length > 0)
                                                {
                                                    objectSH = SHResults[0];
                                                    var actUs = objectSH.get("active_users") + 1;
                                                    var newUs = objectSH.get("new_users") + 1;
                                                    objectSH.set("active_users",actUs);
                                                    objectSH.set("new_users",0);
                                                    objectSH.set("media",1);
                                                    if(newUser)
                                                        objectSH.set("new_users",newUs);

                                                    objectSH.save(null,{
                                                        success: function(createdSHObject)
                                                        {
                                                            response.success("Guardado");
                                                        },
                                                        error: function(errorSHSave){
                                                            response.error(errorSHSave.message);
                                                        }
                                                    });
                                                }
                                                else
                                                {
                                                    objectSH = new SHObject();
                                                    objectSH.set("active_users",1);
                                                    objectSH.set("new_users",0);
                                                    objectSH.set("media",1);
                                                    if(newUser)
                                                        objectSH.set("new_users",1);
                                                    objectSH.set("day",act_day.getDate());
                                                    objectSH.set("month",act_day.getMonth());
                                                    objectSH.set("year",act_day.getFullYear());
                                                    objectSH.set("idea",ideaResult);
                                                    
                                                    objectSH.save(null,{
                                                        success: function(createdSHObject)
                                                        {
                                                            response.success("Guardado");
                                                        },
                                                        error: function(errorSHSave){
                                                            response.error(errorSHSave.message);
                                                        }
                                                    });
                                                }                                                
                                                
                                            },
                                            function(errorSH){
                                                response.error(errorSH.message)
                                            }
                                        );
                                    },
                                    error: function(errorR)
                                    {
                                        response.error(errorR.message);
                                    }
                                });
                            }
                        },
                        function(errorQ){
                            response.error(errorQ.message);
                        }
                    );
                         
                
                
        },
        error: function(errorIdea){
            response.error(errorIdea.message);
        }
    });
 
});


Parse.Cloud.define('yammer', function(request, response) {
    //parametros
    var token = 'Bearer '+request.params.token;

  Parse.Cloud.httpRequest({
    method: 'GET',
        url: "https://api.yammer.com/api/v1/users/by_email.json?email=hector.cano@softtek.com",
        headers: {
            'Authorization': "Bearer SDEKHLbl5hJWb1B5bxnzg"
        },
    success: function(httpResponse) {
      response.success(httpResponse);
    },
    error: function(httpResponse) {
      response.error('Request failed with response code ' + httpResponse.status);
    }
  });
});

Parse.Cloud.define('PostIdeaYammer', function(request, response) {
    //parametros
    var token = 'Bearer '+request.params.token;

  Parse.Cloud.httpRequest({
    method: 'POST',
        url: "https://api.yammer.com/api/v1/messages.json",
        body: {
                "body": "Ignora esta idea2.5",
                "group_id": 6068022
            },
        headers: {
            'Authorization': "Bearer SDEKHLbl5hJWb1B5bxnzg"
        },
    success: function(httpResponse) {
      response.success(httpResponse);
    },
    error: function(httpResponse) {
      response.error('Request failed with response code ' + httpResponse.status);
    }
  });
});

Parse.Cloud.define('checkOfficeToken', function(request, response) {
    //parametros
    var token = 'Bearer '+request.params.token;

  Parse.Cloud.httpRequest({
    method: 'GET',
        url: "https://graph.microsoft.com/beta/me",
        body: {
                "tenant": "softtek.com"
            },
        headers: {
            'Authorization': token
        },
    success: function(httpResponse) {
      response.success(httpResponse);
    },
    error: function(httpResponse) {
      response.error('Request failed with response code ' + httpResponse.status);
    }
  });
});

Parse.Cloud.define('checkOutlookToken', function(request, response) {
    //parametros
    var token = 'Bearer '+request.params.token;

  Parse.Cloud.httpRequest({
    method: 'GET',
        url: "https://outlook.office.com/api/v1.0/me",
        body: {
                "tenant": "softtek.com"
            },
        headers: {
            'Authorization': token
        },
    success: function(httpResponse) {
      response.success(httpResponse);
    },
    error: function(httpResponse) {
      response.error('Request failed with response code ' + httpResponse.status);
    }
  });
});


Parse.Cloud.define('getUserToken', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey(); //Made after the migration
    var userID = request.params.userID;
    var uQuery = new Parse.Query("User");
    uQuery.get(userID,{
        success: function(cUser){
            Parse.Cloud.httpRequest({
                method: 'GET',
                    url: "https://graph.microsoft.com/beta/me",
                    body: {
                            "tenant": "softtek.com"
                        },
                    headers: {
                        'Authorization': cUser.get("token")
                    },
                success: function(httpResponse) {
                    var tokens = {graph: cUser.get("token"), outlook: cUser.get("outlookToken"), resp: httpResponse.data.id}
                    response.success(tokens);
                },
                error: function(httpResponse) {
                  response.error('Request failed with response code ' + httpResponse.status);
                }
            });
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.define('getUser', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();
    var userID = request.params.userID;
    var uQuery = new Parse.Query("User");
    uQuery.get(userID,{
        success: function(cUser){
            response.success(cUser);
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.define('getAnalyticsCoverageProv', function(request, response) {
    Parse.Cloud.useMasterKey(); //Made after the migration
    //parametros
    var device = request.params.device;
    var ideaID = request.params.ideaID;
    var RTObject = Parse.Object.extend("RegisterTime");
    var IdeaObject = Parse.Object.extend("Idea");
    var IdeaQuery = new Parse.Query(IdeaObject);
    
    IdeaQuery.get(ideaID,{
        success: function(IdeaResult){
            var d = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
            var dt = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
            d.setDate(d.getDate()-180);
            dt.setDate(dt.getDate()+1);

            for (var i = 0; i < 10; i++) {

                var finalQuery = new Array();
                var RTQuery = new Parse.Query(RTObject);
                RTQuery.equalTo("idea",IdeaResult);
                RTQuery.include("idea");
                RTQuery.ascending("createdAt");
                RTQuery.greaterThanOrEqualTo("createdAt",d);
                RTQuery.lessThanOrEqualTo("createdAt",dt);
                RTQuery.limit(1000);
                RTQuery.find({useMasterKey: true})
                .then(
                    function(RTResult){
                        finalQuery = finalQuery.concat(RTResult);
                        response.success(finalQuery);
                    },
                    function(error){
                        response.error(error.message);
                    }
                );
            };
        },
        error: function(errorIdea){
            response.error(errorIdea.message);
        }
    });
});

Parse.Cloud.define('getAnalyticsCoverage', function(request, response) {
    Parse.Cloud.useMasterKey(); //Made after the migration
    //parametros
    var device = request.params.device;
    var ideaID = request.params.ideaID;
    var RTObject = Parse.Object.extend("RegisterTime");
    var RTQuery = new Parse.Query(RTObject);
    var IdeaObject = Parse.Object.extend("Idea");
    var IdeaQuery = new Parse.Query(IdeaObject);
    
    IdeaQuery.get(ideaID,{
        success: function(IdeaResult){
            var d = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
            var dt = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
            d.setDate(d.getDate()-365);
            dt.setDate(dt.getDate()+2);

            RTQuery.equalTo("idea",IdeaResult);
            RTQuery.include("idea");
            RTQuery.descending("createdAt");
            RTQuery.greaterThanOrEqualTo("createdAt",d);
            RTQuery.lessThanOrEqualTo("createdAt",dt);
            RTQuery.limit(1000);
            RTQuery.find({useMasterKey: true})
            .then(
                function(RTResult){
                    

                    if(RTResult.length > 0)
                    {
                        var linqQuery = LINQ.Enumerable.From(RTResult).Distinct("$.attributes.userIDString")
                        .OrderBy("$.attributes.userIDString").ToArray();
                        //var linqQuery = RTResult;
                        var r = {origin: IdeaResult.get("locationGpoint"), destinations: linqQuery};    
                        response.success(r);  
                    }
                    else
                    response.success({origin: IdeaResult.get("locationGpoint"),destinations: []});
                    
                },
                function(error){
                    response.error(error.message);
                }
            );
        },
        error: function(errorIdea){
            response.error(errorIdea.message);
        }
    });
});

Parse.Cloud.define('getRosterAsSelect',function(request,response){
    Parse.Cloud.useMasterKey(); //Made after the migration
    var RosterRolQuery = new Parse.Query("RosterRol");
    RosterRolQuery.find({useMasterKey: true})
    .then(
        function(roles){
            var select = "<select id='selectRosterRoles'>";
            for (var i = 0; i < roles.length; i++) {
                select+= "<option value='"+roles[i].id+"'>";
                select+= roles[i].get("titulo");
                select+="</option>";
            };
            select+="</select>";
            response.success(select);
        },
        function(error){
            response.error(error.message);
        }
    );
});

Parse.Cloud.define("saveMultipleRoster",function(request,response){
    Parse.Cloud.useMasterKey();
    var ideaID = request.params.ideaID;
    var roster = request.params.roster;
    var ideaQuery = new Parse.Query("Idea");
    var robj = Parse.Object.extend("Roster");
    ideaQuery.get(ideaID,{
        success: function(ideaResult){
            var rosterQuery = new Parse.Query("Roster");
            rosterQuery.equalTo("idea",ideaResult);
            rosterQuery.find({useMasterKey: true})
            .then(
                function(rosterResult){
                    if(rosterResult.length > 0)
                    {
                        var CurrentRosterObj = LINQ.Enumerable.From(rosterResult).Where(function (x) { 
                            return roster.lastIndexOf(x.attributes.user.id) > -1
                        }).ToArray();
                        
                        console.log(CurrentRosterObj.length+" <----------number");

                        if(!(CurrentRosterObj.length == rosterResult.length && CurrentRosterObj.length == roster.length))
                        {
                            var cRoster = new Array();
                            for (var i = 0; i < CurrentRosterObj.length; i++) {
                                cRoster.push(CurrentRosterObj[i].get("user").id);
                            };

                            console.log(cRoster);
                            var notInCRoster = new Array();
                            for (var i = 0; i < roster.length; i++) {
                                if(cRoster.lastIndexOf(roster[i]) <= -1 )
                                    notInCRoster.push(roster[i]);
                            };

                            console.log(notInCRoster.length+"<---------------not in roster");

                            var toRemove = LINQ.Enumerable.From(rosterResult).Where(function (x) { 
                                return roster.lastIndexOf(x.attributes.user.id) <= -1
                            }).ToArray();
                            
                            var userQuery = new Parse.Query("User");
                            userQuery.containedIn("objectId",notInCRoster);
                            userQuery.find({useMasterKey: true})
                            .then(
                                function(userResults){
                                    var newRosterObj = new Array();

                                    for (var i = 0; i < userResults.length; i++) {
                                        var cUser = userResults[i];
                                        var tempObj = new robj();
                                        tempObj.set("user",cUser);
                                        tempObj.set("idea",ideaResult);
                                        newRosterObj.push(tempObj);
                                    };

                                    console.log("-------------------SHSAHAH-----------");
                                    console.log(notInCRoster);
                                    console.log(newRosterObj);
                                    Parse.Object.saveAll(newRosterObj,{
                                        success: function(newRoster) {
                                           // All the objects were saved.
                                           console.log("toremove*****");
                                           console.log(toRemove);
                                           Parse.Object.destroyAll(toRemove,{
                                                success: function(remRoster) {
                                                   // All the objects were saved.
                                                   response.success(newRoster);
                                                },
                                                error: function(error) {
                                                   // An error occurred while saving one of the objects.
                                                   response.error("failure on destroying objects");
                                                },
                                            });
                                        },
                                        error: function(error) {
                                           // An error occurred while saving one of the objects.
                                           response.error("failure on saving objects");
                                        },
                                    }); 
                                },
                                function(error){
                                    response.error(error.message);
                                }
                            );
                        }
                        else
                            response.success(rosterResult);
                    }
                    else
                    {
                        var userQuery = new Parse.Query("User");
                            userQuery.containedIn("objectId",roster);
                            userQuery.find({useMasterKey: true})
                            .then(
                                function(userResults){
                                    var newRosterObj = new Array();

                                    for (var i = 0; i < userResults.length; i++) {
                                        var cUser = userResults[i];
                                        var tempObj = new robj();
                                        tempObj.set("user",cUser);
                                        tempObj.set("idea",ideaResult);
                                        newRosterObj.push(tempObj);
                                    };

                                    Parse.Object.saveAll(newRosterObj,{
                                        success: function(newRoster) {
                                           // All the objects were saved.
                                            response.success(newRoster);
                                        },
                                        error: function(error) {
                                           // An error occurred while saving one of the objects.
                                           response.error("failure on saving objects");
                                        },
                                    }); 
                                },
                                function(error){
                                    response.error(error.message);
                                }
                            );
                    }
                    
                },
                function(error){
                    response.error(error.message);
                }
            );
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.define('saveRosterUser',function(request,response){
    Parse.Cloud.useMasterKey();
    var ideaID = request.params.ideaID;
    var userID = request.params.userID;
    var rosterRol = request.params.rosterRol;
    var descr = request.params.descr;
    var RosterObj = Parse.Object.extend("Roster");
    var new_Roster = new RosterObj();

    var userQuery = new Parse.Query("User");
    userQuery.get(userID,{
        success: function(userResult){
            var rrolQuery = new Parse.Query("RosterRol");
            rrolQuery.get(rosterRol,{
                success: function(rrolResult){
                    var ideaQuery = new Parse.Query("Idea");
                    ideaQuery.include("user");
                    ideaQuery.get(ideaID,{
                        success: function(ideaResult){
                            new_Roster.set("descripcion",descr);
                            new_Roster.set("idea",ideaResult);
                            new_Roster.set("user",userResult);
                            new_Roster.set("rol",rrolResult);

                            new_Roster.save(null,{
                                success: function(saved){

                                    //DOSTUFFHERE
                                    var NotificacionObject = Parse.Object.extend("Notificacion");
                                    var notificacion = new NotificacionObject();
                                    notificacion.set("user", userResult);
                                    notificacion.set("user_action", ideaResult.get("user"));
                                    notificacion.set("mensaje", ideaResult.get("user").get("name")
                                                           + " has added you to "
                                                           + ideaResult.get("titulo"));
                                    notificacion.set("tipo", "roster");
                                    notificacion.set("idRelacionado",ideaResult.id );
                                    notificacion.save(null,{
                                        success: function(notiSaved){
                                            var pusher = new Pusher({
                                              appId: '148416',
                                              key: '7a5e5c7f6307240cf192',
                                              secret: 'a93459ef65de29ce6103',
                                              encrypted: true
                                            });
                                            pusher.trigger( 'produ', notiSaved.get("user").id, { message: "<p><a href='viewIdea.html?k="+ideaResult.id+
                                                "'>"+notiSaved.get("mensaje")+"</a></p>" } );
                                            response.success(saved);
                                        },
                                        error: function(error){
                                            response.success(saved);
                                        }
                                    });

                                },
                                error: function(error){
                                    response.error(error.message);
                                }
                            });
                        },
                        error: function(error){
                            response.error(error.message);
                        }
                    });
                },
                error: function(error){
                    response.error(error.message);
                }
            });
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.define('getAllUsers',function(request,response){
    Parse.Cloud.useMasterKey();
    var userQuery = new Parse.Query("User");
    userQuery.find({useMasterKey: true})
    .then(
        function(users){
            var userList = [];
            for (var i = 0; i < users.length; i++) {
                var image = (users[i].has("imageYammer"))? users[i].get("imageYammer") : users[i].get("image").url();
                userList.push({id:users[i].id,name: users[i].get("name"), photo: image});
            };
            response.success(userList);  
        },
        function(error){
            response.error(error.message);
        }
    );
});


Parse.Cloud.define('getRoster', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();
    var ideaID = request.params.ideaID;
    var IdeaObject = Parse.Object.extend("Idea");
    var IdeaQuery = new Parse.Query(IdeaObject);
    
    IdeaQuery.get(ideaID,{
        success: function(IdeaResult){
            var RosterQuery = new Parse.Query("Roster");
            RosterQuery.include("user");
            RosterQuery.include("rol");
            RosterQuery.include("idea");
            RosterQuery.equalTo("idea",IdeaResult);
            RosterQuery.find({useMasterKey: true})
            .then(
                function(Roster){
                    response.success(Roster);
                },
                function(error){
                    response.error(error.message);
                }
            );
        },
        error: function(errorIdea){
            response.error(errorIdea.message);
        }
    });
});

Parse.Cloud.define('testDrive',function(request,response){
    Parse.Cloud.useMasterKey();
    
    var queryIdeas = new Parse.Query("Idea");
    queryIdeas.include("user");

    queryIdeas.find({useMasterKey: true})
    .then(
        function(result){
            for (var i = 0; i < result.length; i++) {
                var idea = result[i];
                idea.set("locationCity",idea.get("user").get("locationCity"));
                idea.set("locationCountry",idea.get("user").get("locationCountry"));
            };
            Parse.Object.saveAll(result,{
                success: function(newRoster) {
                   // All the objects were saved.
                    response.success(newRoster);
                },
                error: function(error) {
                   // An error occurred while saving one of the objects.
                   response.error("failure on saving objects");
                },
            });
        },
        function(error){
            response.error(error.message);
        }
    );
    
});

Parse.Cloud.job('defineNewRetention',function(request,status){
    var dateNow = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
    //dateNow.setHours(5);
    var RegistersNew = [];
    var RTQuery = new Parse.Query("RegisterTime");
    var count = 0;
    RTQuery.ascending("createdAt");
    RTQuery.include("idea");
    RTQuery.equalTo("isNew",true);
    RTQuery.greaterThanOrEqualTo("createdAt",dateNow);
    RTQuery.find({useMasterKey: true})
    .then(
        function(RTR){
            var linqQuery = LINQ.Enumerable.From(RTR).Distinct("$.attributes.userIDString+$.attributes.idea.id").ToArray();
            // RegistersNew = LINQ.Enumerable.From(linqQuery).Where(function (x) { 
            //                         return x.createdAt >= dateNow
            //                     }).ToArray();
            RegistersNew = linqQuery;
        },
        function(error){
            status.error(error.message);
        }
    ).then(function() {
            var savings = [];
            var retIndex = [];
            var RetObj = Parse.Object.extend("Retention");
                
            for (var i = RegistersNew.length - 1; i >= 0; i--) {
                //var Like = Parse.Object.extend("Like");
                if(retIndex.length == 0 || retIndex.lastIndexOf(RegistersNew[i].get("idea").id) == -1)
                {
                    var newR = new RetObj();
                    newR.set("idea",RegistersNew[i].get("idea"));
                    newR.set("creation",dateNow);
                    var users = [RegistersNew[i].get("userIDString")];
                    newR.set("newUsers",users);
                    retIndex.push(RegistersNew[i].get("idea").id);
                    savings.push(newR);
                }
                else
                {
                    var id = retIndex.lastIndexOf(RegistersNew[i].get("idea").id);
                    var users = savings[id].get("newUsers");
                    users.push(RegistersNew[i].get("userIDString"));
                    savings[id].set("newUsers",users);
                }
                
            };

            Parse.Object.saveAll(savings,{
                success: function(list) {
                   // All the objects were saved.
                   status.success("Finished! "+list.length);
                },
                error: function(error) {
                   // An error occurred while saving one of the objects.
                   status.error("failure on saving objects");
                },
            }); 
        });
});

Parse.Cloud.job('defineNewRetentionDays',function(request,status){
   var dateNow = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
    //dateNow.setHours(5);
    var dateYestersay = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
    //dateYestersay.setHours(5);
    dateYestersay.setDate(dateYestersay.getDate()-1);

    var stuff = [];
    var RegistersNew = [];
    
    var RTQuery = new Parse.Query("RegisterTime");
    var count = 0;
    RTQuery.greaterThanOrEqualTo("createdAt",dateYestersay);
    RTQuery.lessThan("createdAt",dateNow);
    RTQuery.include("idea");

    RTQuery.find({useMasterKey: true})
    .then(
        function(RTR){
            RegistersNew = LINQ.Enumerable.From(RTR).Distinct("$.attributes.userIDString+$.attributes.idea.id").ToArray();
        },
        function(error){
            status.error(error.message);
        }
    ).then(function() {
            var promises = [];
            for (var i = RegistersNew.length - 1; i >= 0; i--) {
                //var Like = Parse.Object.extend("Like");
                var RetObj = Parse.Object.extend("Retention");
                var queryRet = new Parse.Query("Retention");
                
                var cUser = RegistersNew[i].get("userIDString");       
                var cIdea = RegistersNew[i].get("idea");
                
                queryRet.include("idea");
                queryRet.equalTo("newUsers",cUser);
                queryRet.equalTo("idea",RegistersNew[i].get("idea"));
                
                promises.push(queryRet.find({ //weird promises stuff
                    success: function (retention) {
                        if(retention.length > 0)                  
                        {
                            stuff.push({ret: retention[0],idea: retention[0].get("idea"), date: dateYestersay, date2: dateNow});
                        }
                    },
                    error: function (error) {
                        status.error("Error: " + error.code + " " + error.message);
                    }   
                }));
            };
            Parse.Promise.when(promises).then(function  (argument) {
                //console.log(Enumerable.From(likes).ToArray());   
                makeObjects();
            });
        });
    function makeObjects(){
        var promises = [];
        var saving = [];
        var rtdObj = Parse.Object.extend("RetentionDays");
        var retIndex = [];
            for (var i = stuff.length - 1; i >= 0; i--) {
                //var Like = Parse.Object.extend("Like");
                if(retIndex.length == 0 || retIndex.lastIndexOf(stuff[i].ret.id) == -1)
                {
                    if(stuff[i].ret.get("creation") < stuff[i].date){
                        retIndex.push(stuff[i].ret.id);
                        var newRTD = new rtdObj();
                        newRTD.set("retention",stuff[i].ret);
                        newRTD.set("idea",stuff[i].idea);
                        newRTD.set("day",stuff[i].date);
                        newRTD.set("activeUsers",1);
                        saving.push(newRTD);
                    } 
                }
                else
                {
                    var id = retIndex.lastIndexOf(stuff[i].ret.id);
                    var au = saving[id].get("activeUsers");
                    au++;
                    saving[id].set("activeUsers",au);
                }
            };
        Parse.Object.saveAll(saving,{
            success: function(list) {
               // All the objects were saved.
               status.success("Finished!");
            },
            error: function(error) {
               // An error occurred while saving one of the objects.
               status.error("failure on saving objects");
            },
        });
    } 
});

Parse.Cloud.define('getAnalyticsAdoption', function(request, response) {
    Parse.Cloud.useMasterKey(); //Made after the migration
    //parametros

    var ideaID = request.params.ideaID;
    var IdeaObject = Parse.Object.extend("Idea");
    var IdeaQuery = new Parse.Query(IdeaObject);
    var datosComment = [];

    IdeaQuery.get(ideaID,{
        success: function(IdeaResult){
            var today = new Date();
            var prev_Month = new Date();
            prev_Month.setDate(today.getDate()-91);

            var SHObject = Parse.Object.extend("SessionHistory");
            var querySH = new Parse.Query(SHObject);
            querySH.include("idea");
            querySH.equalTo("idea",IdeaResult)
            querySH.ascending("createdAt");
            querySH.greaterThanOrEqualTo("createdAt",prev_Month);
            querySH.lessThanOrEqualTo("createdAt",today);
            querySH.find({useMasterKey: true})
            .then(
                function(SHResults){
                    var datos = [];
                    var datosNU = [];
                    var datosM = [];
                    var datosNA = [];

                    for(var i = 0; i < SHResults.length; i++)
                    {
                        var objectSH = SHResults[i];
                        var date = new Date(objectSH.get("year"),objectSH.get("month"),objectSH.get("day")).getTime();
                        datos.push([date,objectSH.get("active_users")]);
                        datosNU.push([date,objectSH.get("new_users")]);
                        datosM.push([date,objectSH.get("media")]);
                        var porc = Number(objectSH.get("new_users"))/ Number(objectSH.get("active_users"));
                        porc = porc.toFixed(2)*100;
                        datosNA.push([date,porc]);
                    }
                    datosComment.push({graph: 'column',label: "Daily Active Users", data: datos});
                    datosComment.push({graph: 'column',label: "Daily New Users", data: datosNU});
                    datosComment.push({graph: 'column',label: "Minutes Spent", data: datosM});
                    datosComment.push({graph: 'area',label: "New Users / Active Users", data: datosNA});
                    
                    response.success(datosComment);
                },
                function(error){
                    response.error(error.message);
                }
            );
        },
        error: function(errorIdea){
            response.error(errorIdea.message);
        }
    });
});

Parse.Cloud.define('getAnalyticsRetention', function(request, response) {
    Parse.Cloud.useMasterKey(); //Made after the migration
    //parametros
    var ideaID = request.params.ideaID;
    var IdeaObject = Parse.Object.extend("Idea");
    var IdeaQuery = new Parse.Query(IdeaObject);

    var RetObj = Parse.Object.extend("Retention");
    var RetQuery = new Parse.Query(RetObj);
    var RDObj = Parse.Object.extend("RetentionDays");
    var RDQuery = new Parse.Query(RDObj);
    var resultArray = [];
    IdeaQuery.get(ideaID,{
        success: function(IdeaResult){
            console.log("*----------Retention of "+IdeaResult.id+"-----------*");
            var d = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
            d.setDate(d.getDate()-1);
            var dMonth = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
            dMonth.setDate(dMonth.getDate()-91);
            RDQuery.include("idea");
            RetQuery.include("idea");
            RetQuery.equalTo("idea",IdeaResult);
            RDQuery.equalTo("idea",IdeaResult);
            RDQuery.include("retention");
            RDQuery.ascending("day");
            RetQuery.descending("creation");
            console.log("Days :: "+d);
            console.log("Last :: "+dMonth);
            RetQuery.lessThan("creation",d);
            RetQuery.greaterThanOrEqualTo("creation",dMonth);
            RetQuery.find({useMasterKey: true})
            .then(
                function(RetResults){
                    console.log("--number of retentions"+RetResults.length);
                    RDQuery.find({useMasterKey: true})
                    .then(
                        function(RDResults){
                            for(var i=0; i < RetResults.length; i++){
                                var object = RetResults[i];
                                var queryResult = LINQ.Enumerable.From(RDResults)
                                .Where(function (x) { 
                                    return x.attributes.retention.id == object.id
                                })
                                .ToArray();
                                resultArray.push({rt: object, rd: queryResult});
                            }
                            response.success(resultArray);
                        },
                        function(error){
                            response.error(error.message);
                        }
                    );              
                },
                function(error){
                    response.error(error.message);
                }
            );
        },
        error: function(errorIdea){
            response.error(errorIdea.message);
        }
    }); 
    
});


Parse.Cloud.define('getIdeasBuscador', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();
    var IdeaObject = Parse.Object.extend("Idea");
    var IdeaQuery = new Parse.Query(IdeaObject);
    IdeaQuery.equalTo("esPrivado",false);
    IdeaQuery.include("user");
    IdeaQuery.find({useMasterKey: true})
    .then(
       function(ideaResults){
           var list = [];

           for (var i = ideaResults.length - 1; i >= 0; i--) {
              var object =  ideaResults[i];
              var tagsString = object.get("tags").toString();
                tagsString = tagsString.replace(/,/g," ");
              list.push({id: object.id, title: object.get("titulo"), 
               userId:object.get("user").id, user: object.get("user").get("name"),
                tags: tagsString});
           };

           var linqQueryIdeas = LINQ.Enumerable.From(list).ToArray();


           response.success(linqQueryIdeas);
       },
       function(errorIdea){
           response.error(errorIdea.message);
       }
    );
});

Parse.Cloud.define('getInstaller', function(request, response) {
    Parse.Cloud.useMasterKey(); //Made after the migration
    //parametros
    var fileID = request.params.fileID;
    
    var ArchivoQuery = new Parse.Query("Archivo");
    ArchivoQuery.get(fileID,{
        success: function(archivoR){
            response.success(archivoR);
        },
        error: function(error){
            response.error(error);
        }
    });
});

Parse.Cloud.afterSave("RegisterTime",function(request,response){
    var userID = request.object.get("userIDString");
    var idea = request.object.get("idea");
    var RetQuery = new Parse.Query("Retention");
    RetQuery.equalTo("idea",idea);
    RetQuery.equalTo("newUsers",userID);
    RetQuery.find({useMasterKey: true})
    .then(
        function(retResult){
            if(retResult.length == 0)
            {
                request.object.set("isNew",true);
                request.object.save();
            }        
        },
        function(error){
            console.error(error.message);     
        }
    );
});

Parse.Cloud.afterSave("Comentario", function (request, response) {
   //check if user being saved has profile picture.
   Parse.Cloud.useMasterKey();
   var queryIdea = new Parse.Query("Idea");
   queryIdea.include("user");
   queryIdea.get(request.object.get("idea").id,{
       success: function(ideaResult){
           var count = Number((ideaResult.get("commentCount") == undefined)? 0 : ideaResult.get("commentCount"));
            
           ideaResult.increment("commentCount");
           ideaResult.save(null,{
               success: function(savedIdea){
                   console.log('success');
                   //Guardar notificación en tabla Parse
                   //ideaOwner
                   var UserQuery = new Parse.Query("User");
                   UserQuery.get(request.object.get("user").id,{
                    success: function(userResult){
                        var nameCommenter = userResult.get("name");
                        var UserObj = Parse.Object.extend("User");
                        var owner = ideaResult.get("user");

                        var NotificacionObject = Parse.Object.extend("Notificacion");
                        var notificacion = new NotificacionObject();
                        notificacion.set("user", owner);
                        notificacion.set("user_action", userResult);
                        notificacion.set("mensaje", nameCommenter
                                               + " has commented on "
                                               + ideaResult.get("titulo"));
                        notificacion.set("tipo", "comentario");
                        notificacion.set("idRelacionado",ideaResult.id );
                        notificacion.save();
                        
                        if(owner.id != userResult.id)
                        {
                            Parse.Cloud.run('setPoints',{userID: userResult.id, action:"comment"},{
                                success: function(result){
                                    console.log("Did it");
                                },
                                error: function(error){
                                    console.log("error did it");
                                }
                            });
                        }

                        var pusher = new Pusher({
                          appId: '148416',
                          key: '7a5e5c7f6307240cf192',
                          secret: 'a93459ef65de29ce6103',
                          encrypted: true
                        });
                        pusher.trigger( 'produ', owner.id, { message: "<p><a href='viewIdea.html?k="+ideaResult.id+"'>"+nameCommenter+" has commented on "+ideaResult.get("titulo")+"</a></p>" } );

                        //enviar PUSH
                        var query = new Parse.Query(Parse.Installation);
                        query.equalTo('user', owner);

                        Parse.Push.send({
                         where: query, // Set our Installation query
                         data: {
                           alert: nameCommenter+" has commented on "+ideaResult.get("titulo") 
                         }
                        }, {
                         success: function() {
                           // Push was successful
                           console.log("push success");
                         },
                         error: function(error) {
                           // Handle error
                           console.error(error.message);
                         }
                        });
                    },
                    error: function(error){
                        console.error(error.message);
                    }
                   });
               },
               error: function(errorSave){
                   console.error(errorSave.message);
               }
           });
       },
       error: function(error){
           console.error(error.message);
       }
   });
});

Parse.Cloud.afterSave("ComitteeApprovals", function (request, response) {
    Parse.Cloud.useMasterKey();
    var ideaID = request.object.get("idea").id;
    var committeeQuery = new Parse.Query("Comittee");
    var ideaQuery = new Parse.Query("Idea");
    var comAppQuery = new Parse.Query("ComitteeApprovals");
    ideaQuery.get(ideaID,{
        success: function(ideaResult){
            committeeQuery.find({useMasterKey: true})
            .then(
                function(comitteeResults){
                    comAppQuery.equalTo("idea",ideaResult);
                    comAppQuery.find({useMasterKey: true})
                    .then(
                        function(appResults){
                            console.log(appResults.length);
                            console.log("COMITE");
                            console.log(comitteeResults.length);
                            if(appResults.length == comitteeResults.length){
                                var totalApproved = 0;
                                for (var i = 0; i < appResults.length; i++) {
                                    if(appResults[i].get("isApproved"))
                                        totalApproved++;
                                };
                                console.log("total Aproved: "+totalApproved);
                                if(totalApproved >= comitteeResults.length/2){
                                    ideaResult.set("Fase",3);
                                    ideaResult.save(null,{
                                        success: function(saved){
                                            //SEND NOTI
                                            console.log("SAVEO");
                                            console.log(saved.get("Fase"));
                                            Parse.Cloud.run('setPoints',{action: "approval",userID: ideaResult.get("user").id},{
                                                success: function(result){
                                                    console.log("saved");
                                                },
                                                error: function(error){
                                                    console.log(error.message);
                                                }
                                            });
                                        },
                                        error: function(error){
                                            response.error(error.message);
                                        }
                                    });
                                }
                            }
                        },
                        function(error){
                            response.error(error.message);
                        }
                    );
                },
                function(error){
                    response.error(error.message);
                }
            );
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.afterSave("Archivo", function (request, response) {
    //check if user being saved has profile picture.
    Parse.Cloud.useMasterKey();
    var queryIdea = new Parse.Query("Idea");
    queryIdea.get(request.object.get("idea").id,{
        success: function(ideaResult){
            if(!ideaResult.has("fileCount"))
                ideaResult.set("fileCount",0);

            ideaResult.increment("fileCount");
            if(!ideaResult.has("photoUrl"))
            {
                console.log("without image");
                var ext = request.object.get("nombre").split('.').pop().toLowerCase();
                console.log("extensio of - "+ext+ " "+request.object.get("nombre"));
                if(['gif','png','jpg','jpeg'].lastIndexOf(ext) > -1)
                {
                    ideaResult.set("photoUrl",request.object.get("file").url());
                    console.log("its an image");
                }
            }
            if(!ideaResult.has("hasAPK"))
            {
                console.log("without apk");
                var ext = request.object.get("nombre").split('.').pop().toLowerCase();
                if(['apk'].lastIndexOf(ext) > -1)
                {
                    ideaResult.set("hasAPK",request.object.id);
                    Parse.Cloud.run('setPoints',{action: "apk",userID: request.user.id },{
                        success: function(result){
                            console.log("saved");
                        },
                        error: function(error){
                            console.log(error.message);
                        }
                    });
                }
            }
            if(!ideaResult.has("hasExe"))
            {
                console.log("without exe");
                var ext = request.object.get("nombre").split('.').pop().toLowerCase();
                if(['exe'].lastIndexOf(ext) > -1)
                {
                    ideaResult.set("hasExe",request.object.id);
                }
            }
            if(!ideaResult.has("hasIpa"))
            {
                console.log("without ipa");
                var ext = request.object.get("nombre").split('.').pop().toLowerCase();
                if(['ipa'].lastIndexOf(ext) > -1)
                {
                    ideaResult.set("hasIpa",request.object.id);
                }
            }
            ideaResult.save(null,{
                success: function(savedIdea){
                    console.log('success');
                },
                error: function(errorSave){
                    console.error(errorSave.message);
                }
            });
        },
        error: function(error){
            console.error(error.message);
        }
    });
});

Parse.Cloud.afterDelete("Archivo", function(request) {
        Parse.Cloud.useMasterKey();
  query = new Parse.Query("Idea");
  query.get(request.object.get("idea").id,{
    success: function(ideaResult) {
        var count = Number((ideaResult.get("fileCount") == undefined)? 0 : ideaResult.get("fileCount"));
        ideaResult.increment("fileCount",-1);
        ideaResult.save(null,{
            success: function(savedIdea){
                console.log('success');
            },
            error: function(errorSave){
                console.error(errorSave.message);
            }
        });
    },
    error: function(error) {
      console.error("Error finding related comments " + error.code + ": " + error.message);
    }
  });
});
Parse.Cloud.afterDelete("Comentario", function (request, response) {
    Parse.Cloud.useMasterKey();
    var query = new Parse.Query("Idea");
    var id = request.object.get("idea").id;
    console.log("with master");
    query.get(request.object.get("idea").id,{
        success: function(ideaResult) {
            console.log("Dont know ");
            var count = Number((ideaResult.get("commentCount") == undefined)? 0 : ideaResult.get("commentCount"));
            ideaResult.increment("commentCount",-1);
            ideaResult.save(null,{
                success: function(savedIdea){
                    console.log('success');
                    response.success();
                },
                error: function(errorSave){
                    console.error(errorSave.message);
                    response.error(errorSave);
                }
            });
        },
        error: function(error) {
            console.error("Error finding related comments " + error);
            response.error(error);
        }
    });
});

Parse.Cloud.afterDelete("Idea", function(request) {
    Parse.Cloud.useMasterKey();
    var idea = request.object;
    var likeQuery = new Parse.Query("Like");
    likeQuery.equalTo("idea",idea);
    likeQuery.find({useMasterKey: true})
    .then(
        function(likeResults){
            Parse.Object.destroyAll(likeResults,{
                success: function(list) {
                    //Comment
                    var commQuery = new Parse.Query("Comentario");
                    commQuery.equalTo("idea",idea);
                    commQuery.find({useMasterKey: true})
                    .then(
                        function(commResults){
                            Parse.Object.destroyAll(commResults,{
                                success: function(list) {
                                    //Archivo
                                    var archQuery = new Parse.Query("Archivo");
                                    archQuery.equalTo("idea",idea);
                                    archQuery.find({useMasterKey: true})
                                    .then(
                                        function(archResults){
                                            Parse.Object.destroyAll(archResults,{
                                                success: function(list) {
                                                   // All the objects were saved.
                                                   console.log("deleted everything");
                                                },
                                                error: function(error) {
                                                   // An error occurred while saving one of the objects.
                                                   console.error("failure on destroy the like objects");
                                                },
                                            }); 
                                        },
                                        function(error){
                                            console.error(error.message);
                                        }
                                    );
                                   
                                },
                                error: function(error) {
                                   // An error occurred while saving one of the objects.
                                   console.error("failure on destroy the like objects");
                                },
                            }); 
                        },
                        function(error){
                            console.error(error.message);
                        }
                    );
                },
                error: function(error) {
                   // An error occurred while saving one of the objects.
                   console.error("failure on destroy the like objects");
                },
            }); 
        },
        function(error){
            console.error(error.message);
        }
    );
});

Parse.Cloud.beforeSave("Roster", function (request, response) {
    if(!request.object.has("rol")){
        request.object.set("rol",{"__type":"Pointer",
                            "className":"RosterRol",
                            "objectId":"htSWI8ovsK"});
    }
    if(!request.object.has("descripcion")){
        request.object.set("descripcion","none");
    }
    response.success();
});

Parse.Cloud.beforeSave("Idea", function (request, response) {
    //check if user being saved has profile picture.
            
        if(request.object.has("user"))
        {
            Parse.Cloud.run("getUser",{userID: request.object.get("user").id},{
                success: function(r){
                    if(request.object.dirty("challenge"))
                        {
                            if(request.object.has("challenge"))
                            {
                                var ch = request.object.get("challenge");
                                console.log(ch.id);
                                var queryCH = new Parse.Query("Challenge");
                                queryCH.get(ch.id,{
                                    success: function(challenge){
                                        console.log(challenge);
                                        challenge.increment("countIdeas");
                                        challenge.save();
                                        checkAttr();
                                    },
                                    error: function(error){
                                        console.error(error.message);
                                    }
                                });
                            }
                            else
                                checkAttr();
                        }
                        else
                            checkAttr();
                },
                error: function(error){
                    response.error(error.message);
                }
            });
        }
        else
            response.error("Must have user");
        
        function checkAttr(){
            if (!request.object.has("fileCount"))
                request.object.set("fileCount", 0);
            if (!request.object.has("likeCount"))
                request.object.set("likeCount", 0);
            if (!request.object.has("likePos"))
                request.object.set("likePos", 0);
            if (!request.object.has("commentCount"))
                request.object.set("commentCount", 0);
            if (!request.object.has("Fase"))
                request.object.set("Fase", 1);

            if(request.object.get("user").get("locationCity") != undefined)
                request.object.set("locationCity",request.object.get("user").get("locationCity"));

            if(Number(request.object.get("likeCount")) >= 50)
            {
                if(Number(request.object.get("Fase")) <= 1)
                {
                    request.object.set("Fase",2);
                     var pusher = new Pusher({
                          appId: '148416',
                          key: '7a5e5c7f6307240cf192',
                          secret: 'a93459ef65de29ce6103',
                          encrypted: true
                        });
                        pusher.trigger( 'produ', request.object.get("user").id, { message: "<p>Your Idea has evolved</p>" } );
                }
            }
            if (!request.object.has("locationGpoint"))
            {
                var gpoint = new Parse.GeoPoint(25.711828,-100.3538829);
                request.object.set("locationGpoint", gpoint);
                request.object.set("locationCountry", "Mexico");
            }
            var user_acl = new Parse.ACL();
            user_acl.setPublicReadAccess(false);
            user_acl.setRoleReadAccess("ValidUser",true);
            user_acl.setPublicWriteAccess(false);
            user_acl.setRoleWriteAccess("ValidUser",false);
            user_acl.setWriteAccess(request.object.get("user").id, true);
            user_acl.setReadAccess(request.object.get("user").id, true);

            request.object.setACL(user_acl);
            console.log("ALL DONE WITH IDEA"+ request.object.id);
            response.success();
        }
});

Parse.Cloud.afterSave("Idea",function(request,response){
    Parse.Cloud.useMasterKey();

    var ideaQuery = new Parse.Query("Idea");
    var d = new Date();
    var n= new Date(d- 10000);
    ideaQuery.equalTo("user",request.object.get("user"));
    ideaQuery.lessThanOrEqualTo("createdAt",d);
    ideaQuery.greaterThanOrEqualTo("createdAt",n);
    ideaQuery.ascending("createdAt");
    ideaQuery.find({useMasterKey: true})
    .then(
        function(result){
            console.log("AQUIII_______________zas->");
            console.log(result.length);
            if(result.length>2)
            {
                Parse.Object.destroyAll(result,{
                    success: function(list) {
                       // All the objects were saved.
                       console.log("deleted everything");
                    },
                    error: function(error) {
                       // An error occurred while saving one of the objects.
                       console.error("failure on destroy the like objects");
                    },
                });
            }
            else
            {
                if(request.object.createdAt.getTime() === request.object.updatedAt.getTime())
                {
                    Parse.Cloud.run('setPoints',{action:"publish", userID: request.object.get("user").id},{
                        success: function(result){
                            console.log(result);
                        },
                        error: function(error){
                            console.error(error.message);
                        }
                    });
                }
            }
        },
        function(error){
            console.error(error.message);
        }
    );
});

Parse.Cloud.afterSave("Challenge",function(request,response){
    Parse.Cloud.useMasterKey();
    console.log(request.user);
    if(request.object.createdAt.getTime() === request.object.updatedAt.getTime())
    {
        Parse.Cloud.run('setPoints',{action:"challenge", userID: request.object.get("user").id},{
            success: function(result){
                console.log(result);
            },
            error: function(error){
                console.error(error.message);
            }
        });
    }
});

Parse.Cloud.afterSave("Like",function(request,response){
    Parse.Cloud.useMasterKey();
    console.log(request.user);
    if(request.object.createdAt.getTime() === request.object.updatedAt.getTime())
    {
        Parse.Cloud.run('setPoints',{action:"like", userID: request.object.get("user").id},{
            success: function(result){
                console.log(result);
            },
            error: function(error){
                console.error(error.message);
            }
        });
    }
});


Parse.Cloud.afterSave("Retention", function (request, response) {
    //check if user being saved has profile picture.
    var SHQuery = new Parse.Query("SessionHistory");
    SHQuery.greaterThanOrEqualTo("createdAt",request.object.get("creation"));
    SHQuery.equalTo("idea",request.object.get("idea"));
    SHQuery.find({useMasterKey: true})
    .then(
        function(SHResults){
            if(SHResult.length > 0)
            {
                var SHResult = SHResults[0];
                SHResult.set("new_users",request.object.get("newUsers").length);
                SHResult.save(null,{
                    success: function(saved){
                        console.log("saved SH");
                    },
                    error: function(error){
                        console.error(error.message);
                    }
                });
            }
        },
        function(error){
            console.error(error.message);
        }
    );
});

Parse.Cloud.define("YammerToOffice",function(request,response){
    Parse.Cloud.useMasterKey();
    var userID = request.params.userID;
    var newPass = request.params.pass;
    var token = request.params.token;
    var uQuery = new Parse.Query("User");
    uQuery.get(userID,{
        success:function(cUser){
            cUser.set("password",newPass);
            cUser.set("yamtoOff",true);
            cUser.set("token",token);
            cUser.save(null,{
                success: function(savedUser){
                    response.success(savedUser);
                },
                error: function(error){
                    response.error(error.message);
                }
            });
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.define("eliminarIdea",function(request,response){
    Parse.Cloud.useMasterKey();
    var ideaID = request.params.ideaID;
    var ideaQuery = new Parse.Query("Idea");
    ideaQuery.get(ideaID,{
        success: function(ideaResult){
            if(request.user.id == ideaResult.get("user").id)
                ideaResult.destroy({
                    success: function(myObject) {
                        response.success("destroyed");
                    },
                    error: function(error) {
                        response.error(error.message);
                    }
                });
            else
                response.error("141");
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

//Obtener lista de notificaciones para un usuario
//params:
//  -userID: notificaciones para este usuario
Parse.Cloud.define("getNotifications", function(request, response) {
   //Get current user
   Parse.Cloud.useMasterKey();
   var currentUserID = request.params.userID;
   var currentUser;
   var User = Parse.Object.extend("User");
   var queryUser = new Parse.Query(User);
   queryUser.get(currentUserID,{
     success: function(userResult){
       currentUser = userResult;
     },
     error: function(error){
       response.error("Error al buscar el usuario");
     }  
   }).then(function(){
       var notificaciones = [];
       var query = new Parse.Query("Notificacion");
       query.equalTo("user", currentUser);
       query.include("user");
       query.include("user_action");
       query.descending("createdAt");
       query.find({useMasterKey: true})
       .then(
           function(results) {
               for (var i = 0; i < results.length; ++i) {
                 notificaciones.push(results[i]);
               }
               response.success(notificaciones);
           },
           function() {
               response.error("Failed");
           }
         );
   });
});

Parse.Cloud.define("getfilter", function(request, response) {
    Parse.Cloud.useMasterKey();
    var MSC1 = Parse.Object.extend("Filter");
    var query = new Parse.Query(MSC1);

    var filterClass = Parse.Object.extend("Filter");
    var filterObj = new filterClass();

    query.equalTo("userId", request.user.id);

    query.find
    ({
        success: function(results)                              
        {          
            response.success(results);
        },
        error:function(error) {
            response.error(error);
        }
    }); 
}); 

Parse.Cloud.define("setFilter", function(request, response) {
    Parse.Cloud.useMasterKey();

    var FilterClass = Parse.Object.extend("Filter");
    var filter = new FilterClass();
    filter.set("userId", request.user.id);
    filter.set("idea",true);
    filter.set("challenge",true);
    filter.set("ideaCat",true);
    filter.set("needCat",true);
    filter.set("appCat",true);
    filter.set("otherCat",true);
    filter.set("pocCat",true);
    filter.set("microCat",true);

    filter.save(null,{
      success:function(filter) { 
        response.success(filter);
      },
      error:function(error) {
        response.error(error);
      }
    });
});

//_------DEB
//Type: Get
//Function that gives you the filters of a user
//Last modified: pending
//Last modified by: Samuel Martínez
//Created: pending
//Created by: Deborah Díaz
//(Parameters)
//  -userID: user’s objectId
//return
//  -List of filters

Parse.Cloud.define("findFilter",function(request,response){

    Parse.Cloud.useMasterKey();

    var currentUserID = request.params.userID;
    var currentUser;
    var User = Parse.Object.extend("User");
    var queryUser = new Parse.Query(User);
    queryUser.get(currentUserID,{
      success: function(userResult){
        currentUser = userResult;
      },
      error: function(error){
        response.error("Error al buscar el usuario");
      }  
    }).then(function(){

    var FilQuery = new Parse.Query("Filters");
    FilQuery.equalTo("user",currentUser);

    var FilObj = Parse.Object.extend("Filters");

    FilQuery.find({useMasterKey: true})
    .then(
        function(filResult){
            if(filResult.length == 0)
            {
                var newFilter = new FilObj();
                newFilter.set("user",currentUser);
                newFilter.set("filter",new Array());
                newFilter.save(null,{
                    success: function(results){
                        response.success(results);
                    },
                    error:function(error){
                        response.error("error de guardar");
                        console.error(error.message);
                    }

                });
            }else
            {
                response.success(filResult[0]);
            }        
        },
        function(error){
            response.error("error de encontrar");
            console.error(error.message);     
        }
    );
});
});

Parse.Cloud.define("getSuggestedIdeas", function (request, response) {  
    //var Idea = Parse.Object.extend("Idea");
    Parse.Cloud.useMasterKey();
    ideas =  [];
    ideasIndex = [];
    files = [];
    comments = [];
    likes = [];
    var userFollowing=[];

    var queryUser = new Parse.Query("Idea");
    var queryTag = new Parse.Query("Idea");
    var userClass = Parse.Object.extend("User");

    var cant = request.params.cantidad; 
    var skip = request.params.skip;
    var tagFollow = request.params.tagFollow;
    var userFollow = request.params.userFollow;

    for (var i = 0; i < userFollow.length; i++) {
        var user = new userClass();
        user.id = userFollow[i];
        userFollowing.push(user);
    };

    //query to get ideas with following users
    queryUser.equalTo("esPrivado", false);
    queryUser.include("user");
    queryUser.containedIn("user", userFollowing);

    //query to get ideas with following tags
    queryTag.equalTo("esPrivado", false);
    queryTag.include("user");
    queryTag.containedIn("tags", tagFollow);

    //main query
    var mainQuery = Parse.Query.or(queryTag, queryUser);
    mainQuery.include("user");
    mainQuery.limit(cant);
    mainQuery.skip(cant*skip);
    mainQuery.find({useMasterKey: true})
    .then(
        function (idea) {

            for (var i = idea.length - 1; i >= 0; i--) {
                ideas.push(idea[i]);
                ideasIndex.push(idea[i].id);
            };          
            
            var views = []; 
          
            for (var i = ideas.length - 1; i >= 0; i--) {
              
                var likeSum = ideas[i].get("likeCount");
                var commentSum = ideas[i].get("commentCount");
                var fileSum = ideas[i].get("fileCount");
              
                views.push({idea: ideas[i], comments: commentSum, likes: likeSum, files: fileSum});
            };

            response.success(views);
        },
        function (error) {
            response.error(error);
        }
    );
});

Parse.Cloud.define("getUserIdeas", function (request, response) {

    Parse.Cloud.useMasterKey();

    var query = new Parse.Query("Idea");
    var cant = request.params.cantidad; 
    var skip = request.params.skip;

    var currentUserID = request.params.currentID;
    var currentUser;
    var User = Parse.Object.extend("User");
    var queryUser = new Parse.Query(User);
    
    queryUser.get(currentUserID,{
      success: function(userResult){
        currentUser = userResult;
      },
      error: function(error){
        response.error("Error al buscar el usuario");
      }  
    }).then(function(){



    ideas =  [];
    ideasIndex = [];
    files = [];
    comments = [];
    likes = [];

    query.include("user");

    query.equalTo("user",currentUser);

    query.limit(cant);
    query.skip(cant*skip);

    query.find({useMasterKey: true})
    .then(
        function (idea) {
            for (var i = idea.length - 1; i >= 0; i--) {
                ideas.push(idea[i]);
                ideasIndex.push(idea[i].id);
            };          
            var views = []; 
          
            for (var i = ideas.length - 1; i >= 0; i--) {
              
                var likeSum = ideas[i].get("likeCount");
                var commentSum = ideas[i].get("commentCount");
                var fileSum = ideas[i].get("fileCount");
              
                views.push({idea: ideas[i], comments: commentSum, likes: likeSum, files: fileSum});
            };
            response.success(views);
            //console.log(Enumerable.From(ideas).ToArray());            
        },
        function (error) {
            // body...
        }
    );
    });
});


/*get random tags except tag user already follow*/
Parse.Cloud.define('getRandomTags', function(request, response) {

    Parse.Cloud.useMasterKey();
    var query = new Parse.Query("Tags");
    var cant = request.params.cantidad; 
    var skip = request.params.skip;
    var tags = request.params.userTags;

    query.notContainedIn("name", tags);
    query.limit(cant);
    query.skip(cant*skip);
    query.find({useMasterKey: true})
    .then(
        function (tags) {

            var arraytags = [];
            for (var i = 0; i < tags.length ; i++) {
              
                var tag = tags[i].get("name");
                arraytags.push(tag);

            };


            response.success(arraytags);
        },
        function(error){
            response.error(error.message);
        }
    );

});

Parse.Cloud.define('getAllTags', function(request, response) {

    Parse.Cloud.useMasterKey();
    var query = new Parse.Query("Tags");
    var cant = request.params.cantidad; 
    var skip = request.params.skip;
    query.limit(cant);
    query.skip(cant*skip);
    query.find({useMasterKey: true})
    .then(
        function (tags) {

            var arraytags = [];
            for (var i = 0; i < tags.length ; i++) {
                var tag = tags[i].get("name");
                arraytags.push(tag);
            };


            response.success(arraytags);
        },
        function(error){
            response.error(error.message);
        }
    );

});


Parse.Cloud.define("setFollowing", function(request, response) {
    Parse.Cloud.useMasterKey(); //Made after the migration
    var UserClass = Parse.Object.extend("User");
    var user = request.user;
    var tag = request.params.tag;


    for(var i = 0; i<tag.length; i++){
    user.addUnique("tagFollowing",tag[i]);
    }
    
    

 
    user.save(null,{
      success:function(user) { 
        response.success(user);
      },
      error:function(error) {
        response.error(error);
      }
    });
});

Parse.Cloud.define("getfilterArray", function(request, response) {
    
    Parse.Cloud.useMasterKey();

    var currentUserID = request.params.currentID;
    var currentUser;
    var User = Parse.Object.extend("User");
    var queryUser = new Parse.Query(User);
    
    queryUser.get(currentUserID,{
      success: function(userResult){
        currentUser = userResult;
      },
      error: function(error){
        response.error("Error al buscar el usuario");
      }  
    }).then(function(){

    var filter = Parse.Object.extend("Filters");
    var query = new Parse.Query(filter);

    query.equalTo("user", currentUser);
 
    query.find
    ({
        success: function(results)                              
        {         
            response.success(results[0].get("filter"));
        },
        error:function(error) {
            
            response.error(error);
        }
    }); 
    });
});

Parse.Cloud.define("deleteFilterArray", function(request, response) {
    Parse.Cloud.useMasterKey(); //Made after the migration

    var filter = Parse.Object.extend("Filters");
    var query = new Parse.Query(filter);

    query.equalTo("user", request.user);

    query.find({useMasterKey: true})
    .then(
        function(user) { 
            user[0].remove("filter", 11); 
            user[0].remove("filter", 22); 
            user[0].remove("filter", 0); 
            user[0].remove("filter", 1); 
            user[0].remove("filter", 2); 
            user[0].remove("filter", 3); 
            user[0].remove("filter", 4); 
            user[0].remove("filter", 5);
            user[0].remove("filter", 100); 
            user[0].save(null,{
              success:function(user) { 
                response.success(user);
              },
              error:function(error) {
                response.error(error);
              }
            });
      },
      function(error) {
        response.error(error);
      }
    );

});

Parse.Cloud.define("getRecentlyAdded", function (request, response) {  
    //var Idea = Parse.Object.extend("Idea");
    Parse.Cloud.useMasterKey();
    var userfilters = request.params.userFilter;
    var cant = request.params.cantidad; 
    var skip = request.params.skip;

            var query = new Parse.Query("Idea");


            ideas =  [];
            ideasIndex = [];
            files = [];
            comments = [];
            likes = [];
            query.equalTo("esPrivado", false);
            query.include("user");
            query.descending("createdAt");
            query.notEqualTo("user",request.user);
            query.containedIn("category", userfilters);
            query.limit(cant);
            query.skip(cant*skip);

            query.find({useMasterKey: true})
            .then(
                function (idea) {
                    for (var i = idea.length - 1; i >= 0; i--) {
                        ideas.push(idea[i]);
                        ideasIndex.push(idea[i].id);
                    };          
                    var views = []; 
                  
                    for (var i = ideas.length - 1; i >= 0; i--) {
                      
                        var likeSum = ideas[i].get("likeCount");
                        var commentSum = ideas[i].get("commentCount");
                        var fileSum = ideas[i].get("fileCount");
                      
                        views.push({idea: ideas[i], comments: commentSum, likes: likeSum, files: fileSum});
                    }
                    response.success(views);
                    //console.log(Enumerable.From(ideas).ToArray());            
                },
                function (error) {
                    // body...
                }
        );

});

Parse.Cloud.define("tagIdeas", function (request, response) {  
    //var Idea = Parse.Object.extend("Idea");
    Parse.Cloud.useMasterKey();
    var query = new Parse.Query("Idea");
    var cant = request.params.cantidad; 
    var skip = request.params.skip;
    query.equalTo("tags", request.params.tagname);
    ideas =  [];
    ideasIndex = [];
    files = [];
    comments = [];
    likes = [];
    query.equalTo("esPrivado", false);
    query.include("user");
    query.descending("createdAt");
    query.limit(cant);
    query.skip(cant*skip);
    query.find({useMasterKey: true})
    .then(
        function (idea) {
            for (var i = idea.length - 1; i >= 0; i--) {
                ideas.push(idea[i]);
                ideasIndex.push(idea[i].id);
            };          
            var views = []; 
          
            for (var i = ideas.length - 1; i >= 0; i--) {
              
                var likeSum = ideas[i].get("likeCount");
                var commentSum = ideas[i].get("commentCount");
                var fileSum = ideas[i].get("fileCount");
              
                views.push({idea: ideas[i], comments: commentSum, likes: likeSum, files: fileSum});
            };
            response.success(views);
            //console.log(Enumerable.From(ideas).ToArray());            
        },
        function (error) {
            // body...
        }
    );
});

Parse.Cloud.define('getFollowing', function(request, response) {

    Parse.Cloud.useMasterKey(); //Made after the migration

    var user = new Parse.Query("User");
    user.get(request.user.id,{
        success: function(result){
            response.success(result);
        },
        error: function(error){
            response.error(error.message);
        }
    });

});

Parse.Cloud.define("removeFollowing", function(request, response) {

    Parse.Cloud.useMasterKey(); //Made after the migration

    var UserClass = Parse.Object.extend("User");
    var user = request.user;
    var tag = request.params.tag; 
    user.remove("tagFollowing", tag); 
    user.save(null,{
      success:function(user) { 
        response.success(user);
      },
      error:function(error) {
        response.error(error);
      }
    });
});

/*Add new tag*/
Parse.Cloud.define("setNewTag", function(request, response) {

    Parse.Cloud.useMasterKey();

    var tagClass = Parse.Object.extend("Tags");
    var query = new Parse.Query("Tags");

    var tags = new tagClass();

    var tagname = request.params.tagname;

    query.equalTo("name", tagname);

    query.find({useMasterKey: true})
    .then(
        function (result) {

            response.success(result);

            if(result.length<1){

               var tags = new tagClass();
                tags.set("name", tagname);
                tags.save(null,{
            
                    success:function(newtag) { 
                        response.success(newtag);
                    },
                    error:function(error) {
                        response.error(error);
                    }
                });


            }
        },
        function(error){
            response.error(error.message);
        }
    );
});



Parse.Cloud.define("deleteCoverImage", function(request,response){
   Parse.Cloud.useMasterKey();

   var archivoID = request.params.archivoID;
   var ArchivoQuery = new Parse.Query("Archivo");
   ArchivoQuery.include("idea");
   ArchivoQuery.get(archivoID,{
       success: function(archivoR){
           var idea = archivoR.get("idea");
           if(idea.get("photoUrl") == archivoR.get("file").url())
           {
               console.log("IGUALESS")
               var ArchivoQuery = new Parse.Query("Archivo");
               ArchivoQuery.equalTo("idea",idea);
               ArchivoQuery.notEqualTo("objectId",archivoID);
               ArchivoQuery.find({useMasterKey: true})
                .then(
                   function(allArchivos){
                       var nChange = true;
                       console.log(allArchivos.length);
                       for (var i = 0; i < allArchivos.length; i++) {
                           var cObject = allArchivos[i];
                           var parts = cObject.get("nombre").split(".");
                           var ext = parts[parts.length - 1];
                           if(["jpg","jpeg","png","git"].indexOf(ext) > -1)
                           {
                               nChange = false;
                               idea.set("photoUrl",cObject.get("file").url()); 
                           }
                       };

                       if(nChange)
                           idea.set("photoUrl","");

                       idea.save(null,{
                           success: function(ideaSaved){
                               archivoR.destroy({
                                   success: function(newComentario)
                                   {
                                       response.success(ideaSaved.get("photoUrl"));
                                   },
                                   error: function(errorR)
                                   {
                                       response.error(result);
                                   }
                               });
                           },
                           error: function(error){
                               response.error(error.message);
                           }
                       });
                   },
                   function(error){
                       response.error(error.message);
                   }
               );
           }
           else
           archivoR.destroy({
               success: function(newComentario)
               {
                   response.success(idea.get("photoUrl"));
               },
               error: function(errorR)
               {
                   response.error(result);
               }
           });
       },
       error: function(error){
           response.error(error.message);
       }
   });
});


Parse.Cloud.define("setFilterArray", function(request, response) {

    Parse.Cloud.useMasterKey(); //Made after the migration

    var filter = Parse.Object.extend("Filters");
    var query = new Parse.Query(filter);
 
    var filterarray = request.params.filterarray; 

    query.equalTo("user", request.user);

    query.find({useMasterKey: true})
    .then(
        function(user) { 

            for (var i = 0; i < filterarray.length; i++) {
                user[0].addUnique("filter",filterarray[i]);
            }
                user[0].save(null,{
                  success:function(user) { 
                    response.success(user);
                  },
                  error:function(error) {
                    response.error(error);
                  }
                });
      },
      function(error) {
        response.error(error);
      }

    );

});

Parse.Cloud.define('getUserFollowing', function(request, response) {

    Parse.Cloud.useMasterKey(); //Made after the migration

    var userID = request.params.userID; 
    var user = new Parse.Query("User");
    var userClass = Parse.Object.extend("User");
    user.get(request.user.id,{
        success: function(result){
        response.success(result);

        },
        error: function(error){
            response.error(error.message);
        }
    });

});

Parse.Cloud.define("getUserIdeasSelected", function (request, response) {

    Parse.Cloud.useMasterKey();

    var query = new Parse.Query("Idea");
    var cant = request.params.cantidad; 
    var skip = request.params.skip;
    var userId = request.params.userID;

    var userSelected;
    var User = Parse.Object.extend("User");
    var queryUser = new Parse.Query(User);
    
    queryUser.get(userId,{
      success: function(userResult){
        userSelected = userResult;
      },
      error: function(error){
        response.error("Error al buscar el usuario");
      }  
    }).then(function(){

        ideas =  [];
        ideasIndex = [];
        files = [];
        comments = [];
        likes = [];

        query.include("user");

        query.equalTo("user",userSelected);

        query.limit(cant);
        query.skip(cant*skip);

        query.find({useMasterKey: true})
        .then(
            function (idea) {
                for (var i = idea.length - 1; i >= 0; i--) {
                    ideas.push(idea[i]);
                    ideasIndex.push(idea[i].id);
                };          
                var views = []; 
              
                for (var i = ideas.length - 1; i >= 0; i--) {
                  
                    var likeSum = ideas[i].get("likeCount");
                    var commentSum = ideas[i].get("commentCount");
                    var fileSum = ideas[i].get("fileCount");
                  
                    views.push({idea: ideas[i], comments: commentSum, likes: likeSum, files: fileSum});
                };
                response.success(views);
                //console.log(Enumerable.From(ideas).ToArray());            
            },
            function (error) {
                // body...
            }
        );
    });
});

Parse.Cloud.define("setUserFollowing", function(request, response) {
    Parse.Cloud.useMasterKey();

    var UserClass = Parse.Object.extend("User");
    var user = request.user;
    var userFollowing = request.params.user;

    user.addUnique("userFollowing",userFollowing);
        user.save(null,{
                  success:function(userSaved) { 
                    response.success(userSaved);
                  },
                  error:function(error) {
                    response.error(error);
                  }
                });

});

Parse.Cloud.define("setUserFollowingWithUID", function(request, response) {
    Parse.Cloud.useMasterKey();

    var UserClass = Parse.Object.extend("User");
    var userID = request.params.uID;
    var userFollowing = request.params.user;
    var uQuery = new Parse.Query("User");
    uQuery.get(userID,{
        success: function(user){
            user.addUnique("userFollowing",userFollowing);
            user.save(null,{
                      success:function(userSaved) { 
                        response.success(userSaved);
                      },
                      error:function(error) {
                        response.error(error);
                      }
                    });
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.define("removeUserFollowing", function(request, response) {

    Parse.Cloud.useMasterKey(); //Made after the migration    

    var UserClass = Parse.Object.extend("User");
    var user = request.user;
    var userF = request.params.user; 
    user.remove("userFollowing", userF); 
    user.save(null,{
      success:function(userSaved) { 
        response.success(userSaved);
      },
      error:function(error) {
        response.error(error);
      }
    });
});

Parse.Cloud.define("getUploadWeek", function(request, response) {

    Parse.Cloud.useMasterKey(); //Made after the migration
    
    var ideaID = request.params.ideaID; 
    var query = new Parse.Query("Idea");

    query.include("user");
    query.include("challenge");
    query.get(ideaID,{
      success: function(result) {
        var queryUpload = new Parse.Query("ChallengeUploads");
        queryUpload.include("user");
        queryUpload.include("challenge");
        queryUpload.equalTo("idea",result);
        queryUpload.equalTo("challenge",result.get("challenge"));
        queryUpload.ascending("week");
        queryUpload.find({useMasterKey: true})
        .then(
            function(resultsU){
                response.success(resultsU);
            },
            function(error){
                response.error(error.message);
            }
        );
      },
      error: function(error) {
        response.error("Error: " + error.code + " " + error.message);
      }
    });
});

Parse.Cloud.define("pusherTest", function(request, response) {
    var uID = request.params.uID;
    var pusher = new Pusher({
      appId: '148416',
      key: '7a5e5c7f6307240cf192',
      secret: 'a93459ef65de29ce6103',
      encrypted: true
    });
    pusher.trigger( uID, 'noti', { message: "hello world" } );
    response.success("Enviado");    
})

Parse.Cloud.define("isFollowing", function(request, response) {
    Parse.Cloud.useMasterKey();    
   
    var uID = request.params.current;
    var fID = request.params.userID;
    var uQuery = new Parse.Query("User");
    uQuery.get(uID,{
        success: function(uRes){
            if(uRes.get("userFollowing").lastIndexOf(fID) > -1)
                {
                    console.log(uID+" sigue a "+fID);
                    response.success(true);
                }
            else
                {
                    console.log(uID+" NO sigue a "+fID);
                    response.success(false);
                }
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.define("isFollowingTag", function(request, response) {
    Parse.Cloud.useMasterKey();    
   
    var uID = request.params.current;
    var tag = request.params.tag;
    var uQuery = new Parse.Query("User");
    uQuery.get(uID,{
        success: function(uRes){
            if(uRes.get("tagFollowing").lastIndexOf(tag) > -1)
                {
                    console.log(uID+" sigue a "+tag);
                    response.success(true);
                }
            else
                {
                    console.log(uID+" NO sigue a "+tag);
                    response.success(false);
                }
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.define("setVisitedTags", function(request, response) {

    Parse.Cloud.useMasterKey(); //Made after the migration

   var currentUser = request.user;
   var ideaId = request.params.ideaId;


   var ideaTags;
   var tagVisited;

   var queryIdea = new Parse.Query("Idea");
   queryIdea.get(ideaId,{

       success: function(ideaResult) {

           ideaTags = ideaResult.get("tags");
           tagFollowing = currentUser.get("tagFollowing");

           var tags = new Array();

           for (var i = 0; i < ideaTags.length; i++) {
               if(tagFollowing.lastIndexOf(ideaTags[i])<=-1){
                   tags.push(ideaTags[i]);
               }
           }

           if(currentUser.has("tagVisited")){

               var tagObjects = currentUser.get("tagVisited");

               for (var i = 0; i < tagObjects.length; i++) {
                   var currentObject = tagObjects[i];

                   if(tags.lastIndexOf(currentObject.tag)>=0){
                       currentObject.count++;
                       tags.splice(tags.lastIndexOf(currentObject.tag),1);
                   }

               }

               if(tags.length>0){
                   for (var i = 0; i < tags.length; i++) {
                       tagObjects.push({tag:tags[i],count:1});
                   }
               }

               currentUser.set("tagVisited",tagObjects);
                console.log(tagObjects);
                console.log("ooooooooooooooooooooooooooooooooooo");
           }
           else{

               var tagObjects=new Array();

               if(tags.length>0){
                   for (var i = 0; i < tags.length; i++) {
                       tagObjects.push({tag:tags[i],count:1});
                   }
               }

               currentUser.set("tagVisited",tagObjects);

           }

           currentUser.save(null,{
                       
                               success:function(newtagVisited) { 
                                   response.success(newtagVisited);
                               },
                               error:function(error) {
                                   response.error(error);
                               }
                           });

                       },
                   error:function(error) {
                       response.error(error);
                   }
              
   });
});

Parse.Cloud.define("ideasFullCh", function (request, response) {  
    //var Idea = Parse.Object.extend("Idea");
    Parse.Cloud.useMasterKey();
    var query = new Parse.Query("Idea");
    var cant = request.params.cantidad; 
    var skip = request.params.skip;

    var cantCh = request.params.cantCh; 
    var skipCh = request.params.skipCh;
    ideas =  [];
    ideasIndex = [];
    files = [];
    comments = [];
    likes = [];
    query.equalTo("esPrivado", false);
    query.include("user");
    query.notEqualTo("user",request.user);
    query.descending("createdAt");
    query.limit(cant);
    query.skip(cant*skip);
    query.find({useMasterKey: true})
    .then(

        function (idea) {

            var queryCH = new Parse.Query("Challenge");
            queryCH.limit(cantCh);
            queryCH.skip(cantCh*skipCh);
            queryCH.descending("createdAt");
            queryCH.include("user");
            queryCH.find({useMasterKey: true})
            .then(
                function(challenges)
                {

                    var views = []; 
                 
                    for (var i = idea.length - 1; i >= 0; i--) {
                     
                        var likeSum = idea[i].get("likeCount");
                        var commentSum = idea[i].get("commentCount");
                        var fileSum = idea[i].get("fileCount");
                        
                        // var val ={like:0, dislike: 0};

                        // val.like = ideas[i].get("likePos");
                        // val.dislike = val.like - likeSum;
                        views.push({idea: idea[i], comments: commentSum, likes: likeSum, files: fileSum});
                    };

                    for (var i = 0; i < challenges.length; i++) {
                        views.push({idea: challenges[i],comments: 0, likes: 0,files: 0});
                    };
                    var linqQuery = LINQ.Enumerable.From(views).OrderByDescending("$.idea.createdAt").ToArray();
                    response.success(linqQuery);
                },
                function(errorCH){
                    response.error(errorCH);
                }
            );
            //console.log(Enumerable.From(ideas).ToArray());            
        },
        function (error) {
            // body...
        }
    );


});

Parse.Cloud.define("getRecentlyFilter", function (request, response) {  
    //var Idea = Parse.Object.extend("Idea");
    Parse.Cloud.useMasterKey();

    var userfilters = request.params.userFilter;
    var filterCategories = request.params.userFilter;
    var cant = request.params.cantidad; 
    var skip = request.params.skip;
    var cantCh = request.params.cantCh; 
    var skipCh = request.params.skipCh;

    var query = new Parse.Query("Idea");

    ideas =  [];
    ideasIndex = [];
    files = [];
    comments = [];
    likes = [];

    query.include("user");
    query.notEqualTo("user",request.user);
    query.descending("createdAt");
    query.equalTo("esPrivado", false);
    //Location filter
    if(userfilters.indexOf(100) >= 0){
        //Apply location filter
        var location = [];
        location.push(request.user.get("locationCity"));
        location.push(request.user.get("locationCity").replace("GDC ", ""));
        query.containedIn("locationCity", location);
        if(userfilters.length > 1){
            //Apply category filter
            query.containedIn("category", filterCategories);
        }
    }else if(userfilters.length > 0 && userfilters.indexOf(100) < 0){
        //No location filter
        //Apply ONLY category filter
        query.containedIn("category", filterCategories);
    }

    /*if(userfilters.indexOf(11)>=0){
        if(userfilters.indexOf(100) >= 0)
            query.equalTo("locationCity",request.user.get("locationCity"));
    }else{
        if(userfilters.indexOf(100) >= 0)
            query.equalTo("locationCity",request.user.get("locationCity"));
        query.containedIn("category", userfilters);
    }*/

    query.limit(cant);
    query.skip(cant*skip);
    query.find({useMasterKey: true})
    .then(

        function (idea) {

            var queryCH = new Parse.Query("Challenge");
            queryCH.limit(cantCh);
            queryCH.skip(cantCh*skipCh);
            queryCH.descending("createdAt");
            queryCH.containedIn("category", userfilters);
            queryCH.include("user");
            queryCH.find({useMasterKey: true})
            .then(
                function(challenges)
                {

                    var views = []; 
                 
                    for (var i = idea.length - 1; i >= 0; i--) {
                     
                        var likeSum = idea[i].get("likeCount");
                        var commentSum = idea[i].get("commentCount");
                        var fileSum = idea[i].get("fileCount");
                        
                        views.push({idea: idea[i], comments: commentSum, likes: likeSum, files: fileSum});
                    };

                    for (var i = 0; i < challenges.length; i++) {
                        views.push({idea: challenges[i],comments: 0, likes: 0,files: 0});
                    };
                    var linqQuery = LINQ.Enumerable.From(views).OrderByDescending("$.idea.createdAt").ToArray();
                    response.success(linqQuery);
                },
                function(errorCH){
                    response.error(errorCH);
                }
            );
            //console.log(Enumerable.From(ideas).ToArray());            
        },
        function (error) {
            // body...
        }
    );


});


Parse.Cloud.define('getCategories', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();

    var category = new Parse.Query("itemCategory");
    category.find({useMasterKey: true})
    .then(
        function(categoryResults){
                    response.success(categoryResults);
        },
        function(error){
            response.error(error.message);
        }
    );
});





// get subcategory

Parse.Cloud.define('getSubcategories', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();
    var categoryId = request.params.categoryId; 

    var subcategoryArray = new Array();
    var itemCategory = new Parse.Query("itemCategory");
    itemCategory.get(categoryId, {
        success: function(categoryObj) {
            // response.success(categoryObj);
            var subcategory = new Parse.Query("itemCategoryRelation");
            subcategory.equalTo("Category", categoryObj);
            subcategory.include("Subcategory");
            subcategory.descending("updatedAt");
            subcategory.find({useMasterKey: true})
            .then(
                function(subcategoriesIds) {

                    var category = categoryObj.get("name").replace(/[^a-zA-Z0-9]/g, '')
                    subcategoryArray.push(category);

                    for (var i = 0; i < subcategoriesIds.length; i++) {
                        subcategoryArray.push(subcategoriesIds[i].get("Subcategory").get("name"));
                    }

                    response.success(subcategoryArray);                
                    
                },//finish success
                function(error) {
                    response.error(error.message);
                }
            );
        },
        error: function(error) {
            response.error(error.message);
        }
    });
});

//get items per subcategory 

Parse.Cloud.define('getItemsSubcategory', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();

    var subcategoryName = request.params.subcategoryName;
    var item = new Parse.Query("item");

    var subcategory = new Parse.Query("itemSubcategory");
    subcategory.equalTo("name",subcategoryName);

    subcategory.find({useMasterKey: true})
    .then(
        function(subcategoryObj){

                        item.equalTo("subcategory",subcategoryObj[0]);
                        item.find({useMasterKey: true})
                        .then(
                            function(items){
                                        response.success(items);
                            },
                            function(error){
                                response.error(error.message);
                            }
                        );

        },
        function(error){
            response.error(error.message);
        }
    );

});


//get item
Parse.Cloud.define('getItem', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();

    var itemId = request.params.itemId;
    var item = new Parse.Query("item");
    item.include("subcategory");
    item.equalTo("objectId",itemId);
    item.find({useMasterKey: true})
    .then(
        function(item){

                    response.success(item[0]);       

        },
        function(error){
            response.error(error.message);
        }
    );

});


Parse.Cloud.define('getItemsRelated', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();

    var itemId = request.params.itemId;
    var subcategory = request.params.subcategory;
    var item = new Parse.Query("item");
    
    var subcategoryObj = new Parse.Query("itemSubcategory");
    subcategoryObj.equalTo("name",subcategory);
    subcategoryObj.find({useMasterKey: true})
    .then(

        function(subcategoryResult){

            item.equalTo("subcategory",subcategoryResult[0]);
            item.notEqualTo("objectId",itemId);
            item.greaterThan("availability", 0);
            item.descending("countOrders");
            item.find({useMasterKey: true})
            .then(
                function(items){
                    response.success(items);
                },
                function(error){
                    response.error(error.message);
                }
            );    

        },
        function(error){
            response.error(error.message);
        }
    );


    });


Parse.Cloud.define('getPopularItems', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();
    var item = new Parse.Query("item");
    item.greaterThan("availability", 0);
    item.descending("countOrders");
    item.limit(12); 
    item.find({useMasterKey: true})
    .then(
        function(items){
            response.success(items);
        },
        function(error){
            response.error(error.message);
        }
    );    

});




Parse.Cloud.define("setOrder", function(request, response) {

    Parse.Cloud.useMasterKey();
    var currentUser = request.user;
    var Order = Parse.Object.extend("orders");
    var order = new Order();

    var orders = new Parse.Query("orders");
    orders.equalTo("user",request.user);
    orders.equalTo("status","pending");
    orders.find({useMasterKey: true})
    .then(
                function(ideasR){
                    if(ideasR.length>0){

                        response.success(ideasR[0].id);

                    }

                    else{

                        order.set("user", currentUser);
                        order.set("status", "pending");

                        order.save(null, {
                          success: function(neworder) {
                            response.success(neworder.id);
                          
                          },
                          error: function(error) {
                            response.error(error);
                          }
                        });

                    }
                },
                function(error){
                    console.error(error.message);
                    response.error(error.message);
                }
            );

});



Parse.Cloud.define("getItemsCart", function(request, response) {

    Parse.Cloud.useMasterKey();
    var currentUser = request.user;

    var orders = new Parse.Query("orders");
    orders.equalTo("user",request.user);
    orders.equalTo("status","pending");
    orders.find({useMasterKey: true})
    .then(
     function(orderPending) {
            var ordersD = new Parse.Query("orderDetails");
            ordersD.include("item");
            ordersD.equalTo("order",orderPending[0]);
            ordersD.find({useMasterKey: true})
            .then(
             function(itemsCart) {
                response.success(itemsCart);
              
              },
              function(error) {
                response.error(error);
              }

            );
      
      },
      function(error) {
        response.error(error);
      }

    );

});


Parse.Cloud.define("updateItem", function(request, response) {

    Parse.Cloud.useMasterKey();
    var orderID = request.params.orderID;
    var quantity = request.params.quantity;

    // Create a pointer to an object of class Point with id dlkj83d
    var Order = Parse.Object.extend("orderDetails");
    var order = new Order();
    order.id = orderID;

    // Set a new value on quantity
    order.set("quantity", quantity);

    // Save
    order.save(null, {
      success: function(update) {
        response.success(update);
      },
      error: function(point, error) {
        response.error(error);
      }
    });


});


Parse.Cloud.define("deleteItem", function(request, response) {

    Parse.Cloud.useMasterKey();
    var orderID = request.params.orderID;

     var query = new Parse.Query("orderDetails");
        query.equalTo("objectId", orderID);
        query.find({useMasterKey: true})
        .then(
            function(result) {
                result[0].destroy({
                    success: function(object) {
                        alert('Delete Successful');
                        response.success(object);
                    },
                    error: function(object, error) {
                       response.error(error);
                    }
                });
            },
            function(error) {
                response.error(error);
            }
        );
});


Parse.Cloud.define('getPuzzles', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();

    var uQuery = new Parse.Query("User");
    uQuery.get(request.user.id,{
        success: function(cUser){
            
            var puzzles = (cUser.has("puzzles"))? cUser.get("puzzles") : 0;
            response.success(puzzles);

        },
        error: function(error){
            response.error(error.message);
        }
});

});


Parse.Cloud.define('payOrder', function(request, response) {

    Parse.Cloud.useMasterKey();
    var puzzles = request.params.puzzles;
    


    var uQuery = new Parse.Query("User");
    uQuery.get(request.user.id,{
        success: function(cUser){

            var User = Parse.Object.extend("User");
            var user = new User();
            user.id = request.user.id;
            var actualPuzzles = cUser.attributes.puzzles;
            var total = actualPuzzles-puzzles;

            // Set a new value on quantity
            user.set("puzzles", total);

            // Save
            user.save(null, {
              success: function(update) {
               

             var orders = new Parse.Query("orders");
             orders.equalTo("status","pending");
             orders.find({useMasterKey: true})
            .then(
                    function(order){

                         var orders = new Parse.Query("orders");
                         orders.equalTo("status","pending");
                         orders.find({useMasterKey: true})
                         .then(
                                function(order){

                                    // Create a pointer to an object of class Point with id dlkj83d
                                    var Orders = Parse.Object.extend("orders");
                                    var orders = new Orders();
                                    orders.id = order[0].id;

                                    // Set a new value on quantity
                                    orders.set("status", "paid");

                                    // Save
                                    orders.save(null, {
                                      success: function(update) {
                                        response.success(total);
                                      },
                                      error: function(point, error) {
                                        response.error(error);
                                      }
                                    });

                                },
                                function(error){
                                    response.error(error.message);
                                }
                            );

                    },
                    function(error){
                        response.error(error.message);
                    }
                );



              },
              error: function(point, error) {
                response.error(error);
              }
            });

        },
        error: function(error){
            response.error(error.message);
        }
    });


});

Parse.Cloud.define('getNumberItems', function(request, response) {

    Parse.Cloud.useMasterKey(); //Made after the migration    

       var uQuery = new Parse.Query("User");
        uQuery.get(request.user.id,{
            success: function(cUser){


                var orders = new Parse.Query("orders");
                orders.equalTo("status","pending");
                orders.equalTo("user",cUser);
                orders.find({useMasterKey: true})
                .then(
                  function(order){
                    
                    if(order.length > 0)
                    {
                        var orderDetails = new Parse.Query("orderDetails");
                        orderDetails.equalTo("order",order[0]);
                        orderDetails.find({useMasterKey: true})
                        .then(
                          function(item){

                            var totalItems = 0;

                            for (var i = 0; i < item.length; i++) {
                                totalItems= totalItems + item[i].attributes.quantity;
                            };

                            response.success(totalItems);

                          },
                          function(point, error) {
                            response.error(error);
                          }
                        );
                    }
                    else
                        response.success(0);

                  },
                  function(point, error) {
                    response.error(error);
                  }
                );

        },

        error: function(point, error) {
            response.error(error);
        }
        });


});


Parse.Cloud.define('setOrderDetails', function(request, response) {
    //parametros
    Parse.Cloud.useMasterKey();
    var itemID = request.params.itemID;
    var quantity = request.params.quantity;
    var itemQuery = new Parse.Query("item");

    itemQuery.get(itemID,{ //get item object
        success: function(item){
                var orderId = request.params.orderId;
                var orderQuery = new Parse.Query("orders");
                orderQuery.get(orderId,{
                    success: function(orderResult){

                        var query = new Parse.Query("orderDetails");
                        query.equalTo("order", orderResult);
                        query.equalTo("item", item);
                        query.find({useMasterKey: true})
                        .then(
                          function(results) {
                            if(results.length>0){

                                var actualQuantity = results[0].attributes.quantity;
                                var newQuantity = actualQuantity + quantity;

                                var Order = Parse.Object.extend("orderDetails");
                                var order = new Order();
                                order.id = results[0].id;

                                // Set a new value on quantity
                                order.set("quantity", newQuantity);

                                // Save
                                order.save(null, {
                                  success: function(update) {
                                    response.success(update);
                                  },
                                  error: function(point, error) {
                                    response.error(error);
                                  }
                                });

                            }

                            if(results.length<1){

                                var OrderDetails = Parse.Object.extend("orderDetails");
                                var orderD = new OrderDetails();

                                orderD.set("order", orderResult);
                                orderD.set("item", item);
                                orderD.set("quantity", quantity);

                                orderD.save(null, {
                                  success: function(newItem) {
                                    response.success(newItem);
                                  
                                  },
                                  error: function(error) {
                                    response.error(error);
                                  }
                                });

                            }
                          },
                          function(error) {
                            response.error("Error: " + error.code + " " + error.message);
                          }
                        );

                    },
                    error: function(error){
                        response.error(error.message);
                    }
                });
        },
        error: function(error){
            response.error(error.message);
        }
    }); // itemquery
});




Parse.Cloud.define('updateItemsPaid', function(request, response) {
        //parametros
        Parse.Cloud.useMasterKey();

        var currentUser = request.user;
        var orders = new Parse.Query("orders");
        orders.equalTo("user", request.user);
        orders.equalTo("status", "pending");
        orders.find({useMasterKey: true})
        .then(
                function(orderPending) {

                        var query = new Parse.Query("orderDetails");
                        query.equalTo("order", orderPending[0]);
                        query.include("item");
                        query.find({useMasterKey: true})
                        .then(

                                function(results) {

                                        console.log(results)
                                        for (var i = 0, len = results.length; i < len; i++) {
                                                var result = results[i].attributes.item;
                                                var id = result.id
                                                var quantity = results[i].attributes.quantity
                                                var availability = result.attributes.availability
                                                var newAvailability = availability - quantity
                                                var countOrders = result.attributes.countOrders
                                                var newCountOrders = countOrders + quantity

                                                result.set("id", id);
                                                result.set("availability", newAvailability)
                                                result.set("countOrders", newCountOrders);
                                                result.save();
                                        } //for

                                        response.success("saved");
                                }, //success items
                                function(error) {
                                        alert("Error: " + error.code + " " + error.message);
                                }

                        ); //find

                },
                function(error) {
                        response.error(error.message);
                }
        );


});


Parse.Cloud.define("setSkills", function(request, response) {

    Parse.Cloud.useMasterKey();
    var arraySkills = request.params.arraySkills;
    var currentUser = request.user;
    var User = Parse.Object.extend("User");
    var user = new User();

    user.set("objectId", currentUser.id);
    user.set("skills", arraySkills );

    user.save(null, {
      success: function(userSkills) {
        response.success(userSkills);
      
      },
      error: function(error) {
        response.error(error);
      }
    });

});


Parse.Cloud.define("getSkills", function(request, response) {

    Parse.Cloud.useMasterKey();

    var user = new Parse.Query("User");
    user.get(request.user.id,{
        success: function(user){
            response.success(user.attributes.skills);
        },
        error: function(error){
            response.error(error.message);
        }
    });
});


Parse.Cloud.define('getSuggestedIdeasTotal', function(request, response) {

    Parse.Cloud.useMasterKey();

    var cant = request.params.cantidad; //cant
    var skip = request.params.skip; //skip

    var tagOfLikes = []; // tag de los likes que el usuario hizo
    var arrayRecentlySearch = []; //arreglo de las ideas recientes buscadas
    var arrayTagIdeasSearch = []; // arreglo de los tagd e las ideas buscadas
    var userIdeasTags = []; //arreglo de los tag de las ideas creadas pro el usaurio
    var recentlySearch = []; //

    ideas = [];
    ideasIndex = [];
    files = [];
    comments = [];
    likes = [];

    var like = new Parse.Query("Like");
    like.include("user");
    like.include("idea");
    like.find({useMasterKey: true})
    .then(

        function(likesArray) { //se trae a todos los likes


            var queryLikes = LINQ.Enumerable.From(likesArray).Where(function(x) {
                return request.user.id == x.attributes.user.id && x.attributes.esPositivo == true;
            }).ToArray();

            for (var i = 0; i < queryLikes.length; i++) {
                for (var j = 0; j < queryLikes[i].attributes.idea.attributes.tags.length; j++) {
                    tagOfLikes.push(queryLikes[i].attributes.idea.attributes.tags[j]);
                };
            };

            tagOfLikes = eliminateDuplicates(tagOfLikes);

            console.log("tagOfLikes");
            console.log(tagOfLikes);

            var arrayRecentlySearch = (request.user.get("recentlySearch")) ? request.user.get("recentlySearch") : new Array();

            if (arrayRecentlySearch.length > 0) {
                for (var i = 0; i < arrayRecentlySearch.length; i++) {
                    recentlySearch.push(arrayRecentlySearch[i].idea);
                };
            }

            console.log(recentlySearch);

            var idea = new Parse.Query("Idea");
            idea.containedIn("objectId", recentlySearch);
            idea.find({useMasterKey: true})
            .then(
                function(ideasArray) {

                    console.log("ideasArray");
                    console.log(ideasArray);

                    for (var i = 0; i < ideasArray.length; i++) {
                        arrayTagIdeasSearch.push(ideasArray[i]);
                    };

                    arrayTagIdeasSearch = eliminateDuplicates(arrayTagIdeasSearch);

                    var allideas = new Parse.Query("Idea");
                    allideas.include("user");
                    allideas.equalTo("user", request.user);
                    allideas.find({useMasterKey: true})
                    .then(

                        function(userIdeas) {
                        console.log("userIdeas");
                        console.log(userIdeas);

                            for (var i = 0; i < userIdeas.length; i++) {
                                console.log(i);
                            
                                for (var j = 0; j < userIdeas[i].attributes.tags.length; j++) {

                                    userIdeasTags.push(userIdeas[i].attributes.tags[j]);
                                    console.log(userIdeas[i].attributes.tags[j]);
                                };
                            };
                            console.log("userIdeasTags");
                            console.log(userIdeasTags);

                            userIdeasTags = eliminateDuplicates(userIdeasTags);

                            var userFollowing = [];
                            var queryUser = new Parse.Query("Idea");
                            var queryTag = new Parse.Query("Idea");
                            var querySkills = new Parse.Query("Idea");
                            var queryLikes = new Parse.Query("Idea");
                            var querySearch = new Parse.Query("Idea");
                            var queryParticipate = new Parse.Query("Idea");
                            var querySuggested = new Parse.Query("Idea");



                            var userClass = Parse.Object.extend("User");

                            var userSkills = (request.user.get("skills")) ? request.user.get("skills") : new Array();
                            var tagFollow = (request.user.get("tagFollowing")) ? request.user.get("tagFollowing") : new Array();
                            var userFollow = (request.user.get("userFollowing")) ? request.user.get("userFollowing") : new Array();
                            var recommended = (request.user.get("recommended")) ? request.user.get("recommended") : new Array();

                            var arrayTagVisited = new Array();
                            var arrayUserSearch = new Array();
                            var countPopular = 0;

                            for (var i = 0; i < userFollow.length; i++) {
                                var user = new userClass();
                                user.id = userFollow[i];
                                userFollowing.push(user);
                            };


                            //query to get ideas with following users
                            queryUser.equalTo("esPrivado", false);
                            queryUser.include("user");
                            queryUser.containedIn("user", userFollowing);

                            //query to get ideas with following tags
                            queryTag.equalTo("esPrivado", false);
                            queryTag.include("user");
                            queryTag.containedIn("tags", tagFollow);

                            //query to get ideas with visited tags
                            querySkills.equalTo("esPrivado", false);
                            querySkills.include("user");
                            querySkills.containedIn("tags", userSkills);

                            //query to get ideas with visited tags
                            queryLikes.equalTo("esPrivado", false);
                            queryLikes.include("user");
                            queryLikes.containedIn("tags", tagOfLikes);

                            //query to get ideas with visited tags
                            querySearch.equalTo("esPrivado", false);
                            querySearch.include("user");
                            querySearch.containedIn("tags", arrayTagIdeasSearch);

                            //query to get ideas with visited tags
                            queryParticipate.equalTo("esPrivado", false);
                            queryParticipate.include("user");
                            queryParticipate.containedIn("tags", userIdeasTags);

                            //query to get ideas with visited tags
                            querySuggested.equalTo("esPrivado", false);
                            querySuggested.include("user");
                            querySuggested.containedIn("objectId", recommended);

                            //main query
                            var mainQuery = Parse.Query.or(queryTag, queryUser, querySkills, queryLikes, querySearch, queryParticipate, querySuggested);
                            mainQuery.include("user");
                            mainQuery.descending("createdAt");
                            mainQuery.limit(cant);
                            mainQuery.notEqualTo("user",request.user);
                            mainQuery.skip(cant * skip);
                            mainQuery.find({useMasterKey: true})
                            .then(
                                function(idea) {
                                    console.log("###################");
                                    console.log(idea);

                                    for (var i = idea.length - 1; i >= 0; i--) {
                                        ideas.push(idea[i]);
                                        ideasIndex.push(idea[i].id);
                                    };

                                    var views = [];

                                    for (var i = ideas.length - 1; i >= 0; i--) {

                                        var likeSum = ideas[i].get("likeCount");
                                        var commentSum = ideas[i].get("commentCount");
                                        var fileSum = ideas[i].get("fileCount");

                                        views.push({
                                            idea: ideas[i],
                                            comments: commentSum,
                                            likes: likeSum,
                                            files: fileSum
                                        });
                                    };

                                    response.success(views);
                                },
                                function(error) {
                                    response.error(error.message);
                                }
                            );

                        },
                        function(error) {
                            response.error(error.message);
                        }
                    );

                },
                function(error) {
                    response.error(error.message);
                }
            );

        },
        function(error) {
            response.error(error.message);
        }
    );

    function eliminateDuplicates(arr) {
        var z,
            len = arr.length,
            out = [],
            obj = {};

        for (z = 0; z < len; z++) {
            obj[arr[z]] = 0;
        }
        for (z in obj) {
            out.push(z);
        }
        return out;
    }



});

Parse.Cloud.define("setRecentlySearch", function(request, response) {
    Parse.Cloud.useMasterKey();

    var ideaId = request.params.ideaId; // id de la idea
    var date = request.params.date; // id de la idea

    var queryIdea = new Parse.Query("Idea");

    queryIdea.get(ideaId, { //encontrar la idea 
        success: function(ideaResult) {

            var newItem = [{
                idea: ideaResult.id,
                dateSearch: date
            }];

            if (!request.user.has("recentlySearch") || request.user.attributes.recentlySearch.length <= 0) { //no hay nada is (undefined) or []

                var User = Parse.Object.extend("User");
                var user = new User();

                user.set("objectId", request.user.id);
                user.set("recentlySearch", newItem);

                user.save();
                response.success("saved");

            }

            else {

                var arrayRecentlySearch = request.user.attributes.recentlySearch;

                if(changeDesc(arrayRecentlySearch,"idea",ideaId)===true){ // lo encontro y cambio 

                    var User = Parse.Object.extend("User");
                    var user = new User();

                    user.set("objectId", request.user.id);
                    user.set("recentlySearch", arrayRecentlySearch);

                    user.save();
                    response.success("true");

                }

                else{

                    arrayRecentlySearch.push({idea: ideaResult.id,dateSearch: date});

                    var User = Parse.Object.extend("User");
                    var user = new User();

                    user.set("objectId", request.user.id);
                    user.set("recentlySearch", arrayRecentlySearch);

                    user.save();
                    response.success("false");

                }

            }

        },
        error: function(error) {
            response.error(error.message);
        }
    });

    function changeDesc( array, value, idIdea ) {
        var search  = false;
            for (var i in array) {
                if (array[i].idea == idIdea) {
                    array[i].dateSearch = date;
                search = true;
            }
        }

        return search;
    }

});

Parse.Cloud.define("recommendationIdeas", function(request, response) {
  // Set up to modify user data
  Parse.Cloud.useMasterKey();
  var skillsA= new Array();
  var skillsB= new Array();

  var userA = request.params.userA; // id de la idea
  var userB = request.params.userB; // id de la idea

  var percentage = "";

  var uQuery = new Parse.Query("User");

  uQuery.get(userA,{


        success: function(cUser){
            skillsA = cUser.attributes.skills;
            console.log("---------------------skillsA--------------------------");
            console.log(skillsA);



            uQuery.get(userB,{
                success: function(userBe){
                    console.log("---------------------skillsB--------------------------");
                    console.log(skillsB);
                    skillsB = userBe.attributes.skills;
                    percentage = similar(skillsA,skillsB);
                    response.success(percentage);

                },
                error: function(error){
                    response.error(error.message);
                }
            });







        },
        error: function(error){
            response.error(error.message);
        }
    }).then(function() {

    var User = Parse.Object.extend("User");
    var user = new User();


    user.set("objectId", userA);
    user.set("recommendedIdeas", percentage);

    return user.save();




    // Set the job's success status
    status.success("Migration completed successfully.");
  }, function(error) {
    // Set the job's error status
    status.error("Uh oh, something went wrong.");
  });


        function similar(a,b) {
        var lengthA = a.length;
        var lengthB = b.length;
        var equivalency = 0;
        var minLength = (a.length > b.length) ? b.length : a.length;    
        var maxLength = (a.length < b.length) ? b.length : a.length;    
        for(var i = 0; i < minLength; i++) {
            if(a[i] == b[i]) {
                equivalency++;
            }
        }


        var weight = equivalency / maxLength;
        return (weight * 100) + "%";
    }

});




Parse.Cloud.job("recommendationJob", function(request, status) {
    Parse.Cloud.useMasterKey();
    var skillsA = new Array();
    var skillsB = new Array();
    var tagsA = new Array();
    var tagsB = new Array();
    var usersA = new Array();
    var usersB = new Array();
    var participateA = new Array();
    var participateB = new Array();
    var likesA = new Array();
    var likesB = new Array();
    var dislikesA = new Array();
    var dislikesB = new Array();
    var searchA = new Array();
    var searchB = new Array();
    var averageA = new Array();
    var averageB = new Array();
    var actualUsers = [];
    var otherUsers = [];
    var average = [];
    var allDifference = [];
    var similarity = 0;
    var array = [];
    var mainQuery = [];
    var savings = [];
    var User = Parse.Object.extend("User");

    var objectToSave = [];
    var ideasUsers = [];

    var roster = new Parse.Query("Roster");
    roster.include("idea");
    roster.include("user");
    roster.find({useMasterKey: true})
    .then(
        function(rosterArray) {
        var idea = new Parse.Query("Idea");
        idea.include("user");
        idea.find({useMasterKey: true})
        .then(
            function(likeArray) {
                var idea = new Parse.Query("Idea");
                idea.include("user");
                idea.find({useMasterKey: true})
                .then(
                    function(ideasArray) {
                        ideasUsers = ideasArray;
                        var user = new Parse.Query("User");
                        user.descending("createdAt");
                        user.find({useMasterKey: true})
                        .then(
                            function(allUsers) {

                                for (var i = 0; i < allUsers.length; i++) {

                                    objectToSave = [];
                                    searchA = [];
                                    likesA = [];
                                    dislikesA = [];
                                    participateA = [];

                                    var userA = allUsers[i];

                                    skillsA = (userA.attributes.skills) ? userA.attributes.skills : new Array();
                                    tagsA = (userA.attributes.tagFollowing) ? userA.attributes.tagFollowing : new Array();
                                    usersA = (userA.attributes.userFollowing) ? userA.attributes.userFollowing : new Array();

                                    var arraySearch = (userA.attributes.recentlySearch) ? userA.attributes.recentlySearch : new Array();

                                    if(arraySearch.length>0){
                                        for (var x = 0; x < arraySearch.length; x++) {
                                            searchA.push(arraySearch[x].idea);
                                        };
                                    }
                                    else{
                                        searchA=[];
                                    }


                                    var arrayLikesA = LINQ.Enumerable.From(likeArray).Where(function(x) {
                                        return userA.id == x.attributes.user.id && x.attributes.esPositivo == true;
                                    }).ToArray();

                                    if(arrayLikesA.length>0){
                                        for (var a = 0; a < arrayLikesA.length; a++) {
                                            likesA.push(arrayLikesA[a].attributes.idea.id);
                                        };
                                    }
                                    else{
                                        likesA=[];
                                    }

                                    var arraydislikesA = LINQ.Enumerable.From(likeArray).Where(function(x) {
                                        return userA.id == x.attributes.user.id && x.attributes.esPositivo == false;
                                    }).ToArray();

                                    if(arraydislikesA.length>0){
                                        for (var a = 0; a < arraydislikesA.length; a++) {
                                            dislikesA.push(arraydislikesA[a].attributes.idea.id);
                                        };
                                    }
                                    else{
                                        dislikesA=[];
                                    }


                                    var arrayParticipateA = LINQ.Enumerable.From(ideasUsers).Where(function(x) {
                                        return userA.id == x.attributes.user.id;
                                    }).ToArray();


                                    var arrayParticipateRosterA = LINQ.Enumerable.From(rosterArray).Where(function(x) {
                                        return userA.id == x.attributes.user.id;
                                    }).ToArray();

                           
                                    // console.log("===========arrayParticipateRosterA");
                                    // console.log(arrayParticipateA);
                                    // console.log(arrayParticipateRosterA);
                                    if(arrayParticipateA.length>0){
                                        for (var a = 0; a < arrayParticipateA.length; a++) {
                                            for (var b = 0; b < arrayParticipateA[a].attributes.tags.length; b++) {
                                                participateA.push(arrayParticipateA[a].attributes.tags[b]);
                                            };
                                        };

                                        participateA = eliminateDuplicates(participateA);
                                        // console.log(participateA);
                                    }
                                    if(arrayParticipateRosterA.length>0){
                                         for (var a = 0; a < arrayParticipateRosterA.length; a++) {
                                            for (var b = 0; b < arrayParticipateRosterA[a].attributes.idea.attributes.tags.length; b++) {
                                                participateA.push(arrayParticipateRosterA[a].attributes.idea.attributes.tags[b]);
                                            };
                                        };
                                        participateA = eliminateDuplicates(participateA);
                                        // console.log(participateA);
                                    }
                                    else{
                                        participateA=[];
                                    }


                                    for (var j = 0; j < allUsers.length; j++) {

                                        searchB = [];
                                        likesB = [];
                                        dislikesB = [];
                                        average = [];
                                        participateB = [];

                                        if (i !== j) {

                                            var userB = allUsers[j];
                                            skillsB = (userB.attributes.skills) ? userB.attributes.skills : new Array();
                                            tagsB = (userB.attributes.tagFollowing) ? userB.attributes.tagFollowing : new Array();
                                            usersB = (userB.attributes.userFollowing) ? userB.attributes.userFollowing : new Array();
                                            var arraySearch = (userB.attributes.recentlySearch) ? userB.attributes.recentlySearch : new Array();
                                            for (var y = 0; y < arraySearch.length; y++) {
                                                searchB.push(arraySearch[y].idea);
                                            };

                                            var arraylikesB = LINQ.Enumerable.From(likeArray).Where(function(x) {
                                                return userB.id == x.attributes.user.id && x.attributes.esPositivo == true;
                                            }).ToArray();

                                            if(arraylikesB.length>0){
                                                for (var a = 0; a < arraylikesB.length; a++) {
                                                    likesB.push(arraylikesB[a].attributes.idea.id);
                                                };
                                            }
                                            else{
                                                likesB=[];
                                            }


                                            var arraydislikesB = LINQ.Enumerable.From(likeArray).Where(function(x) {
                                                return userB.id == x.attributes.user.id && x.attributes.esPositivo == false;
                                            }).ToArray();

                                            if(arraydislikesB.length>0){
                                                for (var a = 0; a < arraydislikesB.length; a++) {
                                                    dislikesB.push(arraydislikesB[a].attributes.idea.id);
                                                };
                                            }
                                            else{
                                                dislikesB=[];
                                            }

                                            var arrayParticipateB = LINQ.Enumerable.From(ideasUsers).Where(function(x) {
                                                return userB.id == x.attributes.user.id;
                                            }).ToArray();

                                            var arrayParticipateRosterB = LINQ.Enumerable.From(rosterArray).Where(function(x) {
                                                return userB.id == x.attributes.user.id;
                                            }).ToArray();

                                            // console.log("===========arrayParticipateRosterB");
                                            // console.log(arrayParticipateB);
                                            // console.log(arrayParticipateRosterB);



                                            if(arrayParticipateB.length>0){
                                                for (var a = 0; a < arrayParticipateB.length; a++) {
                                                    for (var b = 0; b < arrayParticipateB[a].attributes.tags.length; b++) {
                                                        participateB.push(arrayParticipateB[a].attributes.tags[b]);
                                                    };
                                                };

                                                participateB = eliminateDuplicates(participateB);
                                                // console.log(participateB);
                                            }

                                            

                                            if(arrayParticipateRosterB.length>0){
                                                 for (var a = 0; a < arrayParticipateRosterB.length; a++) {
                                                    for (var b = 0; b < arrayParticipateRosterB[a].attributes.idea.attributes.tags.length; b++) {
                                                        participateB.push(arrayParticipateRosterB[a].attributes.idea.attributes.tags[b]);
                                                    };
                                                };
                                                participateB = eliminateDuplicates(participateB);
                                                // console.log(participateB);
                                            }



                                            else{
                                                participateB=[];
                                            }


                                            if (skillsA.length !== 0 && skillsB.length !== 0) {
                                                average.push(similar(skillsA, skillsB));
                                            }
                                            if (tagsA.length !== 0 && tagsB.length !== 0) {
                                                average.push(similar(tagsA, tagsB));
                                            }
                                            if (usersA.length !== 0 && usersB.length !== 0) {
                                                average.push(similar(usersA, usersB));
                                            }
                                            if (likesA.length !== 0 && likesB.length !== 0) {
                                                average.push(similar(likesA, likesB));
                                            }
                                            if (dislikesA.length !== 0 && dislikesB.length !== 0) {
                                                average.push(similar(dislikesA, dislikesB));
                                            }


                                            if (participateA.length !== 0 && participateB.length !== 0) {

                                                console.log("YEAHHHHHHH");
                                                average.push(similar(participateA, participateB));
                                            }

                                            


                                            var total = 0;

                                            if(average.length>0){

                                                for (var s = 0; s < average.length; s++) {
                                                    total = total + average[s];
                                                };
                                                similarity = total / average.length;

                                                if (similarity >= 50) {

                                                    var arrayIdeasSuggested = getIdeasSuggested(userA.id,userB.id);

                                                    for (var p = 0; p < arrayIdeasSuggested.length; p++) {
                                                        objectToSave.push(arrayIdeasSuggested[p]);
                                                    };

                                                   
                                                }
                                            }
                                        }

                                    }

                                    if(objectToSave.length>0){
                                        objectToSave = eliminateDuplicates(objectToSave);
                                        console.log("user: "+userA.id+"---------"+objectToSave);
                                        var newUser = new User();
                                        newUser.set("objectId", userA.id);
                                        newUser.set("recommended", objectToSave);
                                        savings.push(newUser);
                                        mainQuery = []
                                    }



                                }

                                console.log("savings----------------------");
                                console.log(savings);
                                Parse.Object.saveAll(savings, {
                                    success: function(objs) {
                                        status.success("yeah");
                                    },
                                    error: function(error) {
                                        status.error("Error "+error.message);
                                    }
                                });
                            },
                            function(error) {
                                status.error("Error "+error.message);
                            }
                        );
                    },
                    function(error) {
                       status.error("Error "+error.message);
                    }
                );
            },
            function(error) {
                    status.error("Error "+error.message);
                }
            );
},
        function(error) {
            status.error("Error "+error.message);
        }
    );

    function similar(a, b) {
        var difference = [];
        var lengthA = a.length;
        var lengthB = b.length;
        var equivalency = 0;

        for (var q = 0; q < a.length; q++) {
            if (b.indexOf(a[q])>-1) {
                equivalency++;
            }
        }
        if (equivalency === 0 || a.length === 0 || b.length === 0 ) {
            return 0;
        }
        else {
            var weight = equivalency / a.length;
            console.log(weight);
            return (weight * 100);
        }
    }



    function getIdeasSuggested(userA,userB) {

        var differentSkills = difference(skillsA, skillsB);
        var differentTags = difference(tagsA, tagsB);
        var differentTagsSkills = differentSkills.concat(differentTags);

        var differentUsers = difference(usersA, usersB);
        var differentLikes = difference(likesA, likesB);
        var differentSearch = difference(searchA, searchB);
        var differentParticipate = difference(participateA, participateB);
        console.log("user A :  "+userA+"  userb: "+userB);
        console.log("DIFFEREEENNCEEEEEEE:  "+differentParticipate);
        console.log(participateA);
        console.log(participateB);



        for (var i = 0; i < ideasUsers.length; i++) {
            for (var j = 0; j < differentTagsSkills.length; j++) {
                if (ideasUsers[i].attributes.tags.indexOf(differentTagsSkills[j]) > -1) {
                    mainQuery.push(ideasUsers[i].id);
                }
            }

            for (var j = 0; j < differentUsers.length; j++) {
                if (ideasUsers[i].attributes.user.id.indexOf(differentUsers[j]) > -1) {
                    mainQuery.push(ideasUsers[i].id);
                }
            }

            for (var j = 0; j < differentLikes.length; j++) {
                if (ideasUsers[i].id.indexOf(differentLikes[j]) > -1) {
                    mainQuery.push(ideasUsers[i].id);
                }
            }

            for (var j = 0; j < differentSearch.length; j++) {
                if (ideasUsers[i].id.indexOf(differentSearch[j]) > -1) {
                    mainQuery.push(ideasUsers[i].id);
                }
            }

            for (var j = 0; j < differentParticipate.length; j++) {
                if (ideasUsers[i].attributes.tags.indexOf(differentParticipate[j]) > -1) {
                    mainQuery.push(ideasUsers[i].id);
                }
            }

        };

        return eliminateDuplicates(mainQuery);

    }

    function difference(a, b) {

        array = [];

        for (var t = 0; t < b.length; t++) {
            if (a.indexOf(b[t]) < 0) {
                array.push(b[t]);
            }
        };
        return array;
    }

    function eliminateDuplicates(arr) {
        var z,
            len = arr.length,
            out = [],
            obj = {};

        for (z = 0; z < len; z++) {
            obj[arr[z]] = 0;
        }
        for (z in obj) {
            out.push(z);
        }
        return out;
    }

});

//*****************COSAS DE G QUE NO SIRVEN PARA NADA*******************************


Parse.Cloud.define("rememberTime", function(request, response) {
    Parse.Cloud.useMasterKey();
    function dropIt() {
        var SinHoras = Parse.Object.extend("SinHoras");
        var query = new Parse.Query(SinHoras);
        query.equalTo("avisado", false);
        query.descending("createdAt");
        query.limit(1000);
        query.find({useMasterKey: true})
        .then(
            function(results) {
                console.log("results");
                console.log(results);
                
                var users = [];
                for (var i = 0; i < results.length; i++) {
                    users.push(results[i].get("user"));
                };

                console.log("users");
                console.log(users);

                var pushQuery = new Parse.Query(Parse.Installation);
                pushQuery.containedIn("user", users);

                Parse.Push.send({
                  where: pushQuery,
                  data: {
                    alert: "Recuerda registrar tus horas",
                    title: "Aún hay tiempo"
                  }
                }, {
                  success: function() {
                      response.success("Done");                    
                  },
                  error: function(error) {
                      response.error("There was something wrong");
                    console.log(error);
                  }
                });

            },
            function(error) {
                response.error("There was something wrong");
                console.log(error);
            }
        );
    }
    dropIt();  
});

Parse.Cloud.beforeSave("SinHoras", function(request, response) {
 Parse.Cloud.useMasterKey();
 var sinHoras = request.object;
 if(sinHoras.get("username") != undefined){
     var query = new Parse.Query("_User");
    query.equalTo("username", sinHoras.get("username"));
    query.find({useMasterKey: true})
    .then(
        function(user) {
            sinHoras.set("user", 
                {"__type":"Pointer",
                "className":"_User",
                "objectId":user[0].id});
        },
        function(error) {
            console.log(error);
        }
    );
 }
 response.success();
});


Parse.Cloud.define("onAlertOpen", function(request, response) {
    Parse.Cloud.useMasterKey();
    var userId = request.params.userId; console.log(userId);
    var userQuery = new Parse.Query(Parse.User);
    userQuery.get(userId,{
        success: function(user) { console.log(user);
            var SinHoras = Parse.Object.extend("SinHoras");
            var query = new Parse.Query(SinHoras);
            query.equalTo("user", user);
            query.equalTo("avisado", false);
            query.descending("createdAt");
            query.first({useMasterKey: true})
            .then(
                function(sinHoras) {console.log(sinHoras);
                    sinHoras.set("avisado", true);
                    sinHoras.save({
                        success: function(saved) { console.log(saved);
                            response.success("Ok");
                        },
                        error: function(error) {
                            console.log(error);
                            response.error("Something went wrong");
                        }
                    });
                },
                function(error) {
                    console.log(error);
                    response.error("Something went wrong");
                }
            );
        },
        error: function(error) {
            console.log(error);
            response.error("Something went wrong");
        }
    });
    
});

//**********************************************************************************


Parse.Cloud.job('deleteOldRecenltySearch', function(request, status) {
    Parse.Cloud.useMasterKey();
    var saving = [];

    var userQuery = new Parse.Query("User");
    userQuery.descending("createdAt");
    userQuery.find({useMasterKey: true})
    .then(
        function(userArray) {

            var monthAgo = new Date();
            var month = monthAgo.getMonth();
            monthAgo.setMonth(monthAgo.getMonth() - 1);
            monthAgo.setHours(0, 0, 0);
            if (monthAgo.getMonth() == month) {
                monthAgo.setDate(0);
                monthAgo.setHours(0, 0, 0);
            }

            for (var i = 0; i < userArray.length; i++) {
                var search = (userArray[i].has("recentlySearch"))? userArray[i].get("recentlySearch") : new Array();

                var arrayNewRecentlySearch = new Array();

                for (var j = 0; j < search.length; j++) {

                    var searchDate = new Date(search[j].dateSearch);
                    console.log("###########################");
                    console.log(searchDate);
                    console.log(monthAgo);


                    if (searchDate >= monthAgo) {
                        arrayNewRecentlySearch.push(search[j]);
                    }

                };


                    var User = Parse.Object.extend("User");
                    var updateUser = new User();
                    updateUser.set("objectId", userArray[i].id);
                    updateUser.set("recentlySearch", arrayNewRecentlySearch);
                    saving.push(updateUser);
                


            };
            Parse.Object.saveAll(saving, {
                success: function(result) {
                    status.success("yeah");
                },
                error: function(error) {
                    status.error("Error " + error.message);
                }
            });

        },
        function(error) {
            status.error("Error "+error.message);
        }
    );


});


Parse.Cloud.define("getCartHistory", function(request, response) {

    Parse.Cloud.useMasterKey();
    var array = [];

    var orders = new Parse.Query("orders");
    orders.equalTo("status","paid");
    orders.equalTo("user",request.user);
    orders.find({useMasterKey: true})
    .then(
        function(ordersArray){

            console.log(ordersArray);

            var orderDetails = new Parse.Query("orderDetails");
            orderDetails.include("order");
            orderDetails.include("item");
            orderDetails.find({useMasterKey: true})
            .then(
                function(ordersDetailsArray){

                    console.log(ordersDetailsArray);

                    for (var i = 0; i < ordersArray.length; i++) {
                    
                        var itemsOfOrder = LINQ.Enumerable.From(ordersDetailsArray).Where(function(x) {
                            return ordersArray[i].id == x.attributes.order.id;
                        }).ToArray();
                        console.log(itemsOfOrder);

                        array.push({"order":ordersArray[i].id,
                                    "date":ordersArray[i].updatedAt,
                                    "items":itemsOfOrder       
                                    });

                        console.log(array);

                    };

                    response.success(array);
                },
                function(error){
                    response.error(error.message);
                }
            );

        },
        function(error){
            response.error(error.message);
        }
    );
});


//Type: Get
//Function that indicates you if a user is Directive 
//Last modified: 27/10/2015
//Last modified by: Samuel Martínez
//Created: 27/10/2015
//Created by: Samuel Martínez
//(Parameters)
//  -email: user’s email
//return
//  -boolean

Parse.Cloud.define("isDirective",function(request,response){
   Parse.Cloud.useMasterKey();
   var email = request.params.email;
   var dirQuery = new Parse.Query("Directivo");
   dirQuery.equalTo("email",email);
   dirQuery.find({useMasterKey: true})
    .then(
        function(directivos){
            if(directivos.length > 0)
                response.success(true);
            else
                response.success(false);
        },
        function(error){
            response.error(error.message);
        }
   ); 
});

//Type: Get
//Function that indicates you if a user is in the Comittee 
//Last modified: 20/11/2015
//Last modified by: Samuel Martínez
//Created: 20/11/2015
//Created by: Samuel Martínez
//(Parameters)
//  -email: user’s email
//return
//  -boolean

Parse.Cloud.define("isComittee",function(request,response){
   Parse.Cloud.useMasterKey();
   var email = request.params.email;
   var dirQuery = new Parse.Query("Comittee");
   dirQuery.equalTo("email",email);
   dirQuery.find({useMasterKey: true})
    .then(
    function(comite){
        if(comite.length > 0)
            response.success(true);
        else
            response.success(false);
    },
    function(error){
        response.error(error.message);
    }
   ); 
});

//Type: Get
//Function that gives you the estimations made by the current user to a proposal. 
//Last modified: 30/10/2015
//Last modified by: Samuel Martínez
//Created: 30/10/2015
//Created by: Samuel Martínez
//(Parameters)
//  -ideaID: publication’s objectId
//return
//  -Estimacion object

Parse.Cloud.define("getUserEstimations",function(request,response){
   Parse.Cloud.useMasterKey();
   var user = request.user;
   var ideaID = request.params.ideaID;
   var ideaQuery = new Parse.Query("Idea");
   ideaQuery.get(ideaID,{
    success: function(ideaResult){
        var estQuery = new Parse.Query("Estimacion");
        estQuery.include("user");
        estQuery.include("idea");
        estQuery.equalTo("user",user);
        estQuery.equalTo("idea",ideaResult);
        estQuery.find({useMasterKey: true})
        .then(
            function(estRes){
                response.success(estRes);
            },
            function(error){
                response.error(error.message);
            }
        );
    },
    error: function(error){
        response.error(error.message);
    }
   }); 
});

//Type: Get
//Function that lets brings you all estimations from a proposal. 
//Last modified: 10/11/2015
//Last modified by: Samuel Martínez
//Created: 10/11/2015
//Created by: Samuel Martínez
//(Parameters)
//  -ideaID: publication’s objectId
//return
//  -hasmap of ParseObjects
//    --owner {time, money, people}
//    --community {count,time, money, people}
//    --roster {count,time, money, people}

Parse.Cloud.define("AllEstimations",function(request,response){
    Parse.Cloud.useMasterKey();
    var ideaObj = Parse.Object.extend("Idea");
    var allEstimationsObject = {
        owner: {time: 0, money: 0, people: 0},
        community:{count:0,time: 0, money: 0, people: 0},
        roster: {count:0,time: 0, money: 0, people: 0}
    }
    var queryEst = new Parse.Query("Estimacion");
    queryEst.include("idea");
    queryEst.include("user");
    queryEst.equalTo("idea",{"__type":"Pointer",
                "className":"Idea",
                "objectId":request.params.ideaID});
    queryEst.find({useMasterKey: true})
    .then(
        function(resultEst){
            if(resultEst.length > 0)
            {
                var queryRoster = new Parse.Query("Roster");
                queryRoster.equalTo("idea",{"__type":"Pointer",
                            "className":"Idea",
                            "objectId":request.params.ideaID});
                queryRoster.find({useMasterKey: true})
                .then(
                    function(rosterResult){
                        var rosterArray = [];
                        for (var i = rosterResult.length - 1; i >= 0; i--) {
                            rosterArray.push(rosterResult[i].get("user").id);
                        };
                        var counter = 0;
                        var counterRoster = 0;
                        var idea = resultEst[0].get("idea");
                        for (var i = 0; i < resultEst.length; i++) {
                            var currentObject = resultEst[i];
                            if(currentObject.get("user").id == idea.get("user").id)
                                allEstimationsObject.owner = currentObject.get("estimaciones");
                            else
                            {
                                if(rosterArray.lastIndexOf(currentObject.get("user").id) > -1)
                                {
                                    allEstimationsObject.roster.time+= Number(currentObject.get("estimaciones").time);
                                    allEstimationsObject.roster.money+= Number(currentObject.get("estimaciones").money);
                                    allEstimationsObject.roster.people+= Number(currentObject.get("estimaciones").people);
                                    counterRoster++;
                                }
                                else
                                {
                                    allEstimationsObject.community.time+= Number(currentObject.get("estimaciones").time);
                                    allEstimationsObject.community.money+= Number(currentObject.get("estimaciones").money);
                                    allEstimationsObject.community.people+= Number(currentObject.get("estimaciones").people);
                                    counter++;
                                }
                            }
                        };
                        allEstimationsObject.community.time = (counter> 0)? allEstimationsObject.community.time/counter : 0;
                        allEstimationsObject.community.money = (counter> 0)? allEstimationsObject.community.money/counter : 0;
                        allEstimationsObject.community.people = (counter> 0)? allEstimationsObject.community.people/counter : 0;
                        
                        allEstimationsObject.roster.time = (counterRoster> 0)? allEstimationsObject.roster.time/counterRoster : 0;
                        allEstimationsObject.roster.money = (counterRoster> 0)? allEstimationsObject.roster.money/counterRoster : 0;
                        allEstimationsObject.roster.people = (counterRoster> 0)? allEstimationsObject.roster.people/counterRoster : 0;
                        
                        allEstimationsObject.community.count = counter;
                        allEstimationsObject.roster.count = counterRoster;
                        response.success(allEstimationsObject);     
                    },
                    function(error){
                        response.error(error.message);
                    }
                );
            }
            else
                response.success(allEstimationsObject);
        },
        function(error){
            response.error(error.message);
        }
    );
});

//Type: Delete
//Function that lets you delete a comment class. 
//Last modified: 11/11/2015
//Last modified by: Samuel Martínez
//Created: 11/11/2015
//Created by: Samuel Martínez
//(Parameters)
//  -commentID: comment’s objectId
//return
//  -boolean true if successful

Parse.Cloud.define("eliminarComentario",function(request,response){
    Parse.Cloud.useMasterKey();
    var commentID = request.params.commentID;
    var commentQuery = new Parse.Query("Comentario");
    commentQuery.get(commentID,{
        success: function(commentResult){
            commentResult.destroy({
                success: function(myObject) {
                    response.success(true);
                },
                error: function(error) {
                    response.error(error.message);
                }
            });
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

//Type: Delete
//Function that lets you delete a challenge pointer from an Idea class. 
//Last modified: 13/11/2015
//Last modified by: Samuel Martínez
//Created: 13/11/2015
//Created by: Samuel Martínez
//(Parameters)
//  -ideaID: publication’s objectId
//return
//  -an Idea object

Parse.Cloud.define("deleteChallengeRelation",function(request,response){
    Parse.Cloud.useMasterKey();
    var ideaID = request.params.ideaID;
    var ideaQuery = new Parse.Query("Idea");
    ideaQuery.get(ideaID,{
        success: function(ideaResult){
            var challengeID = ideaResult.get("challenge").id;
            ideaResult.set("challenge",null);

            ideaResult.save(null,{
                success: function(myObject) {
                    response.success(challengeID);
                },
                error: function(error) {
                    response.error(error.message);
                }
            });
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

//Type: Get
//Function that gets you all the proposals. 
//Last modified: 16/11/2015
//Last modified by: Samuel Martínez
//Created: 16/11/2015
//Created by: Samuel Martínez
//(Parameters)
//  
//return
//  -a list of Ideas objects

Parse.Cloud.define("getProposals",function(request,response){
    Parse.Cloud.useMasterKey();
    appQuery = new Parse.Query("Approvals");
    appQuery.include("idea");
    appQuery.equalTo("user",request.user);
    appQuery.find({useMasterKey: true})
    .then(
        function(appResults){
            var ideaQuery = new Parse.Query("Idea")
            var linqIdeas = LINQ.Enumerable.From(appResults).Select("$.attributes.idea.id").ToArray();
            ideaQuery.equalTo("Fase",2);
            ideaQuery.limit(10);
            ideaQuery.notContainedIn("objectId",linqIdeas);
            ideaQuery.descending("likeCount");
            ideaQuery.include("user");
            ideaQuery.find({useMasterKey: true})
            .then(
                function(ideaResults){
                    var unapprovedIdeas = LINQ.Enumerable.From(appResults)
                                .Where(function (x) { 
                                    return !x.attributes.isApproved
                                }).Select("$.attributes.idea").ToArray();
                    var approvedIdeas = LINQ.Enumerable.From(appResults)
                                .Where(function (x) { 
                                    return x.attributes.isApproved
                                }).Select("$.attributes.idea").ToArray();
                    var allProposals = {pending: ideaResults, approved: approvedIdeas, unapproved: unapprovedIdeas};
                    response.success(allProposals);    
                },
                function(error){
                    response.error(error.message);
                }
            );
        },
        function(error){
            response.error(error.message);
        }
    );
});

//Type: Get
//Function that gets you all the proposals for the comittee. 
//Last modified: 20/11/2015
//Last modified by: Samuel Martínez
//Created: 20/11/2015
//Created by: Samuel Martínez
//(Parameters)
//  
//return
//  -a list of Ideas objects

Parse.Cloud.define("getProposalsComittee",function(request,response){
    Parse.Cloud.useMasterKey();
    var ideaQuery = new Parse.Query("Idea");
    ideaQuery.equalTo("Fase",3);
    ideaQuery.find({useMasterKey: true})
    .then(
        function(ideaResults){
            var appUser = new Parse.Query("ComitteeApprovals");
            appUser.equalTo("user",request.user);
            appUser.include("idea");
            appUser.find({useMasterKey: true})
            .then(
                function(appUserResults){
                    var linqIdeas = LINQ.Enumerable.From(appUserResults).Select("$.attributes.idea").ToArray();
                    var ideasParams = ideaResults.concat(linqIdeas);
                    var appQuery = new Parse.Query("Approvals");
                    appQuery.equalTo("isApproved",true);
                    appQuery.limit(10);
                    appQuery.descending("createdAt");
                    appQuery.include("idea");
                    appQuery.include("user");
                    appQuery.notContainedIn("idea",ideasParams);
                    appQuery.find({useMasterKey: true})
                    .then(
                        function(ideaAppResults){
                            var approvedAlready = LINQ.Enumerable.From(appUserResults)
                                .Where(function (x) { 
                                    return x.attributes.isApproved
                                }).Select("$.attributes.idea").ToArray();
                            var arrayResult = new Array();
                            var arrayResultIndex = new Array();
                            for (var i = 0; i < ideaAppResults.length; i++) {
                                var object = ideaAppResults[i];
                                var objectIdea = object.get("idea");
                                if(arrayResultIndex.length == 0 || arrayResultIndex.indexOf(objectIdea.id) <= -1)
                                {
                                    objectIdea.attributes["whoApproved"] = new Array();
                                    objectIdea.attributes["whoApproved"].push(object.get("user").get("name"));
                                    arrayResultIndex.push(objectIdea.id);
                                    arrayResult.push(objectIdea);
                                }
                                else
                                {
                                    arrayResult[arrayResultIndex.indexOf(objectIdea.id)].attributes["whoApproved"].push(object.get("user").get("name"));
                                }
                            };
                            var allProposals = {pending: arrayResult, approved: approvedAlready, production: ideaResults};
                            response.success(allProposals);
                        },
                        function(error){
                            response.error(error.message);
                        }
                    );
                },
                function(error){
                    response.error(error.message);
                }
            );
        },
        function(error){
            response.error(error.message);
        }
    );
});

//Type: Get
//Function that tells if a user approved an idea
//Last modified: 16/11/2015
//Last modified by: Samuel Martínez
//Created: 16/11/2015
//Created by: Samuel Martínez
//(Parameters)
//  -ideaID: ideas objectId
//return
//  -boolean

Parse.Cloud.define("isApproved",function(request,response){
    Parse.Cloud.useMasterKey();
    var appQuery = new Parse.Query("Approvals");
    appQuery.equalTo("user",request.user);
    appQuery.equalTo("idea",{"__type":"Pointer",
                            "className":"Idea",
                            "objectId":request.params.ideaID});
    appQuery.find({useMasterKey: true})
    .then(
        function(result){
            if(result.length > 0)
            {
                if(result[0].get("isApproved"))
                    response.success("Approved");
                else
                    response.success("Unapproved");
            }
            else
                response.success("Pending");
        },
        function(error){
            response.error(error.message);
        }
    );
});

//Type: Business Logic
//Function that approves an idea. 
//Last modified: 16/11/2015
//Last modified by: Samuel Martínez
//Created: 16/11/2015
//Created by: Samuel Martínez
//(Parameters)
//  -ideaID: ideas objectId
//return
//  -status of approval

Parse.Cloud.define("setApproval",function(request,response){
    Parse.Cloud.useMasterKey();
    var appQuery = new Parse.Query("Approvals");
    appQuery.equalTo("user",request.user);
    appQuery.equalTo("idea",{"__type":"Pointer",
                            "className":"Idea",
                            "objectId":request.params.ideaID});
    appQuery.find({useMasterKey: true})
    .then(
        function(appResults){
            if(appResults.length > 0)
            {
                var objectApp = appResults[0];
                var dataBool = objectApp.get("isApproved");
                objectApp.set("isApproved",!dataBool);
                objectApp.save(null,{
                    success: function(myObject) {
                        if(dataBool)
                            response.success(myObject);
                        else
                            response.success(myObject);
                    },
                    error: function(error) {
                        response.error(error.message);
                    }
                });
            }
            else
            {
                var object = Parse.Object.extend("Approvals");
                var newApp = new object();
                newApp.set("idea",{"__type":"Pointer",
                            "className":"Idea",
                            "objectId":request.params.ideaID});
                newApp.set("user",request.user);
                newApp.set("isApproved",request.params.isApproved);
                newApp.save(null,{
                    success: function(saved){
                        response.success(saved);
                    },
                    error: function(error){
                        response.error(error.message);
                    }
                });
            }
        },
        function(error){
            response.error(error.message);
        }
    );
});

//Type: Business Logic
//Function that sets user points. 
//Last modified: 16/11/2015
//Last modified by: Samuel Martínez
//Created: 16/11/2015
//Created by: Samuel Martínez
//(Parameters)
//  -action: string of action made
//return
//  -total points

Parse.Cloud.define("setPoints",function(request,response){
    Parse.Cloud.useMasterKey();
    var userID = request.params.userID;
    var userQuery = new Parse.Query("User");
    userQuery.get(userID,{
        success: function(user){
            var action = request.params.action;
            var totalPoints = 0;
            var points = 0;
            if(user.has("puzzles"))
                totalPoints = Number(user.get("puzzles"));
            if(user.has("points"))
                points = Number(user.get("points"));
            else
                points = totalPoints;

            switch(action){
                case "comment":  totalPoints+= 2; points+= 2; break;
                case "like":  totalPoints+= 2.5; totalPoints+= 2.5; break;
                case "publish":  totalPoints+= 5; totalPoints+= 5; break;
                case "apk":  totalPoints+= 7.5; totalPoints+= 7.5; break;
                case "challenge":  totalPoints+= 10; totalPoints+= 10; break;
                case "phase":  totalPoints+= 25; totalPoints+= 25; break;
                case "approval":  totalPoints+= 50; totalPoints+= 50; break;
            }
            user.set("puzzles",totalPoints);
            user.set("points",totalPoints);
            user.save(null,{
                 success: function(saved){
                     response.success(saved);
                 },
                 error: function(error){
                     response.error(error.message);
                 } 
             });
        },
        error: function(error){
            response.error(error.message);
        }
    });
     
});

//Type: Insert
//Function that hides a proposal from the directive
//Last modified: 19/11/2015
//Last modified by: Samuel Martínez
//Created: 19/11/2015
//Created by: Samuel Martínez
//(Parameters)
//  -ideaID: ideas objectId
//  -userID: users objectId
//return
//  -boolean

Parse.Cloud.define("hideProposal",function(request,response){
    Parse.Cloud.useMasterKey();
    var ideaID = request.params.ideaID;
    var userID = request.params.userID;
    var ideaQuery = new Parse.Query("Idea");
    ideaQuery.get(ideaID,{
        success: function(result){
            if(result.has("hideToUser"))
            {
                var arrayUse = result.get("hideToUser");
                if(arrayUse.indexOf(userID) <= -1)
                    arrayUse.push(userID);
                result.set("hideToUser",arrayUse);
                result.save(null,{
                    success: function(saved){
                        response.success(true);
                    },
                    error: function(error){
                        response.error(error.message);
                    }
                });
            }
            else
            {
                var arrayUse = new Array();
                arrayUse.push(userID);
                result.set("hideToUser",arrayUse);
                result.save(null,{
                    success: function(saved){
                        response.success(true);
                    },
                    error: function(error){
                        response.error(error.message);
                    }
                });
            }
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

//Type: Get
//Function that tells if a user,from the comittee, approved an idea
//Last modified: 20/11/2015
//Last modified by: Samuel Martínez
//Created: 20/11/2015
//Created by: Samuel Martínez
//(Parameters)
//  -ideaID: ideas objectId
//return
//  -boolean

Parse.Cloud.define("isComitteeApproved",function(request,response){
    Parse.Cloud.useMasterKey();
    var appQuery = new Parse.Query("ComitteeApprovals");
    appQuery.equalTo("user",request.user);
    appQuery.equalTo("idea",{"__type":"Pointer",
                            "className":"Idea",
                            "objectId":request.params.ideaID});
    appQuery.find({useMasterKey: true})
    .then(
        function(result){
            if(result.length > 0)
            {
                if(result[0].get("isApproved"))
                    response.success("Approved");
                else
                    response.success("Unapproved");
            }
            else
                response.success("Pending");
        },
        function(error){
            response.error(error.message);
        }
    );
});

//Type: Business Logic
//Function that approves an idea by the comittee. 
//Last modified: 20/11/2015
//Last modified by: Samuel Martínez
//Created: 20/11/2015
//Created by: Samuel Martínez
//(Parameters)
//  -ideaID: ideas objectId
//return
//  -status of approval

Parse.Cloud.define("setComitteeApproval",function(request,response){
    Parse.Cloud.useMasterKey();
    var appQuery = new Parse.Query("ComitteeApprovals");
    appQuery.equalTo("user",request.user);
    appQuery.equalTo("idea",{"__type":"Pointer",
                            "className":"Idea",
                            "objectId":request.params.ideaID});
    appQuery.find({useMasterKey: true})
    .then(
        function(appResults){
            if(appResults.length > 0)
            {
                var objectApp = appResults[0];
                var dataBool = objectApp.get("isApproved");
                objectApp.set("isApproved",!dataBool);
                objectApp.save(null,{
                    success: function(myObject) {
                        if(dataBool)
                            response.success("Unapproved");
                        else
                            response.success("Approved");
                    },
                    error: function(error) {
                        response.error(error.message);
                    }
                });
            }
            else
            {
                var object = Parse.Object.extend("ComitteeApprovals");
                var newApp = new object();
                newApp.set("idea",{"__type":"Pointer",
                            "className":"Idea",
                            "objectId":request.params.ideaID});
                newApp.set("user",request.user);
                newApp.set("isApproved",request.params.isApproved);
                newApp.save(null,{
                    success: function(saved){
                        var text = (request.params.isApproved)? "Approved" : "Unapproved";
                        response.success(text);
                    },
                    error: function(error){
                        response.error(error.message);
                    }
                });
            }
        },
        function(error){
            response.error(error.message);
        }
    );
});

Parse.Cloud.define("setAppInstall",function(request,response){
    Parse.Cloud.useMasterKey();
    var object = Parse.Object.extend("AppInstall");
    var objectAppIn = new object();
    objectAppIn.set("ideaID","1");
    objectAppIn.save(null,{
        success: function(myObject) { 
            response.success(true);
        },
        error: function(error) {
            response.error(error.message);
        }
    });
});

Parse.Cloud.beforeSave("AppInstall", function (request, response) {
    //check if user being saved has profile picture.
    var ideaID = request.object.get("ideaID");
    var userID = request.object.get("userID");
    var ideaQuery = new Parse.Query("Idea");
    var userQuery = new Parse.Query("User");

    ideaQuery.get(ideaID,{
        success: function(ideaResult){
            userQuery.get(userID,{
                success: function(userResult){
                    request.object.set("user",userResult);
                    request.object.set("idea",ideaResult);
                    response.success();
                },
                error: function(error){
                    console.error(error.message);
                    response.success();
                }
            });
        },
        error: function(error){
            console.error(error.message);
            response.success();
        }
    });
});

Parse.Cloud.define("getLanguage",function(request,response){
    Parse.Cloud.httpRequest({
        method: 'GET',
        //url: "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDkqRzKqUWgxkmI9rMI3cvo9EvYvM6oVXA&latlng=19.407766,-99.12178279999999&sensor=false&language=en",
        url: "https://www.googleapis.com/language/translate/v2/detect?q="+request.params.descr+"&key=AIzaSyDfw8a7QaN_HXqHITuz8CkM-91Ocj_J10c",
        //url: "https://www.googleapis.com/language/translate/v2?key=AIzaSyDkqRzKqUWgxkmI9rMI3cvo9EvYvM6oVXA&target=de&q=Hello%20world",
        success: function(httpResponse) {     
            response.success(httpResponse);
        },
        error: function(httpResponse) {
          response.error('Request failed with response code ' + httpResponse.status);
        }
    });
});

Parse.Cloud.define("saveError",function(request,response){

    Parse.Cloud.useMasterKey(); //Made after the migration
    
    var errorObj = Parse.Object.extend("Error");
    var error = new errorObj();
    error.set("text",request.params.text);
    error.save(null,{
        success: function(saved){
            response.success(true);
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.define("loginQR",function(request,response){
    var pusher = new Pusher({
      appId: '148416',
      key: '7a5e5c7f6307240cf192',
      secret: 'a93459ef65de29ce6103',
      encrypted: true
    });
    var logQR = pusher.trigger( 'produ', request.params.qrID, { message: request.params.userID } );

    var errorObj = Parse.Object.extend("Error");
    var error = new errorObj();
    error.set("text","message sent");
    error.save(null,{
        success: function(saved){
            response.success(true);
        },
        error: function(error){
            response.error(false);
        }
    });
});

Parse.Cloud.define("getChildrenNodes",function(request,response){
    Parse.Cloud.useMasterKey();
    Parse.Cloud.run('getIdea',{ideaID: request.params.ideaID},{
        success: function(ideaResult){
            var queryIdeas = new Parse.Query("Idea");
            queryIdeas.equalTo("ideaParent",ideaResult);
            queryIdeas.find({useMasterKey: true})
            .then(
                function(results){
                    var resp = {idea: ideaResult, children: results, parent: ideaResult.get("ideaParent")};
                    response.success(resp);
                },
                function(error){
                    response.error(error.message);
                }
            );
        },
        error: function(error){
            response.error(error.message);
        }
    });
});

Parse.Cloud.define("getCarousel",function(request,response){
    Parse.Cloud.useMasterKey();
    var idea = {"__type":"Pointer",
                "className":"Idea",
                "objectId":request.params.ideaID};
    var archivoQuery  = new Parse.Query("Archivo");
    archivoQuery.equalTo("idea",idea);
    archivoQuery.find({useMasterKey: true})
    .then(
        function(allFiles){
            if(allFiles.length > 0)
            {
                var listCarousel = new Array();
                var re =
                /(\.jpg|\.jpeg|\.bmp|\.gif|\.png|\.wav|\.wma|\.asf|\.avi|\.mov|\.mp4|\.wmv)$/i;
                var reVideo = 
                /(\.mp4)$/i;
                for (var i = 0; i < allFiles.length; i++) {
                    var file = allFiles[i].get("nombre");
                    if (re.exec(file)) {
                        if(reVideo.exec(file))
                        {
                            listCarousel.push({url: allFiles[i].get("file").url(),type:"video"});
                        }
                        else
                        {
                            listCarousel.push({url: allFiles[i].get("file").url(),type:"image"});
                        }
                    }
                };
                response.success(listCarousel);
            }
            else
                response.success(new Array());
        },
        function(error){
            response.error(error.message);
        }
    );
});

//Agregado Por Gil para arreglar el problema del nuevo Crypto y Token
//Toma el valor que regresa Crypto y lo asigna a la contraseña del usuario
Parse.Cloud.define("updateUser", function (request, response) {
    
    Parse.Cloud.useMasterKey();

    var username = request.params.username;
    var crypto = request.params.crypto;
    var token = request.params.token;

    var User = Parse.Object.extend("User");
    var query = new Parse.Query(User);
    query.equalTo("username", username);
    query.find({useMasterKey: true})
    .then(
        function(results) {
            var user = results[0];
            user.set("password", crypto);
            user.set("token", token);
            user.save(null,{
              success: function(ruser){                
                response.success(results);
              },
              error: function(error){
                response.error(error.message);
              }
            });            
        },
        function(error) {
            response.error(error.message);
        }
    );
});

Parse.Cloud.define("updateProfilePictures", function (request, response) {
    
    var User = Parse.Object.extend("User");
    var query = new Parse.Query(User);    
    //query.lessThan("updatedAt", new Date("2017-01-30T22:21:17.510Z"));        
    query.equalTo("username", "joseg.hernandez");    
    query.find({useMasterKey: true})
    .then(
        function(results) {            
            if(results.length == 0)
                response.success("Empty results");
            modifyUsers(results, response, 0);
        },
        function(error) {
            response.error(error.message);
        }
    );

    function modifyUsers(users, response, count) {
        if(count == users.length)
            response.success("Ok");
        else{
            var auxUser = users[count];
            console.log(auxUser);
            Parse.Config.get().then(function(config) {
                auxUser.set(
                    "image",
                    {
                        "__type": "File",
                        "name": "default-user.png",
                        "url": config.get("PhotoURL")
                    }
                );
                auxUser.save(null,{
                  success: function(ruser){                
                    modifyUsers(users, response, count+1);
                  },
                  error: function(error){
                    response.error(error.message);
                  }
                });
            });            
        }
    }


});