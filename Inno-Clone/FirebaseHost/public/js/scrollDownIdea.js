$(document).ready(function(){
	

	var menu = $('#menuIdea');
    var origOffsetY = menu.offset().top -73;

    function scroll() {
        if ($(window).scrollTop() >= origOffsetY) {
            $('#menuIdea').addClass('sticky');
            $('#my-tab-content').addClass('menu-padding');
            $(".demo").addClass("stickyLike");
            $(".demo-5").hide();
        } else {
            $('#menuIdea').removeClass('sticky');
            $('.demo').removeClass('stickyLike');
            $(".demo-5").show();
            $('#my-tab-content').removeClass('menu-padding');
        }
    }

    document.onscroll = scroll;
});
