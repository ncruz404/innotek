$(document).ready(function(){
     console.log("HERE ");

     var offset = 0;
     var ideas = [];
     var datos = [];
     var datosComment = [];
     var key = GetUrlValue("k");
    plot();

    function GetUrlValue(VarSearch){
        var SearchString = window.location.search.substring(1);
        if(SearchString.length > 0)
        {   
            var VariableArray = SearchString.split('&');
            for(var i = 0; i < VariableArray.length; i++){
                var KeyValuePair = VariableArray[i].split('=');
                if(KeyValuePair[0] == VarSearch){
                    return KeyValuePair[1];
                }
            }   
        }
        else
            return null;
    }

    function plot() {

        var cUser;
        if(key != null)
            cUser = key;
        else
            cUser = Parse.User.current().id;
        Parse.Cloud.run("ideasUser",{userID : cUser, reqID: Parse.User.current().id},{
            success: function(response){
                ideas = response;
                $("#toI").html(ideas.length);    
                for (var i = 0; i < ideas.length; i++) {
                    if(ideas[i].likesTotal > 0)
                    {
                        var CalcNeg = (ideas[i].likesTotal - ideas[i].likes) / 2;
                        var CalcPos = ideas[i].likesTotal - CalcNeg;
                        CalcNeg*= -1;
                        datos.push({y: ideas[i].idea.get("titulo"),a: CalcPos, b: CalcNeg});
                    }
                    var bHTML = '<tr><td><a href="viewIdea.html?k='+ideas[i].idea.id
                    +'">'+i
                    +'</a></td><td>'+ideas[i].idea.get("titulo")
                    +'</td><td>'+ideas[i].likes
                    +'</td><td>'+ideas[i].comments
                    +'</td><tr>';
                    $("#ideasTable").append(bHTML);
                    var opHTML = '<option value="'+i+'">'+ideas[i].idea.get("titulo")+'</option>';
                    $("#selectIdeaPlot").append(opHTML); 
                }   

                var tempMorrisData = [];
                var countMorris = 0;
                for(var i = 0; i < datos.length; i++)
                {
                    tempMorrisData.push(datos[i]);
                    countMorris++;
                    if(countMorris >= 5)
                        i = datos.length;
                }
                Morris.Bar({
                    element: 'morris-bar-chart',

                    data: tempMorrisData,
                    xkey: 'y',
                    ykeys: ['a', 'b'],
                    labels: ['Likes', 'Dislikes'],
                    hideHover: 'auto',
                    resize: true
                });

                getDataComments(ideas,0,ideas.length,[]);

            },
            error: function(error){
                console.log("Error<"+ error.message);
            }
        });
                                
           
        
    }
    var numMax = 0;
    function getDataComments(ideasList,act,max)
    {
        if(act < max)
        {
            var Comentario = Parse.Object.extend("Comentario");
            var queryComentario = new Parse.Query(Comentario);
            queryComentario.include("idea");
            queryComentario.equalTo("idea", ideasList[act].idea);
            var diaString = moment(ideasList[act].idea.createdAt.toString()).fromNow();
            var counter = 0;
            if(diaString == "a day ago")
                counter = 1;
            else
            if(diaString.indexOf("day") > -1)
                counter = diaString.split(" ")[0];
            var datos = [];
            for(var i=0; i <= counter; i++)
            {
                datos.push([i,0]);
            }
            queryComentario.find({
                success: function(Comments)
                {
                    var tempMax = 0;
                    for(var i = 0; i < Comments.length; i++)
                    {
                        var comObj = Comments[i];
                        var dayString = moment(comObj.createdAt.toString()).fromNow();
                        var dayAct = 0;
                        if(dayString == "a day ago")
                            dayAct = 1;
                        else
                        if(dayString.indexOf("day") > -1)
                            dayAct = dayString.split(" ")[0];
                        datos[dayAct][1]++;
                        tempMax++;
                    }

                    if(tempMax > numMax)
                        numMax = tempMax;
                    datosComment.push({label: ideasList[act].idea.get("titulo"), data: datos});
                    act++;

                    getDataComments(ideasList,act,max);
                },
                error: function(error)
                {
                    console.log(error.message+" ...");
                }
            });

        }
        else
        {
            $("#plotLoad").html("Comentarios vs Tiempo");
            movePlot(0);
        }

    }

    function movePlot(id)
    {

        var options = {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                grid: {
                    hoverable: true //IMPORTANT! this is needed for tooltip to work
                },
                yaxis: {
                    min: 0,
                    max: (numMax+1)
                },
                tooltip: true,
                tooltipOpts: {
                    content: "'%s' of %x.1 is %y.4",
                    shifts: {
                        x: -60,
                        y: 25
                    }
                }
            };
            var tempPlotDatos = [];
            tempPlotDatos.push(datosComment[id]);
            var plotObj = $.plot($("#flot-line-chart"), tempPlotDatos,
            options);
        
    }

    $("#selectIdeaPlot").change(function(){
        movePlot($("#selectIdeaPlot").val());
    });
});
