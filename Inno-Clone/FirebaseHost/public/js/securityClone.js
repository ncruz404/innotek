
//-------------------------------------------------------------------------------------------------------------
//---------------    Necesario para inicializar el componente de parse   --------------------------------------
//-------------------------------------------------------------------------------------------------------------
Parse.initialize("8Ggc53cuVzyccGqmvkFq2juoBFbvkKrqEUzE7jtT", "pcjrCmP3g9PLqzRX6YOHPUF4AcPzB0DVkaFWVoBA");
Parse.serverURL = 'https://parseapi.back4app.com/';
Parse.User.enableRevocableSession();
//-------------------------------------------------------------------------------------------------------------
//---------------    Necesario para inicializar el componente de parse   --------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//---------------    Sección que evita que se entre sin autorización     --------------------------------------
//-------------------------------------------------------------------------------------------------------------

// new code
var currentUser = ();
if (currentUser) {
    // do stuff with the user
} else {
    // show the signup or login page
}

Parse.User.become("session-token-here").then(function (user) {
  // The current user is now set to user.
}, function (error) {
  // The token could not be validated.
});

var user = Parse.User.logIn("my_username", "my_password", {
  success: function(user) {
    user.set("username", "my_new_username");  // attempt to change username
    user.save(null, {
      success: function(user) {
        // This succeeds, since the user was authenticated on the device

        // Get the user from a non-authenticated method
        var query = new Parse.Query(Parse.User);
        query.get(user.objectId, {
          success: function(userAgain) {
            userAgain.set("username", "another_username");
            userAgain.save(null, {
              error: function(userAgain, error) {
                // This will error, since the Parse.User is not authenticated
              }
            });
          }
        });
      }
    });
  }
});

// old code

var currentUser = Parse.User.current();
/*
if (currentUser) {
  Parse.Cloud.run('checkOfficeToken',{token: currentUser.get("token")},{
    success: function(response){
      var url = window.location.pathname;
      var filename = url.substring(url.lastIndexOf('/')+1);
      if(filename == "index.html")
        window.location = "home.html";
    },
    error: function(error){
      Parse.User.logOut();
      window.location = "index.html"
    }
  });
}
else {
  var url = window.location.pathname;
	var filename = url.substring(url.lastIndexOf('/')+1);
	if(filename != "index.html")
      window.location = "index.html";
}



function logOut(){
  event.preventDefault();
  Parse.User.logOut();
  window.location = "https://login.windows.net/common/oauth2/logout?post_logout_redirect_uri=https://www.innotek.ai/";
}

function requestToken() {
  // Change clientId and replyUrl to reflect your app's values
  // found on the Configure tab in the Azure Management Portal.
  // Also change {your_subdomain} to your subdomain for both endpointUrl and resource.
  var clientId    = 'a8a0c67e-cc5d-4dcd-bd2b-07fbf1a67353';
  var replyUrl    = 'https://www.innotek.ai/page_home/index.html';
  var endpointUrl = 'https://graph.microsoft.com/beta/me/';
  var resource = "https://graph.microsoft.com";
  var authServer  = 'https://login.windows.net/common/oauth2/authorize?';
  var responseType = 'token';
  var tenant = 'softtek.com';
  var url = authServer +
            "response_type=" + encodeURI(responseType) + "&" +
            "client_id=" + encodeURI(clientId) + "&" +
            "resource=" + encodeURI(resource) + "&" +
            "tenant=" + encodeURI(tenant) + "&" +
            "redirect_uri=" + encodeURI(replyUrl);
  window.location = url;
}


function requestTokenOutlook() {
  // Change clientId and replyUrl to reflect your app's values
  // found on the Configure tab in the Azure Management Portal.
  // Also change {your_subdomain} to your subdomain for both endpointUrl and resource.
  var clientId    = 'a8a0c67e-cc5d-4dcd-bd2b-07fbf1a67353';
  var replyUrl    = 'https://www.innotek.ai/page_home/logon.html';
  var endpointUrl = 'https://outlook.office.com/api/v1.0/me/';
  var resource = "https://outlook.office.com";
  var authServer  = 'https://login.windows.net/common/oauth2/authorize?';
  var responseType = 'token';
  var tenant = 'softtek.com';
  var url = authServer +
            "response_type=" + encodeURI(responseType) + "&" +
            "client_id=" + encodeURI(clientId) + "&" +
            "resource=" + encodeURI(resource) + "&" +
            "tenant=" + encodeURI(tenant) + "&" +
            "redirect_uri=" + encodeURI(replyUrl);
  window.location = url;
}
*/
//-------------------------------------------------------------------------------------------------------------
//---------------    Sección que evita que se entre sin autorización     --------------------------------------
//-------------------------------------------------------------------------------------------------------------

window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
    heap.load("2843715222");


(function() {

  try{
    $("body").append('<script type="text/javascript">'+
        'window.heap = window.heap || [], heap.load = function (e, t) { window.heap.appid = e, window.heap.config = t = t || {}; var r = t.forceSSL || "https:" === document.location.protocol, a = document.createElement("script"); a.type = "text/javascript", a.async = !0, a.src = (r ? "https:" : "http:") + "//cdn.heapanalytics.com/js/heap-" + e + ".js"; var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(a, n); for (var o = function (e) { return function () { heap.push([e].concat(Array.prototype.slice.call(arguments, 0))) } }, p = ["addEventProperties", "addUserProperties", "clearEventProperties", "identify", "removeEventProperty", "setEventProperties", "track", "unsetEventProperty"], c = 0; c < p.length; c++) heap[p[c]] = o(p[c]) };'+
        'heap.load("2843715222");</script>');

    $("#logOutBtn").click(function(event){
      event.preventDefault();
      logOut();
    });
  }catch(err){
    console.log(err);
    setTimeout(function() {
      $("body").append('<script type="text/javascript">'+
          'window.heap = window.heap || [], heap.load = function (e, t) { window.heap.appid = e, window.heap.config = t = t || {}; var r = t.forceSSL || "https:" === document.location.protocol, a = document.createElement("script"); a.type = "text/javascript", a.async = !0, a.src = (r ? "https:" : "http:") + "//cdn.heapanalytics.com/js/heap-" + e + ".js"; var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(a, n); for (var o = function (e) { return function () { heap.push([e].concat(Array.prototype.slice.call(arguments, 0))) } }, p = ["addEventProperties", "addUserProperties", "clearEventProperties", "identify", "removeEventProperty", "setEventProperties", "track", "unsetEventProperty"], c = 0; c < p.length; c++) heap[p[c]] = o(p[c]) };'+
          'heap.load("2843715222");</script>');

      $("#logOutBtn").click(function(event){
        event.preventDefault();
        logOut();
      });
    }, 20);
  }

})();
