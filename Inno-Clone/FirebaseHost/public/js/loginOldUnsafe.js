//-------------------------------------------------------------------------------------------------------------
//---------------    Sección que evita que las formas hagan refresh -------------------------------------------
//-------------------------------------------------------------------------------------------------------------
  function checarUserOffice(objectO,token){    
    var OfArray = (objectO.mail).split("@");
    if(OfArray[1] == "softtek.com")
    {
    Parse.Cloud.run('checarOffice',{mail: objectO.mail},{
        success: function(userResult){          
          if(userResult.length > 0)
          {
            var result = userResult[0];
            if(result.has("imageYammer") && (!result.has("yamtoOff") || !result.get("yamtoOff")))
              YamToOff(objectO,result,token);
            else
              loginOffice(OfArray[0],objectO,token);
          }else
          {
            signupOffice(OfArray[0],objectO,token);
          }
        },
        error: function(error){          
          alert("Error al inicializar con Office");
          Parse.Cloud.run("saveError",{text:"mail: "+object.mail+" token: "+token},{
            success: function(result){
              console.log(result);
            },
            error: function(error){
              console.log(error);
            }
          });
        }
      });
    }
  }
//-------------------------------------------------------------------------------------------------------------
//---------------    Sección que evita que las formas hagan refresh -------------------------------------------
//-------------------------------------------------------------------------------------------------------------

function YamToOff(objectO,user,token){  
  Parse.Cloud.run("checkOfficeToken",{token: token},{
    success: function(response){      
      var pss = JSON.stringify(objectO);
      $.ajax({
             type: "POST",
             url: "https://parseapi.back4app.com/functions/crypto",
             data: {"office": pss},
             headers: {"X-Parse-Application-Id": "2cgQyq7vFCDDITezUeRY4Nww7Sk0mBgLyeHkm6hh",
                     "X-Parse-REST-API-Key": "pIgIQRiiSOQLtKoPyy4aB0z9OKVDDIXuoM0Sn8sx"},
             success: function (responsePass) {              
              Parse.Cloud.run("YammerToOffice",{pass: responsePass.result,userID: user.id,token:token},{
                  success: function(user) {                        
                    loginOffice(user.get("username"),objectO,token);     
                  },
                  error: function(user, error) {
                   console.log(user, error);
                  }
                }); 
             },
             dataType: "json"
           });
    },
    error:function(error){
      console.log(error);
    }
  });
}

function loginOffice(username, pass,token){  
      var un = username.toString();
      var pss = JSON.stringify(pass);      
      $.ajax({
             type: "POST",
             url: "https://parseapi.back4app.com/functions/crypto",
             data: {"office": pss},
             headers: {"X-Parse-Application-Id": "2cgQyq7vFCDDITezUeRY4Nww7Sk0mBgLyeHkm6hh",
                     "X-Parse-REST-API-Key": "pIgIQRiiSOQLtKoPyy4aB0z9OKVDDIXuoM0Sn8sx"},
             success: function (responsePass) {              
              Parse.Cloud.run('updateUser',
              {
                username: un,
                crypto: responsePass.result,
                token
              },{
                    success: function(){
                        Parse.User.logIn(un, responsePass.result, {
                        success: function() {     
                          getLocation();
                        },
                        error: function(user, error) {
                          alert("Error al inicializar con Office");
                          console.log(user,error);                          
                          (function() {
                      $("#signin").velocity("callout.shake");
                    }, 2000);                        }
                      });
                    },
                    error: function(error){
                      console.log(error);
                      alert("Error al inicializar con Office");
                    }
                  });
             },
             dataType: "json"
           });
}

function signupOffice(username,offObject,token){  
  Parse.Cloud.run("checkOfficeToken",{token: token},{
    success: function(response){      
      var pss = JSON.stringify(offObject);
      var formattedResponse = JSON.parse(response.text);
      if(formattedResponse.mail != offObject.mail)
      {
        alert("Theres something wrong with your credentials");
        window.location = "https://login.windows.net/common/oauth2/logout?post_logout_redirect_uri=https://inno-clone.firebaseapp.com";
      }
      else
      {
        $.ajax({
          type: "POST",
          url: "https://parseapi.back4app.com/functions/crypto",
          data: {"office": pss},
          headers: {"X-Parse-Application-Id": "2cgQyq7vFCDDITezUeRY4Nww7Sk0mBgLyeHkm6hh",
                   "X-Parse-REST-API-Key": "pIgIQRiiSOQLtKoPyy4aB0z9OKVDDIXuoM0Sn8sx"},
          success: function (responsePass) {          
            var user = new Parse.User();
            var un = username.toString();
            var mail = offObject.mail.toString();
            user.set("username", formattedResponse.mailNickname);
            user.set("password", responsePass.result);
            user.set("email", formattedResponse.mail); 
            user.set("token",token);
            user.set("name", offObject.givenName);
            user.signUp(null, {
              success: function(user) {
                loginOffice(un,offObject,token);              
              },
              error: function(user, error) {        
                switch(error.code){
                  case 203: $("#reauth-email").text("La Dirección de correo ya está registrada");
                  break;
                  case 125: $("#reauth-email").text("La Dirección de correo es inválida");
                  break;
                  case 202: $("#reauth-email").text("El Nombre de usuario ya está registrada");
                  break;
                  case -1: $("#reauth-email").text("Debes agregar una contraseña");
                  break;
                  default: ;
                }        
              }
            });
          },
          dataType: "json"
        });
      }
    },
    error: function(error){
      window.location = "https://inno-clone.firebaseapp.compage_home/error.html"
    }
  });
}

function modalAlert(message,title){
  var superalertModal = '<!-- Modal -->'+
    '<div class="modal fade" id="maModal" role="dialog">'+
      '<div class="modal-dialog">'+
        '<!-- Modal content-->'+
        '<div class="modal-content">'+
          '<div class="modal-header">'+
            '<button type="button" class="close" data-dismiss="modal">&times;</button>'+
            '<h4 id="maTitle" class="modal-title">'+((title)? title : "")+'</h4>'+
          '</div>'+
          '<div class="text-center modal-body">'+
              '<h3 id="maMessage">'+message+'</h3>'+
          '</div>'+
          '<div class="modal-footer">'+
            '<button id="mabtnClose" type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
          '</div>'+
        '</div>'+
      '</div>'+
    '</div>';
  $('#maModal').remove();
  $("body").append(superalertModal);
  $('#maModal').modal({ show: false})
  $('#maModal').modal('show');
  $('#maModal').on('shown.bs.modal', function () {
    $('#mabtnClose').focus();
  });
}
//-------------------------------------------------------------------------------------------------------------
//-----------------------------      Login con credenciales      ----------------------------------------------
//-------------------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------------------
//-----------------------------      Login con nuevo usuario     ----------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-----------------------------      Login con nuevo usuario     ----------------------------------------------
//-------------------------------------------------------------------------------------------------------------
function getLocation() {
  window.location = "home.html";
  // if (navigator.geolocation) {
  //     navigator.geolocation.getCurrentPosition(showPosition);
  // } else { 
  //     alert("Geolocation is not supported by this browser.");
  // }
}

function showPosition(position) {
    var userlocation = new Parse.GeoPoint({latitude: position.coords.latitude, longitude: position.coords.longitude});
    Parse.Cloud.run("registerTime",{geopoint: userlocation,ideaID: "mwYHeL3QNl",userID : Parse.User.current().id, device: "web", newUser: false, evento:"LogIn"},{
      success: function(response){
        Parse.Cloud.run('setUserLocation',{gpoint: userlocation},{
          success: function(result){
            //requestTokenOutlook();
            window.location = "home.html";
          },
          error: function(error){
            //requestTokenOutlook();
            window.location = "home.html";
          }
        });
      },
        error: function(error){
          
        }
      });
}
$(".card-container").velocity({ 
  translateY: [0,300],
}, 2000);
