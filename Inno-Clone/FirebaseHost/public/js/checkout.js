var total=0;
var actualpuzzles=0;
var totalPuzzles= 0 ;
var listItems;

Parse.Cloud.run("getItemsCart", {
}, {
        success: function(items) { // return subcategory name array

            listItems=items;

            for (var i = 0; i < items.length; i++) {
                createRow(items[i]);
                 };

                if(items.length===0){
                    $("#cartList").append("<p>  No products yet</p>");
                }
           

            $("#totalCart strong").append(total);
                            
        },
        error: function(error) {
                console.log(error.message);
        }
});


function createRow(order){


total=total+order.attributes.item.get("price")*order.get("quantity");


$("#cart #cartList").append(
    '<tr class="'+order.id+' item-list">'+
    '<td data-th="Product">'+
        '<div class="row">'+
            '<div class="col-sm-2 hidden-xs"><img src="'+order.attributes.item.attributes.img.url()+'" alt="..." class="img-responsive"/></div>'+
            '<div class="col-sm-10">'+
                '<h4 class="nomargin">'+order.attributes.item.get("name")+'</h4>'+
                '<p>'+order.attributes.item.get("description").substring(0,100)+'...</p>'+
                '<p>Availability: '+order.attributes.item.attributes.availability+'</p>'+
            '</div>'+
        '</div>'+
    '</td>'+
    '<td data-th="Price">'+order.attributes.item.get("price")+'</td>'+
    '<td data-th="Quantity">'+
        '<input type="number" min="1" class="form-control text-center" max="'+order.attributes.item.attributes.availability+'"value="'+order.get("quantity")+'">'+
    '</td>'+
    '<td data-th="Subtotal" class="text-center">'+order.attributes.item.get("price")*order.get("quantity")+'</td>'+
    '<td class="actions" data-th="">'+
        '<button class="btn btn-info btn-sm" id="refreshItem"><i class="fa fa-refresh"></i></button>'+
        '<button class="btn btn-danger btn-sm" id="deleteItem"><i class="fa fa-trash-o"></i></button>'+                             
    '</td>'+
'</tr>');


}


$('body').on('click', '#refreshItem', function() {


    var orderID=$(this).parent().parent().attr('class');
    var algo = orderID.split(" ")[0];
    var newQuantity=$("tr."+algo+" td:nth-child(3) input").val();
    var price=parseInt($("tr."+algo+" td:nth-child(2)").text());


    if($("tr."+algo+" td:nth-child(3) input").val()<= $("tr."+algo+" td:nth-child(3) input").attr("max") ){



    console.log(newQuantity);



      Parse.Cloud.run("updateItem", {
                orderID: algo,
                quantity: parseInt(newQuantity)
        }, {
                success: function(items) { // return subcategory name array

                    console.log(price);
                    console.log(items.attributes.quantity);

                    $("tr."+algo+" td:nth-child(4)").text(price*items.attributes.quantity);
                    var totalH=0;
                    $( "#cartList tr td:nth-child(4)").each(function() {
                      totalH = totalH + parseInt($( this ).text());
                    });
                    $("#totalCart strong").empty();
                    $("#totalCart strong").append(totalH);
                    total=totalH;
                                    
                },
                error: function(error) {
                        console.log(error.message);
                }
        });

  }
  else{
    $("tr."+algo+" td:nth-child(3) input").val($("tr."+algo+" td:nth-child(3) input").attr("max"));
    alert("Not enough Item available, Sorry!");
  }


});

$('body').on('click', '#deleteItem', function() {

    var orderID=$(this).parent().parent().attr('class').split(" ")[0];

         Parse.Cloud.run("deleteItem", {
                orderID: orderID
        }, {
                success: function(result) { // return subcategory name array
                    $("#"+orderID).html("");
                 },
                error: function(error) {
                        console.log(error.message);
                }
        });

         var total=0;
         $(this).parent().parent().remove();
         $( "#cartList tr td:nth-child(4)").each(function() {
                      total = total + parseInt($( this ).text());
        });

         $("#totalCart strong").html(total);

         if ( $('#cartList').children().length < 0 ) {
                 $("#cartList").append("<p>  No products yet</p>")
            }

});

$('body').on('click', '#checkoutBtn', function() {

    var listItems = $( "#cartList" ).find( ".item-list" ).length;

    if(listItems<1){
        $('#noProducts').modal('toggle');
    }
    else{


         Parse.Cloud.run("getPuzzles", {
        }, {
                success: function(result) { // return subcategory name array
                     $("#puzzleList tbody tr:nth-child(1) td:nth-child(2)").text(result);
                     $("#puzzleList tbody tr:nth-child(2) td:nth-child(2)").text(total);
                     $("#puzzleList tbody tr:nth-child(3) td:nth-child(2)").text(result-total);
                     totalPuzzles=result-total;
                     actualpuzzles=result;
                         $("#reviewOrder").html("");
                     $("#reviewOrder").append('<div class="form-group"><div class="col-xs-12"><strong>Order Total</strong><div class="pull-right"><span>'+total+' <i class="fa fa-puzzle-piece"></i></span></div></div></div>');
                 },
                error: function(error) {
                        console.log(error.message);
                }
        });


         Parse.Cloud.run("getItemsCart", {
        }, {
                success: function(items) { // return subcategory name array

                    listItems=items;

                    console.log(listItems);

                    for (var i = 0; i < items.length; i++) {
                        createReviewOrder(items[i]);
                         };

                        $('#checkoutTab').addClass('active');
                        $("#cartTab").removeClass('active');
                         stepForward("div.bs-wizard div:nth-child(1)",  "div.bs-wizard div:nth-child(2)" );

                                    
                },
                error: function(error) {
                        console.log(error.message);
                }
        });




    }





});

function createReviewOrder(order){



    $("#reviewOrder").prepend(

        '<div class="form-group">'+
            '<div class="col-sm-3 col-xs-3"><img class="img-responsive" src="'+order.attributes.item.attributes.img.url()+'"></div>'+
            '<div class="col-sm-6 col-xs-6">'+
                '<div class="col-xs-12">'+order.attributes.item.attributes.name+'</div>'+
                '<div class="col-xs-12">'+
                    '<small>Quantity:<span>'+order.attributes.quantity+'</span></small>'+
               '</div>'+
            '</div>'+
            '<div class="col-sm-3 col-xs-3 text-right">'+
                '<h6>'+order.attributes.quantity*order.attributes.item.attributes.price+'<i class="fa fa-puzzle-piece"></i></h6>'+
            '</div>'+
        '</div>'+
        '<div class="form-group"><hr /></div>');

}


$('body').on('click', '#placeOrderBtn', function() {



    if($("#formDesk").val()>0 && $("#formCity").val()!=="x" && $("#formGcd").val()!=="x" && $("#formCountry").val()!=="x"){
        
        $(".form-group input").removeClass("error");

        if(totalPuzzles<0){
            $('#NoEnoughPuzzles').modal('show'); 
            $('#NoEnoughPuzzles div .modal-body p:nth-child(2) strong').html(total-actualpuzzles);
        }
        else{
             $('#confirmOrderModal').modal('show');


        }

    }

    else{
        if($("#formCountry input").val()===""){
            $("#formCountry input").addClass("error");
        }
        if($("#formCity input").val()===""){
            $("#formCity input").addClass("error");
        }
        if($("#formGdc input").val()===""){
            $("#formGdc input").addClass("error");
        }
        if($("#formDesk").value===undefined || $("#formDesk").value<1){
            $("#formDesk").addClass("error");
        }

        if($("#formCountry input").val()!==""){
            $("#formCountry input").removeClass("error");
        }
        if($("#formCity input").val()!==""){
            $("#formCity input").removeClass("error");
        }
        if($("#formGdc input").val()!==""){
            $("#formGdc input").removeClass("error");
        }
        if($("#formDesk").value!==undefined && $("#formDesk").value>0){
            $("#formDesk").addClass("error");
        }

    }

});

$('body').on('click', '#confirmationBtn', function() {

              Parse.Cloud.run("updateItemsPaid", {
                    }, {
                    success: function(items) { 

                        console.log(items);

         Parse.Cloud.run("payOrder", {
            puzzles: total
        }, {
                success: function(puzzles) { // return subcategory name array
                    console.log(puzzles);
                    $("nav #puzzle a").html(puzzles+"<span> </span>");

                    Parse.Cloud.run("getNumberItems", {
                    }, {
                    success: function(numberItems) { 
                     $("#shoppping-cart").empty();
                     $("#shoppping-cart").append('<a href="checkout.html">'+numberItems+'<span></span></a>');
                    },
                            error: function(error) {
                                    console.log(error.message);
                            }
                    });
                 
                },
                error: function(error) {
                        console.log(error.message);
                }
        });









                    },
                            error: function(error) {
                                    console.log(error.message);
                            }
                    });








});
