$(document).ready(function(){
     var offset = 0;
     var dias=[];
     var datosComment = [];
     plot();

    function plot() {

        movePlot(0);
                                    
    }
    var numMax = 0;
    var xMax = 0;
    function getDataComments()
    {
        var SHObject = Parse.Object.extend("SessionHistory");
        var querySH = new Parse.Query(SHObject);
        querySH.find({
            success: function(SHResults){
                var datos = [];
                var datosNU = [];
                var datosM = [];
                xMax = SHResults.length + 1;
                
                for(var i = 0; i < SHResults.length; i++)
                {
                    var objectSH = SHResults[i];
                    var date = new Date(objectSH.get("year"),objectSH.get("month"),objectSH.get("day")).getTime();
                    datos.push([date,objectSH.get("active_users")]);
                    datosNU.push([date,objectSH.get("new_users")]);
                    datosM.push([date,objectSH.get("media")]);

                }
                datosComment.push({label: "Daily Active Users", data: datos});
                datosComment.push({label: "Daily New Users", data: datosNU});
                datosComment.push({label: "Minutes Spent", data: datosM});
                movePlot(0);
                $("#plotLoad").html("Analytics");
                var opHTML ="<option value='0'>Daily Active Users</option>";
                opHTML +="<option value='1'>Daily New Users</option>";
                opHTML +="<option value='2'>Minutes Spent</option>";
                $("#selectPlot").html(opHTML);
            },
            error: function(error){
                //alert(error.message);
            }
        });        
    }

    function movePlot(id)
    {
        Parse.Cloud.run("getAnalyticsAdoption",{ideaID: "mwYHeL3QNl"},{
            success: function(response){
                
                var datosComment = response;
                var layout = ["active users", "new users", "minutes spent"]

                var d = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate());
                var dmax = d.getTime();
                d.setDate(d.getDate()-5);
                var dmin =  d.getTime();
                var tempPlotDatos = [];
                    tempPlotDatos.push(datosComment[0]);

                var plot = $.plot("#flot-line-chart", tempPlotDatos, {
                    series: {
                        lines: {
                            show: true
                        },
                        points: {
                            show: true
                        }
                    },
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    xaxis: {
                                mode: "time",
                                minTickSize: [1, "day"],
                                min: dmin,
                                max: dmax,
                                timeformat: "%d/%m/%y"
                            }
                });

                $("<div id='tooltip'></div>").css({
                    position: "absolute",
                    display: "none",
                    border: "1px solid #fdd",
                    padding: "2px",
                    "background-color": "#fee",
                    opacity: 0.80
                }).appendTo("body");

                $("#flot-line-chart").bind("plothover", function (event, pos, item) {

                        if (item) {
                            var x = item.datapoint[0];
                            var y = item.datapoint[1];
                            var d = new Date(x);
                            var n = d.toDateString();
                            $("#tooltip").html(n+" "+y+" "+layout[id])
                                .css({top: item.pageY+5, left: item.pageX+5})
                                .fadeIn(200);
                        } else {
                            $("#tooltip").hide();
                        }
                    
                });
                
                
            },
            error: function(error){
                //alert(error.message);
            }
        });
    }

    $("#selectPlot").change(function(){
        movePlot($("#selectPlot").val());
    });
});
