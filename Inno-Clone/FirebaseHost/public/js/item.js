function GetUrlValue(VarSearch) {
        var SearchString = window.location.search.substring(1);
        if (SearchString.length > 0) {
                var VariableArray = SearchString.split('&');
                for (var i = 0; i < VariableArray.length; i++) {
                        var KeyValuePair = VariableArray[i].split('=');
                        if (KeyValuePair[0] == VarSearch) {
                                return KeyValuePair[1];
                        }
                }
        }
        else
                return null;
}


var itemID = GetUrlValue("d").replace('-', ' ');




Parse.Cloud.run("getItem", {
        itemId: itemID //le mande el primer id
}, {
        success: function(item) { // return subcategory name array

                $("#productName").html(item.get("name"));
                $("#price-item").html(item.get("price") + '<i class="fa fa-puzzle-piece"></i>');
                $("#description-item").html(item.get("description"));
                $("#img-item").attr("src", item.get("img").url());
                $("#availabilityItem strong").append(item.get("availability"));
                if(item.get("availability")===0){

                    $("#quantityInput").remove();
                    $("#addCart").remove();


                }
                else{
                $("#quantityInput").attr({
                   "max" : item.get("availability"),        // substitute your own
                   "min" : 1          // values (or variables) here
                    });
                }



                Parse.Cloud.run("getItemsRelated", {
                itemId: itemID, //le mande el primer id
                subcategory: item.get("subcategory").get("name")
        }, {
                success: function(items) { // return subcategory name array
                    
                    for (var i = 0; i < items.length; i++) {
                        createItem(items[i],"#related-items");
                    };
                   
                   
                },
                error: function(error) {
                        console.log(error.message);
                }
        });


        },
        error: function(error) {
                console.log(error.message);
        }
});


$('body').on('click', '#addCart', function() {

    var max =$("#quantityInput").attr("max");
    var quantityNew = $("#quantityInput").val();

    if(quantityNew > max)
    {
        alert("Not enought item left, Sorry!")
    }

    else{

        var quantity = document.getElementById('quantityInput').value;

        Parse.Cloud.run("setOrder", {
        }, {
            success: function(orderId) { // return subcategory name array

                console.log(orderId);

                        Parse.Cloud.run("setOrderDetails", {
                        orderId: orderId, //le mande el primer id
                        itemID: GetUrlValue("d").replace('-', ' '), //le mande el primer id
                        quantity: parseInt(document.getElementById('quantityInput').value)
                        }, {
                            success: function(result) { // return subcategory name array
                                    console.log(result);
                                    $("#quantityInput").val("1");


                                        Parse.Cloud.run("getNumberItems", {
                                        }, {
                                        success: function(numberItems) { // return subcategory name array
                                                $("#shoppping-cart a").text(numberItems);
                                                $("#shoppping-cart a").append("<span></span>");
                                        },
                                        error: function(error) {
                                                console.log(error.message);
                                        }
                                        });
    

                            },
                            error: function(error) {
                                    console.log(error.message);
                            }
                        });
  
            },
            error: function(error) {
                    console.log(error.message);
            }
        });
}

});


