var idea = {name:"", descr: "", tags:[], isPrivate: false};
var key = GetUrlValue("k");
$(document).ready(function(){
	
	var Idea = Parse.Object.extend("Idea");
	var query = new Parse.Query(Idea);
	query.get(key, {
	  success: function(ideaObject) {
	    // The object was retrieved successfully.
	    idea.name = ideaObject.get("titulo");
	    idea.descr = ideaObject.get("descripcion");
	    idea.isPrivate = ideaObject.get("esPrivado");
	    idea.tags = ideaObject.get("tags");
	    $("#inputNameUI").val(idea.name);
	    $("#inputDescrUI").val(idea.descr);
	    $("#inputPrivateUI").prop("checked", idea.isPrivate);
	    Tags.bootstrapVersion = "3";
        $("#smallUI").tags({
          suggestions: [],
          tagData: idea.tags
        });
        Tags.bootstrapVersion = "3";
        $("#small").tags({
          suggestions: [],
          tagData: []
        });

        var File = Parse.Object.extend("Archivo");
		var query = new Parse.Query(File);
		query.equalTo("idea", ideaObject);
		query.find({
		  success: function(results) {
		    // Do something with the returned Parse.Object values
		    for (var i = 0; i < results.length; i++) { 
		      var object = results[i];
		      loadFiles(object);
		    }
		  },
		  error: function(error) {
		    alert("Error: " + error.code + " " + error.message);
		  }
		});

	  },
	  error: function(object, error) {
	    // The object was not retrieved successfully.
	    // error is a Parse.Error with an error code and message.
	    alert("error");
	  }
	});

	function loadFiles(object)
	{
		$("#fileContainerUI").append("<li>"+object.get("nombre")+"<a>[X]</a></li>");
	}

	function metLista(act,max,ideaObject)
	{
		$("#finishBtnUI").html("Guardando "+(max-act)+" archivos....");
		if(act < max)
		{
			var file = $("#photoUploadUI").get(0).files[act];
			var name = $("#photoUploadUI").get(0).files[act].name;
			var parseFile = new Parse.File(name, file);
			parseFile.save().then(function(){
				  	
			});

			var Archivo = Parse.Object.extend("Archivo");
			var archivo = new Archivo();
			archivo.set("file",parseFile);
			archivo.set("nombre",name);
			archivo.set("idea",ideaObject);
			archivo.save(null, {
		      success: function(object) {
		      	act = act+1;
		        metLista(act,max,ideaObject);
		      },
		      error: function(model, error) {
		        alert("No funciono!");
		      }
		    });
		}
		else
		{
			window.location="formIdea.html?k="+key;
		}
	}				

    $("#finishBtnUI").click(function(){
        idea.name = $("#inputNameUI").val().trim();
        idea.descr = $("#inputDescrUI").val().trim();
        idea.isPrivate = $("#inputPrivateUI").prop("checked");
        idea.tags = $("#smallUI").tags().getTags();
        
        if(idea.name.length > 0 && idea.descr.length > 0 )
        {				        
			var files = document.getElementById("photoUploadUI").files;

			var Idea = Parse.Object.extend("Idea");
			var query = new Parse.Query(Idea);

			query.get(key, {
			  success: function(ideaObject) {
			    // The object was retrieved successfully.
			    ideaObject.set("titulo",idea.name);
	      		ideaObject.set("descripcion",idea.descr);
	      		ideaObject.set("esPrivado",idea.isPrivate);
	      		ideaObject.set("tags",idea.tags);

	      		if(files.length > 0)
				{		
					metLista(0,files.length, ideaObject);	
				}
				else
				{
					ideaObject.save(null, {
				      success: function(object) {
				        $("#finishBtnUI").html("Guardado!");
						window.location="formIdea.html?k="+key;
				      },
				      error: function(model, error) {
				        alert("No funciono!");
				      }
				    });
				}

			  },
			  error: function(object, error) {
			    // The object was not retrieved successfully.
			    // error is a Parse.Error with an error code and message.
			    alert("error");
			  }
			});
		}
    });
	
	$("#deleteBtnUI").click(function(){
		var r = confirm("Deseas eliminar esta idea?");
		if (r == true) {
			var Idea = Parse.Object.extend("Idea");
			var query = new Parse.Query(Idea);

			query.get(key, {
			  success: function(ideaObject) {
			    // The object was retrieved successfully.
			   	ideaObject.destroy({
					success: function(myObject) {
					// The object was deleted from the Parse Cloud.
						window.location="youIn.html";
					},
					error: function(myObject, error) {
					// The delete failed.
					// error is a Parse.Error with an error code and message.
					}
				});
			  },
			  error: function(object, error) {
			    // The object was not retrieved successfully.
			    // error is a Parse.Error with an error code and message.
			    alert("error");
			  }
			}); 
		} 
	});

});
